<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PostalCode extends Model
{
    //
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
    protected $guarded = ['id'];
	/*
    protected $fillable = [
        'code', 'nuskinLeadTime','zone_code','zone_id'
    ];
	*/
	
	public function zone()
	{
		return $this->belongsTo('App\Model\Zone', 'zone_id');
	}
	
	public function city()
	{
		return $this->belongsTo('App\Model\City', 'city_id');
	}
	
    public function courierLeadtimes()
    {
        return $this->hasMany('App\Model\CourierLeadtime', 'postal_code_id', 'id');
    }
}
