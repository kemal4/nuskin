<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class tbl_gudang extends Model
{
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
    protected $table = 'tbl_gudang';
    protected $primaryKey = 'id_gudang';
    public $incrementing = false;

    public function tbl_delivery_order()
    {
        return $this->hasMany('App\Model\tbl_delivery_order');
    }
}
