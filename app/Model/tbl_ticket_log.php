<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class tbl_ticket_log extends Model
{
	use LogsActivity;
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;

    protected $table = 'tbl_ticket_log';
    public $incrementing = false;
    protected static $submitEmptyLogs = false;
}
