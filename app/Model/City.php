<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class City extends Model
{
    //
    use SoftDeletes;
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
    protected $guarded = ['id'];
	
	public function province()
	{
		return $this->belongsTo('App\Model\Province', 'province_id');
	}
}
