<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class tbl_kodepos extends Model
{
    protected $table = 'tbl_kodepos';
    protected $fillable = 
    [
        'kelurahan',
        'kecamatan',
        'provinsi',
        'kodepos',
        'city',
        'leadtime_nuskin',
        'leadtime_fl',
        'leadtime_jne',
        'id_zonasi'
    ];
}