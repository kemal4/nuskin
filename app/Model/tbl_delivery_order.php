<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class tbl_delivery_order extends Model
{
    use SoftDeletes;
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
    protected $table = "tbl_delivery_order";
    //public $incrementing = false;
	
	
    protected $guarded = ['id'];
	/*
    protected $fillable = 
    [
	    'create_date',     
        'order_id',          
        'transporter',       
        'order_type',        
        'customer_refer',    
        'customer_id',       
        'customer_name',     
        'phone',             
        'city',              
        'zip',               
        'grosswiight',       
        'net_weight',        
        'volume',            
        'hawb',              
        'adress',   
        'status',  
    	'delivery_number',
    	'is_preview',
    	'pic',
    	'created_at',
    	'delivered_date',
    	'is_delivered',
    	'on_time_provider',
    	'on_time_nuskin',
    	'eta_nuskin',
    	'eta_provider',
		'on_time_nuskin',
		'on_time_provider',
		'leadtime_nuskin',
		'leadtime_courier',
		'warehouse_id',
		
		
    	'is_deleted'
    ];
	*/

}
