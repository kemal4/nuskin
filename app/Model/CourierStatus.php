<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class CourierStatus extends Model
{
    //
    use SoftDeletes;
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
    protected $guarded = ['id'];
		
	public function courier()
	{
		return $this->belongsTo('App\Model\Courier', 'courier_id');
	}
	
	public function nuskin_status()
	{
		return $this->belongsTo('App\Model\NuskinStatus', 'nuskin_status_id');
	}
	
	
}
