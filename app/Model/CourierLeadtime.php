<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CourierLeadtime extends Model
{
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
	protected $table = 'courier_leadtimes';
    //
    protected $guarded = ['id'];
	/*
    protected $fillable = [
        'courierLeadTime', 'postal_code_id','courier_id'
    ];
	*/
	
	public function postalCode()
	{
		return $this->belongsTo('App\Model\PostalCode', 'postal_code_id');
	}
	public function courier()
	{
		return $this->belongsTo('App\Model\Courier', 'courier_id');
	}
	
}
