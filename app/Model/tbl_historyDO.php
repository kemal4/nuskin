<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class tbl_historyDO extends Model
{
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;
    protected $table = 'tbl_historydo';
    // protected $guarded = ['id'];
	protected $fillable = ['id_delivery','date_vendor','status','pic','message'];
	protected static $submitEmptyLogs = false;
}
