<?php

namespace App\Http\Middleware;

use Closure;

class AuthKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $api_key = $request->header('api_key');
        if ($api_key != 'ABCDEFGHIJKL') {
            return response()->json([
                'message' => 'App key not found',
                'api_key' => $api_key
            ], 401);
        }
        return $next($request);
    }
}
