<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\tbl_delivery_order;

use View;

class BaseController extends Controller
{
    public function __construct() {
    	
    	$date = date("Y/m/d");
        $overdate = tbl_delivery_order::where('created_at', '<=', $date)
        ->orderBy('id', 'DESC')
        ->get();

        $count = count($overdate);

    	View::share('count',$count);
    }

}
