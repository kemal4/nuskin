<?php

namespace App\Http\Controllers;

use App\Model\NuskinStatus;
use Illuminate\Http\Request;
use DataTables;
use Auth;

class NuskinStatusController extends Controller
{
	protected $model_name = "Nuskin's Status";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Log::error('----PostalCodeController------ - '.json_encode($request->all()));
        if ($request->ajax()) {
            //$data = NuskinStatus::latest()->get();
            $data = NuskinStatus::select('*');
			
			if($request->input('order.0.column') == 0){
                $data = NuskinStatus::orderBy('created_at','desc');			
			}
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
					->editColumn('is_final', function ($row) {
						return ($row->is_final)?'<span class="badge badge-info">Yes</span>':'No';
					})	
                    ->rawColumns(['action','is_final'])
                    ->make(true);
        }
      
        return view('settings/nuskinstatus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        NuskinStatus::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					'display_name' => $request->display_name, 
					'is_final' => $request->is_final
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\NuskinStatus  $nuskinStatus
     * @return \Illuminate\Http\Response
     */
    public function show(NuskinStatus $nuskinStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\NuskinStatus  $nuskinStatus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = NuskinStatus::find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\NuskinStatus  $nuskinStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NuskinStatus $nuskinStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\NuskinStatus  $nuskinStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        NuskinStatus::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
}
