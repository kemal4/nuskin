<?php

namespace App\Http\Controllers;

use App\Model\CourierStatus;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use App\Model\Courier;
use App\Model\NuskinStatus;

class CourierStatusController extends Controller
{
	protected $model_name = "Courier's Status";
	protected $statuses = null;
	protected $couriers = null;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$this->statuses = NuskinStatus::select('id','display_name')->get();
		$this->couriers = Courier::select('id','display_name')->get();
		
        //Log::error('----PostalCodeController------ - '.json_encode($request->all()));
        if ($request->ajax()) {
            //$data = CourierStatus::with('nuskinStatus:id,display_name')->with('courier:id,display_name')->latest()->get();
            $data = CourierStatus::with('nuskin_status:id,display_name')->with('courier:id,display_name');
			
			if($request->input('order.0.column') == 0){
                $data = CourierStatus::with('nuskin_status:id,display_name')->with('courier:id,display_name')->orderBy('created_at','desc');			
			}
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
					->editColumn('match_word', function ($row) {
						return ($row->match_word)?'<span class="badge badge-info">Yes</span>':'No';
					})	
					/*
					->editColumn('courier_id', function ($row) {
						return $row->courier->display_name;
					})	
					->editColumn('nuskin_status_id', function ($row) {
						return $row->nuskinStatus->display_name;
					})	
					*/
                    ->rawColumns(['action','match_word'])
                    ->make(true);
        }
		
		$statuses = $this->statuses;
		$couriers = $this->couriers;
      
        return view('settings/courierstatus')->with(compact('statuses','couriers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        CourierStatus::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					'contain' => $request->contain, 
					'match_word' => $request->match_word, 
					'courier_id' => $request->courier_id, 
					'nuskin_status_id' => $request->nuskin_status_id
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\CourierStatus  $courierStatus
     * @return \Illuminate\Http\Response
     */
    public function show(CourierStatus $courierStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\CourierStatus  $courierStatus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = CourierStatus::with('nuskin_status:id,display_name')->with('courier:id,display_name')->find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\CourierStatus  $courierStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourierStatus $courierStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\CourierStatus  $courierStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        CourierStatus::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
}
