<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Session;
use App\Imports\KodePosImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Model\tbl_topics;
use App\Model\tbl_kodepos;
use App\Model\tbl_users;

use DB;


class KelolaMenu extends Controller
{


    public function addMenu() {

    	 return view('spadmin.addmenu');
    }

    public function listtopics() {

    	$topics = tbl_topics::all();
    	return view('topics.listtopics', compact('topics'));
    }

    public function addtopics() {

    	return view('topics.create');
    }

    public function storetopics(Request $request) {

    	$addtopics = new tbl_topics;
    	$addtopics->name = $request->name;

    	$addtopics->save();

    	Session::flash('Success','Data Changed!');    
        return redirect()->route('kelolatopics');
    }

    public function edittopics($id) {
     
        $edittop = tbl_topics::where('id', $id)->first();

    	return view('topics.edit', compact('edittop'));
    }

    public function storeedit(Request $request, $id) {

    	$topicstore = tbl_topics::find($id);

	    $topicstore->name = $request->name;
	    $topicstore->save();

    	Session::flash('Success','Data Changed!');    
        return redirect()->route('kelolatopics');
    }

    public function destroy($id) {
        
        
        $topics = tbl_topics::find($id);
    	$topics->delete();
        
		Session::flash('Success','Data Changed!');    
 	    return redirect()->back();
    }

    public function indexpos() {

        $kodepos = DB::table('tbl_kodepos')
        ->leftJoin('tbl_zonasi', 'tbl_kodepos.id_zonasi', '=', 'tbl_zonasi.code_zonasi')
        ->select('tbl_kodepos.*', 'tbl_zonasi.label_zonasi')
        ->get();
        return view('kodepos.list',compact('kodepos'));
    }

    public function createkode(){
        $zonename = DB::table('tbl_zonasi')->pluck('code_zonasi');
        return view('kodepos.create',compact('zonename'));
    }

    public function addkode(Request $request) {

        $addkode = new tbl_kodepos;
        $addkode->kodepos        = $request->kodepos;
        $addkode->id_zonasi      = $request->zona;
        $addkode->kelurahan      = $request->kelurahan;
        $addkode->kecamatan      = $request->kecamatan;
        $addkode->leadtime_nuskin= $request->leadnuskin;
        $addkode->leadtime_fl    = $request->leadfl;
        $addkode->leadtime_jne   = $request->leadjne;
        $addkode->city           = $request->city;
        $addkode->provinsi       = $request->provinsi;
        $addkode->save();
        
        Session::flash('Success','Data Changed!');    
        return redirect()->route('kelolakodepos');
    }
    public function importexcel(Request $request) 
    {
        
        $trunc = tbl_kodepos::query()->truncate();
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        
        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();

        $file->move(public_path('DeliveryVendo'), $nama_file);
        Excel::import(new KodePosImport, public_path('/DeliveryVendo/'.$nama_file));
      
        Session::flash('sukses','Data Successfully Imported');
           
        return redirect()->route('kelolakodepos');
    }
    public function nameeditkode($id) {

        $koded = tbl_kodepos::where('id', $id)->first();
        $zonename = DB::table('tbl_zonasi')->pluck('code_zonasi');
        return view('kodepos.editkode', compact('koded','zonename'));
    }

    public function storeeditkode(Request $request, $id) {

        $kodestore = tbl_kodepos::find($id);
        $kodestore->kodepos         = $request->kodepos;
        $kodestore->kelurahan       = $request->kelurahan;
        $kodestore->kecamatan       = $request->kecamatan;
        $kodestore->leadtime_nuskin= $request->leadnuskin;
        $kodestore->leadtime_fl    = $request->leadfl;
        $kodestore->leadtime_jne   = $request->leadjne;
        $kodestore->city            = $request->city;
        $kodestore->provinsi        = $request->provinsi;
        $kodestore->save();

        Session::flash('Success','Data Changed!');    
        return redirect()->route('kelolakodepos');
    }

    public function destroykode($id) {

        $delkod = tbl_kodepos::find($id);
        $delkod->delete();
        
        Session::flash('Success','Data Changed!');    
        return redirect()->back();
    }

}