<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

use DB;
use Hash;


class UserController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:view_setting|create_setting|edit_setting|delete_setting', ['only' => ['index','store']]);
         $this->middleware('permission:create_setting', ['only' => ['create','store']]);
         $this->middleware('permission:edit_setting', ['only' => ['edit','update']]);
         $this->middleware('permission:delete_setting', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $data = User::all()->where('deleted_at', null);
        // $data = User::orderBy('id','DESC')->paginate(5);
        // dd(Hash::make(12341234));
        return view('users.index',compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);


        if(array_key_exists('image',$input)) {
            $files = $input['image'];
            
            $name = rand(1,999);
            $extension = $files->getClientOriginalExtension();
            $newname = $name. '.' .$extension;
            
            $files->move(public_path('image/avatar'), $newname);

            $input['photo_path'] = 'image/avatar/'.$newname;

        }

        $error = '';
        if(User::where('name', $request->input('name'))->exists())
        {
            $error .= 'Name already exists <br>';
        }

        if(User::where('email', $request->input('email'))->exists())
        {
            $error .= 'Email already exists';
        }

        if(strlen($error) > 0)
        {
            return back()->withError($error);
        }
        $user = User::create($input);
        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $namerole = $user->roles->pluck('name','name')->all();

        
        // foreach ($userRole as $key) {
        //     $namerole = $key;
        // }
        
        try{
            return view('users.edit',compact('user','roles','namerole'));
        }
        catch (\Exception $ex)
        {
            return back()->withError(trim($ex->getMessage(), 'compact(): '));
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required',
            'password'  => 'same:confirm-password',
            'roles'     => 'required'
        ]);

        $queryName = User::where('name', $request->input('name'));
        $queryEmail = User::where('email', $request->input('email'));
        $queryCompare = User::find($id);

        // $error = '';
        // if(($queryName->exists()) && ($queryName->first()->name == $queryCompare->name))
        // {
        //     $error .= 'Name already exists <br>';
        // }

        // if(($queryEmail->exists()) && ($queryEmail->first()->email == $queryCompare->name))
        // {
        //     $error .= 'Email already exists';
        // }

        // if(strlen($error) > 0)
        // {
        //     return back()->withError($error);
        // }

        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }

        if (!empty($input['password'])) {
            $user = User::find($id);

            if (!empty($input['image'])) {
                $files = $input['image'];
                
                $name = rand(1,999);
                $extension = $files->getClientOriginalExtension();
                $newname = $name. '.' .$extension;
                
                $files->move(public_path('image/avatar'), $newname);

                $input['photo_path'] = 'image/avatar/'.$newname;
            }else {
                $input['photo_path'] = $input['photo_path'];
            }

            try{
                $user->update($input);
            }
            catch(\Illuminate\Database\QueryException $ex)
            {
                $errorCode = $ex->errorInfo[1];
                if($errorCode == 1062){
                    preg_match('/users_.+_unique/', $ex->getMessage(), $m );
                    $message = explode("_", $m[0]);
                    return back()->withError(ucfirst($message['1'])." already exists");
                }
                
            }
            
        } else{
            $user = User::find($id);
            $user->name  = $input['name'];
            $user->email = $input['email'];

            if (!empty($input['image'])) {
                $files = $input['image'];
                
                $name = rand(1,999);
                $extension = $files->getClientOriginalExtension();
                $newname = $name. '.' .$extension;
                
                $files->move(public_path('image/avatar'), $newname);

                $user->photo_path = 'image/avatar/'.$newname;
            } else {
                $user->photo_path = $input['photo_path'];
            }

            try{
                $user->save();
            }
            catch(\Illuminate\Database\QueryException $ex)
            {
                $errorCode = $ex->errorInfo[1];
                if($errorCode == 1062){
                    preg_match('/users_.+_unique/', $ex->getMessage(), $m );
                    $message = explode("_", $m[0]);
                    return back()->withError(ucfirst($message['1'])." already exists");
                }
                
            }

            
        }

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // User::find($id)->delete();
        $user = User::find($id);
        $user ->deleted_at = Carbon::now();
        $user->save();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }

    public function userprofile($id) {

        $user = User::find($id);
        return view('users.userprofile',compact('user'));
    }


    public function updateprofile(Request $request, $id) {
        
        $this->validate($request, [
            'password' => 'same:confirm-password',
            'image'    => 'max:2000'
        ]);

        $input = $request->all();
        
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
            $user = User::find($id);

            if (!empty($input['image'])) {
                $files = $input['image'];
                
                $name = rand(1,999);
                $extension = $files->getClientOriginalExtension();
                $newname = $name. '.' .$extension;
                
                $files->move(public_path('image/avatar'), $newname);

                $input['photo_path'] = 'image/avatar/'.$newname;
            }else {
                $input['photo_path'] = $input['photo_path'];
            }

            $user->update($input);
        } else{
            $user = User::find($id);

            if (!empty($input['image'])) {
                $files = $input['image'];
                
                $name = rand(1,999);
                $extension = $files->getClientOriginalExtension();
                $newname = $name. '.' .$extension;
                
                $files->move(public_path('image/avatar'), $newname);

                $user->photo_path = 'image/avatar/'.$newname;
            } else {
                $user->photo_path = $input['photo_path'];
            }

            $user->save();
        }

        return redirect()->back()->with('success','update successfully');  
    }
}