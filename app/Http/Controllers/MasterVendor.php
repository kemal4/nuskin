<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Model\tbl_vendor;
class MasterVendor extends Controller
{
    public function index() {

    	$vendor = tbl_vendor::all();

    	return view('vendor.index', compact('vendor'));
    }
    public function addVendor() {

    	return view('vendor.create');
    }
    public function storeVendor(Request $request) {

    	$tblvendor = new tbl_vendor;
    	$tblvendor->label_vendor 	= $request->label;
    	$tblvendor->code_vendor 	= $request->code;

    	$tblvendor->save();

    	Session::flash('sukses','Data  Berhasil Ditambah!');
    	return redirect()->route('list-mastervendor');
    }
    public function editVendor($id) {

    	$editven = tbl_vendor::where('id', $id)->first();

    	return view('vendor.editvendor', compact('editven'));
    }
    public function storeedit(Request $request, $id) {

    	$tablevendor = tbl_vendor::find($id);

        $tablevendor->label_vendor 	= $request->label;
    	$tablevendor->code_vendor 	= $request->code;
	    $tablevendor->save();

    	Session::flash('sukses','Data  Successfully Changed!');    
    	return redirect()->route('list-mastervendor');
    }
    public function destroyMasterVendor($id) {

        $deletevendor = tbl_vendor::find($id);
        $deletevendor->delete();

		Session::flash('sukses','Data Has Been Deleted!');    
    	return redirect()->route('list-mastervendor');
    }
}
