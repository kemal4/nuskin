<?php

namespace App\Http\Controllers;

use App\Model\tbl_delivery_order;
use App\Model\tbl_kelola_vendor;
use Illuminate\Http\Request;
use Session;
use DB;
class KelolaVendor extends Controller
{
    public function index() {

        // $kelolavendor = DB::table('tbl_vendor');
        
        $kelolavendor = tbl_kelola_vendor::all();

        return view('vendor.indexkelola',compact('kelolavendor'));
    }

    public function addVendor() {
        $labelvendor = DB::table('tbl_vendor')->pluck('label_vendor');
        $labelzonasi = DB::table('tbl_zonasi')->pluck('label_zonasi');
    	return view('vendor.createkelola',compact('labelvendor','labelzonasi'));
    }
    public function storeVendor(Request $request) {

        $tblkelolavendor = new tbl_kelola_vendor;
    	$tblkelolavendor->label_vendor 	= $request->vendor;
    	$tblkelolavendor->label_zonasi 	= $request->zonasi;
    	$tblkelolavendor->value 	    = $request->value;

    	$tblkelolavendor->save();

    	Session::flash('Success','Data Changed!');
    	return redirect()->route('list-kelolavendor');
    }
    public function editVendor($id) {

        $editkelvendor = tbl_kelola_vendor::where('id', $id)->first();
        $labelvendor = DB::table('tbl_vendor')->pluck('label_vendor');
        $labelzonasi = DB::table('tbl_zonasi')->pluck('label_zonasi');
        foreach($labelvendor as $mykey)
        {
          $label_vendor = $mykey;
        }
        foreach($labelzonasi as $mykeyzon)
        {
          $label_zonasi = $mykeyzon;
        }

        return view('vendor.editkelola', compact('editkelvendor','labelvendor','labelzonasi','label_vendor','label_zonasi'));
        
    }
    public function storeedit(Request $request, $id) {

    	$tblkelolavendor = tbl_kelola_vendor::find($id);

        $tblkelolavendor->label_vendor 	= $request->vendor;
    	$tblkelolavendor->label_zonasi 	= $request->zonasi;
    	$tblkelolavendor->value 	    = $request->value;
	    $tblkelolavendor->save();

    	Session::flash('Success','Data Changed!');    
    	return redirect()->route('list-kelolavendor');
    }
    public function destroyKelolaVendor($id) {

        $deletekelolavendor = tbl_kelola_vendor::find($id);
        $deletekelolavendor->delete();
        // ->update(['is_deleted' => '1']);

		Session::flash('Success','Data Changed!');    
    	return redirect()->route('list-kelolavendor');
    }
}
