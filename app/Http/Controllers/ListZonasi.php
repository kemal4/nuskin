<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;

use App\Model\tbl_zonasi;

class ListZonasi extends Controller
{
    public function index() {

    	$zonasi = tbl_zonasi::all();

    	return view('zonasi.index', compact('zonasi'));
    }

    public function addZonasi() {

    	return view('zonasi.create');
    }

    public function storeZonasi(Request $request) {

    	$tblzonasi = new tbl_zonasi;
    	$tblzonasi->label_zonasi 	= $request->label;
    	$tblzonasi->code_zonasi 	= $request->code;

    	$tblzonasi->save();

    	Session::flash('sukses','Data Successfully Added!');
    	return redirect()->route('listzonasi');
    }

    public function editZonasi($id) {

    	$listzon = tbl_zonasi::where('id', $id)->first();

    	return view('zonasi.edit', compact('listzon'));
    }

    public function storeedit(Request $request, $id) {

    	$zonstore = tbl_zonasi::find($id);

	    $zonstore->label_zonasi = $request->label;
	    $zonstore->code_zonasi 	= $request->code;
	    $zonstore->save();

    	Session::flash('sukses','Data Has Been Changed!');    
    	return redirect()->route('listzonasi');
    }

    public function destroyZonasi($id) {

        $zona = tbl_zonasi::find($id);
        $zona->delete();
        // ->update(['is_deleted' => '1']);

		Session::flash('sukses','Data Has Been Deleted!');    
    	return redirect()->route('listzonasi');
    }
}
