<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Model\tbl_delivery_order;
use App\Model\tbl_ticket;
use App\Model\Post; 
use App\User;
use Carbon\Carbon;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;
use DataTables;
use Illuminate\Support\Str;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{


    public function index6666()
    {
	

			
        return view('admin.home');

	}



    public function index333()
    {
	
	

		$data = [];
				
		$data['nuskinTotal'] = [0 => 10, 1 => 20];
		$data['vendors'] = [
							0 => [
									'name' => 'FL',
									'data' => [0 => 3 , 1 => 5],
								],
							1 => [
									'name' => 'JNE',
									'data' => [0 => 2 , 1 => 2],
								],
							2 => [
									'name' => 'MrSpeedy',
									'data' => [0 => 5 , 1 => 3],
								],
						];
		$data['monthLabel'] = [0 => '2030 Jan', 1 => '2030 Feb'];
		$data['summary'] = [
						'totalDelivery'  	=> 20,
						'totalSucceed'   	=> 15,
						'totalComplaint' 	=> 8,
						'totalSolved'  	 	=> 5,
						'totalFulfilled' 	=> 13,
						'totalComplaintNon'	=> 0,
						'totalSolvedNon' 	=> 0,
		];

			
        return view('admin.home')->with(compact('data'));

	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
	

        $newNow = Carbon::parse('last year'); 
		if($newNow->day < 10)
			$newNow->day += 10;
		if($newNow->day > 10)
			$newNow->day -= 10;


		$monthLabel = [];
		// $vendorList = DB::table('tbl_vendor')->select('')
		$vendorList = array('FL','JNE','MrSpeedy');
		$vendors = [];
		$vendors[0]['name']='FL';
		$vendors[1]['name']='JNE';
		$vendors[2]['name']='MrSpeedy';

		$nuskinTotal = [];
		$i = 0;
		$mgu = [];
		
		//$allYear = DB::select ( DB::raw("select year(created_at) as gye, month(created_at) as gmo, transporter, count(id) as aggregate from `tbl_delivery_order` where deleted_at is null and`is_preview` <> 'yes' and is_deleted <> 'no' group by gye, gmo, transporter")
		$allYear = DB::select ( DB::raw("select year(created_at) as gye, month(created_at) as gmo, transporter, count(id) as aggregate from `tbl_delivery_order` where deleted_at is null and ( is_preview is null or is_preview = '' ) group by gye, gmo, transporter")
				);
				//->get();
		
		while($i < 12){
			
			$mgu[] = memory_get_usage();
			
			$newNow = $newNow->addmonth();
		        $monthLabel[$i] = $newNow->format('Y M');
			logger($newNow);
			//Log::error('---------- start month  '.$i);

			$nuskinTotal[$i] = 0;
			
			for($j = 0 ; $j < 3; $j++  ){
				$vendors[$j]['data'][$i] = 0;
			}
			for($j = 0 ; $j < 3; $j++  ){
				foreach($allYear as $allData){
					if($newNow->month == $allData->gmo && $newNow->year == $allData->gye && $vendors[$j]['name'] == $allData->transporter){
						$vendors[$j]['data'][$i] += $allData->aggregate;
						$nuskinTotal[$i] += $allData->aggregate;				
					}				
				}
			}
					
			$i++;
		}	

		$summary = [];
		
		$summary['totalDelivery'] = $nuskinTotal[11];
		//$summary['totalSucceed'] = DB::table('tbl_delivery_order')->select('on_time_nuskin')
		$summary['totalSucceed'] = tbl_delivery_order::select('on_time_nuskin')
	                                ->whereMonth('created_at',$newNow->month)
	                                ->whereYear('created_at',$newNow->year)
									//->where('status','delivered')
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
									->where('is_delivered',true)
									->count();
			$summary['totalComplaint'] = tbl_ticket::select('is_locked')
	                                ->whereMonth('created_at',$newNow->month)
	                                ->whereYear('created_at',$newNow->year)
	                                ->where('non_deliv','!=', '1')
	                                ->count();
	        $summary['totalSolved'] = tbl_ticket::select('is_locked')
	                                ->whereMonth('created_at',$newNow->month)
	                                ->whereYear('created_at',$newNow->year)
	                                ->where('non_deliv','!=', '1')
	                                ->where('status','Solved')
	                                ->count();
	        //$summary['totalFulfilled'] = DB::table('tbl_delivery_order')->select('on_time_nuskin')
	        $summary['totalFulfilled'] = tbl_delivery_order::select('on_time_nuskin')
	                                ->whereMonth('created_at',$newNow->month)
	                                ->whereYear('created_at',$newNow->year)
	                                ->where('on_time_nuskin',true)
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
					//->where('is_preview','<>','yes')
	                                ->count();
	        // dump($newNow->month);
	        // dd($newNow->year);
	        $summary['totalComplaintNon'] = tbl_ticket::select('is_locked')
	                                ->whereMonth('created_at',$newNow->month)
	                                ->whereYear('created_at',$newNow->year)
	                                ->where('non_deliv','1')
	                                ->count();
;
	        $summary['totalSolvedNon'] = tbl_ticket::select('is_locked')
	                                ->whereMonth('created_at',$newNow->month)
	                                ->whereYear('created_at',$newNow->year)
	                                ->where('non_deliv','1')
	                                ->where('status','Solved')
	                                ->count();

		$data = [];
		$data['summary'] = $summary;
		$data['nuskinTotal'] = $nuskinTotal;
		$data['vendors'] = $vendors;
		$data['monthLabel'] = $monthLabel;
	
		if (empty(session('bellNotification') ) ){
			session(['bellNotification' => "1"]);
			$totalNotif = 0;
			
			$user = Auth::user();
			$today = Carbon::today();
			
			
			if($user->hasAnyPermission(['view_delivery', 'create_delivery', 'edit_delivery'])){				
				$bellNotificationDOTotal = tbl_delivery_order::select(['id'])
						->where('status','onprocess')
						->where('eta_nuskin','<', $today)
						//->where('is_preview', '<>', 'yes')	
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
						->count();
				if(!empty($bellNotificationDOTotal)){
					session(['bellNotificationDOTotal' => $bellNotificationDOTotal]);
					$totalNotif += $bellNotificationDOTotal;
				}
			}
						
			if($user->hasAnyPermission(['view_ticket_delivery', 'create_ticket_delivery', 'edit_ticket_delivery'])){				
				
				$bellNotificationTicketTotal = tbl_ticket::select(['id'])
						->where('status','<>', 'Solved')
						->where('due','<', $today->format('m/d/Y'))
						->count();
				if(!empty($bellNotificationTicketTotal)){
					session(['bellNotificationTicketTotal' => $bellNotificationTicketTotal]);
					$totalNotif += $bellNotificationTicketTotal;
				}
			}
			
			if($totalNotif > 0)
				session(['bellNotificationTotal' => $totalNotif]);
		}
	
		//echo json_encode($data, 128);
		//die();
		$userId = User::pluck('id')->toArray();
		$recentActivities = Activity::latest()
                                ->whereNotNull('causer_id')
                                ->whereIn('causer_id', $userId)
                ->limit(5)
                ->get();

        $recentActivities = $this->systemLogDescription($recentActivities);

        //announcement added by Ryan 23092020
        $post = Post::where('type', 'Announcement')
        		->where('posted', true)
        		->orderBy('created_at', 'desc')
        		->orderBy('title', 'asc')
               	->first();
    	// carbon uses "before" instead of "ago". this line is for replacing "before" with "ago" 
        $data['announcement'] = null;
        if($post)
        {
        	$data['announcement']['time'] = preg_replace('/before/', 'ago',Carbon::parse($post->created_at)->diffForHumans(Carbon::now()));
        	$user = User::find($post->pic);
	        $data['announcement']['content'] =[
	        	'name' => $post->title,
	        	'message' =>$post->message,
	        	'photo_path' => $user->photo_path
	        	];
        } 	
    	
	        
        return view('admin.home')->with(compact('data','recentActivities'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexORIGINAL()
    {
	

        $newNow = Carbon::parse('last year'); 

	$monthLabel = [];

	// $vendorList = DB::table('tbl_vendor')->select('')
	$vendorList = array('FL','JNE','MrSpeedy');
	$vendors = [];
	$vendors[0]['name']='FL';
	$vendors[1]['name']='JNE';
	$vendors[2]['name']='MrSpeedy';

	$nuskinTotal = [];
	$i = 0;
	$mgu = [];
	while($i < 12){
		
		$mgu[] = memory_get_usage();
		
		$newNow = $newNow->addmonth();	
	        $monthLabel[$i] = $newNow->format('Y M');

		//Log::error('---------- start month  '.$i);

		//$nuskinTotal[$i] = DB::table('tbl_delivery_order')->select('on_time_nuskin')
		$nuskinTotal[$i] = tbl_delivery_order::select('on_time_nuskin')
				->whereMonth('created_at',$newNow->month)
				->whereYear('created_at',$newNow->year)
				//->where('is_preview','<>','yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
				->count();
				
		
		for($j = 0 ; $j < 3; $j++  ){
			//$vendors[$j]['data'][$i] = DB::table('tbl_delivery_order')->select('on_time_nuskin')
			$vendors[$j]['data'][$i] = tbl_delivery_order::select('on_time_nuskin')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
								->where('transporter',$vendors[$j]['name'])
				//->where('is_preview','<>','yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                                ->count();
		}
		$i++;
	}
	

	$summary = [];
	
	$summary['totalDelivery'] = $nuskinTotal[11];
	//$summary['totalSucceed'] = DB::table('tbl_delivery_order')->select('on_time_nuskin')
	$summary['totalSucceed'] = tbl_delivery_order::select('on_time_nuskin')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
								//->where('status','delivered')
								->where('is_delivered',true)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
								->count();
	$summary['totalComplaint'] = DB::table('tbl_ticket')->select('is_locked')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
                                ->count();
        $summary['totalSolved'] = DB::table('tbl_ticket')->select('is_locked')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
                                ->where('status','Solved')
                                ->count();
        //$summary['totalFulfilled'] = DB::table('tbl_delivery_order')->select('on_time_nuskin')
        $summary['totalFulfilled'] = tbl_delivery_order::select('on_time_nuskin')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
                                ->where('on_time_nuskin',true)
				//->where('is_preview','<>','yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                                ->count();
        $summary['totalComplaintNon'] = 0;
        $summary['totalSolvedNon'] = 0;

	$data = [];
	$data['summary'] = $summary;
	$data['nuskinTotal'] = $nuskinTotal;
	$data['vendors'] = $vendors;
	$data['monthLabel'] = $monthLabel;
	
		//echo json_encode($data, 128);
		//die();
		
        return view('admin.home')->with(compact('data'));
    }

    public function systemLog()
    {
    	return view('admin.systemlog');
    }

    public function getAllSystemLogJson()
    {
    	$userId = User::pluck('id')->toArray();
    	$query = Activity::latest()
    		->whereNotNull('causer_id')
    		->whereIn('causer_id', $userId)
    		->get();

    	return Datatables::of($query)
    		->addColumn('image', function($query)
    		{
    			return '<img class="img-size-50 mr-3 mr-2 img-circle" src="'.asset($query->causer->photo_path).'" alt="User Avatar">';
    		})
    		->addColumn('causer_name', function($query)
    		{
    			return $query->causer->name;
    		})
    		->addColumn('role', function($query)
    		{	
    			$roles = [];
    			if(!empty($query->causer->getRoleNames())) 
    			{
    				foreach($query->causer->getRoleNames() as $v)
    				{
    					array_push($roles,$v);
    				}
    			}
    			$roles = implode(", ",$roles);
    			return $roles;
    		})
            ->editColumn('description', function($query)
            {
            	if(class_basename($query->subject) == 'tbl_ticket')
	    		{
	    			$query->description = $query->description." Ticket ".$query->subject->id_ticket;
	    		}
	    		else if(class_basename($query->subject) == 'tbl_delivery_order')
	    		{
	    			$query->description = $query->description." DO with SO ".$query->subject->order_id;
	    		}
	    		else if(class_basename($query->subject) == 'User')
	    		{
	    			$query->description = $query->description." user ".$query->subject->name;
	    		}
	    		else
	    		{
	    			$query->description = $query->description." on ".class_basename($query->subject);
	    		}
            	return $query->description;
            })
            ->rawColumns(['image'])
            ->make(true);	
    }

    public function systemLogDescription($recentActivities)
    {
    	foreach ($recentActivities as $activity) {
    		if(class_basename($activity->subject) == 'tbl_ticket')
    		{
    			$activity->description = $activity->description." Ticket ".$activity->subject->id_ticket;
    		}
    		else if(class_basename($activity->subject) == 'tbl_delivery_order')
    		{
    			$activity->description = $activity->description." DO with SO ".$activity->subject->order_id;
    		}
    		else if(class_basename($activity->subject) == 'User')
    		{
    			$activity->description = $activity->description." user ".$activity->subject->name;
    		}
    		else
    		{
    			$activity->description = $activity->description." on ".class_basename($activity->subject);
    		}
    		// dump($recentActivity->changes);
    	}
    	return $recentActivities;
    }
}
