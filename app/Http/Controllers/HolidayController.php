<?php

namespace App\Http\Controllers;

use App\Model\Holiday;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class HolidayController extends Controller
{
	protected $model_name = 'Holiday';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
		
        if ($request->ajax()) {
            //$data = Holiday::latest()->get();
            $data = Holiday::select('*');
			
			if($request->input('order.0.column') == 0){
                $data = Holiday::orderBy('created_at','desc');			
			}
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('settings/holiday');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		Log::error('----PostalCodeController------ - '.json_encode($request->all()));
        Holiday::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					//'holiday_date' => $request->holiday_date,
					'holiday_date' => Carbon::Parse($request->holiday_date),
					'description' => $request->description
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function show(Holiday $holiday)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Holiday::find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Holiday $holiday)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Holiday::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
}
