<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Model\tbl_gudang;
use App\Role;
use App\Permission;

class KelolaRole extends Controller
{
    function __construct()
    {
        $this->middleware('permission:view_setting|create_setting|edit_setting|delete_setting', ['only' => ['index','addGudang']]);
        $this->middleware('permission:create_setting', ['only' => ['gudang','addGudang']]);
        $this->middleware('permission:edit_setting', ['only' => ['editgudang','storegudang']]);
        $this->middleware('permission:delete_setting', ['only' => ['destroy']]);
    }    

    public function index() {
        $gudang = tbl_gudang::all();
        // return view('spadmin.kelolarole',['gudang'=>$gudang]);
    	return view('warehouse.index',['gudang'=>$gudang]);
    }
    public function gudang() {
        return view('warehouse.create');

    }
    public function addGudang(Request $request) {
        $gudang = new tbl_gudang;
        $gudang->lokasi_gudang = $request->gudang;
        $gudang->code_gudang = $request->codegudang;
        $gudang->nama_gudang = '';
        $gudang->koordinat = '';
        
        
        $gudang->save();
        Session::flash('Success','Data Changed!');   
        // return view('spadmin.kelolarole');
        return redirect()->route('kelolarole');
    }
    public function editgudang($id) {
     
        $editgudang = tbl_gudang::where('id_gudang', $id)->first();
        
    	return view('warehouse.edit', compact('editgudang'));
    }
    public function storegudang(Request $request, $id) {

    	$gudangstore = tbl_gudang::find($id);

	    $gudangstore->lokasi_gudang = $request->gudang;
	    $gudangstore->save();

    	Session::flash('Success','Data Changed!');    
        return redirect()->route('kelolarole');
    }
    public function destroy($id) {
        
        
        $gudang = tbl_gudang::find($id);
        // dd($gudang);
    	$gudang->delete();
        
		Session::flash('Success','Data Changed!');    
 	    return redirect()->back();
    }
}
