<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Permission;
use DB;

class RoleController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('permission:view_setting|create_setting|edit_setting|delete_setting', ['only' => ['index','store']]);
        $this->middleware('permission:create_setting', ['only' => ['create','store']]);
        $this->middleware('permission:edit_setting', ['only' => ['edit','update']]);
        $this->middleware('permission:delete_setting', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {

        $roles = Role::orderBy('id','DESC')->paginate(5);
        return view('roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
            
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$allMenu = [
			['idx' => 2, 'title' => 'Delivery Order' , 'data' => [] ],
			['idx' => 3, 'title' => 'Complaint Ticket - Delivery' , 'data' => [] ],
			['idx' => 4, 'title' => 'Complaint Ticket - Other' , 'data' => [] ],
			['idx' => 5, 'title' => 'Performance' , 'data' => [] ],
			['idx' => 6, 'title' => 'Setting'  ],
		];
        $permissions = [];
		$allPermission = Permission::where('show',true)->get();
		foreach ($allMenu as &$menu)	{			
			$tempArr = [];
			foreach($allPermission as $temp) {				
				if($temp->menu_id == $menu['idx']){
					//echo $temp->name.' - '.$menu['title'];
					$tempArr[] = $temp;								
				}
			}
			$menu['data'] = $tempArr;
		}		
		//dd($allMenu);
		
        $permission = $allMenu;
		
		
        return view('roles.create',compact('permission'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);


        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')
                        ->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();

		
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();


		$allMenu = [
			['idx' => 2, 'title' => 'Delivery Order' , 'data' => [] ],
			['idx' => 3, 'title' => 'Complaint Ticket - Delivery' , 'data' => [] ],
			['idx' => 4, 'title' => 'Complaint Ticket - Other' , 'data' => [] ],
			['idx' => 5, 'title' => 'Performance' , 'data' => [] ],
			['idx' => 6, 'title' => 'Setting'  ],
		];
        $permissions = [];
		$allPermission = Permission::where('show',true)->get();
		foreach ($allMenu as &$menu)	{			
			$tempArr = [];
			foreach($allPermission as $temp) {				
				if($temp->menu_id == $menu['idx'] && in_array($temp->id, $rolePermissions) ){
					//echo $temp->name.' - '.$menu['title'];
					$tempArr[] = $temp;								
				}
			}
			$menu['data'] = $tempArr;
		}		
		//dd($allMenu);
		
        $permission = $allMenu;
		


        return view('roles.show',compact('role','rolePermissions','permission'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
		$allMenu = [
			['idx' => 2, 'title' => 'Delivery Order' , 'data' => [] ],
			['idx' => 3, 'title' => 'Complaint Ticket - Delivery' , 'data' => [] ],
			['idx' => 4, 'title' => 'Complaint Ticket - Other' , 'data' => [] ],
			['idx' => 5, 'title' => 'Performance' , 'data' => [] ],
			['idx' => 6, 'title' => 'Setting'  ],
		];
        $permissions = [];
		$allPermission = Permission::where('show',true)->get();
		foreach ($allMenu as &$menu)	{			
			$tempArr = [];
			foreach($allPermission as $temp) {				
				if($temp->menu_id == $menu['idx']){
					//echo $temp->name.' - '.$menu['title'];
					$tempArr[] = $temp;								
				}
			}
			$menu['data'] = $tempArr;
		}		
		
        $permission = $allMenu;
        //$permission = Permission::get();
				
        $role = Role::find($id);
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();


        return view('roles.edit',compact('role','permission','rolePermissions'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
        // dd($request->all());

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();


        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')
                        ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
        return redirect()->route('roles.index')
                        ->with('success','Role deleted successfully');
    }
}
