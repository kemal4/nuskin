<?php

namespace App\Http\Controllers\Performance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Exports\UnfulfilledExport;
use App\Exports\UndeliveredExport;

use App\Model\tbl_delivery_order;
use App\Model\Zone;
use App\Model\Courier;

class PerformanceController extends Controller
{
    //

	public function index(Request $request) {	// lead_time
		
		$hasChartData = false;
		$startPeriod = Carbon::parse('first day of this month 00:00:00');
		$endPeriod = Carbon::parse('last day of this month 23:59:59');
				
		if($request->filterStartDate){
			$startPeriod = Carbon::parse($request->filterStartDate.' 00:00:00');
			$endPeriod = Carbon::parse($request->filterEndDate.' 23:59:59');
		}
		$filterStartDate = $startPeriod->toDateString();
		$filterEndDate = $endPeriod->toDateString();
		//echo '---------------  '.gettype($filterStartDate).'     '.$filterStartDate;
		
		//$providers = DB::table('tbl_vendor')->select('id', 'common_name')->get();
		$providers = Courier::select('id', 'display_name')->get();
		
		$totals = [];
		$totals[] = ['Provider','Total'];
		$totalChart = sizeof($providers);
		
		$i=0;
		foreach($providers as $provider){
			$i++;
			$provider->idx = $i;
			$provider->totalDelivered = tbl_delivery_order::select('id')
								->whereBetween('created_at',[$startPeriod,$endPeriod])
								//->where('transporter',$provider->common_name)
								->where('courier_id',$provider->id)
								->where('is_delivered',true)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
								->count();
			
			$provider->totalOnTime = tbl_delivery_order::select('id')
								->whereBetween('created_at',[$startPeriod,$endPeriod])
								//->where('transporter',$provider->common_name)
								->where('courier_id',$provider->id)
								->where('is_delivered',true)
								->where('on_time_provider',true)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
								->count();
			
			$provider->totalLate = $provider->totalDelivered - $provider->totalOnTime;
			
			$chartData = [];
			$chartData[] = ['Delivered','Total'];
			$chartData[] = ['Fulfilled', $provider->totalOnTime];
			$chartData[] = ['Unfulfilled', $provider->totalLate];
			
			$provider->chartData = $chartData;
			
			//$totals[] = [$provider->common_name, $provider->totalDelivered];
			$totals[] = [$provider->display_name, $provider->totalDelivered];
			if(!empty($provider->totalDelivered))
				$hasChartData = true;
		}
		 
		 

		$data = [];
		
		return view('performance.lead_time')->with(compact('hasChartData','totals','totalChart','providers','filterStartDate','filterEndDate'));


	}
	
	public function downloadUnfulfilled($courier,$courier_id,$startDate,$endDate){
		
		return (new UnfulfilledExport($courier_id,$startDate,$endDate))->download('Unfulfilled_'.$courier.'_'.$startDate.'_'.$endDate.'.xlsx');
		
	}
	
	public function successRate(Request $request) {
		
		$startPeriod = Carbon::parse('first day of this month 00:00:00');
		$endPeriod = Carbon::parse('last day of this month 23:59:59');
				
		if($request->filterStartDate){
			$startPeriod = Carbon::parse($request->filterStartDate.' 00:00:00');
			$endPeriod = Carbon::parse($request->filterEndDate.' 23:59:59');
		}
		$filterStartDate = $startPeriod->toDateString();
		$filterEndDate = $endPeriod->toDateString();
		//echo '---------------  '.gettype($filterStartDate).'     '.$filterStartDate;
		
		//$providers = DB::table('tbl_vendor')->select('id', 'common_name')->get();
		$providers = Courier::select('id', 'display_name')->get();
		
		// $allStatus = load from DB 
		$allStatus = array(
				 ['status' => 'delivered', 'name'=>'Delivered'],
				 ['status' => 'undelivered', 'name'=>'Undelivered'],
				 ['status' => 'incident', 'name'=>'Incident'],
				 ['status' => 'return', 'name'=>'Return'],
				 ['status' => 'onprocess', 'name'=>'On Process'],
		);
			
		
		$totals = [];
		$totals[] = ['Status','Total'];
		$totalChart = sizeof($providers);
		
		$nuskin = (object) array();;
		$nuskin->chartData = [0,0,0,0,0];
		$nuskin->labels = array('Delivered','Undelivered','Incident','Return','On Process');
				
		$i=-1;
		$hasChartData = false;
		foreach($providers as $provider){
			$i++;
			$provider->idx = $i;
			$provider->totalall = 0;
			$tempTotal = [];
			
			$dataset = [];
			$dataset[] = ['Status','Total'];
			$j=0;
			foreach($allStatus as $status){
				$tempTotal[$status['status']] = tbl_delivery_order::select('id')
								->whereBetween('created_at',[$startPeriod,$endPeriod])
								//->where('transporter',$provider->common_name)
								->where('courier_id',$provider->id)
								->where('status',$status['status'])
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
								->count();				
				$dataset[] = [$status['name'],$tempTotal[$status['status']]];
				($nuskin->chartData)[$j] += $tempTotal[$status['status']];
				$provider->totalall += $tempTotal[$status['status']];
				$j++;
			}
			
			
			$provider->total = $tempTotal;
			$provider->chartData = $dataset;
			
			if($provider->totalall > 0)
				$hasChartData = true;
		}
		
		foreach($nuskin->labels as $k => $v)
			$totals[] = [$v,($nuskin->chartData)[$k]];
		
		 
		 

		$data = [];
		return view('performance.success_rate')->with(compact('hasChartData','nuskin','allStatus','totals','totalChart','providers','filterStartDate','filterEndDate'));

	}
	
	public function downloadUndelivered($courier,$courier_id,$startDate,$endDate){
		
		return (new UndeliveredExport($courier_id,$startDate,$endDate))->download('Failed_'.$courier.'_'.$startDate.'_'.$endDate.'.xlsx');
		
	}
	
	
	public function providerLeadTime(Request $request, $pid,$startDate = '',$endDate = '') {
		
		$hasChartData = false;
		if(empty($startDate)){
			$startPeriod =  Carbon::parse('first day of this month 00:00:00');
			$endPeriod =  Carbon::parse('last day of this month 23:59:59');
		}
		else {
			$startPeriod = Carbon::parse($startDate.' 00:00:00');
			$endPeriod = Carbon::parse($endDate.' 23:59:59');			
		}
		
				
		if($request->filterStartDate){
			$startPeriod = Carbon::parse($request->filterStartDate.' 00:00:00');
			$endPeriod = Carbon::parse($request->filterEndDate.' 23:59:59');
		}
		$filterStartDate = $startPeriod->toDateString();
		$filterEndDate = $endPeriod->toDateString();
		
		//$provider = DB::table('tbl_vendor')->select('common_name')
		$provider = Courier::select('id','display_name')
				->where('id',$pid)->first();

		$zones = Zone::select('id', 'code')->get();
		$zones->providerName = $provider->display_name;
		$zones->providerId = $pid;
		
		$totals = [];
		$totals[] = ['Zone','Total'];
		$totalChart = sizeof($zones);
		$totalChart = 0;
		
		$i=0;
		foreach($zones as $zone){
			$zone->idx = $i;
			$zone->totalDelivered = tbl_delivery_order::join('postal_codes', 'tbl_delivery_order.zip', '=', 'postal_codes.code')
						->select('tbl_delivery_order.id')						
						->whereBetween('tbl_delivery_order.created_at',[$startPeriod,$endPeriod])
						//->where('tbl_delivery_order.transporter',$provider->common_name)
						->where('tbl_delivery_order.courier_id',$provider->id)
						->where('tbl_delivery_order.is_delivered',true)
						//->where('tbl_delivery_order.deleted_at',null)
						->where('postal_codes.zone_code',$zone->code)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
						->count();
			
			if ($zone->totalDelivered && $zone->totalDelivered > 0)
				$totalChart ++;
			
			$zone->totalOnTime = tbl_delivery_order::select('tbl_delivery_order.id')
						->join('postal_codes', 'tbl_delivery_order.zip', '=', 'postal_codes.code')
						->whereBetween('tbl_delivery_order.created_at',[$startPeriod,$endPeriod])
						//->where('tbl_delivery_order.transporter',$provider->common_name)
						->where('tbl_delivery_order.courier_id',$provider->id)
						->where('tbl_delivery_order.is_delivered',true)
						->where('tbl_delivery_order.on_time_provider',true)
						//->where('tbl_delivery_order.deleted_at',null)
						->where('postal_codes.zone_code',$zone->code)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
						->count();
			
			$zone->totalLate = $zone->totalDelivered - $zone->totalOnTime;
			
			$chartData = [];
			$chartData[] = ['Delivered','Total'];
			$chartData[] = ['Fulfilled', $zone->totalOnTime];
			$chartData[] = ['Unfulfilled', $zone->totalLate];
			
			$zone->chartData = $chartData;
			
			$totals[] = [$zone->code, $zone->totalDelivered];
			
			if(!empty($zone->totalDelivered))
				$hasChartData = true;
			$i++;			
		}		 

		$data = [];
		
		return view('performance.lead_time_provider')->with(compact('hasChartData','totals','totalChart','zones','filterStartDate','filterEndDate'));

	}	
	
	public function zoneLeadTime(Request $request, $pid, $zid, $startDate = '',$endDate = '') {
		
		$hasChartData = false;
		if(empty($startDate)){
			$startPeriod =  Carbon::parse('first day of this month 00:00:00');
			$endPeriod =  Carbon::parse('last day of this month 23:59:59');
		}
		else {
			$startPeriod = Carbon::parse($startDate.' 00:00:00');
			$endPeriod = Carbon::parse($endDate.' 23:59:59');			
		}
						
		if($request->filterStartDate){
			$startPeriod = Carbon::parse($request->filterStartDate.' 00:00:00');
			$endPeriod = Carbon::parse($request->filterEndDate.' 23:59:59');
		}
		$filterStartDate = $startPeriod->toDateString();
		$filterEndDate = $endPeriod->toDateString();
		
		//$provider = DB::table('tbl_vendor')->select('common_name')
		$provider = Courier::select('id','display_name')
				->where('id',$pid)->first();

		$zone = Zone::select('name', 'code')
				->where('id',$zid)->first();
		$info = clone $zone;
		$info->providerName = $provider->display_name;
		
		//$zipCodes = tbl_delivery_order::select('tbl_delivery_order.city','tbl_delivery_order.zip')
		//$zipCodes = tbl_delivery_order::select('tbl_delivery_order.zip')
		$zipCodes = DB::table('tbl_delivery_order')
						//->select('tbl_delivery_order.zip')
						->select(DB::raw('count(*) as totalDelivered, zip, cities.name as city_name'))
						//->select('tbl_delivery_order.zip')
						->join('postal_codes', 'tbl_delivery_order.zip', '=', 'postal_codes.code')
						->join('cities', 'postal_codes.city_id', '=', 'cities.id')
						->whereBetween('tbl_delivery_order.created_at',[$startPeriod,$endPeriod])
						//->where('tbl_delivery_order.transporter',$provider->common_name)
						->where('tbl_delivery_order.courier_id',$provider->id)
						->where('tbl_delivery_order.is_delivered',true)
						->where('tbl_delivery_order.deleted_at',null)
						->where('postal_codes.zone_code',$info->code)
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
						->groupBy('zip')
						->orderBy('totalDelivered', 'desc')
						->get();
						
		//echo json_encode($zipCodes);
		//die();
		$totals = [];
		$info->delivered = 0;
		$info->fulfilled = 0;
		$info->unfulfilled = 0;
		
		$i=1;		
		foreach($zipCodes as $zipCode){
			$zipCode->idx = $i;
			//$zipCode->totalDelivered = $zipCode->totalDelivered;
			/*
			$zipCode->totalDelivered = tbl_delivery_order::select('tbl_delivery_order.id')						
						->whereBetween('tbl_delivery_order.created_at',[$startPeriod,$endPeriod])
						->where('tbl_delivery_order.transporter',$provider->common_name)
						->where('tbl_delivery_order.is_delivered',true)
						->where('tbl_delivery_order.city',$zipCode->city)
						->where('tbl_delivery_order.zip',$zipCode->zip)
						->where('tbl_delivery_order.deleted_at',null)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
						->count();
			*/
			
			$zipCode->totalOnTime = tbl_delivery_order::select('tbl_delivery_order.id')						
						->whereBetween('tbl_delivery_order.created_at',[$startPeriod,$endPeriod])
						//->where('tbl_delivery_order.transporter',$provider->common_name)
						->where('tbl_delivery_order.courier_id',$provider->id)
						->where('tbl_delivery_order.is_delivered',true)
						->where('tbl_delivery_order.on_time_provider',true)
						//->where('tbl_delivery_order.city',$zipCode->city)
						->where('tbl_delivery_order.zip',$zipCode->zip)
						->where('tbl_delivery_order.deleted_at',null)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
						->count();
			
			$zipCode->totalLate = $zipCode->totalDelivered - $zipCode->totalOnTime;
			
			$chartData = [];
			$chartData[] = ['Delivered','Total'];
			$chartData[] = ['Fulfilled', $zipCode->totalOnTime];
			$chartData[] = ['Unfulfilled', $zipCode->totalLate];
		
			$zipCode->chartData = $chartData;
			
			
		$info->delivered += $zipCode->totalDelivered;
		$info->fulfilled += $zipCode->totalOnTime;
		$info->unfulfilled += $zipCode->totalLate;
			
			$totals[] = [$info->code, $zipCode->totalDelivered];
			
			$i++;			
		}		
		
		$info->fulPct =  0;
		$info->unfulPct = 0;
		if($info->delivered > 0){
			$info->fulPct = round($info->fulfilled/$info->delivered*100 ,2);
			$info->unfulPct = round($info->unfulfilled/$info->delivered*100 ,2);			
		}

		$data = [];
		
		return view('performance.lead_time_zone')->with(compact('hasChartData','totals','zipCodes','info','filterStartDate','filterEndDate'));


	}
	
	


         public function a() {

        }


}
