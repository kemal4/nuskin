<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponStatusController extends Controller
{

	//example url for First Logistic (http://localhost/nuskin-mvp/trackdemo/STDEMO01/100000ST/1000/1234500)
	//example url for MrSpeedy (http://localhost/nuskin-mvp/deliveries/1234)

    public function mrspeedy($status) {

        switch (connection_status()) {   
            case CONNECTION_NORMAL:
              $txt = 'Connection is in a normal state';
              break;
            case CONNECTION_ABORTED:
              $txt = 'Connection aborted';
              break;
            case CONNECTION_TIMEOUT:
              $txt = 'Connection timed out';
              break;
            case (CONNECTION_ABORTED & CONNECTION_TIMEOUT):
              $txt = 'Connection aborted and timed out';
              break;
            default:
              $txt = 'Unknown';
              break;
        }

        if ($txt = 'Connection is in a normal state') {

            $data = \App\Model\tbl_respon_status::select('delivery_id','order_id','order_name','address','status','created_at AS status_datetime','created_at AS created_datetime')
            									->where('status','like', '%' .$status. '%')
            									->orWhere('delivery_id','like', '%' .$status. '%')
            									->orWhere('address','like', '%' .$status. '%')
            									->take(100)
            									->get();

            if(count($data) > 0){ 
    				$deliver = count($data);

    				if ($deliver > 100) {
		    			return response()->json([ 'message' => 'Maximum number of deliveries to be returned. Cannot be higher than 100.' ]);
    				} else {
		                $res['is_successful'] 		= true;
		                $res['deliveries'] 			= $data;
		                $res['deliveries_count'] 	= count($data);

		                return response($res, 200);
    				}

            } else {
                return response()->json([
                    'message' => 'invalid_string.',
                ], 404);
            }

        } else {
            return response($txt, 599);
        }

    }

    public function firstlogistic($APPID, $account, $ref, $hawb) {
    	switch (connection_status()) {   
            case CONNECTION_NORMAL:
              $txt = 'Connection is in a normal state';
              break;
            case CONNECTION_ABORTED:
              $txt = 'Connection aborted';
              break;
            case CONNECTION_TIMEOUT:
              $txt = 'Connection timed out';
              break;
            case (CONNECTION_ABORTED & CONNECTION_TIMEOUT):
              $txt = 'Connection aborted and timed out';
              break;
            default:
              $txt = 'Unknown';
              break;
        }
 
        if ($txt = 'Connection is in a normal state') {
        	
        	$APPIDdefault = 'STDEMO01';
        	$accountdefault = '100000ST';
        	$refdefault = 1000;

        	if ($APPIDdefault == $APPID && $accountdefault == $account && $refdefault == $ref) {

            	$data = \App\Model\tbl_respon_status::where('delivery_id',$hawb) //delivery_id == hawb untuk simulasi
            				->where('courier', 'FL')
            				->get();             				
            	
	            if(count($data) > 0){ 
	                $res['message'] = "Success!";
	                $res['values'] = $data;

	                return response($res, 200);
	            } else {
	                return response()->json([
	                    'message' => 'Order_id Not Found.',
	                ], 404);
	            }

        	} else {
        		if ($refdefault != $ref ) {
	    			return response()->json([
	                    'Error' => 'Data unavailable, please check Your REF and contact us for more support !!',
	                ], 404);
        		}else {
        			return response()->json([
	                    'Error' => 'Wrong APP-ID and ACCOUNT !!',
	                ], 404);
        		}

        	}	
        } else {
            return response($txt, 599);
        }

    }

}
