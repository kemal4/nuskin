<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Model\tbl_delivery_order;
use Carbon\Carbon;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;

class CashboardController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		
        $this->middleware(['auth','verified']);
        parent::__construct();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
	echo "===========";
	die();
	echo "===========";
	die();

        $newNow = Carbon::parse('last year'); 

	$monthLabel = [];

	// $vendorList = DB::table('tbl_vendor')->select('')
	$vendorList = array('FL','JNE','MrSpeedy');
	$vendors = [];
	$vendors[0]['name']='FL';
	$vendors[1]['name']='JNE';
	$vendors[2]['name']='MrSpeedy';

	$nuskinTotal = [];
	$i = 0;
	while($i < 12){
		$newNow = $newNow->addmonth();	
	        $monthLabel[$i] = $newNow->format('Y M');

		Log::error('---------- start month  '.$i);

		$nuskinTotal[$i] = DB::table('tbl_delivery_order')->select('id')
				->whereMonth('created_at',$newNow->month)
				->whereYear('created_at',$newNow->year)
				->where('is_preview','<>','yes')
				->count();
				
		
		for($j = 0 ; $j < 3; $j++  ){
			$vendors[$j]['data'][$i] = DB::table('tbl_delivery_order')->select('id')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
								->where('transporter',$vendors[$j]['name'])
				->where('is_preview','<>','yes')
                                ->count();
		}
		$i++;
	}
	

	$summary = [];
	
	$summary['totalDelivery'] = $nuskinTotal[11];
	$summary['totalSucceed'] = DB::table('tbl_delivery_order')->select('id')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
								//->where('status','delivered')
								->where('is_delivered',true)
								->count();
	$summary['totalComplaint'] = DB::table('tbl_ticket')->select('id')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
                                ->count();
        $summary['totalSolved'] = DB::table('tbl_ticket')->select('id')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
                                ->where('status','Solved')
                                ->count();
        $summary['totalFulfilled'] = DB::table('tbl_delivery_order')->select('id')
                                ->whereMonth('created_at',$newNow->month)
                                ->whereYear('created_at',$newNow->year)
                                ->where('on_time_nuskin',true)
				->where('is_preview','<>','yes')
                                ->count();
        $summary['totalComplaintNon'] = 0;
        $summary['totalSolvedNon'] = 0;

	$data = [];
	$data['summary'] = $summary;
	$data['nuskinTotal'] = $nuskinTotal;
	$data['vendors'] = $vendors;
	$data['monthLabel'] = $monthLabel;
/*
	echo '<br>';
	echo json_encode($summary);
	echo '<br>';
	echo '<br>';
	echo '<br>';
	echo json_encode($nuskin);
	echo '<br>';
	echo '<br>';
	echo json_encode($vendors);
	echo '<br>';
	echo '<br>';
	echo '<br>';
	echo json_encode($monthLabel);
	echo '<br>';
	echo '<br>';
	echo '<br>';
	die();
*/

        //return view('admin.home',['data' => $data]);
        return view('admin.home')->with(compact('data'));
    }

}
