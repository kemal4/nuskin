<?php

namespace App\Http\Controllers;

use App\Model\Zone;
use Illuminate\Http\Request;
use DataTables;
use Auth;

class ZoneController extends Controller
{
	protected $model_name = 'Zone';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Log::error('----PostalCodeController------ - '.json_encode($request->all()));
        if ($request->ajax()) {
            //$data = Zone::latest()->get();
            $data = Zone::select('*');
			
			if($request->input('order.0.column') == 0){
                $data = Zone::orderBy('created_at','desc');			
			}
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('settings/zone');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Zone::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					'code' => $request->code
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
        //Session::flash('alertGeneralType','success');    
        //Session::flash('alertGeneralTitle',$alertMessage);  
   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    //public function edit(Zone $zone)
    public function edit($id)
    {
        //
        $model = Zone::find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    //public function destroy(Zone $zone)
    public function destroy($id)
    {
        //
        Zone::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
}
