<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Model\tbl_pic;
use App\Model\tbl_delivery_order;
use App\Model\tbl_ticket;
use App\Model\tbl_gudang;
use App\Model\tbl_vendor;
use App\Model\tbl_historyDO;
use App\Model\PostalCode;
use App\Model\Courier;
use App\Model\City;
use App\Model\NuskinStatus;
use Carbon\Carbon;
use Session;
use DB;
use Auth;
use DataTables;
use App\Imports\DeliveryVendorImport;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use tbl_delivery_order as GlobalDeliveryvendor;

class DeliveryController extends Controller
{
	
	function __construct()
    {
        // $this->middleware('permission:view_delivery', ['only' => ['index','detailPickup']]);
        // $this->middleware('permission:edit_delivery', ['only' => ['editDO','saveEditDO']]);
        // $this->middleware('permission:create_delivery', ['only' 
        //  					=> ['createlist','store','import_excel','uploadDataPreview','saveDO','cancelPreviewDO']]);
    }

	// TODO change into DB
	protected $courierToIdMap = array(
		'JNE' => 1, 
		'MrSpeedy' => 2,
		'FL' => 3,
	);
	
	
	protected $holidayDates = array(
						'2020-01-01',
						'2020-01-25',
						'2020-03-22',
						'2020-03-25',
						'2020-04-10',
						'2020-05-01',
						'2020-05-07',
						'2020-05-21',
						'2020-05-22',
						'2020-05-24',
						'2020-05-25',
						'2020-05-26',
						'2020-05-27',
						'2020-06-01',
						'2020-07-31',
						'2020-08-17',
						'2020-08-20',
						'2020-10-29',
						'2020-12-24',
						'2020-12-25'
					);
	
    public function index(Request $request) {

		$date = date("Y/m/d");
		
		$overdate = [];
		$solvedDO = [];
		$deliveryvendor = [];
		$warehouses = DB::table('tbl_gudang')->whereNull('deleted_at')->pluck('lokasi_gudang','id_gudang');
		
		$today = Carbon::today();
            
            $query = tbl_delivery_order::select(['id'])
					->where('status','onprocess')
					->where('eta_nuskin','<', $today)
					//->where('is_preview', '<>', 'yes')	
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
					->count();
		//dd($query);
		$alertGeneral = [];
		/*
		if(!empty($query)){
			$alertGeneral['danger'] = [
										'title' => 'you have '.$query.' overdue Delivery Order',
										'message' => ''
									];						
			$alertGeneral['timeout'] = 0;
			
		}		
		*/
		session(['filter_set' => null]);
		
		session(['filter_start_date' => '']);
		session(['filter_end_date' => '']);
		session(['filter_invoice' => '']);
		session(['filter_hawb' => '']);
		session(['filter_customer' => '']);
		session(['filter_courier_id' => '']);
		session(['filter_status_id' => '']);

		$statuses = NuskinStatus::select('id','display_name')->get();
		$couriers = Courier::select('id','display_name')->where('active',1)->get();
		

		if ($request->is('api/*')) {	
			return response()->json([
				'statuses' => $statuses,
				'couriers'=> $couriers,
				'warehouses'=> $warehouses,
				'deliveryvendor'=> $deliveryvendor, 
				'overdate'=> $overdate, 
				'solvedDO'=> $solvedDO,
				'alertGeneral'=> $alertGeneral
			], 200);
		}
		else
		{
			return view('admin.list_dv_vendor',compact('statuses','couriers','warehouses','deliveryvendor', 'overdate', 'solvedDO','alertGeneral'));
		}
}
	
	public function getAllListJsonBAK(Request $request){
		
		//Log::error('---------- - '.json_encode($request->all()));
		Log::error('----getAllListJson-ajax----- - '.json_encode($request->ajax()));


		$today = Carbon::today();
		
		$columns = array(
                0 => "created_at" ,
                1 => "order_id" ,
                2 => "create_date" ,
                3 => "transporter" ,
                4 => "status" ,
                5 => "hawb" ,
                6 => "customer_refer" ,
                7 => "created_at" ,
                8 => "customer_id" ,
                9 => "customer_name" ,
                10 => "phone" ,
                11 => "city" ,
                12 => "zip" ,
                13 => "warehouse" ,
                14 => "leadtime_nuskin" ,
                15 => "eta_nuskin" ,
                //16 => "leadtime_courier" ,
                //17 => "eta_provider" ,
                //18 => "id" 
		);
			
		$totalData = tbl_delivery_order::count();
		
		$totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
		
		Log::error('---($order == 0)--- - '.$order  . ' = dir '.$dir);
		if($request->input('order.0.column') == 0){
			$order = 'created_at';
			$dir = 'desc';
		}
		Log::error('---($order == 0)--- - '.$order  . ' = dir '.$dir);
           		  
        if(empty($request->input('search.value')))
        {            
            $allDo = tbl_delivery_order::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 
            $allDo =  tbl_delivery_order::where('order_id','LIKE',"%{$search}%")
                            ->orWhere('transporter', 'LIKE',"%{$search}%")
                            ->orWhere('customer_refer', 'LIKE',"%{$search}%")
                            ->orWhere('customer_id', 'LIKE',"%{$search}%")
                            ->orWhere('customer_name', 'LIKE',"%{$search}%")
                            ->orWhere('phone', 'LIKE',"%{$search}%")
                            ->orWhere('city', 'LIKE',"%{$search}%")
                            ->orWhere('zip', 'LIKE',"%{$search}%")
                            ->orWhere('hawb','=', $search)
                            ->orWhere('status', 'LIKE',"%{$search}%")
					->where('is_preview', '<>', 'yes')	
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            //->orWhere('hawb', 'LIKE',"%{$search}%")

            $totalFiltered = tbl_delivery_order::where('order_id','LIKE',"%{$search}%")
                            ->orWhere('transporter', 'LIKE',"%{$search}%")
                            ->orWhere('customer_refer', 'LIKE',"%{$search}%")
                            ->orWhere('customer_id', 'LIKE',"%{$search}%")
                            ->orWhere('customer_name', 'LIKE',"%{$search}%")
                            ->orWhere('city', 'LIKE',"%{$search}%")
                            ->orWhere('phone', 'LIKE',"%{$search}%")
                            ->orWhere('zip', 'LIKE',"%{$search}%")
                            ->orWhere('hawb', '=',"{$search}")
                            ->orWhere('status', 'LIKE',"%{$search}%")
					->where('is_preview', '<>', 'yes')	
                             ->count();
                            //->orWhere('hawb', 'LIKE',"%{$search}%")
        }

        $data = array();
        if(!empty($allDo))
        {
			$can_edit = (Auth::user()->rolename == 'Admin' || Auth::user()->rolename == 'warehouse' );
			$can_complaint = (Auth::user()->rolename == 'Admin' || Auth::user()->rolename == 'crm' );
			
            foreach ($allDo as $order)
            {
                $show =  route('detail_pickup',$order->id);
                $edit =  route('edit_do',$order->id);
                $ticket =  route('newticket',$order->id);

                $nestedData['temp'] = "";
                //$nestedData['order_id'] = $order->order_id;
				if(!empty( $order->is_partial_so) )
					$nestedData['order_id'] = '<span class="badge badge-info" data-toggle="tooltip" title="Partial SO">'.$order->order_id.'</span>';
				else 
					$nestedData['order_id'] = $order->order_id;
                $nestedData['create_date'] = date('m/d/Y',strtotime(Carbon::Parse($order->create_date)));
                $nestedData['transporter'] = $order->transporter;
                $nestedData['status'] = $this->setStatusBadge($order->status);
                $nestedData['hawb'] = $order->hawb;
                $nestedData['customer_refer'] = $order->customer_refer;
                //$nestedData['created_at'] = $order->created_at;
                $nestedData['created_at'] = date('m/d/Y',strtotime($order->created_at));
                $nestedData['customer_id'] = $order->customer_id;
                $nestedData['customer_name'] = $order->customer_name;
                $nestedData['phone'] = $order->phone;
                $nestedData['city'] = $order->city;
                $nestedData['zip'] = $order->zip;
                $nestedData['warehouse'] = $order->warehouse_name;
                $nestedData['leadtime_nuskin'] = $order->leadtime_nuskin;
                //$nestedData['eta_nuskin'] = $order->eta_nuskin;   
				$tempStr = date('m/d/Y',strtotime($order->eta_nuskin));
				if(empty($order->eta_nuskin))
					$nestedData['eta_nuskin'] = '';
				else {
					$nestedData['eta_nuskin'] = date('m/d/Y',strtotime($order->eta_nuskin));
					if($order->eta_nuskin < $today)
						$nestedData['eta_nuskin'] = '<span class="badge badge-danger">'.$nestedData['eta_nuskin'].'</span>';
						
				}
							
            //    $nestedData['leadtime_courier'] = $order->leadtime_courier;
                //$nestedData['eta_provider'] = $order->eta_provider;                
            //    $nestedData['eta_provider'] = date('m/d/Y',strtotime($order->eta_provider));
					$opts = "<a href='{$show}' class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'><i class='fas fa-info-circle'></i></a>";
					if($can_edit)
						$opts = $opts." <a href='{$edit}' class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>";
					if($can_complaint)
						$opts = $opts." <a href='{$ticket}' class='btn btn-xs btn-info' data-toggle='tooltip' title='Ticket' ><i class='fas fa-list-ul'></i></a>";
                $nestedData['options'] = $opts;
                $data[] = $nestedData;

            }
        }
		// &emsp; button action &emsp;
                       
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
		
		
		
	}
	
	public function setFilter($query){
		
				
		//if (!empty(session('filter_start_date') ) && (strcasecmp(session('filter_start_date'),'Invalid date') && !empty(session('filter_end_date')) != 0 ) )
		if (!empty(session('filter_start_date') ) && !empty(session('filter_end_date'))  ) 
			//$query = $query->whereBetween('created_at',[Carbon::parse(session('filter_start_date').' 00:00:00'),Carbon::parse(session('filter_end_date').' 23:59:59')]);
			if (session('filter_start_date') == session('filter_end_date'))
			{
				$query = $query->where('create_date', Carbon::parse(session('filter_start_date')));
			}
			else
			{
				$query = $query->whereBetween('create_date',[Carbon::parse(session('filter_start_date').' 00:00:00'),Carbon::parse(session('filter_end_date').' 23:59:59')]);
			}
		if (!empty(session('filter_invoice') ) )
			$query = $query->where('order_id',session('filter_invoice'));
		if (!empty(session('filter_hawb') )  )
			$query = $query->where('hawb',session('filter_hawb'));
		if (!empty(session('filter_customer') ) ) 
			$query = $query->whereRaw("LOWER(customer_name) LIKE ? ",['%'.trim(strtolower(session('filter_customer'))).'%']);
		if (!empty(session('filter_courier_id') ) ) 
			$query = $query->where('courier_id',session('filter_courier_id'));
		if (!empty(session('filter_status_id') ) ) 
			$query = $query->where('status_id',session('filter_status_id'));
	
		return 	$query ;	
	}
	
    public function getAllListJson(Request $request)
    {
        //
		//Log::error('----ajaxOrderDelivered-ajax----- - '.json_encode($request->ajax()));
		
        //if ($request->ajax()) {
			//Log::error('---------- - '.json_encode($request->all()));
			
            $query = tbl_delivery_order::
					select(['id', 'order_id', 'create_date', 'transporter', 'status',
							'hawb','customer_refer','created_at','customer_id','customer_name',
							'phone','city','zip','warehouse_name','leadtime_nuskin','eta_nuskin','is_partial_so','is_delivered',
							//'leadtime_courier','eta_provider',
							])
					//->where('status', 'delivered')
					//->where('is_preview', '<>', 'yes')	
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						});
							//'','','','','','','','','',
			if (!($request->is('api/*'))) {
					if ($request->session()->has('filter_set')) {
						//
						$query = $this->setFilter($query);
					}
					
				if($request->input('order.0.column') == 0){
            	    $query = $query->orderBy('created_at','desc');
					
				}				
				//$query = tbl_delivery_order::latest()->get();
            	        /*
						->addColumn('warehouse','')
						->editColumn('warehouse', function ($row) {	
							return $row->warehouse_name;
						})
						*/
            	return Datatables::of($query)
            	        ->addIndexColumn()
            	        ->addColumn('check_box', function($query)
            	        {
            	        	return '<input type="checkbox" class="singleAllCheckbox" id="'.$query->id.'" value="'.$query->id_gudang.'">';
            	        })
						->editColumn('status', function ($row) {
							return $this->setStatusBadge($row->status);
						})		
						->editColumn('eta_nuskin', function ($row) {
							//if($row->eta_nuskin < $today)
							if(empty($row->is_delivered) && $row->eta_nuskin < Carbon::today())
								return '<span class="badge badge-danger">'.$row->eta_nuskin.'</span>';
							else
								return $row->eta_nuskin;
						})		
						->editColumn('created_at', function ($row) {
							return $row->created_at ? with(new Carbon($row->created_at))->format('m/d/Y') : '';
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') like ?", ["%$keyword%"]);
						})
						->editColumn('create_date', function ($row) {
							return $row->create_date ? with(new Carbon($row->create_date))->format('m/d/Y') : '';;
						})
						->filterColumn('create_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(create_date,'%m/%d/%Y') like ?", ["%$keyword%"]);
						})
						->editColumn('order_id', function ($row) {
							if(!empty( $row->is_partial_so) )
								return '<span class="badge badge-info" data-toggle="tooltip" title="Partial SO">'.$row->order_id.'</span>';
							else 
								return $row->order_id;
						})
						
						
            	        ->addColumn('options', function($row){   
							
							$show =  route('detail_pickup',$row->id);
							$edit =  route('edit_do',$row->id);
							$ticket =  route('newticket',$row->id);
	
							$optStr = "<a href=".$show." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'><i class='fas fa-info-circle'></i></a>";
							if(Auth::user()->can('edit_delivery'))
								$optStr .= "
									<a href=".$edit." class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>";
							// just view if user()->can('view_ticket_delivery' and 'create_ticket_delivery') her/his can create no view only.
							// if(Auth::user()->can('create_ticket_delivery') || Auth::user()->can('view_ticket_delivery'))
							if(Auth::user()->can('create_ticket_delivery') || Auth::user()->can('view_ticket_delivery')  || Auth::user()->can('edit_ticket_delivery'))
							//if(Auth::user()->can('create_ticket_delivery'))
								$optStr .= "
									<a href=".$ticket." class='btn btn-xs btn-info' data-toggle='tooltip' title='Ticket' ><i class='fas fa-ticket-alt'></i></a>";
							
							return $optStr;
							//return "<a href=".$show." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'><i class='fas fa-info-circle'></i></a>
							//		<a href=".$edit." class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>
							//		<a href=".$ticket." class='btn btn-xs btn-info' data-toggle='tooltip' title='Ticket' ><i class='fas fa-list-ul'></i></a>";
            	        })
            	        // ->editColumn('warehouse_name', function($row)
            	        // {
            	        // 	$content = "<div class='warehouse_text'>".$row->warehouse_name."</div>";
            	        // 	$content .= "<div class='warehouse_select' style='display: none;'>";
            	        // 	$content .= "<select>";
            	        // 	$tbl_gudang = tbl_gudang::select('id_gudang','lokasi_gudang')->get();

            	        // 	foreach ($tbl_gudang as $t_g)
            	        // 	{
            	        // 		$content .= "<option value='".$t_g->id_gudang."'>".$t_g->lokasi_gudang."</option>";
            	        // 	}

            	        // 	$content .= "</select></div>";

            	        // 	return $content;
            	        // })				
            	        ->rawColumns(['options','status','order_id','eta_nuskin','check_box'])
						//->smart(false)
            	        ->make(true);
            }
            else
            {
            	return response()->json($query->get(), 200);
            }

					
        //}
      
    }
	
	
    public function ajaxOrderDelivered(Request $request)
    {
		
		$user = Auth::user();
        //
		//Log::error('----ajaxOrderDelivered-ajax----- - '.json_encode($request->ajax()));
		
        //if ($request->ajax()) {
			//Log::error('---------- - '.json_encode($request->all()));
			
		
		
            
            $query = tbl_delivery_order::
					select(['id', 'order_id', 'create_date', 'transporter', 'status',
							'hawb','customer_refer','created_at','customer_id','customer_name',
							'phone','city','zip','warehouse_name','leadtime_nuskin','eta_nuskin','is_partial_so','is_delivered',
							//'leadtime_courier','eta_provider',
							])
					->where('status', 'delivered')
					//->where('is_preview', '<>', 'yes')	
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})				
					;
							//'','','','','','','','','',
			if (!($request->is('api/*'))) {
					if ($request->session()->has('filter_set')) {
						//
						$query = $this->setFilter($query);
					}
				
				if($request->input('order.0.column') == 0){
            	    $query = $query->orderBy('created_at','desc');
					
				}				
				//$query = tbl_delivery_order::latest()->get();
            	return Datatables::of($query)
            	        ->addIndexColumn()
            	        //->addColumn('warehouse','')
						->editColumn('status', function ($row) {
							return $this->setStatusBadge($row->status);
						})		
						->editColumn('eta_nuskin', function ($row) {
							//if($row->eta_nuskin < $today)
							if(empty($row->is_delivered) && $row->eta_nuskin < Carbon::today())
								return '<span class="badge badge-danger">'.$row->eta_nuskin.'</span>';
							else
								return $row->eta_nuskin;
						})		
						->editColumn('created_at', function ($row) {
							return $row->created_at ? with(new Carbon($row->created_at))->format('m/d/Y') : '';
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') like ?", ["%$keyword%"]);
						})
						->editColumn('create_date', function ($row) {
							return $row->create_date ? with(new Carbon($row->create_date))->format('m/d/Y') : '';;
						})
						->filterColumn('create_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(create_date,'%m/%d/%Y') like ?", ["%$keyword%"]);
						})
						->editColumn('order_id', function ($row) {
							if(!empty( $row->is_partial_so) )
								return '<span class="badge badge-info" data-toggle="tooltip" title="Partial SO">'.$row->order_id.'</span>';
							else 
								return $row->order_id;
						})
						
						
            	        ->addColumn('options', function($row){   
							
							$show =  route('detail_pickup',$row->id);
							$edit =  route('edit_do',$row->id);
							$ticket =  route('newticket',$row->id);
							
							$optStr = "<a href=".$show." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'><i class='fas fa-info-circle'></i></a>";
							if(Auth::user()->can('edit_delivery'))
								$optStr .= "
									<a href=".$edit." class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>";
							if(Auth::user()->can('create_ticket_delivery') || Auth::user()->can('view_ticket_delivery') || Auth::user()->can('edit_ticket_delivery'))
								$optStr .= "
									<a href=".$ticket." class='btn btn-xs btn-info' data-toggle='tooltip' title='Ticket' ><i class='fas fa-list-ul'></i></a>";
							
							return $optStr;
            	        })				
            	        ->rawColumns(['options','status','order_id','eta_nuskin'])
						//->smart(false)
            	        ->make(true);
			}
			else
            {
            	return response()->json($query->get(), 200);
            }		
        //}
      
    }
	
	
    public function ajaxOrderOverDue(Request $request)
    {		
		
	
		$today = Carbon::today();
            
            $query = tbl_delivery_order::
					select(['id', 'order_id', 'create_date', 'transporter', 'status',
							'hawb','customer_refer','created_at','customer_id','customer_name',
							'phone','city','zip','warehouse_name','leadtime_nuskin','eta_nuskin','is_partial_so','is_delivered',
							//'leadtime_courier','eta_provider',
							])
					->where('status','<>', 'delivered')
					->where('eta_nuskin','<', $today)
					//->where('is_preview', '<>', 'yes')	
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})				
					;
							//'','','','','','','','','',
			if (!($request->is('api/*'))) {	
				if ($request->session()->has('filter_set')) {
					//
					$query = $this->setFilter($query);
				}
				
				if($request->input('order.0.column') == 0){
            	    $query = $query->orderBy('created_at','desc');
					
				}				
				//$query = tbl_delivery_order::latest()->get();
            	return Datatables::of($query)
            	        ->addIndexColumn()
            	        //->addColumn('warehouse','')
						->editColumn('status', function ($row) {
							return $this->setStatusBadge($row->status);
						})		
						->editColumn('eta_nuskin', function ($row) {
							//if($row->eta_nuskin < $today)
							if(empty($row->is_delivered) && $row->eta_nuskin < Carbon::today())
								return '<span class="badge badge-danger">'.$row->eta_nuskin.'</span>';
							else
								return $row->eta_nuskin;
						})		
						->editColumn('created_at', function ($row) {
							return $row->created_at ? with(new Carbon($row->created_at))->format('m/d/Y') : '';
						})
						->filterColumn('created_at', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(created_at,'%m/%d/%Y') like ?", ["%$keyword%"]);
						})
						->editColumn('create_date', function ($row) {
							return $row->create_date ? with(new Carbon($row->create_date))->format('m/d/Y') : '';;
						})
						->filterColumn('create_date', function ($query, $keyword) {
							$query->whereRaw("DATE_FORMAT(create_date,'%m/%d/%Y') like ?", ["%$keyword%"]);
						})
						->editColumn('order_id', function ($row) {						
							if(!empty( $row->is_partial_so) )
								return '<span class="badge badge-info" data-toggle="tooltip" title="Partial SO">'.$row->order_id.'</span>';
							else 
								return $row->order_id;
						})
						
            	        ->addColumn('options', function($row){   
							
							$show =  route('detail_pickup',$row->id);
							$edit =  route('edit_do',$row->id);
							$ticket =  route('newticket',$row->id);
	
							$optStr = "<a href=".$show." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'><i class='fas fa-info-circle'></i></a>";
							if(Auth::user()->can('edit_delivery'))
								$optStr .= "
									<a href=".$edit." class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>";
							if(Auth::user()->can('create_ticket_delivery') || Auth::user()->can('view_ticket_delivery')  || Auth::user()->can('edit_ticket_delivery'))
								$optStr .= "
									<a href=".$ticket." class='btn btn-xs btn-info' data-toggle='tooltip' title='Ticket' ><i class='fas fa-list-ul'></i></a>";
							
							return $optStr;
							//return "<a href=".$show." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'><i class='fas fa-info-circle'></i></a>
							//		<a href=".$edit." class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>
							//		<a href=".$ticket." class='btn btn-xs btn-info' data-toggle='tooltip' title='Ticket' ><i class='fas fa-list-ul'></i></a>";
            	        })				
            	        ->rawColumns(['options','status','eta_nuskin','order_id'])
						//->smart(false)
            	        ->make(true);
			}
			else
            {
            	return response()->json($query->get(), 200);
            }
        //}
      
    }
	
	
	
	public function setStatusBadge($status){
		$statusBadge = [
				'onprocess'=>'info',
				'return'=>'warning',
				'delivered'=>'success',
				'undelivered'=>'danger',
				'incident'=>'danger',
				'Hand Over Kurir'=>'info',
		
		];
		$statusMap = [
				'onprocess'=>'On Process',
				'return'=>'Return',
				'delivered'=>'Delivered',
				'undelivered'=>'Undelivered',
				'incident'=>'Incident',
				'Hand Over Kurir'=>'Hand Over Kurir',
		
		];
		
		return '<span class="badge badge-'.$statusBadge[$status].'">'.$statusMap[$status].'</span>';
		
	}
		
    public function checkFieldExist(Request $request , $field = '', $value = '') {
		
		$data = [];
		$data['field_exist'] = false;
		if(strlen($field) > 0){
			$data['field_exist'] = tbl_delivery_order::where($field,$value)->exists();	
		}
		
        return response()->json($data);
    }

	public function import_excel(Request $request) {
        
		//dd($request->all());
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx',
            'wh_id' => 'required',			
        ]);
		
		$sss = "";
		$sss .= Auth::user()->id;
		$sss .= "-";
		$sss .= time();

 
		$wh_id = $request->wh_id;
		$warehouse = tbl_gudang::find($wh_id);
 
        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        
        $file->move(public_path('DeliveryVendo'), $nama_file);
		
		$courierToId = Courier::pluck('id','display_name');
		// try
		// {
			Excel::import(new DeliveryVendorImport($warehouse,$sss,$courierToId), public_path('/DeliveryVendo/'.$nama_file));

		// }
		// catch (\Exception $ex)
		// {
		// 	return back()->withError('Something Wrong.');
		// }
        
		session(['warehouse_name' => $warehouse->lokasi_gudang]);
		session(['previewKey' => $sss]);
		// if ($request->is('api/*')) {	
         	return redirect()->route('preview');
  //   	}
  //   	else
  //   	{
  //   		return response()->json(['message' => 'success'],200);
  //   	}
    }

    public function detailPickup(Request $request, $id) {
        
        $delven = tbl_delivery_order::where('id', $id)->first();

        $deliverylogs = tbl_historyDO::where('id_delivery',$id)
        ->orderby('created_at','DESC')
        ->get();

        $client = new Client();

        $response = $client->request('POST', 'https://ws.firstlogistics.co.id/nuskin/track.php', [
			'form_params' => [
				//'FUNCTION' => 'track',
				//'FUNCTION' => 'hawbdemo',
				'APPID' => 'NSK634UP',
				'ACCOUNT' => '0700000010',
				'REF' => $delven->hawb
				//'REF' => $delven->order_id
				//'REF' => '85928834'
				
			]
		]);

		$jsonBody = $response->getBody()->getContents();
		$body = json_decode($jsonBody, true);
		$body['transporter'] = $delven->transporter;
        if ($request->is('api/*')) {	
        	return response()->json(['delven' => $delven, 'deliverylogs' => $deliverylogs, 'body' => $body], 200);
    	}
    	else
    	{
    		return view('admin.detail_pickup',compact('delven','deliverylogs', 'body'));
    	}

    }

    public function createlist(Request $request) {
		//Auth::user()->name;
		
        $listgudang  = tbl_gudang::whereNull('deleted_at')->pluck('lokasi_gudang','id_gudang');
        $listcourier = Courier::where('active',1)->pluck('display_name','id');
		
		$cities = City::select('id','name')->get();
		
        // return view('admin.list_input_data',compact('listgudang','listcourier','cities'));
        if ($request->is('api/*')) {
        	return response()->json(['listgudang' => $listgudang,
        		'listcourier' => $listcourier,
        		'cities' => $cities
        	]);
        }
        else{
        	return view('admin.add_delivery',compact('listgudang','listcourier','cities'));
        }
        
    }

    public function store(Request $request) {
        
        request()->validate([
            'invoiceid'       => 'required',
            'createdate'      => 'required',
            'transporter'     => 'required',
            //'ordertype'       => 'required',
            //'customrefer'     => 'required',
            'customid'        => 'required',
            'customname'      => 'required',
            'phone'           => 'required',
            'city'            => 'required',
            'zip'             => 'required',
            'grosswight'      => 'required',
            'netweight'       => 'required',
            'volume'          => 'required',
            'adress'          => 'required',
            // 'gudangdo'          => 'required',
            'hawb'            => 'required',
            'delivery_number' => 'required'
            
        ]);
            //'hawb'            => 'required|unique:tbl_delivery_order',

		$date = now();
		if ($request->is('api/*')) {
			$user = 'api';
		}
		else{
			$user = Auth::user()->name;
		}
        
        $status = 'Handover Kurir';
        $status = 'onprocess';
		
        $perusahaan = new tbl_delivery_order;
        $perusahaan->create_date            = Carbon::parse($request->createdate." 00:00:00");
        $perusahaan->order_id               = $request->invoiceid;
        $perusahaan->order_type             = $request->ordertype;
        $perusahaan->customer_refer         = $request->delivery_number;
        $perusahaan->customer_id            = $request->customid;
        $perusahaan->customer_name          = $request->customname;
        $perusahaan->phone                  = $request->phone;
        $perusahaan->city                   = $request->city;
        $perusahaan->zip                    = $request->zip;
        $perusahaan->grosswiight            = $request->grosswight;
        $perusahaan->net_weight             = $request->netweight;
        $perusahaan->volume                 = $request->volume;
        $perusahaan->hawb                   = $request->hawb;
        $perusahaan->adress                 = $request->adress;
        //$perusahaan->delivery_number        = $request->delivnumber;
        $perusahaan->delivery_number        = $request->delivery_number;
        $perusahaan->status                 = $status;
        $perusahaan->status_id                 = 1;
        // $perusahaan->gudang                 = $request->gudangdo;
        $perusahaan->pic                    = $user;
        //$perusahaan->delivery_date          = $date;
        $perusahaan->is_preview             = '';
        $perusahaan->is_deleted             = '';
		
		$courierName = Courier::pluck('display_name','id');		
        $perusahaan->courier_id            = $request->transporter;
        $perusahaan->transporter           = $courierName[$request->transporter];
		
        $listgudang = DB::table('tbl_gudang')->pluck('lokasi_gudang','id_gudang');
		$perusahaan->warehouse_id                 = $request->gudangdo;
		$perusahaan->warehouse_name                 = $listgudang[$request->gudangdo];
		
		if(tbl_delivery_order::select('id')
				->where('order_id', $perusahaan->order_id)
				//->where('is_preview', '<>', 'yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
					->exists() ){
				
				$perusahaan->is_partial_so = true;
				
				$tempres = tbl_delivery_order::where('order_id', $perusahaan->order_id)
					->update(['is_partial_so' => true]);
	
			};
		
		
		
		// TODO
		// get leadtime based on postal code
		$leadtime_nuskin = DB::table('postal_codes')->select('nuskinLeadTime')
						->where('code',$perusahaan->zip)->first();
		if($leadtime_nuskin != null){
			$eta_nuskin = Carbon::Parse(''.($this->getNextWorkingDay(date('Y-m-d'),$leadtime_nuskin->nuskinLeadTime)).' 23:59:59');
			$perusahaan->leadtime_nuskin = $leadtime_nuskin->nuskinLeadTime;
			$perusahaan->eta_nuskin = $eta_nuskin;			
		}
		
		$leadtime_courier = DB::table('postal_codes')->select('courier_leadtimes.courierLeadTime')
						->join('courier_leadtimes', 'postal_codes.id', '=', 'courier_leadtimes.postal_code_id')
						->where('postal_codes.code',$perusahaan->zip)
						->where('courier_leadtimes.courier_id',$this->courierToIdMap[$perusahaan->transporter])
						->first();						
						//TODO ,change courier into DB
						//->join('tbl_vendor', 'tbl_vendor.id', '=', 'courier_id.postal_code_id')
						//->where('tbl_vendor.id', '=', '$model->courier_id')
		if($leadtime_courier != null){
			$eta_provider = Carbon::Parse(''.($this->getNextWorkingDay(date('Y-m-d'),$leadtime_courier->courierLeadTime,false)).' 23:59:59');
			$perusahaan->leadtime_courier = $leadtime_courier->courierLeadTime;
			$perusahaan->eta_provider = $eta_provider;			
		}
		
		// count eta
		

        $perusahaan->save();
		// add activity log , set status onprocess with desc Handover Kurir
		
		
		tbl_historyDO::create([

						'id_delivery'=> $perusahaan->id,
						'date_vendor'=>$date,
						'status'=>$status,
						'message'=> 'Handover to Courier',
						'pic'=> $user
			
					]);
        //Session::flash('sukses','DO Successfully Created!');    
		$alertGeneral = [];
		$alertGeneral['success'] = [
									'title' => "DO Successfully Created",
									'message' => ''
								];
        Session::flash('alertGeneralType','success');    
        Session::flash('alertGeneralTitle','DO Successfully Created');  
        if ($request->is('api/*')) {
        	return response()->json(['message' => 'success'], 200);
        }
        else
        {
        	return redirect()->route('list-pickup');
        }
        

    }
	
	public function  getNextWorkingDay($startDateStr, $leadTime, $excludeSaturday=true, $excludeSunday=true ){

			$startDate = strtotime($startDateStr);
			$nextWorkingDayStr = $startDateStr;

			while ($leadTime > 0){
					$nextWorkingDay = strtotime($nextWorkingDayStr.' +1 day');
					$nextWorkingDayStr = date('Y-m-d', $nextWorkingDay);

					//echo "<br>";
					//echo '\n'.$nextWorkingDayStr.' : ';
					if (in_array($nextWorkingDayStr, $this->holidayDates)){
							//echo "public holidays , skip";
							continue;
					}
					$nextWorkingDayInt = date('N', strtotime($nextWorkingDayStr));  // Monday 1 , Sunday 7
					if ($excludeSunday && $nextWorkingDayInt == 7){
							//echo "it's sunday , skip";
							continue;
					}
					if ($excludeSaturday && $nextWorkingDayInt == 6){
							//echo "it's saturday , skip";
							continue;
					}

					//echo ' count today ';
					$leadTime --;
			}


			return $nextWorkingDayStr;
	}


    public function uploadDataPreview() {
		
		$previewKey = session('previewKey', 'xxxxxxxxxxxx'); 

        //$olddata = tbl_delivery_order::where('is_preview', 'yes')
        $olddata = tbl_delivery_order::where('is_preview', $previewKey)
               ->get();
		
		$alertWarning = false;
		$alertDanger = false;
		
		$data = [];
		$dataDO = [];
		
		$data['total'] = sizeof($olddata);
		$data['valid'] = 0;
		$data['postalCode'] = 0;
		$data['duplicate'] = 0;
		
		//echo sizeof($data);
		foreach($olddata as $item){
			if(tbl_delivery_order::select('id')
				->where('hawb', $item->hawb)
				//->where('is_preview', '<>', 'yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
					->exists() ){
				$item->duplicatedHawb = true;
				$alertDanger =true;		

				//tbl_delivery_order::where('is_preview', 'yes')
				tbl_delivery_order::where('id', $item->id)
					->delete();
			}
			if(tbl_delivery_order::select('id')
				->where('delivery_number', $item->delivery_number)
				//->where('is_preview', '<>', 'yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
					->exists() ){
				$item->duplicatedDeliveryNumber = true;
				$alertDanger =true;		

				//tbl_delivery_order::where('is_preview', 'yes')
				tbl_delivery_order::where('id', $item->id)
					->delete();
			}
			if(PostalCode::select('id')->where('code', $item->zip)->doesntExist() ){
				$item->zipNotExist = true;
				$alertWarning = true;
				$data['postalCode'] += 1;
			}
			
			if(tbl_delivery_order::select('order_id')
				->where('order_id', $item->order_id)
				//->where('is_preview', '<>', 'yes')
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
					->exists() ){
						$item->is_partial_so = true;	
					}
			//if($item->duplicatedHawb || $item->duplicatedDeliveryNumber || $item->zipNotExist)
			if($item->duplicatedHawb || $item->duplicatedDeliveryNumber){
				array_unshift($dataDO,$item);
				$data['duplicate'] += 1;				
			}
			else {
				$dataDO[] = $item;
				$data['valid'] += 1;
			} 
		}
		
		$data['data'] = $dataDO;
		
		if($alertDanger || $alertWarning ){
			$alertGeneral = [];
			if($alertDanger){				
				$alertGeneral['danger'] = [
									'title' => "Duplicated HAWB / Delivery Number",
									'message' => ''
								];
			}
			if($alertWarning){
				$alertGeneral['warning'] = [
									'title' => "Zip Doesn't exist",
									'message' => ''
								];
			}
			
			return view('admin.preview', compact('data','alertGeneral'));
		}
		
		/*
		foreach($data as $item){
			echo json_encode($item, JSON_PRETTY_PRINT);
		}
		*/
        return view('admin.preview', compact('data'));
    }

    public function saveDO() {
		
		$previewKey = session('previewKey', 'xxxxxxxxxxxx'); 

        //$olddata = tbl_delivery_order::where('is_preview', 'yes')				
        //$olddata = tbl_delivery_order::where('is_preview', $previewKey)				
        //        ->get('order_id');

        $olddata = tbl_delivery_order::select('order_id','id')->where('is_preview', $previewKey)
                ->get();

		$date = now();
		$user = Auth::user()->name;

		foreach($olddata as $temp){
			if((tbl_delivery_order::where('order_id', $temp->order_id)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
					->count()) > 0)
				$deliv = tbl_delivery_order::where('order_id', $temp->order_id)
					->update(['is_partial_so' => true]);

			tbl_historyDO::create([

						'id_delivery'=> $temp->id,
						'date_vendor'=>$date,
						'status'=> 'onprocess',
						'message'=> 'Handover to Courier',
						'pic'=> $user
			
					]);

		}

        $deliv = tbl_delivery_order::where('is_preview', $previewKey)
            ->update(['is_preview' => '']);

		
		session(['previewKey' => 'xxxxxxxxxxxx']);

        Session::flash('sukses','DO Successfully Imported!');
        return redirect()->route('list-pickup');
    }

    public function cancelPreviewDO() {
        
		$previewKey = session('previewKey', 'xxxxxxxxxxxx'); 

        $deliv = tbl_delivery_order::where('is_preview', $previewKey)
            //->update(['is_preview' => 'no','is_deleted' => '1']);
			->delete();


		session(['previewKey' => 'xxxxxxxxxxxx']);
        // Session::flash('sukses','Invalid data!');
        return redirect()->route('list-pickup');
    }

    public function filterDO(Request $request) {
		
		//dd($request->input());
		if ($request->is('api/*')) {
			$filter = [];
		
			if ($request->filled('filter_start_date') && (strcasecmp($request->input('filter_start_date'),'Invalid date')) != 0){
				$filter['set'] = true;
				$filter['filter_start_date'] = $request->input('filter_start_date');
			}
			if ($request->filled('filter_end_date')){
				$filter['set'] = true;
				$filter['filter_end_date'] = $request->input('filter_end_date');
				
			}
			if ($request->filled('filter_invoice')){
				$filter['set'] = true;
				$filter['filter_invoice'] = $request->input('filter_invoice');
				
			}
			if ($request->filled('filter_hawb')){
				$filter['set'] = true;
				$filter['filter_hawb'] = $request->input('filter_hawb');
				
			}
			if ($request->filled('filter_customer')){
				$filter['set'] = true;
				$filter['filter_customer'] = $request->input('filter_customer');
				
			}
			if ($request->filled('filter_courier_id')){
				$filter['set'] = true;
				$filter['filter_courier_id'] = $request->input('filter_courier_id');
			}
			if ($request->filled('filter_status_id')){
				$filter['set'] = true;
				$filter['filter_status_id'] = $request->input('filter_status_id');
				
			}

			$date = date("Y/m/d");
			
			$overdate = [];
			$solvedDO = [];
			$deliveryvendor = [];
			$warehouses = DB::table('tbl_gudang')->pluck('lokasi_gudang','id_gudang');
			
			$today = Carbon::today();
	            
	            $query = tbl_delivery_order::select(['id'])
						->where('status','onprocess')
						->where('eta_nuskin','<', $today)
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
						->count();
			$alertGeneral = [];

			$statuses = NuskinStatus::select('id','display_name')->get();
			$couriers = Courier::select('id','display_name')->get();

			return response()->json([
				'filter' => $filter,
				'statuses' => $statuses,
				'couriers' => $couriers,
				'warehouses' => $warehouses,
				'deliveryvendor' => $deliveryvendor, 
				'overdate' => $overdate, 
				'solvedDO' => $solvedDO,
				'alertGeneral' => $alertGeneral]);
		}
		else{
			session(['filter_set' => null]);
			
			session(['filter_start_date' => '']);
			session(['filter_end_date' => '']);
			session(['filter_invoice' => '']);
			session(['filter_hawb' => '']);
			session(['filter_customer' => '']);
			session(['filter_courier_id' => '']);
			session(['filter_status_id' => '']);
			
			$filter = [];
			
			if ($request->filled('filter_start_date') && (strcasecmp($request->input('filter_start_date'),'Invalid date')) != 0){
				$filter['set'] = true;
				$filter['filter_start_date'] = $request->input('filter_start_date');
				session(['filter_start_date' => $request->input('filter_start_date')]);
			}
			if ($request->filled('filter_end_date')){
				$filter['set'] = true;
				$filter['filter_end_date'] = $request->input('filter_end_date');
				
				session(['filter_end_date' => $request->input('filter_end_date')]);
			}
			if ($request->filled('filter_invoice')){
				$filter['set'] = true;
				$filter['filter_invoice'] = $request->input('filter_invoice');
				
				session(['filter_invoice' => $request->input('filter_invoice')]);
			}
			if ($request->filled('filter_hawb')){
				$filter['set'] = true;
				$filter['filter_hawb'] = $request->input('filter_hawb');
				
				session(['filter_hawb' => $request->input('filter_hawb')]);
			}
			if ($request->filled('filter_customer')){
				$filter['set'] = true;
				$filter['filter_customer'] = $request->input('filter_customer');
				
				session(['filter_customer' => $request->input('filter_customer')]);
			}
			if ($request->filled('filter_courier_id')){
				$filter['set'] = true;
				$filter['filter_courier_id'] = $request->input('filter_courier_id');
				
				session(['filter_courier_id' => $request->input('filter_courier_id')]);
			}
			if ($request->filled('filter_status_id')){
				$filter['set'] = true;
				$filter['filter_status_id'] = $request->input('filter_status_id');
				
				session(['filter_status_id' => $request->input('filter_status_id')]);
			}
					
			session(['filter_set' => '1']);	

			$date = date("Y/m/d");
			
			$overdate = [];
			$solvedDO = [];
			$deliveryvendor = [];
			$warehouses = DB::table('tbl_gudang')->pluck('lokasi_gudang','id_gudang');
			
			$today = Carbon::today();
	            
	            $query = tbl_delivery_order::select(['id'])
						->where('status','onprocess')
						->where('eta_nuskin','<', $today)
						//->where('is_preview', '<>', 'yes')	
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
						->count();
			//dd($query);
			$alertGeneral = [];
			
			/*
			if(!empty($query)){
				$alertGeneral['danger'] = [
											'title' => 'you have '.$query.' overdue Delivery Order',
											'message' => ''
										];						
				$alertGeneral['timeout'] = 0;			
			}		
			*/
			//session(['filter_set' => '0']);

			$statuses = NuskinStatus::select('id','display_name')->get();
			$couriers = Courier::select('id','display_name')->get();

			// // dd([Carbon::parse($filter['filter_start_date'].' 00:00:00'),Carbon::parse($filter['filter_end_date'].' 23:59:59')]);
			// dd($filter['filter_start_date'] == $filter['filter_end_date']);

			// dump(Carbon::parse(session('filter_start_date'))->format('Y-m-d'));
			// dd(Carbon::parse(session('filter_end_date')));

	        return view('admin.list_dv_vendor',compact('filter','statuses','couriers','warehouses','deliveryvendor', 'overdate', 'solvedDO','alertGeneral'));
    	}

    }

    public function editDO(Request $request, $id) {

        $dataDO = tbl_delivery_order::where('id', $id)->first();
        $listgudang  = tbl_gudang::pluck('lokasi_gudang','id_gudang');
        $listcourier = Courier::pluck('display_name','id');		
		$cities = City::select('id','name')->get();
		if ($request->is('api/*')) {	
			return response()->json([
				'dataDO' => $dataDO,
				'listgudang'=> $listgudang,
				'listcourier'=> $listcourier,
				'cities'=> $cities
			], 200);
		}
		else{
			return view('admin.edit_do', compact('dataDO','listgudang','listcourier','cities'));
		}
        
    }

    public function saveEditDO(Request $request, $id) {
    	if ($request->is('api/*')) {	
        	$user = 'api';
    	}
    	else{
    		$user = Auth::user()->name;
    	}
        $deliv_order = tbl_delivery_order::findOrFail($id);

        if ($request == '') {
            
            if ($request->is('api/*')) {
            	return response()->json(['message' => 'Invalid Data!'],200);	
            }
            else{
            	Session::flash('sukses','Invalid Data!');
            	return redirect()->route('list-pickup');	
            }
            
        }else {
            $deliv_order->order_id          = $request->invoiceid;
            $deliv_order->create_date       = $request->createdate;
            //$deliv_order->transporter       = $request->transporter;
            $deliv_order->order_type        = $request->ordertype;
            $deliv_order->customer_id       = $request->customid;
            $deliv_order->customer_name     = $request->customname;
            $deliv_order->phone             = $request->phone;
            $deliv_order->city              = $request->city;
            $deliv_order->zip               = $request->zip;
            $deliv_order->grosswiight       = $request->grosswight;
            $deliv_order->net_weight        = $request->netweight;
            $deliv_order->volume            = $request->volume;
            $deliv_order->hawb              = $request->hawb;
            $deliv_order->delivery_number   = $request->delivery_number;
            $deliv_order->customer_refer    = $request->delivery_number;
            $deliv_order->adress            = $request->adress;
            $deliv_order->pic               = $user;
        
		
			$courierName = Courier::pluck('display_name','id');		
			$deliv_order->courier_id            = $request->transporter;
			$deliv_order->transporter           = $courierName[$request->transporter];
			
			$listgudang = DB::table('tbl_gudang')->pluck('lokasi_gudang','id_gudang');
			$deliv_order->warehouse_id                 = $request->gudangdo;
			$deliv_order->warehouse_name                 = $listgudang[$request->gudangdo];
		}
		//dd($request->input());

        $deliv_order->save();
		tbl_historydo::create([

						'id_delivery'=> $deliv_order->id,
						'date_vendor'=> now(),
						'status'=> $deliv_order->status,
						'message'=> 'Delivery Order Edited',
						'pic'=> $user
			
					]);
		if ($request->is('api/*')) {
			return response()->json(['message' => 'Data has been changed!']);
		}
		else{
        	Session::flash('sukses','Data has been changed!');
        	return redirect()->route('list-pickup');
		}
    }

    public function notifDO() {

        $date = date("Y/m/d");
        $overdate = tbl_delivery_order::where('created_at', '<=', $date)
        ->orderBy('id', 'DESC')
        ->get();

        $count = count($overdate);
        
    }

    public function doubleDO($order_id) {

    $doubleDO = tbl_delivery_order::where('order_id', $order_id)
        ->orderBy('id', 'DESC')
        ->get();

        return response()->json($doubleDO);

    }

    public function manualstatus(Request $request, $id) {

        $user = Auth::user()->name;
        $data = tbl_delivery_order::where('id', '=', $id)->first();

        $date = date("Y/m/d");
        $history = new tbl_historyDO;
        $history->id_delivery = $data->id;
        $history->date_vendor = $date;
        $history->status      = $request->changestatus;
        $history->message     = $request->description;
        $history->pic         = $user;
        
        $history->save();

        $delivery = tbl_delivery_order::find($data->id);
        $delivery->status = $request->changestatus;

        $delivery->save();
        
        return redirect()->back();

    }

    public function getTemplate(){
    	$file = public_path()."/file/delivery-order/template.xls";
    	return response()->download($file, 'AWB Template.xls');
    }

}
