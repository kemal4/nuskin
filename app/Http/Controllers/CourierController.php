<?php

namespace App\Http\Controllers;

use App\Model\Courier;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use Illuminate\Support\Facades\Log;

class CourierController extends Controller
{
	protected $model_name = 'Courier';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $query = Courier::all();
		Log::error('----CourierController------ - '.json_encode($request->all()));
		if ($request->ajax()) {
			Log::error('----ajax courier------ - ');
            //$data = Courier::latest()->get();
			$data = Courier::select('*');
			
			if($request->input('order.0.column') == 0){
                $data = Courier::orderBy('created_at','desc');			
			}
			
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-warning btn-sm editProduct"><i class="fas fa-edit"></i></a>';
                        //Delete is commented by user request   
						// if(Auth::user()->can('delete_setting'))   
      //                      $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete"   data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
                    ->addColumn('active', function($row){
                        if(Auth::user()->can('edit_setting')){
                            if ($row->active == true) {
                                return '<a href='.route('courier.change', $row->id).'><button class="btn btn-block btn-danger">Unactivate</button></a>';
                            }
                            else {

                                return '<a href='.route('courier.change', $row->id).'><button class="btn btn-block btn-success">Activate</button></a>';
                            }
                        }
                    })
					->editColumn('is_sms_notification', function ($row) {
						return ($row->is_sms_notification)?'<span class="badge badge-info">On</span>':'Off';
					})	
					->editColumn('is_delivery_check', function ($row) {
						return ($row->is_delivery_check)?'<span class="badge badge-info">On</span>':'Off';
					})	
                    ->rawColumns(['action','is_sms_notification','is_delivery_check','active'])
                    ->make(true);
        }
      
        return view('settings/courier');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		Log::error('----CourierController------ - '.json_encode($request->all()));
		
        Courier::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					'code' => $request->code,
					'display_name' => $request->display_name,
					'pic' => $request->pic,
					'pic_email' => $request->pic_email,
					'is_sms_notification' => $request->is_sms_notification,
					'is_delivery_check' => $request->is_delivery_check,
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        //Session::flash('alertGeneralType','success');    
        //Session::flash('alertGeneralTitle',$alertMessage);  
   
        return response()->json(['success'=>$alertMessage]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function show(Courier $courier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Courier::find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Courier $courier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Courier::find($id)->delete();
     
        // return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }

    public function changeActive(Request $request, $id){
        $query = Courier::findOrFail($id);
        if ($query->active == true){
            $query->active = false;
            $query->pic = Auth::user()->id;
            $query->save();
        }
        else{
            $query->active = true;
            $query->pic = Auth::user()->id;
            $query->save();
        }
        return redirect()->route('couriers.index');
    }

}
