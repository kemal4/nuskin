<?php

namespace App\Http\Controllers;

use App\bicycle;
use Illuminate\Http\Request;

class BicycleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bicycle  $bicycle
     * @return \Illuminate\Http\Response
     */
    public function show(bicycle $bicycle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bicycle  $bicycle
     * @return \Illuminate\Http\Response
     */
    public function edit(bicycle $bicycle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bicycle  $bicycle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bicycle $bicycle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bicycle  $bicycle
     * @return \Illuminate\Http\Response
     */
    public function destroy(bicycle $bicycle)
    {
        //
    }
}
