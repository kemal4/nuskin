<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Model\tbl_gudang;
use App\Model\tbl_delivery_order;
use App\Role;
use App\Permission;
use DataTables;
use Carbon\Carbon;

class WarehouseController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:view_setting|create_setting|edit_setting|delete_setting', ['only' => ['index','addGudang']]);
        $this->middleware('permission:create_setting', ['only' => ['gudang','addGudang']]);
        $this->middleware('permission:edit_setting', ['only' => ['editgudang','storegudang']]);
        $this->middleware('permission:delete_setting', ['only' => ['destroy']]);
    }    

    public function index() {
        //commented by Ryan 02092020
     //    $gudang = tbl_gudang::all();
     //    // return view('spadmin.kelolarole',['gudang'=>$gudang]);
    	// return view('warehouse.index',['gudang'=>$gudang]);

        // return view gudang, added by Ryan 02092020
        $gudang = tbl_gudang::all();
        return view('warehouse.index',['gudang'=>$gudang]);
    }

    //return table data for warehouse, added by Ryan 02092020
    public function getAllListJson(Request $request)
    {
        $query = tbl_gudang::whereNull("deleted_at")->get();

        return Datatables::of($query)
            ->addIndexColumn()
            ->addColumn("action", function($query)
            {
                return "<a href=".route('editgudangs', ['id_gudang' => $query->id_gudang ])." class='btn btn-warning btn-sm' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>
                    <a href='#' data-id=".$query->id_gudang." class='btn btn-danger btn-sm deleteProduct' data-toggle='tooltip' title='Delete'><i class='fas fa-trash'></i></a>  ";
            })
            ->make(true);
    }

    public function gudang() {
        return view('warehouse.create');

    }
    public function addGudang(Request $request) {
        $gudang = new tbl_gudang;
        $gudang->lokasi_gudang = $request->gudang;
        $gudang->code_gudang = $request->codegudang;
        $gudang->nama_gudang = '';
        $gudang->koordinat = '';
        
        
        $gudang->save();
        Session::flash('Success','Data Changed!');   
        // return view('spadmin.kelolarole');
        return redirect()->route('kelolarole');
    }
    public function editgudang($id) {
     
        $editgudang = tbl_gudang::where('id_gudang', $id)->first();
        
    	return view('warehouse.edit', compact('editgudang'));
    }
    public function storegudang(Request $request, $id) {

    	$gudangstore = tbl_gudang::find($id);

        $gudangstore->code_gudang   = $request->codegudang;
	    $gudangstore->lokasi_gudang = $request->gudang;
	    $gudangstore->save();

    	Session::flash('Success','Data Changed!');    
        return redirect()->route('kelolarole');
    }
    public function destroy($id) {
        
        
  //       $gudang = tbl_gudang::find($id);
  //       // dd($gudang);
  //   	$gudang->delete();
        
		// Session::flash('Success','Data Changed!');    
 	//     return redirect()->back();


        //Softdelete
        $deliveryOrder = tbl_delivery_order::where('warehouse_id', $id)->exists();
        if(!($deliveryOrder))
        {
            $tbl_gudang = tbl_gudang::find($id);
            $tbl_gudang->deleted_at = Carbon::now();
            $tbl_gudang->save();
            return response()->json([
            'success' => true,
            'message'=>'Warehouse deleted successfully..'
        ]);
        }

        return response()->json([
            'success' => false,
            'message'=>'This warehouse is used in delivery Order.'
        ]);
    }

    public function getWarehouse()
    {
        $query = tbl_gudang::select('id_gudang','lokasi_gudang')->get();

        return response()->json($query);
    }
}
