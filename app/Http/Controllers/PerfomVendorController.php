<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\tbl_delivery_order;
use App\Model\tbl_perfom_vendor;
use App\Model\tbl_ticket;
use App\Exports\PerfomVendorExcel;
 
use Session;
use DB;
use Auth;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class PerfomVendorController extends Controller
{
    public function index() {

        $truncate    = tbl_perfom_vendor::query()->truncate();

        $performance = tbl_delivery_order::where('status', '!=', '')
                            ->whereIn('status', array('solved','fail'))
                            ->select([
                                    'order_id',
                                    'city',
                                    'zip',
                                    'transporter',
                                    'status',
                                    'created_at',
                                    'deliver_date',
                                    'fulfillday'
                                    ]);

        DB::table('tbl_perfom_vendor')->insertUsing([ 
                                    'hawb',
                                    'city',
                                    'zip',
                                    'transporter',
                                    'status',
                                    'start_date',
                                    'deliver_date',
                                    'fullfilday'], $performance);


        $perfomAll          = DB::table('tbl_perfom_vendor')
                                    ->select(array('transporter',
                                    DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                                             SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                                    ))
                                    // ->where('')
                                    ->groupBy('transporter')
                                    ->get();

        //Total Complain
        $complainticketJNE  = tbl_ticket::where('transporter', '=', 'JNE')
                                    ->get();
                                    
        $tcJNE              = count($complainticketJNE);

        $complainticketFL   = tbl_ticket::where('transporter', '=', 'FL')
                                    ->get();
                                    
        $tcFL               = count($complainticketFL);

        $complainMrSpeedy   = tbl_ticket::where('transporter', '=', 'MrSpeedy')
                                    ->get();
                                    
        $tcSpeedy               = count($complainMrSpeedy);

        $data = [
            'perfom'            => $perfomAll,
            'tcktJNE'           => $tcJNE,
            'tcktFL'            => $tcFL,
            'tcktSpeedy'        => $tcSpeedy
        ];

        return view('admin.performa_vendor')->with($data);
    }

    public function exportVD() {

        // $data = tbl_perfom_vendor::where('status','=','fail');
        return Excel::download(new PerfomVendorExcel(), 'performance_vendor.xlsx');
    }

    public function detailVD(Request $request, $id) {

        if ($id == 'JNE') {

        $detailJNE = DB::table('tbl_perfom_vendor as pv')
            ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city',
                            DB::raw('COUNT(pv.status) as total'),   
                            DB::raw('SUM(CASE  when pv.status = "solved"   then 1 else 0 end) as "fullfill",
                                     SUM(CASE  when pv.status = "fail"  then 1 else 0 end) as "unfull" ')
            ))
            ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city,leadtime_jne from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                $join->on("pv.zip", "=" ,"kd.kodepos");
            })
            ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
            ->where('pv.transporter', '=', 'JNE')
            ->groupBy('kd.city','kd.kodepos')
            ->get();

            //failed
            // $detailJNE = DB::table('tbl_perfom_vendor as pv')
            //     ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city',
            //                     DB::raw('COUNT(pv.status) as total'),   
            //                     DB::raw('SUM(CASE when pv.fullfilday <= kd.leadtime_jne and pv.status = "solved"   then 1 else 0 end) as "fullfill",
            //                              SUM(CASE  when pv.fullfilday >= kd.leadtime_jne and pv.status = "fail"  then 1 else 0 end) as "unfull" ')
            //     ))
            //     ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city,leadtime_jne from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
            //         $join->on("pv.zip", "=" ,"kd.kodepos");
            //     })
            //     ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
            //     ->where('pv.transporter', '=', 'JNE')
            //     ->groupBy('kd.city','kd.kodepos')
            //     ->get();

        $perfomAll  = DB::table('tbl_perfom_vendor')
                        ->select(array('transporter',
                        DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                                 SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                        ))
                        ->where('transporter', '=', 'JNE')
                        ->first();


        $tempdata = [];

        $tempdata['label'][] = "Fulfilled";
        $tempdata['data'][] = $perfomAll->delivsuccess;

        $tempdata['label'][] = "Unfulfilled";
        $tempdata['data'][] = $perfomAll->exceeding;

        $data_chart = json_encode($tempdata);

        }elseif ($id == 'FL') {

        $detailFL  = DB::table('tbl_perfom_vendor as pv')
            ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city','kd.leadtime_fl',
                            DB::raw('COUNT(pv.status) as total'),
                            DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                     SUM(CASE  when pv.status = "fail"  then 1 else 0 end) as "unfull" ')
            ))
            ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city,leadtime_fl from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                $join->on("pv.zip", "=" ,"kd.kodepos");
            })
            ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
            ->where('pv.transporter', '=', 'FL')
            ->groupBy('kd.city','kd.kodepos')
            ->get();


        $perfomAll  = DB::table('tbl_perfom_vendor')
                        ->select(array('transporter',
                        DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                                 SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                        ))
                        ->where('transporter', '=', 'FL')
                        ->first();


        $tempdata = [];

        $tempdata['label'][] = "Fulfilled";
        $tempdata['data'][] = $perfomAll->delivsuccess;

        $tempdata['label'][] = "Unfulfilled";
        $tempdata['data'][] = $perfomAll->exceeding;

        $data_chart = json_encode($tempdata);

        }else{

        $detailSpeedy  = DB::table('tbl_perfom_vendor as pv')
            ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city', 
                            DB::raw('COUNT(pv.status) as total'),
                            DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                     SUM(CASE  when pv.status = "fail" then 1 else 0 end) as "unfull" ')
            ))
            ->where('pv.transporter', '=', 'MrSpeedy')
            ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                $join->on("pv.zip", "=" ,"kd.kodepos");
            })
            ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
            ->groupBy('kd.city','kd.kodepos')
            ->get(); 

        $perfomAll  = DB::table('tbl_perfom_vendor')
                        ->select(array('transporter',
                        DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                                 SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                        ))
                        ->where('transporter', '=', 'MrSpeedy')
                        ->first();

        $tempdata = [];

        $tempdata['label'][] = "Fulfilled";
        $tempdata['data'][] = $perfomAll->delivsuccess;

        $tempdata['label'][] = "Unfulfilled";
        $tempdata['data'][] = $perfomAll->exceeding;

        $data_chart = json_encode($tempdata);

        }

        if ($id == 'JNE') {
            $data = [
                'zonacity'      => $detailJNE,
                'id'            => $id,
                'chart_data'    => $data_chart
            ];
        }elseif ($id == 'FL') {
            $data = [
                'zonafl'        => $detailFL,
                'id'            => $id,
                'chart_data'    => $data_chart
            ];
        }else{
            $data = [
                'zonaspeedy'    => $detailSpeedy,
                'id'            => $id,
                'chart_data'    => $data_chart
            ];
        }
        if ($request->is('api/*')) {    
            return response()->json([
                'data' => $data
            ], 200);
        }
        else{
            return view('admin.list_detail_perfom')->with($data); 
        }

        
    }

    public function filterVD(Request $request) {

        $datemonth  = $request->filtermonth;
        $dateyears  = $request->filteryears;
        $trans      = $request->transporter;
        $month      = date('m',strtotime($datemonth));
        $years      = date('Y',strtotime($dateyears));

        if ($trans == 'JNE') {
            if ($request->filtermonth == null) {
                $zonaJNE     = DB::table('tbl_perfom_vendor as pv')
                    ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city',
                                    DB::raw('COUNT(pv.status) as total'),                            
                                    DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                             SUM(CASE  when pv.status = "fail" then 1 else 0 end) as "unfull" ')
                    ))
                    ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                        $join->on("pv.zip", "=" ,"kd.kodepos");
                    })
                    ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
                    ->whereYear('pv.deliver_date', '=', $years)
                    ->where('pv.transporter', '=', $trans)
                    ->groupBy('pv.zip','kd.kodepos')
                    ->get();
            }else{
                $zonaJNE     = DB::table('tbl_perfom_vendor as pv')
                    ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city',
                                    DB::raw('COUNT(pv.status) as total'),                            
                                    DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                             SUM(CASE  when pv.status = "fail" then 1 else 0 end) as "unfull" ')
                    ))
                    ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                        $join->on("pv.zip", "=" ,"kd.kodepos");
                    })
                    ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
                    ->whereMonth('pv.deliver_date', '=', $month)
                    ->where('pv.transporter', '=', $trans)
                    ->groupBy('pv.zip','kd.kodepos')
                    ->get();
                
            }

       $perfomAll  = DB::table('tbl_perfom_vendor')
                ->select(array('transporter',
                DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                         SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                ))
                ->where('transporter', '=', 'JNE')
                ->first();

        $tempdata = [];

        $tempdata['label'][] = "Fulfilled";
        $tempdata['data'][] = $perfomAll->delivsuccess;

        $tempdata['label'][] = "Unfulfilled";
        $tempdata['data'][] = $perfomAll->exceeding;

        $data_chart = json_encode($tempdata);

        }elseif ($trans == 'FL') {
        if ($month) {
            $zonaFL  = DB::table('tbl_perfom_vendor as pv')
                ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city', 
                                DB::raw('COUNT(pv.status) as total'),
                                DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                         SUM(CASE  when pv.status = "fail" then 1 else 0 end) as "unfull" ')
                ))
                ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                    $join->on("pv.zip", "=" ,"kd.kodepos");
                })
                ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
                ->whereMonth('pv.deliver_date', '=', $month)
                ->where('pv.transporter', '=', $trans)
                ->groupBy('pv.zip','kd.kodepos')
                ->get();
        }else{
            $zonaFL  = DB::table('tbl_perfom_vendor as pv')
                ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city', 
                                DB::raw('COUNT(pv.status) as total'),
                                DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                         SUM(CASE  when pv.status = "fail" then 1 else 0 end) as "unfull" ')
                ))
                ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                    $join->on("pv.zip", "=" ,"kd.kodepos");
                })
                ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
                ->whereYear('pv.deliver_date', '=', $years)
                ->where('pv.transporter', '=', $trans)
                ->groupBy('pv.zip','kd.kodepos')
                ->get();
        }

        $perfomAll  = DB::table('tbl_perfom_vendor')
                        ->select(array('transporter',
                        DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                                 SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                        ))
                        ->where('transporter', '=', 'FL')
                        ->first();


        $tempdata = [];

        $tempdata['label'][] = "Fulfilled";
        $tempdata['data'][] = $perfomAll->delivsuccess;

        $tempdata['label'][] = "Unfulfilled";
        $tempdata['data'][] = $perfomAll->exceeding;

        $data_chart = json_encode($tempdata);

        }else {
        $detailSpeedy  = DB::table('tbl_perfom_vendor as pv')
            ->select(array('zn.label_zonasi','pv.transporter','pv.zip','kd.city', 
                            DB::raw('COUNT(pv.status) as total'),
                            DB::raw('SUM(CASE when pv.status = "solved" then 1 else 0 end) as "fullfill",
                                     SUM(CASE  when pv.status = "fail" then 1 else 0 end) as "unfull" ')
            ))
            ->join(DB::raw("(SELECT distinct(kodepos), id_zonasi,city from tbl_kodepos GROUP BY kodepos,id_zonasi,city) as kd"),function($join){
                $join->on("pv.zip", "=" ,"kd.kodepos");
            })
            ->leftJoin('tbl_zonasi as zn', 'kd.id_zonasi', '=', 'zn.code_zonasi')
            ->whereMonth('pv.deliver_date', '=', $month)
            ->where('pv.transporter', '=', $trans)
            ->groupBy('pv.zip','kd.kodepos')
            ->get();

        $perfomAll  = DB::table('tbl_perfom_vendor')
                        ->select(array('transporter',
                        DB::raw('SUM(CASE status when "solved" then 1 else 0 end) as "delivsuccess",
                                 SUM(CASE status when "fail" then 1 else 0 end) as "exceeding" ')
                        ))
                        ->where('transporter', '=', 'MrSpeedy')
                        ->first();


        $tempdata = [];

        $tempdata['label'][] = "Fulfilled";
        $tempdata['data'][] = $perfomAll->delivsuccess;

        $tempdata['label'][] = "Unfulfilled";
        $tempdata['data'][] = $perfomAll->exceeding;

        $data_chart = json_encode($tempdata);
        }

        if ($trans == 'JNE') {
            $data = [
                'zonacity'      => $zonaJNE,
                'id'            => $trans,
                'chart_data'    => $data_chart
            ];
        }elseif ($trans == 'FL') {
            $data = [
                'zonafl'        => $zonaFL,
                'id'            => $trans,
                'chart_data'    => $data_chart
            ];
        }else{
            $data = [
                'zonaspeedy'    => $detailSpeedy,
                'id'            => $trans,
                'chart_data'    => $data_chart
            ];
        }
        if ($request->is('api/*')) {    
            return response()->json([
                'data' => $data
            ], 200);
        }
        else{
            return view('admin.list_detail_perfom')->with($data); 
        }
        

    }
    
}
