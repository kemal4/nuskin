<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Session;
use DB;
use Auth;
use App\Http\Controllers\Controller;
use App\Model\tbl_ticket;
use App\Model\tbl_ticket_log;
use App\Model\tbl_delivery_order;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Carbon\Carbon;
use DataTables;
use App\Model\tbl_topics;

class TicketController extends Controller
{
    public function release(Request $request, $id = 0) {

		if($id > 0)
			tbl_ticket::where('id',$id)->update(['is_locked' => false]);

        if ($request->is('api/*')) {  
            return response()->json([
                'message' => 'sukses' 
            ], 200);
        }
        else{
            return redirect()->route('listticket'); 
        }
		
	}
   public function index(Request $request, $id = 0) {

		if($id > 0)
			tbl_ticket::where('id',$id)->update(['is_locked' => false]);

        $ticket = tbl_ticket::orderBy('created_at', 'DESC')->where('non_deliv', '')->get();;

		$today = Carbon::today();
		
		$totalOverDue = 0;
        foreach($ticket as $tt)
			if ($tt->status != 'Solved' && !empty($tt->due) && Carbon::Parse($tt->due) < $today){
				
				$tt->isOverDue = true;
				$totalOverDue ++;
			}
			
		//die();

        $statusbelum    = 'Unprocessed';
        $statusdiproses = 'On Process';
        $statussolved   = 'Solved';
        
        $ticketbelum    = tbl_ticket::where('status', '=', $statusbelum)
        ->where('non_deliv', '')
        ->orderBy('created_at', 'ASC')
        ->get();
		
		
		$ticketdiproses = tbl_ticket::where('status', '=', $statusdiproses)
        ->where('non_deliv', '')
        ->orderBy('created_at', 'ASC')
        ->get();
        $ticketsolved = tbl_ticket::where('status', '=', $statussolved)
        ->where('non_deliv', '')
        ->orderBy('created_at', 'ASC')
        ->get();

        // $ticketnondev = tbl_ticket::where('non_deliv', '=', 1)
        // ->orderBy('created_at', 'ASC')
        // ->get();

		$alertGeneral = [];
		/*
		if($totalOverDue > 0){
			$alertGeneral['danger'] = [
										'title' => 'you have '.$totalOverDue.' overdue complaints',
										'message' => ''
									];						
			$alertGeneral['timeout'] = 0;
			
		}		
		*/
		//dd($alertGeneral);
		if ($request->is('api/*')) {  
            return response()->json([
                'alertGeneral' => $alertGeneral,
                'ticket'=> $ticket,
                'ticketdiproses'=> $ticketdiproses,
                'ticketsolved'=> $ticketsolved, 
                'ticketbelum'=> $ticketbelum, 
                // 'ticketnondev'=> $ticketnondev,
            ], 200);
        }
        else{
            return view('admin.list_ticket',compact('alertGeneral','ticket','ticketdiproses','ticketsolved','ticketbelum',
                // 'ticketnondev'
        ));  
        }
        
    }
    public function nonDVIndex(Request $request, $id = 0) 
    {
        if($id > 0)
            tbl_ticket::where('id',$id)->update(['is_locked' => false]);

        $ticket = tbl_ticket::orderBy('created_at', 'DESC')->where('non_deliv', '1')->get();;

        $today = Carbon::today();
        
        $totalOverDue = 0;
        foreach($ticket as $tt)
            if ($tt->status != 'Solved' && !empty($tt->due) && Carbon::Parse($tt->due) < $today){
                
                $tt->isOverDue = true;
                $totalOverDue ++;
            }

        $statusbelum    = 'Unprocessed';
        $statusdiproses = 'On Process';
        $statussolved   = 'Solved';
        
        $ticketbelum    = tbl_ticket::where('status', '=', $statusbelum)
        ->where('non_deliv', '1')
        ->orderBy('created_at', 'ASC')
        ->get();
        
        
        $ticketdiproses = tbl_ticket::where('status', '=', $statusdiproses)
        ->where('non_deliv', '1')
        ->orderBy('created_at', 'ASC')
        ->get();
        $ticketsolved = tbl_ticket::where('status', '=', $statussolved)
        ->where('non_deliv', '1')
        ->orderBy('created_at', 'ASC')
        ->get();

        // $ticketnondev = tbl_ticket::where('non_deliv', '=', 1)
        // ->orderBy('created_at', 'ASC')
        // ->get();

        $alertGeneral = [];
        if ($request->is('api/*')) {  
            return response()->json([
                'alertGeneral' => $alertGeneral,
                'ticket'=> $ticket,
                'ticketdiproses'=> $ticketdiproses,
                'ticketsolved'=> $ticketsolved, 
                'ticketbelum'=> $ticketbelum, 
                // 'ticketnondev'=> $ticketnondev,
            ], 200);
        }
        else{
            return view('admin.list_ticket',compact('alertGeneral','ticket','ticketdiproses','ticketsolved','ticketbelum',
                // 'ticketnondev'
        ));  
        }
    }
    public function getsoDetail(Request $request , $id) {
		$tbl_topics = tbl_topics::all()->count();

		$data = [];
		$tbl_ticket = tbl_ticket::where('order_id',$id);

        $tbl_ticket_topics = $tbl_ticket->distinct('topics')->count('topics');

        $data['ticket_exist'] = $tbl_ticket->exists();
		// if(!$data['ticket_exist']){
        if(($tbl_ticket_topics < $tbl_topics) && ($tbl_ticket_topics >= 0))
        {
            $tbl_ticket_topics = $tbl_ticket->distinct('topics')->get();
            $data['topics'] = [];

            foreach ($tbl_ticket_topics as $key) {
                array_push($data['topics'], $key->topics);
            }

			$temp = tbl_delivery_order::select('hawb','customer_name','phone')
						->where('order_id',$id)
						->orderBy('id', 'desc')
						->first();
			if($temp){
				$data['last_hawb'] = $temp->hawb;
				$data['customer_name'] = $temp->customer_name;
				$data['phone'] = $temp->phone;	

				$data['allHawb'] = tbl_delivery_order::select('hawb','status')
						->where('order_id',$id)
						->orderBy('id', 'desc')
						->get();
			}
			else 
				$data['doesnt_exist'] = true;
			
		}
        // else
        // {
        //     if($tbl_ticket->distinct('topics')->count('topics') <= $tbl_topics->count())
        //     {
        //         $tbl_ticket_topics = $tbl_ticket->distinct('topics')->get();
        //         $data['topics'] = [];

        //         foreach ($tbl_ticket_topics as $key) {
        //             array_push($data['topics'], $key->topics);
        //         }

        //     }
        // }
		
        //$soDetail = DB::table('tbl_delivery_order')->where('order_id',$id)->first();
        //return response()->json($soDetail);

        // print_r($soDetail);exit;
        if ($request->is('api/*')) {
            return response()->json(['data' => $data], 200);
        }
        else{
            return response()->json($data);
        }
        
    }
    public function storeticket(Request $request) {
        if ($request->is('api/*')) {
            $user = 'api';
        }
        else{
            $user = Auth::user()->name;
        }

        if($request->order_id == null){
            $transpoter = $request->member_id;
            $non_deliv  = 1;
            $orderr = '';
        }else if($request->member_id == null) {
            $transpoter = $request->hawb;
            $orderr = $request->order_id;
        }

        $vendor     = tbl_delivery_order::where('hawb','=', $transpoter)->first();


        if($vendor == null ){
            $ekspedisi = '';
            $zip = '';
        }else {
            $ekspedisi  = $vendor->transporter;
            $zip        = $vendor->zip;
        }

        if ($request->order_id) {
            $data = request()->validate([
                'complaintType' => 'required',
                'order_id'      => 'required|max:10|',
                'topic'         => 'required',
                'nama_pelapor'  => 'required',
                'email'         => 'required',
                'telepon'       => 'required',
                'customer'      => 'required',
                'message'       => 'required',
                'attachments'   => 'mimes:bmp,gif,png,jpg,jpeg|max:2000',
                'attachments2'  => 'mimes:bmp,gif,png,jpg,jpeg|max:2000'
            ],
            [
            'order_id.required'      => 'The SO Number field is required!',
            'email.required'         => 'The Email field is required!',
            'telepon.required'       => 'The Phone field is required!',
            'nama_pelapor.required'  => 'The Complainer field is required!',
            'message.required'       => 'The message field is required!',
            ]);
        }else{
            $data = request()->validate([
            //     'topic'         => 'required',
                'complaintType' => 'required',
                'nama_pelapor'  => 'required',
                'email'         => 'required',
                'telepon'       => 'required',
                'customerId'      => 'required',
                'customer'      => 'required',
                'message'       => 'required',
                'attachments'   => 'mimes:png,jpg,jpeg,pdf|max:2000',
                'attachments2'  => 'mimes:png,jpg,jpeg,pdf|max:2000'
            ],
            [
            'email.required'         => 'The Email field is required!',
            'telepon.required'       => 'The Phone field is required!',
            'nama_pelapor.required'  => 'The Complainer field is required!',
            ]);
        }

       
        $orderready = tbl_ticket::where('order_id', '=', $request->order_id )->first();
        $yymm       = date("Ymd");
        $ticketdate = substr($yymm, 2,4);
        //$uniqueId   = $ticketdate.''.mt_rand(10,100000);
        $uniqueId   = IdGenerator::generate([
				'table' => 'tbl_ticket', 
				'field' => 'id_ticket', 
				'length' => 7, 
				'prefix' =>date('ym'),
				'reset_on_prefix_change' => true
				]);
//output: 1910000001

        // if ($orderready == null) {

            $status = 'Unprocessed';
            $ticketstore = new tbl_ticket;

            $ticketstore->id_ticket              = $uniqueId;
            $ticketstore->order_id               = $orderr;
            $ticketstore->topics                 = $request->topic;
            $ticketstore->email                  = $request->email;
            $ticketstore->nama_pelapor           = $request->nama_pelapor;
            $ticketstore->customer_id            = $request->customerId;
            $ticketstore->nama_customer          = $request->customer;
            $ticketstore->telepon                = $request->telepon;

            if ($request->complaintType == "Non-Delivery") {
                $ticketstore->non_deliv           = $non_deliv;
            }

            if ($request->hawb) {
                $ticketstore->hawb                = $request->hawb;
            }

            
            $ticketstore->zip                      = $zip;
            $ticketstore->transporter              = $ekspedisi;

            if($ticketstore->topics == 'Barang Rusak'){
                $ticketstore->prioritas              = 'High';
            }else{
                $ticketstore->prioritas              = 'Low';
            }

            if ($request->message) {
                $ticketstore->message            = $request->message;
            }else{
                $ticketstore->message            = '';
            }

            $ticketstore->status                 = $status;
			$ticketstore->attachments = '';
			$ticketstore->attachments2 = '';
			//dd($request->attachments);
			//dd($request->attachments2);
			//die();
            if ($request->attachments) {
                $fileName = $uniqueId.'01.'.$request->attachments->extension(); 
                $request->attachments->move(public_path('upload'), $fileName);
                $ticketstore->attachments            = $fileName;
            }
            if ($request->attachments2) {
                $fileName = $uniqueId.'02.'.$request->attachments2->extension(); 
                $request->attachments2->move(public_path('upload'), $fileName);
                $ticketstore->attachments2            = $fileName;
            }

			$ticketstore->due = date("m/d/Y", strtotime(Carbon::today().' +1 day'));
            $ticketstore->save();
            $ticketlog = new tbl_ticket_log;
           
            $ticketlog->id_ticket         = $ticketstore->id;
            $ticketlog->description       = 'Create New Ticket<br>
            Information: '.$request->message;
            $ticketlog->activity          = 'Create Ticket';

            $ticketlog->pic               =  $user;
            
            $ticketlog->save();

            $deliv = tbl_delivery_order::where('order_id', $request->order_id)
            ->update(['is_complain' => 1]);

        // }else {
        //     Session::flash('sukses','SO is already exists!');    
        //     return redirect()->route('listticket');
        // }

        Session::flash('sukses','Ticket Successfully Created!');    
       
        return redirect()->route('detailticket', $ticketstore->id);
    }


    public function detailticket(Request $request, $id, $pid = 0) {
		

		
        $ticketdetail = tbl_ticket::where('id', $id)->first();
        //$id_ticket = tbl_ticket_log::where('id', $id)->value('id_ticket');//id ticket doang
        ///buat ticket log
        $ticketlogs = tbl_ticket_log::where('id_ticket',$id)
        ->orderby('created_at','DESC')
        ->get();
		
		$viewOnly = false;
		if($ticketdetail->status == 'Solved')
			$viewOnly = true;
		
		$alertGeneral = [];
		
		if($ticketdetail->is_locked){
            if ($request->is('api/*')) {  
                if ($ticketdetail->locked_by == 'api'){
                    $ticketdetail->locked_time = now();
                }
                else{
                    $viewOnly = true;
                    $alertGeneral['warning'] = [
                                                'title' => "Ticket has been locked by ".$ticketdetail->locked_by,
                                                'message' => ''
                                            ];
                    $alertGeneral['timer'] = 0;
                    $alertGeneral['timeout'] = 0;
                }
            }
			elseif($ticketdetail->locked_by == Auth::user()->name){
				$ticketdetail->locked_time = now();
            }
			else {
				$viewOnly = true;
				$alertGeneral['warning'] = [
											'title' => "Ticket has been locked by ".$ticketdetail->locked_by,
											'message' => ''
										];
				$alertGeneral['timer'] = 0;
				$alertGeneral['timeout'] = 0;
			}
		}
		else {
			$ticketdetail->is_locked = true;
			$ticketdetail->locked_by = Auth::user()->name;
			$ticketdetail->locked_time = now();
			
		}
		$ticketdetail->save();
		$ticketdetail->viewOnly = $viewOnly;

        $attachments = json_decode($ticketdetail->multiattachment) ;

        // $data = array_push($attachments, "red","blue");
        // dd($attachments);

        if ($attachments) {
            foreach ($attachments as $file ) {
                // $image1 = Storage::disk('local')->exists('public/'.$file);
                $multiimage = file_exists(public_path(). '/upload/'. $file);
            }
        }else{
            $multiimage = '';
        }

        $image1 = file_exists(public_path(). '/upload/'. $ticketdetail->attachments);
        $image2 = file_exists(public_path(). '/upload/'. $ticketdetail->attachments2);


		if($pid > 0){
            if ($request->is('api/*')) {  
                return response()->json([
                    'ticketdetail' => $ticketdetail,
                    'ticketlogs'=> $ticketlogs,
                    'alertGeneral'=> $alertGeneral,
                    'attachments'=> $attachments, 
                    'multiimage'=> $multiimage, 
                    'image1'=> $image1,
                    'image2'=> $image2,
                ], 200);
            }
            else{
                return view('admin.detail_ticket_'.$pid, compact('ticketdetail','ticketlogs','alertGeneral','attachments','multiimage','image1','image2'));
            } 	
        }
        if ($request->is('api/*')) {  
            return response()->json([
                'ticketdetail' => $ticketdetail,
                'ticketlogs'=> $ticketlogs,
                'alertGeneral'=> $alertGeneral,
                'attachments'=> $attachments, 
                'multiimage'=> $multiimage, 
                'image1'=> $image1,
                'image2'=> $image2,
            ], 200);
        }
        else{
            return view('admin.detail_ticket', compact('ticketdetail','ticketlogs','alertGeneral','attachments','multiimage','image1','image2'));    	
        }
    }

    public function ticket(Request $request, $id = 0) {
				
		$hawb = "";
		$order_id = "";
			$nama_pelapor = '';
			$telepon = '';
			$customerName = '';
            $customerId = '';
			$status = '';
		if($id > 0){
			$temp = tbl_delivery_order::find($id);
			$ticket = tbl_ticket::firstWhere('order_id',$temp->order_id);
			if ($ticket != null)
				return redirect()->route('detailticket', $ticket->id);
			$hawb = $temp->hawb;
			$status = $temp->status;
			$order_id = $temp->order_id;
			$nama_pelapor = $temp->customer_name;
			$telepon = $temp->phone;
			$customerName = $temp->customer_name;
		}
        $topicname = DB::table('tbl_topics')->pluck('name');
		
        if ($request->is('api/*')) { 
            return response()->json([
                'topicname' => $topicname,
                'hawb'=> $hawb,
                'order_id'=> $order_id,
                'customer'=> $customer, 
                'telepon'=> $telepon, 
                'nama_pelapor'=> $nama_pelapor,
                'status'=> $status,
            ], 200);
        }
        else{
            return view('admin.add_new_ticket',compact('topicname','hawb','order_id','customerName','telepon','nama_pelapor','status'));
        }
        

    }
    public function autofill() {
        $invoiceid = DB::table('tbl_delivery_order')->get('order_id');
         dd($invoiceid);
        return view('admin.add_new_ticket');

    }

    public function reopenticket(Request $request, $id) {
        if ($request->is('api/*')) { 
            $user = 'api';
        }
        else
        {
            $user = Auth::user()->name;
        }
        
        $reopen = tbl_ticket::where('id', $id)->first();
        $ticketlogi = new tbl_ticket_log();
        $reopen->status = 'Unprocessed';
        $reopen->save();
        $ticketlogi->id_ticket        = $reopen->id;
        $ticketlogi->activity         = 'Re-Open Ticket';
        $ticketlogi->description      = 'Re-Open';
        $ticketlogi->pic              = $user;
        $ticketlogi->save();
        if ($request->is('api/*')) {  
            return response()->json([
                'message' => 'sukses'
            ], 200);
        }
        else{
            return redirect()->route('listticket');
        }
        
       

    }
    public function storeeditticket(Request $request, $id) {

        if ($request->is('api/*')) {  
            $user = 'api';
        }
        else{
            $user = Auth::user()->name;
        }
        $ticket = tbl_ticket::find($id);
        $ticketlogi = new tbl_ticket_log();///buat data baru di tabel tiket log
        $idticket = tbl_ticket::where('id', $id)->value('id');//id doang contoh : 4679(tbl ticket)
        // $idticket = tbl_ticket_log::where('id_ticket',$ticket->id);

        if($ticket->status == 'Solved'){
            $ticket->status == 'Solved';
        } elseif ($request->status == 'Solved') {
            $ticket->status             = $request->status;
        } elseif ($request->status == 'On Process') {
            $ticket->status             = 'On Process';
        } elseif ($request->status == null && $ticket->status == 'On Process') {
            $ticket->status == 'On Process';
        } else {
            $ticket->status             = 'Unprocessed';
        }



        if ($request->duedetail) {
            $ticket->due                = $request->duedetail;
        } else {
            $ticket->status     = $ticket->status;   
        }

        if ($request->hasFile('image' )) {

            $attachments = json_decode($ticket->multiattachment) ;
            
                if ($attachments) {
                    $files = $request->file('image');
                        foreach($files as $file ) {
                            $name = rand(1,999);
                            $extension = $file->getClientOriginalExtension();
                            $newname = $name. '.' .$extension;
                     
                                // Storage::putFileAs('public', $file, $newname);
                                $file->move(public_path('upload'), $newname);

                            $dataname[] = $newname;
                        }

                    $dataarray = array_merge($attachments, $dataname);
                    $ticket->multiattachment = json_encode($dataarray);

                } else {
                    $files = $request->file('image');
                    foreach($files as $file ) {
                        $name = rand(1,999);
                        $extension = $file->getClientOriginalExtension();
                        $newname = $name. '.' .$extension;
                 
                            // Storage::putFileAs('public', $file, $newname);
                            $file->move(public_path('upload'), $newname);

                        $dataname[] = $newname;
                    }
                    $ticket->multiattachment = json_encode($dataname);
                }

        }


        $ticket->order_id               = $ticket->order_id;
        if($ticket->status == 'Unprocessed'){
            $activitylog                    = 'Update Status :  Unprocessed' ;
        }elseif($ticket->status == 'On Process'){
            $activitylog                    = 'Update Status :  On Process' ;
        }elseif($ticket->status == 'Solved'){
            $activitylog                    = 'Update Status :  Solved' ;
        }else{
            $activitylog    = '';
        }
        
        $ticketlogi->activity         = $activitylog;
        $ticketlogi->pic              = $user ;
        $ticketlogi->id_ticket        = $idticket;
        $defaultdesc = '';
        $defaultdue = '';
        // $defaultdue = date('Y-m-d',strtotime($ticket->due . "+4 days"));
        if($request->description == null){
            $ticketlogi->description = $defaultdesc;
        }else {
            $ticketlogi->description      = $request->description;
        }
        if($request->duedetail == null){
            $ticketlogi->due = $defaultdue;
        }else {
            $ticketlogi->due   = $request->duedetail;
        }

		$ticket->is_locked = false;
        $ticket->save();
        $ticketlogi->save();
        
        if ($request->is('api/*')) {  
            return response()->json(['message' => 'Data has been changed!']);
        }
        else{
            Session::flash('berhasil','Data has been changed!');

            return redirect()->back();            
        }
        
    }

    public function filterTicket(Request $request) {
        $so_number  = $request->filtersonumber;
        
        $ticket     = tbl_ticket::whereIn('order_id', array($so_number))->get();
        return view('admin.list_ticket',['ticket'=>$ticket]);
        
    }

    public function detailcomplaindo($order_id) {

        $ticketdetail = tbl_ticket::where('order_id', $order_id)->first();
        $id = $ticketdetail->id;

        ///buat ticket log
        $ticketlogs = tbl_ticket_log::where('id_ticket',$id)
        ->orderby('created_at','DESC')
        ->get();

        return view('admin.detail_ticket', compact('ticketdetail','ticketlogs'));       
    }

    public function nondelivticket() {

        return view('admin.list_ticket_non_deliv');
    }

    public function getAllListJson()
    {
        $query = tbl_ticket::all();

        return Datatables::of($query)
            ->addColumn("action", function($query){
                $btn ='';

                if(Auth::user()->can('edit_ticket_delivery'))
                {
                    $btn .= "<a href=".route('detailticket', ['id' => $query->id])." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'>
                          <i class='fas fa-info-circle'></i>
                        </a>";
                    if($query->status == 'Solved')
                    {
                        $btn .= "<a class='btn btn-xs btn-warning btn-modal-open' data-toggle='tooltip' title='Re Open' tid=".$query->id.">
                          <i class='far fa-folder-open'></i>
                        </a> ";
                    }
                    if($query->is_locked)
                    {
                        $btn = "<a href=".route('releaseticket', ['id' => $query->id])." class='btn btn-xs btn-info' data-toggle='tooltip' title='Release Lock'>
                         <i class='fas fa-lock-open'></i>
                         <!--<i class='fas fa-unlock-alt'></i>-->
                       </a> ";
                    }
                        
                }
                return $btn;
            })
            ->make(true);
    }

    public function getAllNotResolvedJson()
    {
        $query    = tbl_ticket::where('status', '=', 'Unprocessed')
        ->orderBy('created_at', 'ASC')
        ->get();

        return Datatables::of($query)
            ->addColumn("action", function($query){
                $btn ='';

                if(Auth::user()->can('edit_ticket_delivery'))
                {
                    $btn .= "<a href=".route('detailticket', ['id' => $query->id])." class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'>
                       <i class='fas fa-search'></i>
                        </a>";
                    if($query->status == 'Solved')
                    {
                        $btn .= "<a class='btn btn-xs btn-warning btn-modal-open' data-toggle='tooltip' title='Re Open' tid=".$query->id.">
                          <i class='far fa-folder-open'></i>
                        </a> ";
                    }
                    if($query->is_locked)
                    {
                        $btn = "<a href=".route('releaseticket', ['id' => $query->id])." class='btn btn-xs btn-info' data-toggle='tooltip' title='Release Lock'>
                         <i class='fas fa-lock-open'></i>
                         <!--<i class='fas fa-unlock-alt'></i>-->
                       </a> ";
                    }
                        
                }
                return $btn;
            })
            ->editColumn('status', function($query)
            {
                $class = ($query->status=="Solved")?"success" : (($query->status=="On Proccess")?"info" : "warning");
                return '<span class="badge badge-'.$class.' ">
                                     '.$query->status.'
                                   </span>';
            })
            ->editColumn('created_at', function($query)
            {
                return date("m/d/Y",strtotime($query->created_at));
            })
            ->rawColumns(['status','action'])
            ->make(true);

    }
   
}