<?php

namespace App\Http\Controllers;

use App\Model\PostalCode;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use App\Model\Zone;
use App\Model\City;
use App\Model\Courier;
use App\Model\CourierLeadtime;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PostalCodesImport;
use App\Imports\PostalCityProvinceImport;
use App\Imports\CourierLeadtimesImport;

class PostalCodeController extends Controller
{
	protected $model_name = "Postal Code";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        logger('----PostalCodeController------ - '.json_encode($request->all()));
        if ($request->ajax()) {
            
			//$data = PostalCode::with('zone:id,code')->with('courierLeadtimes.courier')->latest()->get();
            $data = PostalCode::with('zone:code,id')->with('city:name,id')->with('courierLeadtimes.courier');
			
			if($request->input('order.0.column') == 0){
                $data = $data->orderBy('created_at','desc');			
			}		
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
					/*
					->editColumn('zone_id', function ($row) {
						return $row->zone->code;
					})	
					->editColumn('city_id', function ($row) {
						return $row->city->name;
					})	
					->editColumn('match_word', function ($row) {
						return ($row->match_word)?'<span class="badge badge-info">Yes</span>':'No';
					})	
					->editColumn('nuskin_status_id', function ($row) {
						return $row->nuskinStatus->display_name;
					})		
                    ->rawColumns(['action','match_word'])
                    */
					->rawColumns(['action'])
                    ->make(true);
        }
		
		$couriers = Courier::select('id','display_name')->get();
		$zones = Zone::select('id','code')->get();
		$cities = City::select('id','name')->get();
      
        return view('settings/postalcode')->with(compact('zones','cities','couriers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		//logger('----data from courier leadtime------ - '.json_encode($request->all()));
		
        $model = PostalCode::updateOrCreate(['id' => $request->model_id],
                [
					'code' => $request->code, 
					'nuskinLeadTime' => $request->nuskinLeadTime, 
					'zone_id' => $request->zone_id, 
					'city_id' => $request->city_id, 
				]);        
				
		foreach($request->courierLeadTime as $k => $v)
			CourierLeadtime::updateOrCreate(['postal_code_id' => $model->id, 'courier_id' => $k],
                [
					'courierLeadTime' => $v, 
				]);   
		
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PostalCode  $postalCode
     * @return \Illuminate\Http\Response
     */
    public function show(PostalCode $postalCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PostalCode  $postalCode
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		logger('----edit PotalCodeController------ id - '.$id);
		//logger('----edit PotalCodeController------ - '.json_encode($request->all()));
        $model = PostalCode::with('zone:id,code')->with('city:id,name')->with('courierLeadtimes.courier:id,display_name')->find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PostalCode  $postalCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostalCode $postalCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PostalCode  $postalCode
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		CourierLeadtime::where('postal_code_id',$id)->delete();
        PostalCode::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
	
	public function import_excel(Request $request) {
        		
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx,text/csv'
        ]);
 
//        Excel::import(new PostalCodesImport, $request->file('file') );

		$import = new PostalCodesImport();
		$import->import($request->file('file'));

		//dd($import->errors());
        
        return redirect()->route('postalcodes.index');
    }

	public function import_excel_courier(Request $request) {
        	
			
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx,text/csv'
        ]);
 
//        Excel::import(new PostalCodesImport, $request->file('file') );

		$import = new CourierLeadtimesImport();
		$import->import($request->file('file'));

		//dd($import->errors());
        
        return redirect()->route('postalcodes.index');
    }

	public function import_excel_postalcityprovince(Request $request) {
        	
			
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx,text/csv'
        ]);
 
//        Excel::import(new PostalCodesImport, $request->file('file') );

		$import = new PostalCityProvinceImport();
		$import->import($request->file('file'));

		//dd($import->errors());
        
        return redirect()->route('postalcodes.index');
    }

    public function checkCode(Request $request, $code){
        $postalCodes = PostalCode::where('code', $code)->exists();

        return response()->json($postalCodes);
    }

}
