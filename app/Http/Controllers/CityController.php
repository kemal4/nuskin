<?php

namespace App\Http\Controllers;

use App\Model\City;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use App\Model\Province;

class CityController extends Controller
{
	protected $model_name = "City";
	protected $provinces = null;
	
    public function __construct()
    {
   		$this->provinces = Province::select('id','name')->get();
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        logger('----PostalCodeController------ - '.json_encode($request->all()));
        if ($request->ajax()) {
            //$data = CourierStatus::with('nuskinStatus:id,display_name')->with('courier:id,display_name')->latest()->get();
            $data = City::with('province:name,id');
			
			if($request->input('order.0.column') == 0){
                $data = City::with('province:name,id')->orderBy('created_at','desc');
			}
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
					/*
					->editColumn('province_id', function ($row) {
						return $row->province->name;
					})
					*/
                    ->rawColumns(['action'])
                    ->make(true);
        }
		
		$provinces = $this->provinces;
      
        return view('settings/city')->with(compact('provinces'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        City::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					'province_id' => $request->province_id, 
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = City::with('province:id,name')->find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        City::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
}
