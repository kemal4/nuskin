<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Post;
use Carbon\Carbon;
use Session;
use DB;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Log;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->is('api/*')) {
            $user = 'api';
        }
        else{
            $user = Auth::user()->name;
        }

        $data = request()->validate([
                'title'     => 'required',
                'type'      => 'required',
                'message'   => 'required'
            ],
            [
            'title.required'    => 'The Title field is required!',
            'type.required'     => 'The Type field is required!',
            'message.required'  => 'The Message field is required!',
            ]);

        $post = new post;

        $post->title = $request->title;
        $post->type = $request->type;
        $post->message = $request->message;
        $post->posted = false;
        $post->pic = $user;
        $post->save();
        
        return redirect()->route('post.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $post = Post::find($id);
        
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       if ($request->is('api/*')) {
            $user = 'api';
        }
        else{
            $user = Auth::user()->id;
        }

        $data = request()->validate([
                'title'     => 'required',
                'type'      => 'required',
                'message'   => 'required'
            ],
            [
            'title.required'    => 'The Title field is required!',
            'type.required'     => 'The Type field is required!',
            'message.required'  => 'The Message field is required!',
            ]);

        $post = Post::find($id);

        $post->title = $request->title;
        $post->type = $request->type;
        $post->message = $request->message;
        $post->pic = $user;
        $post->save();
        
        return redirect()->route('post.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllListJson(Request $request)
    {
        $query = Post::all();

        return Datatables::of($query)
            ->addColumn("action", function($query){
                return "<a href=".route('post.edit', $query->id)." class='btn btn-xs btn-warning' data-toggle='tooltip' title='Edit'><i class='fas fa-edit'></i></a>";
            })
            ->addColumn("posted", function($query)
            {
                if ($query->posted == true) {
                    return '<a href='.route('post.change', $query->id).'><button class="btn btn-block btn-danger">UNPOST</button></a>';
                }
                else {

                    return '<a href='.route('post.change', $query->id).'><button class="btn btn-block btn-success">POST</button></a>';
                }
            })
            ->editColumn('created_at', function ($query) {  
                return $query->created_at ? with(new Carbon($query->created_at))->format('Y-m-d H:i:s') : '';
            })
            ->rawColumns(['posted','action'])
            ->make(true);
    }
    public function changePosted(Request $request, $id){
        $query = Post::findOrFail($id);
        if ($query->posted == true){
            $query->posted = false;
            $query->pic = Auth::user()->id;
            $query->save();
        }
        else{
            $query->posted = true;
            $query->pic = Auth::user()->id;
            $query->save();
        }
        return redirect()->route('post.index');
    }
}
