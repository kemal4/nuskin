<?php

namespace App\Http\Controllers;

use App\Model\Store;
use Illuminate\Http\Request;
use DataTables;
use Auth;
use App\Model\City;

class StoreController extends Controller
{
	protected $model_name = "Store";
	protected $cities = null;
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Log::error('----PostalCodeController------ - '.json_encode($request->all()));
		
		$this->cities = City::select('id','name')->get();
		
        if ($request->ajax()) {
            //$data = CourierStatus::with('nuskinStatus:id,display_name')->with('courier:id,display_name')->latest()->get();
            $data = Store::with('city:id,name');
			
			if($request->input('order.0.column') == 0){
                $data = $data->orderBy('created_at','desc');			
			}
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
						$btn = '';
						if(Auth::user()->can('edit_setting'))
                           $btn .= '<a href="javascript:void(0)" data-toggle="tooltip" title="Edit" data-id="'.$row->id.'" class="edit btn btn-warning btn-xs editProduct"><i class="fas fa-edit"></i></a>';
						if(Auth::user()->can('delete_setting'))   
                           $btn .= ' <a href="javascript:void(0)" data-toggle="tooltip" title="Delete" data-id="'.$row->id.'" class="btn btn-danger btn-xs deleteProduct"><i class="fas fa-trash-alt"></i></a>';
    
                            return $btn;
                    })
					/*
					->editColumn('city_id', function ($row) {
						return $row->city->name;
					})
					*/
                    ->rawColumns(['action','match_word'])
                    ->make(true);
        }
		
		$cities = $this->cities;
      
        return view('settings/store')->with(compact('cities'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Store::updateOrCreate(['id' => $request->model_id],
                [
					'name' => $request->name, 
					'code' => $request->code, 
					'city_id' => $request->city_id, 
					'address_address' => $request->address_address, 
					'address_latitude' => $request->address_latitude, 
					'address_longitude' => $request->address_longitude, 
				]);        
		
		$alertMessage = $this->model_name.' Successfully Edited';
		if(empty($request->model_id))
			$alertMessage = $this->model_name.' Successfully Created';
			
        return response()->json(['success'=>$alertMessage]);
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Store::with('city:id,name')->find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Store::find($id)->delete();
     
        return response()->json(['success'=>$this->model_name.' deleted successfully.']);
    
    }
}
