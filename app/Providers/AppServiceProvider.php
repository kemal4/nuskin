<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
	/*
		DB::listen(function ($query) {
            // $query->sql
            // $query->bindings
            // $query->time
		Log::error('lasy query : '.$query->sql);
        });
	*/

	    Schema::defaultStringLength(191);

	//echo "-------------manggilappProvidersAppServiceProvider";
		

		if(env('ENFORCE_SSL', false)) {
			$url->forceScheme('https');
		}
        //
    }
}
