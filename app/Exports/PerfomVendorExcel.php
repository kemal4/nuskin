<?php

namespace App\Exports;

use App\Model\tbl_delivery_order;
use App\Model\tbl_perfom_vendor;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PerfomVendorExcel implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      
        return tbl_perfom_vendor::all();
    }

    public function headings(): array
    {
        return [
        	'No',
            'Hawb',
            'City',
            'Transporter',
            'Zip',
            'Fullfilday',
            'Status',
            'Star Date',
            'Deliver Date',
        ];
    }
}
