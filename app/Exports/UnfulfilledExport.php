<?php

namespace App\Exports;

use App\Model\tbl_delivery_order;
use DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UnfulfilledExport implements FromQuery, WithMapping, WithHeadings, WithColumnFormatting
{
    use Exportable;
	
    //public function __construct($courier, $startDate, $endDate)
    public function __construct($courier_id, $startDate, $endDate)
    {
        $this->courier_id = $courier_id;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }
		
    public function query()
    {
        return tbl_delivery_order::query()
								->whereBetween('created_at',[$this->startDate,$this->endDate])
								//->where('transporter',$this->courier)
								->where('courier_id',$this->courier_id)
								->where('is_delivered',true)
								->where('on_time_provider','<>',true);
    }
	
    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_DATE_XLSX14,
            'C' => NumberFormat::FORMAT_DATE_XLSX14,
            'H' => NumberFormat::FORMAT_DATE_XLSX14,
            'I' => NumberFormat::FORMAT_DATE_XLSX14,
        ];
    }
	
    public function map($do): array
    {
        return [
			'',
            $do->order_id,
            Date::dateTimeToExcel(Carbon::parse($do->create_date)),
            $do->transporter,
            $do->hawb,
            Date::dateTimeToExcel($do->created_at),
            $do->leadtime_courier,
            Date::dateTimeToExcel(Carbon::parse($do->eta_provider)),
            //Date::dateTimeToExcel($do->eta_provider),
            //Date::dateTimeToExcel($do->delivered_date),
            Date::dateTimeToExcel(Carbon::parse($do->delivered_date)),
            $do->order_type,
            $do->customer_refer,
            $do->customer_id,
            $do->customer_name,
            $do->phone,
            $do->adress,
            $do->city,
            $do->zip,
			
        ];
    }
	
    public function headings(): array
    {
        return [
            'No.',
            'Invoice Number',
            'Invoice Date',
			'Courier',
            'hawb',
            'Handover Date',
            'LeadTime',
            "Courier's ETA",
            'Delivered Date',
            'OrderType',
            'Delivery Number',
            'customerID',
            'customerName',
            'phone',
            'address',
            'city',
            'zip'

        ];
    }
}
