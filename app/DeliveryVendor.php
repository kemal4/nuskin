<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryVendor extends Model
{
    protected $table = "deliveryvendor";
    public $incrementing = false;
    protected $fillable = 
    ['date','order_id','customer','awb','vendor_delivery','due_date','status','item',
    'telepon','gudang','alamat','delivery_number','pic','is_preview'];
}
