<?php

namespace App\Helpers;

use App\User;

class Helper {
	
    public static function sidebar($user) {

        $data = User::find($user);
        return $data->photo_path;
    }

}