<?php

namespace App\Imports;


use App\Model\tbl_kodepos;
use Maatwebsite\Excel\Concerns\ToModel;

class KodePosImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        // die();
        return new tbl_kodepos([
            'kelurahan'             => $row[0],
            'kecamatan'             => $row[1], 
            'provinsi'              => $row[2], 
            'kodepos'               => $row[3], 
            'city'                  => $row[4], 
            'leadtime_nuskin'       => $row[5], 
            'leadtime_fl'           => $row[6], 
            'leadtime_jne'          => $row[7],
            'id_zonasi'             => $row[8],

        ]);
    }
}
