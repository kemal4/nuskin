<?php

namespace App\Imports;

// use App\DeliveryVendor;
use App\Model\tbl_delivery_order;
use Maatwebsite\Excel\Concerns\ToModel;
use Auth;
use DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\Importable;
use App\Model\Courier;


class DeliveryVendorImport implements ToModel //, SkipsOnError
{
	
    use Importable; //, SkipsErrors;
	
	private $warehouse = null;
	
	private $previewKey = null;
	
	protected $courierToIdMap = array(
		'JNE' => 1, 
		'MrSpeedy' => 2,
		'FL' => 3,
	);

	// protected $courierToIdMap = null;


	
	public function __construct($warehouse, $previewKey,$courierToId)
    {
        $this->warehouse=$warehouse;
        $this->previewKey=$previewKey;
		$this->courierToIdMap = $courierToId;
		// $query = Courier::all();
  //       foreach($query as $qu)
  //       {
  //       	$qu->	
  //       }
    }
	
	protected $statusToMap = array(
		1 => 'onprocess',
		2 => 'delivered',
		3 => 'undelivered',
		4 => 'return',
		5 => 'incident',
	);
	
	
	protected $holidayDates = array(
						'2020-01-01',
						'2020-01-25',
						'2020-03-22',
						'2020-03-25',
						'2020-04-10',
						'2020-05-01',
						'2020-05-07',
						'2020-05-21',
						'2020-05-22',
						'2020-05-24',
						'2020-05-25',
						'2020-05-26',
						'2020-05-27',
						'2020-06-01',
						'2020-07-31',
						'2020-08-17',
						'2020-08-20',
						'2020-10-29',
						'2020-12-24',
						'2020-12-25'
					);
	
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    //public function model(array $row)
    public function modelImportDummy(array $row)
    {

		if(!isset($row[12]) || !is_numeric($row[12]))
			return NULL;

        $user = Auth::user()->name;
        $status = 'onprocess';
		$status_id = 1;
				
		$lt_nuskin = 0;
		$eta_nuskin = null;
		$lt_courier = 0;
		$eta_provider = null;
		
		/*
		$asd = [];
		echo ' - gettype : '.gettype('');
		$asd['dateYmd'] = date('Y-m-d');
		echo ' - gettype : '.gettype($asd['dateYmd']);
		$asd['rawRow'] = ($row[1]);
		echo ' - gettype : '.gettype($asd['rawRow']);
		$asd['rawConvert'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[1]);
		echo ' - gettype : '.gettype($asd['rawConvert'] );
		
		$asd['rawTimestamp'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($row[1]);
		echo ' - gettype : '.gettype($asd['rawTimestamp'] );
		//echo ' - gettype : '.gettype( ($asd['rawConvert'])['date'] );
		
		$asd['rawConvertStr'] = date('Y-m-d', $asd['rawTimestamp']);
		echo ' - gettype : '.gettype($asd['rawConvertStr'] );
		$asd['newDate'] = strtotime($asd['rawConvertStr'].' +1 day');
		echo ' - gettype : '.gettype($asd['newDate'] );		
		$asd['newDateStr'] = date('Y-m-d', $asd['newDate']);
		echo ' - gettype : '.gettype($asd['newDateStr'] );
		
		$asd['newDate'] = strtotime($asd['rawConvert'].' +1 day');
		echo ' - gettype : '.gettype($asd['newDate'] );		
		$asd['rawConvertStr'] = date('Y-m-d', $asd['newDate']);
		echo ' - gettype : '.gettype($asd['rawConvertStr'] );
		*/
		
		
		$created_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($row[1]);;
		$created_date =  strtotime(date('Y-m-d', $created_date).' +1 day');
		$created_date_str = date('Y-m-d', $created_date);;
		//dd($asd);
		
		// TODO
		// get leadtime based on postal code
		$leadtime_nuskin = DB::table('postal_codes')->select('nuskinLeadTime')
						->where('code',trim($row[12]))->first();
		if($leadtime_nuskin != null){
			$eta_nuskin = Carbon::Parse(''.($this->getNextWorkingDay($created_date_str,$leadtime_nuskin->nuskinLeadTime)).' 23:59:59');
			$lt_nuskin = $leadtime_nuskin->nuskinLeadTime;
		}
		//echo ' asdasd : '.$lt_nuskin;
		
		$leadtime_courier = DB::table('postal_codes')->select('courier_leadtimes.courierLeadTime')
						->join('courier_leadtimes', 'postal_codes.id', '=', 'courier_leadtimes.postal_code_id')
						->where('postal_codes.code',trim($row[12]))
						->where('courier_leadtimes.courier_id',$this->courierToIdMap[trim($row[2])])
						->first();						
						//TODO ,change courier into DB
						//->join('tbl_vendor', 'tbl_vendor.id', '=', 'courier_id.postal_code_id')
						//->where('tbl_vendor.id', '=', '$model->courier_id')
						

		$is_delivered = 0;
		$is_on_time = 0;
		$on_time_nuskin = 0;
		$delivered_date = null;
		
		if($leadtime_courier != null){
			$eta_provider = Carbon::Parse(''.($this->getNextWorkingDay($created_date_str,$leadtime_courier->courierLeadTime,false)).' 23:59:59');
			$lt_courier = $leadtime_courier->courierLeadTime;
			
			$status = 'delivered';
			$status_id = 2;
			
			$is_delivered = 1;
			$is_on_time = mt_rand(0,1);
			$on_time_nuskin = 1;
			if($is_on_time){
				
				$delivered_date =  strtotime(date('Y-m-d', $eta_provider->timestamp).' -1 day');
			}
			else {
				$delivered_date = strtotime(date('Y-m-d', $eta_provider->timestamp).' +1 day');
			}
			$delivered_date = date('Y-m-d', $delivered_date);
			//$delivered_date = Carbon::Parse(''.($this->getNextWorkingDay($created_date_str,$leadtime_courier->courierLeadTime,false)).' 23:59:59');
			//dd($delivered_date);
			
		}
		else {
			$status_id= mt_rand(3,5);
			$status = $this->statusToMap[$status_id];
		}
		
		//echo ' before return ';

        return new tbl_delivery_order([
            'create_date'       => Carbon::parse($row[1]),
            'transporter'       => $row[2],
            'order_id'          => $row[3],
            'order_type'        => $row[4],
            'customer_refer'    => $row[5],
            'customer_id'       => $row[6],
            'customer_name'     => $row[7],
            'phone'             => $this->getPhone($row[8]),
            'adress'            => ($row[9].' - '.$row[10]),
            'city'              => $row[11],
            'zip'               => $row[12],
            'grosswiight'       => $row[13],
            'net_weight'        => $row[14],
            'volume'            => $row[15],
            'hawb'              => $row[16],
            'delivery_number'   => $row[5],	// same as customer_refer ??
            //'delivery_number'   => $row[17],
            
			'created_at'            => $created_date,
            'on_time_nuskin'            => $on_time_nuskin,
            'is_delivered'            => $is_delivered,
            'on_time_provider'            => $is_on_time,
            'delivered_date'            => $delivered_date,
			
            'status'            => $status,
            'status_id'            => $status_id,
			
            'pic'               => $user,
            //'is_preview'        => 'yes',
            'is_preview'        => $this->previewKey,
            'is_deleted'        => '',
			'leadtime_nuskin' 	=> $lt_nuskin,
			'eta_nuskin' 		=> $eta_nuskin,
			'leadtime_courier' 	=> $lt_courier,
			'eta_provider' 		=> $eta_provider,
			'warehouse_id' 		=> $this->warehouse->id_gudang,
			'warehouse_name' 		=> $this->warehouse->lokasi_gudang,
        ]);

    }
	
	public function getPhone($rawPhone){
		
		$result = str_replace('-','',$rawPhone);
		$result = str_replace(' ','',$rawPhone);
		$result = ltrim($result, '+');
		$result = ltrim($result, '62');
		$result = ltrim($result, '0');
		
		return $result;
		
	}
	
	//public function model666(array $row)
	public function model(array $row)
    {
		if(!isset($row[12]) || !is_numeric($row[12]))
			return NULL;

        $user = Auth::user()->name;
        $status = 'onprocess';
		$status_id = 1;
				
		$lt_nuskin = 0;
		$eta_nuskin = null;
		$lt_courier = 0;
		$eta_provider = null;
		// TODO
		// get leadtime based on postal code
		$leadtime_nuskin = DB::table('postal_codes')->select('nuskinLeadTime')
						->where('code',trim($row[12]))->first();
		if($leadtime_nuskin != null){
			$eta_nuskin = Carbon::Parse(''.($this->getNextWorkingDay(date('Y-m-d'),$leadtime_nuskin->nuskinLeadTime)).' 23:59:59');
			$lt_nuskin = $leadtime_nuskin->nuskinLeadTime;
		}
		//echo ' asdasd : '.$lt_nuskin;
		
		$leadtime_courier = DB::table('postal_codes')->select('courier_leadtimes.courierLeadTime')
						->join('courier_leadtimes', 'postal_codes.id', '=', 'courier_leadtimes.postal_code_id')
						->where('postal_codes.code',trim($row[12]))
						->where('courier_leadtimes.courier_id',$this->courierToIdMap[trim($row[2])])
						->first();						
						//TODO ,change courier into DB
						//->join('tbl_vendor', 'tbl_vendor.id', '=', 'courier_id.postal_code_id')
						//->where('tbl_vendor.id', '=', '$model->courier_id')
		if($leadtime_courier != null){
			$eta_provider = Carbon::Parse(''.($this->getNextWorkingDay(date('Y-m-d'),$leadtime_courier->courierLeadTime,false)).' 23:59:59');
			$lt_courier = $leadtime_courier->courierLeadTime;
		}
		
		//echo ' before return ';

        return new tbl_delivery_order([
            'create_date'       => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[1]),
            'transporter'       => $row[2],
            'courier_id'       	=> $this->courierToIdMap[trim($row[2])],
            'order_id'          => $row[3],
            'order_type'        => $row[4],
            'customer_refer'    => $row[5],
            'customer_id'       => $row[6],
            'customer_name'     => $row[7],
            'phone'             => $this->getPhone($row[8]),
            'adress'            => ($row[9].' - '.$row[10]),
            'city'              => $row[11],
            'zip'               => $row[12],
            'grosswiight'       => $row[13],
            'net_weight'        => $row[14],
            'volume'            => $row[15],
            'hawb'              => $row[16],
            'delivery_number'   => $row[5],	// same as customer_refer ??
            //'delivery_number'   => $row[17],
            'status'            => $status,
            'status_id'            => $status_id,
            'pic'               => $user,
            //'is_preview'        => 'yes',
            'is_preview'        => $this->previewKey,
            'is_deleted'        => '',
			'leadtime_nuskin' 	=> $lt_nuskin,
			'eta_nuskin' 		=> $eta_nuskin,
			'leadtime_courier' 	=> $lt_courier,
			'eta_provider' 		=> $eta_provider,
			'warehouse_id' 		=> $this->warehouse->id_gudang,
			'warehouse_name' 	=> $this->warehouse->lokasi_gudang,
        ]);

    }
	
	
	public function  getNextWorkingDay($startDateStr, $leadTime, $excludeSaturday=true, $excludeSunday=true ){

			$startDate = strtotime($startDateStr);
			$nextWorkingDayStr = $startDateStr;

			while ($leadTime > 0){
					$nextWorkingDay = strtotime($nextWorkingDayStr.' +1 day');
					$nextWorkingDayStr = date('Y-m-d', $nextWorkingDay);

					if (in_array($nextWorkingDayStr, $this->holidayDates)){
							continue;
					}
					$nextWorkingDayInt = date('N', strtotime($nextWorkingDayStr));  // Monday 1 , Sunday 7
					if ($excludeSunday && $nextWorkingDayInt == 7){
							continue;
					}
					if ($excludeSaturday && $nextWorkingDayInt == 6){
							continue;
					}

					$leadTime --;
			}


			return $nextWorkingDayStr;
	}


}