<?php

namespace App\Imports;

use App\Model\PostalCode;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use App\Model\Zone;
use App\Model\City;
use App\Model\Province;

class PostalCityProvinceImport implements ToModel, SkipsOnError, WithBatchInserts, WithChunkReading
{
    use Importable, SkipsErrors;
    //use Importable;
	
	protected $ZoneToIdMap = array(
		'Z1' => 1, 
		'Z2' => 2, 
		'Z3' => 3, 
		'Z4' => 4, 
	);
	
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {		
		if(!isset($row[1]) || !is_numeric($row[1]))
			return NULL;
		
		//dd($row);
		
		$prop = Province::updateOrCreate(['name' => trim($row[0])]);
		//dd($prop);
		
		$siti = City::updateOrCreate(['name' => trim($row[2])],
                [
					'province_id' => $prop->id, 
				]);
		
		PostalCode::updateOrCreate(['code' => trim($row[1])],
                [
					'city_id' => $siti->id, 
				]);
		
		
		
		return NULL;
		/*
        return new PostalCode([
            //
			'code'     => $row[0],
			'nuskinLeadTime'    => $row[1], 
			'zone_code'     => $row[3],
			'zone_id'    => $row[2], 
        ]);
		*/
    }
    
    public function batchSize(): int
    {
        return 100;
    }
    
    public function chunkSize(): int
    {
        return 100;
    }
	
}