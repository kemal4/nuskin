<?php

namespace App\Imports;

use App\Model\CourierLeadtime;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\Importable;
use DB;

class CourierLeadtimesImport implements ToModel, SkipsOnError
{
    use Importable, SkipsErrors;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
		$asd = DB::table('postal_codes')->select('id')
									->where('code',''.$row[0].'')->first();
		//echo $asd->id;						
		//dd($asd) ;
		//die();
									//->where('code',strval($row[0]))->first(), 
        return new CourierLeadtime([
            //		
			
			'courierLeadTime'     => $row[1],
			'postal_code_id'    => $asd->id, 
			'courier_id'     => 3,
        ]);
		
		// JNE	1
		// FL	3
    }
}
