<?php

namespace App\Imports;

use App\Model\PostalCode;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\Importable;

class PostalCodesImport implements ToModel, SkipsOnError
{
		
    use Importable, SkipsErrors;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PostalCode([
            //
			'code'     => $row[0],
			'nuskinLeadTime'    => $row[1], 
			'zone_code'     => $row[3],
			'zone_id'    => $row[2], 
        ]);
    }
	
}
