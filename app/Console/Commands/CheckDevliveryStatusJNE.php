<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Model\tbl_historyDO;
use App\Model\tbl_delivery_order;

class CheckDevliveryStatusJNE extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delivery:checkStatusJNE';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Delivery Status every xx minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

	        $doList = tbl_delivery_order::select('id','transporter','hawb','is_delivered','fulfillday')
                        ->where('status','onprocess')
                        ->where('courier_id',1)
                        ->where('is_delivered',NULL)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                        ->get();
                        //->where('status','onprocess')

		echo sizeof($doList);


        $baseUrl = 'http://apiv2.jne.co.id:10101/tracing/api/list/v1/cnote/';
        $baseUrl = 'http://apiv2.jne.co.id:10101/tracing/api/list/v1/cnote/';
	$postParams = array(
			'username' => 'NSI',
			'api_key' => '4c6fe00fc93a47d2b77c75deeca7b4a4',
			);

	$date = now();
	$user = 'botAPI'; 

        foreach($doList as $order){

		//$postParams['REF'] = $order->hawb;
		//$postParams['REF'] = '85928974';

            $resp = $this->callHttp($baseUrl.$order->hawb, $postParams);

            if($resp->getStatusCode() == 200){
                        // add logic to add new data into avtivity log , robot / system as PIC



		$jsonBody = json_decode($resp->getBody()->getContents(), true);

		$courierSts = '';
		$receivedBy = '';
                $msgHistory = '';

		if (array_key_exists("cnote",$jsonBody)){

                	$courierSts = trim($jsonBody['cnote']['pod_status']);
                	$receivedBy = $jsonBody['cnote']['last_status'];

                        $msgHistory = '<dl><dt>Courier Status</dt><dd>'.$courierSts.'</dd>';
                        $msgHistory .= '<dt>Status datetime</dt><dd>'.$jsonBody['cnote']['cnote_pod_date'].'</dd>';
                        $msgHistory .= '<dt>Last Status</dt><dd>'.$jsonBody['cnote']['last_status'].'</dd>';

                        $msgHistory .= '<dt>signature</dt><dd><a href="'.$jsonBody['cnote']['signature'].'">signature</a></dd>';
                        $msgHistory .= '<dt>photo</dt><dd><a href="'.$jsonBody['cnote']['photo'].'">photo</a></dd>';

                        $msgHistory .= '</dl>';

//dd($msgHistory);

			echo strcasecmp($courierSts,'DELIVERED');
			if (strcasecmp($courierSts,'DELIVERED') == 0){
			
////DB::enableQueryLog();
				echo 'update : '. $order->id;

				$tempres = tbl_delivery_order::where('id', $order->id)
					->update([
							'is_delivered' => true, 
							'status' => 'delivered',
							'delivered_date' => $date						
						]);

/*
				$tempres = tbl_delivery_order::updateOrCreate(
					['order_id' => $order->id],
					[
                                                        'is_delivered' => true,
                                                        'status' => 'delivered',
                                                        'delivered_date' => $date 
                                                ]

				);
*/

				echo 'res - '.$tempres.' - ';
////$query = DB::getQueryLog();

////$query = end($query);

////print_r($query);
			}
			
			
			if (strcasecmp($courierSts,$order->fulfillday) != 0){		

                                $tempres = tbl_delivery_order::where('id', $order->id)
                                        ->update([
                                                        'fulfillday' => $courierSts
                                                ]);


				tbl_historyDO::create([

					'id_delivery'=> $order->id,
					'date_vendor'=>$date,
					'status'=> $courierSts,
                                        'message'=> $msgHistory,
//					'message'=> $receivedBy,
					'pic'=> $user

				]);
//					'status'=> 'onprocess',

			}
		}
		else {

        	        $courierSts = $jsonBody['Error'];
	                $receivedBy = 'Error';

		
		}

//dd($jsonBody);

            }
        }
    }


        public function callHttp($baseSMSurl, $postParams){
                $client = new Client();

                $response = $client->request('POST', $baseSMSurl, ['form_params' => $postParams]);

                return  $response;

        }

}
