<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Model\tbl_historyDO;
use App\Model\tbl_delivery_order;

class CheckDevliveryStatusFL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delivery:checkStatusFL';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Delivery Status every xx minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

	        $doList = tbl_delivery_order::select('id','transporter','hawb','is_delivered','fulfillday')
                        ->where('status','onprocess')
                        ->where('courier_id',3)
                        ->where('is_delivered',NULL)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                        ->get();
                        //->where('status','onprocess')

		echo sizeof($doList);


        $baseUrl = 'https://ws.firstlogistics.co.id/nuskin/track.php';
	$postParams = array(
			'APPID' => 'NSK634UP',
			'ACCOUNT' => '0700000010',
			'REF' => 'xxxxxxx',
			);

	$date = now();
	$user = 'botAPI'; 

        foreach($doList as $order){

		$postParams['REF'] = $order->hawb;
		//$postParams['REF'] = '85928974';

            $resp = $this->callHttp($baseUrl, $postParams);

            if($resp->getStatusCode() == 200){
                        // add logic to add new data into avtivity log , robot / system as PIC



		$jsonBody = json_decode($resp->getBody()->getContents(), true);

		$courierSts = '';
		$receivedBy = '';

                $msgHistory = '';

		if (array_key_exists("CurrentStatus",$jsonBody)){

                	$courierSts = trim($jsonBody['CurrentStatus']);
                	$receivedBy = trim($jsonBody['ReceivedBy']);

                        $msgHistory = '<dl><dt>Courier Status</dt><dd>'.$courierSts.'</dd>';
                        $msgHistory .= '<dt>Status datetime</dt><dd>'.$jsonBody['track_history'][0]['date_time'].'</dd>';

                        $msgHistory .= '<dt>recipient</dt><dd>'.$receivedBy.'</dd>';

                        $msgHistory .= '</dl>';

//dd($msgHistory);
			echo strcasecmp($courierSts,'DELIVERED');
			if (strcasecmp($courierSts,'DELIVERED') == 0){
			
////DB::enableQueryLog();
				echo 'update : '. $order->id;

				$tempres = tbl_delivery_order::where('id', $order->id)
					->update([
							'is_delivered' => true, 
							'status' => 'delivered',
							'delivered_date' => $date						
						]);
/*
				$tempres = tbl_delivery_order::updateOrCreate(
					['order_id' => $order->id],
					[
                                                        'is_delivered' => true,
                                                        'status' => 'delivered',
                                                        'delivered_date' => $date 
                                                ]

				);
*/

//				echo 'res - '.$tempres.' - ';
////$query = DB::getQueryLog();

////$query = end($query);

////print_r($query);
			}
			
			
			if (strcasecmp($courierSts,$order->fulfillday) != 0){		

                                $tempres = tbl_delivery_order::where('id', $order->id)
                                        ->update([
                                                        'fulfillday' => $courierSts
                                                ]);



				tbl_historyDO::create([

					'id_delivery'=> $order->id,
					'date_vendor'=>$date,
					'status'=> $courierSts,
                                        'message'=> $msgHistory,
					'pic'=> $user

				]);
//					'message'=> 'courierStatus : '.$courierSts.' ; - receivedBy : '.$receivedBy.' ;',

//					'status'=> 'onprocess',

			}
		}
		else {

//        	        $courierSts = $jsonBody['Error'];
	                $receivedBy = 'Error';

		
		}

//dd($jsonBody);

            }
        }
    }


        public function callHttp($baseSMSurl, $postParams){
                $client = new Client();

                $response = $client->request('POST', $baseSMSurl, ['form_params' => $postParams]);

                return  $response;

        }

}
