<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Model\tbl_delivery_order;
use App\Model\Courier;

class SendMailNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:sendMail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send mail notifications to user or customer the day after order created';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		$allCourier = Courier::all();
		$today = Carbon::today();

		foreach ($allCourier as $courier){
			if(!empty($courier->pic_email)){
				
				$allData = tbl_delivery_order::select('hawb','order_id','created_at','eta_nuskin')
							->where('transporter',$courier->common_name)
							->where('courier_id',$courier->display_name)
							->where('status','onprocess')
							->where('eta_nuskin','<', $today)
							//->where('is_preview', '<>', 'yes')
						->where(function($query) {
								$query->whereNull('is_preview')
									->orWhere('is_preview', '');
							})
							->get();
							
				if(!empty($allData) && sizeof($allData) > 0){
								
					$to_name = $courier->display_name;
					$to_email = $courier->pic_email;
					$data = array(
							'name'=>$to_name, 
							'body' => 'There is overdue Deliveries',
							'allData' => $allData
							);
					
					$mail_tos = array_map("trim", explode(',', $courier->pic_email));
							
					Mail::send('emails.testmail', $data, 
						function($message) use ($to_name, $to_email,$mail_tos) {
								$message->subject('Overdue Reminder');
								$message->to($mail_tos);
								//$message->to($to_email, $to_name)->subject('Overdue Reminder');
								$message->cc('okim.garibaldi@gmail.com', $to_name);
								$message->from('nuskin@gmail.com','nuskin');
							}
						
						);
					
				}
			}	
				
		}
    }
}
