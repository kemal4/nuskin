<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Model\tbl_historyDO;
use App\Model\tbl_delivery_order;


class CheckDevliveryStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delivery:checkStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Delivery Status every xx minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function ZZZhandle()
    //public function handle()
    {
	echo Carbon::parse('this month');
	echo  Carbon::parse('first day of this month 00:00:00');
	echo  Carbon::parse('last day of this month 23:59:59');
	echo '<br>';
	echo '==============              ';
	
	$recExist = DB::table('zones')->where('id', 1)->exists();
	echo 'type: '.gettype($recExist).' - value : '.$recExist;
	echo '          =====================               ';
	$recExist = DB::table('zones')->where('id', 12)->doesntExist();
	echo 'type: '.gettype($recExist).' - value : '.$recExist;
	echo '          =====================               ';
	
	//die();
	
    $client = new Client();

    $response = $client->request('POST', 'http://ws.firstlogistics.co.id/', [
			'form_params' => [
				'FUNCTION' => 'track',
				//'FUNCTION' => 'hawbdemo',
				'APPID' => 'STDEMO01',
				'ACCOUNT' => '10000ST',
				'REF' => '85928834'
				//'REF' => '85928834'
				
			]
		]);

    echo $response->getStatusCode();
    echo "<br>";
	echo '<br>';
	echo '\n\r';
    echo $response->getHeaderLine('content-type');
	echo '<br>';
	echo '\n\r';
    echo "<br>";
	echo $response->getBody();
	echo '<br>';
	echo '\n\r';
    echo json_encode($response->getBody());
	echo '<br>';
	echo '\n\r';
    echo json_encode($response->getBody(), JSON_PRETTY_PRINT);
	echo '<br>';
	echo '\n\r';

	
	
	
	/*
	http://ws.firstlogistics.co.id/
		
		FUNCTION		Valid values are : track
		APPID			An Access id to access this API
		ACCOUNT			Your membership account number as our registered customer
		REF				the value could be an AWB number or an order number
	*/
	
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    //public function XXhandle()
    {
        //
	        $doList = tbl_delivery_order::select('id','transporter','hawb','is_delivered')
                        ->where('status','onprocess')
                        ->where('is_delivered',NULL)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                        ->get();
                        //->where('status','onprocess')

		echo sizeof($doList);

		//echo json_encode(tbl_delivery_order::where('id', 1)->get());

        //$baseUrl = [];
        // base url for each vendor is saved in vendor table
        $baseUrl = 'http://nuskin-dev.joranconsulting.com/testkim/vendorSim.php';
        
        echo 'run check status';        

        Log::error('run check status - '.sizeof($doList));

		$date = now();
		echo $date;

        foreach($doList as $order){
                

            $resp = $this->callHttp($baseUrl);

            if($resp->getStatusCode() == 200){
                        // add logic to add new data into avtivity log , robot / system as PIC


				$newStatus = 'onprocess';
			
				if(stripos($resp->getBody() , "delivered") !== false){
/*				
				echo "=== ";
                echo stripos($resp->getBody() , "delivered");
				echo " === ";
                echo stripos($resp->getBody() , "delivered") !== false;
				
*/
					echo " === ";
					echo $resp->getBody();
					echo " === ";
					
					
					
						
                    DB::table('tbl_delivery_order')->where('id',$order->id)
							->update([
										'status' => 'delivered',
										'delivered_date' => $date,
										'is_delivered' => true
									]);

					$newStatus = 'delivered';
					echo "=== DO id : ".$order->id." ===";
                			
					DB::table('tbl_historydo')->insert([

						'id_delivery'=> $order->id,
						'date_vendor'=>$date,
						'status'=>$newStatus,
						'message'=> $resp->getBody(),
						'pic'=> 'system'
			
					]);
	
				}						
								
            }
        }
    }

        public function callHttp($baseSMSurl){
				//echo 'callHttp';
                //$client = new \GuzzleHttp\Client();
                $client = new Client();

                $response = $client->request('GET', $baseSMSurl);

                // http response code / status
                //echo $response->getStatusCode();
                //echo "<br>";
                //echo $response->getHeaderLine('content-type');
                //echo "<br>";
                // response body
                //echo $response->getBody();
                //echo "<br>";

                return  $response;

        }

}
