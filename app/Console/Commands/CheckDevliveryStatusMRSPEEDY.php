<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Model\tbl_historyDO;
use App\Model\tbl_delivery_order;

class CheckDevliveryStatusMRSPEEDY extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delivery:checkStatusMRSPEEDY';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Delivery Status every xx minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

	        $doList = tbl_delivery_order::select('id','transporter','hawb','is_delivered','fulfillday')
                        ->where('status','onprocess')
                        ->where('courier_id',2)
                        ->where('is_delivered',NULL)
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                        ->get();
                        //->where('status','onprocess')

		echo sizeof($doList);


        $baseUrl = 'https://robot.mrspeedy.co/api/business/1.1/deliveries?search_text=';
	$orderUrl = 'https://robot.mrspeedy.co/api/business/1.1/orders?order_id=';
//        $baseUrl = 'https://robotapitest.mrspeedy.co/api/business/1.1/deliveries?search_text=';
//			'X-DV-Auth-Token' => '894C1925F18F2DDD8BC7A2D14D95F8031BAB9AB9'
	$postParams = array(
			'X-DV-Auth-Token' => '6FA0BEB69C939611EA762D0202470542A8D30030'
			);

	$date = now();
	$user = 'botAPI'; 

        foreach($doList as $order){

		//$postParams['REF'] = $order->hawb;
		//$postParams['REF'] = '85928974';

            $resp = $this->callHttp($baseUrl.$order->hawb, $postParams);

            if($resp->getStatusCode() == 200){
                        // add logic to add new data into avtivity log , robot / system as PIC



		$jsonBody = json_decode($resp->getBody()->getContents(), true);

		$courierSts = '';
		$receivedBy = '';
		$searchDelivery = null;
		$msgHistory = '';

//		dd($jsonBody);

		if (array_key_exists("deliveries_count",$jsonBody) && $jsonBody['deliveries_count'] > 0){
//		if (array_key_exists("deliveries",$jsonBody)){

			foreach( $jsonBody['deliveries'] as $currDelivery )
				if(strcasecmp(trim($currDelivery['client_order_id']),trim($order->hawb)) == 0)
					$searchDelivery = $currDelivery;

//			dd($searchDelivery);

                	$courierSts = trim($searchDelivery['status']);
                	$receivedBy = '';
//                	$receivedBy = $jsonBody['ReceivedBy'];

			$msgHistory = '<dl><dt>Courier Status</dt><dd>'.$courierSts.'</dd>';
			$msgHistory .= '<dt>Status datetime</dt><dd>'.$searchDelivery['status_datetime'].'</dd>';

                            $orderResp = $this->callHttp($orderUrl.trim($searchDelivery['order_id']), $postParams);

			    $searchPoint = null;

                            if($orderResp->getStatusCode() == 200){

		                $jsonBody = json_decode($orderResp->getBody()->getContents(), true);

                              foreach( $jsonBody['orders'][0]['points'] as $currPoint )
                                      if(strcasecmp(trim($currPoint['point_id']),trim($searchDelivery['point_id'])) == 0)
                                              $searchPoint = $currPoint;

				if($searchPoint){
					$msgHistory .= '<dt>tracking_url</dt><dd><a href="'.$searchPoint['tracking_url'].'">track</a></dd>';
					$msgHistory .= '<dt>place_photo_url</dt><dd><a href="'.$searchPoint['place_photo_url'].'">photo</a></dd>';
					$msgHistory .= '<dt>recipient_full_name</dt><dd>'.$searchPoint['checkin']['recipient_full_name'].'</dd>';
					$msgHistory .= '<dt>recipient_position</dt><dd>'.$searchPoint['checkin']['recipient_position'].'</dd>';

				}

                            }



			echo strcasecmp($courierSts,'finished');
			if (strcasecmp($courierSts,'finished') == 0){

			
////DB::enableQueryLog();


			 



				echo 'update : '. $order->id;

				$tempres = tbl_delivery_order::where('id', $order->id)
					->update([
							'is_delivered' => true, 
							'status' => 'delivered',
							'delivered_date' => $date						
						]);


/*
				$tempres = tbl_delivery_order::updateOrCreate(
					['order_id' => $order->id],
					[
                                                        'is_delivered' => true,
                                                        'status' => 'delivered',
                                                        'delivered_date' => $date 
                                                ]

				);
*/

				echo 'res - '.$tempres.' - ';
////$query = DB::getQueryLog();

////$query = end($query);

////print_r($query);
			}

			$msgHistory .= '</dl>';
//dd($msgHistory);

			if (strcasecmp($courierSts,$order->fulfillday) != 0){		

                                $tempres = tbl_delivery_order::where('id', $order->id)
                                        ->update([
                                                        'fulfillday' => $courierSts
                                                ]);


				tbl_historyDO::create([

					'id_delivery'=> $order->id,
					'date_vendor'=>$date,
					'status'=> $courierSts,
					'message'=> $msgHistory,
					'pic'=> $user

				]);
//					'message'=> 'courierStatus : '.$courierSts.' ; - receivedBy : '.$receivedBy.' ;',
//					'status'=> 'onprocess',

			}
		}
		else {

//        	        $courierSts = $jsonBody['Error'];
	                $receivedBy = 'Error';

		
		}

//dd($jsonBody);


            }
        }
    }


        public function callHttp($baseSMSurl, $postParams){
                $client = new Client();

                $response = $client->request('GET', $baseSMSurl, ['headers' => $postParams]);

                return  $response;

        }

}
