<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Model\tbl_delivery_order;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SendSMSNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:sendSms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send sms notifications to user or customer the day after order created';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $customerList = tbl_delivery_order::select('order_id','customer_name','phone','transporter','hawb')
                                ->whereDate('created_at','=',Carbon::today())
					->where(function($query) {
							$query->whereNull('is_preview')
								->orWhere('is_preview', '');
						})
                                ->get();
                                //whereDate('created_at','=',Carbon::yesterday())
        $baseSmsUrl = "http://ginger.nadyne.com:21280/smsnadyne/sms.php?username=212nuskinreg&password=nuskin7788&so
urceaddr=Nu+Skin";

        foreach($customerList as $customer){
                $message = 'Yth. '.$customer->customer_name.' , order anda dengan id : '.$customer->order_id.' , sud
ah dalam proses pengiriman melalui  '.$customer->transporter.' dengan nomer resi '.$customer->hawb;

                $this->callHttp($baseSmsUrl."&destinationaddr=".$customer->phone."&shortmessage=(".$message.")");
        }

    }


        public function callHttp($baseSMSurl){
        
                //$client = new \GuzzleHttp\Client();
                $client = new Client();

                $response = $client->request('GET', $baseSMSurl);

                // http response code / status
                echo $response->getStatusCode();
                echo "<br>";
                echo $response->getHeaderLine('content-type'); 
                echo "<br>";
                // response body
                echo $response->getBody(); 
                echo "<br>";    

        }
}
