<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

	$schedule->command('delivery:checkStatusFL')
			//->everyTenMinutes()
			->hourlyAt(1)
			->withoutOverlapping()
			->runInBackground()
			->appendOutputTo(storage_path() . '/logs/schedulerLog.log');

	$schedule->command('delivery:checkStatusMRSPEEDY')
			//->everyTenMinutes()
			->hourlyAt(6)
			->withoutOverlapping()
			->runInBackground()
			->appendOutputTo(storage_path() . '/logs/schedulerLog.log');

	$schedule->command('delivery:checkStatusJNE')
			//->everyTenMinutes()
			->hourlyAt(11)
			->withoutOverlapping()
			->runInBackground()
			->appendOutputTo(storage_path() . '/logs/schedulerLog.log');

//	$schedule->command('inspire')
//		->hourly()
//		->appendOutputTo(storage/logs/schedulerLog.log);


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
