<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Role extends \Spatie\Permission\Models\Role
{
    //
    use LogsActivity;
	
	protected static $logUnguarded = true;
    protected static $logOnlyDirty = true;

    protected $guarded = ['id'];
    protected static $submitEmptyLogs = false;
}
