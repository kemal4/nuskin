<?php

//echo "--------------manggilroutesweb";

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if ( Auth::user() ) {
		return redirect()->route('home');
	} else {
    	return view('auth.login');
	}
});
Route::get('/login2', function () {
    return view('auth.login2');
});

Auth::routes();



Route::group(['middleware' => ['auth', 'verified',
	// 'user.activity'
]], function () { 

	//Dashboard
	Route::get('/dashboard', 'DashboardController@index')->name('home');
	Route::get('/abcd', 'CashboardController@index')->name('abcd');

	Route::get('/deliveries/{status}', 'ResponStatusController@mrspeedy');
	Route::get('/trackdemo/{appid}/{account}/{ref}/{hawb}', 'ResponStatusController@firstlogistic');

	Route::get('/sytem-log', 'DashboardController@systemLog')->name('dashboard.systemlog');
	Route::post('/sytem-log/get-all-json', 'DashboardController@getAllSystemLogJson')->name('dashboard.systemlogJson');
	
	// Send SMS
	Route::get('/sendsms', 'SendController@index');

	// List of Deliveries
	Route::get('/checkDOField/{field?}/{value?}','DeliveryController@checkFieldExist')->name('checkDOField');
	Route::post('/deliveryOrder/allList', 'DeliveryController@getAllListJson')->name('allDeliveries');
	Route::post('/deliveryOrder/ajaxOrderDelivered', 'DeliveryController@ajaxOrderDelivered')->name('ajaxOrderDelivered');
	Route::post('/deliveryOrder/ajaxOrderOverDue', 'DeliveryController@ajaxOrderOverDue')->name('ajaxOrderOverDue');

	// Delivery Order
	Route::group(['middleware' => ['can:view_delivery']], function () {
		Route::get('/delivery-order', 'DeliveryController@index')->name('list-pickup');
		Route::get('delivery-order/detail/{id}', 'DeliveryController@detailPickup')->name('detail_pickup');
		Route::get('/delivery-order/doubleDO/{order_id}', 'DeliveryController@doubleDO')->name('doubleDO');
		Route::get('/delivery-order/filterDO', 'DeliveryController@filterDO')->name('filterDO');	   
		Route::post('/delivery-order/filterDO', 'DeliveryController@filterDO')->name('filterDO');
		//get template for delivery order page, added by Ryan 02/09/2020
		Route::get('/delivery-order/template', 'DeliveryController@getTemplate')->name('delivery.getTemplate');	   
	});


	Route::group(['middleware' => ['can:create_delivery']], function () {
		Route::post('/importExcel', 'DeliveryController@import_excel')->name('importExcel');
		Route::get('delivery-order/add', 'DeliveryController@createlist')->name('createlist');
		Route::post('/createlist/store', 'DeliveryController@store')->name('storedata');
		Route::get('/delivery-order/preview', 'DeliveryController@uploadDataPreview')->name('preview');
		Route::get('/delivery-order/savedo', 'DeliveryController@saveDO')->name('save-do');
		Route::get('/delivery-order/cancelpreviewdo', 'DeliveryController@cancelPreviewDO')->name('cancelPreviewDo');
	});
	
	Route::group(['middleware' => ['can:edit_delivery']], function () {
		Route::get('/delivery-order/edit/{id}', 'DeliveryController@editDO')->name('edit_do');
		Route::post('/delivery-order/saveeditdo/{id}', 'DeliveryController@saveEditDO')->name('saveEditDO');
	});
	


	// LOST Function in Controller
	// Route::get('/previewexcel', 'DeliveryController@previewexcel')->name('previewexcel');
	// Route::get('/saveexcel', 'DeliveryController@saveexcel')->name('saveexcel');
	// Route::post('/createlist', 'DeliveryController@cancelpreview')->name('cancelpreview');
	// Route::post('/delivery-order/status/{id}', 'DeliveryController@manualstatus')->name('statusmanual');


	//Ticket
	
	Route::group(['middleware' => ['can:view_ticket_delivery']], function () {
		Route::get('/complain-ticket/delivery/{id?}', 'TicketController@index')->name('listticket');
		Route::get('/complain-ticket/non-delivery/{id?}', 'TicketController@nonDVIndex')->name('ticket.nonDVListticket');
		Route::get('/ticket/release/{id?}', 'TicketController@release')->name('releaseticket');
	});
	
	Route::group(['middleware' => ['can:edit_ticket_delivery']], function () {
		Route::get('/ticket/list-ticket/filterTicket', 'TicketController@filterTicket')->name('filterTicket');
		Route::get('/detailticketdo/{order_id}', 'TicketController@detailcomplaindo')->name('detailcomplaindo');
		Route::post('/storeeditticket/{id}', 'TicketController@storeeditticket')->name('storeeditticket');
		Route::get('/complain-ticket/reopenticket/{id}', 'TicketController@reopenticket')->name('re-open');
	});
	
	Route::get('/complain-ticket/detail/{id}', 'TicketController@detailticket')->name('detailticket');
	
	Route::group(['middleware' => ['can:create_ticket_delivery']], function () {
		Route::get('/complain-ticket/add/{id?}', 'TicketController@ticket')->name('newticket');
		Route::post('/ticket/addticket', 'TicketController@storeticket')->name('addticket');
		Route::get('/getticketbyso/{id}','TicketController@getsoDetail')->name('getsodetail');
	});

	Route::post('/complain-ticket/allList', 'TicketController@getAllListJson')->name('allTicket');
	Route::post('/complain-ticket/all-not-solved-list','TicketController@getAllNotResolvedJson')->name('allNotSolvedTicket');

	Route::get('/nondelivery','TicketController@nondelivticket')->name('nondeliv');

	//Performance
	// Route::get('/performance-vendor', 'PerfomVendorController@index')->name('performance_vendor');
	Route::get('/performance-vendor/export', 'PerfomVendorController@exportVD')->name('export_vendor');
	Route::get('/performance-vendor/detail/{id}', 'PerfomVendorController@detailVD')->name('vendordetail');
	Route::get('/performance-vendor/filterZN', 'PerfomVendorController@filterVD')->name('filterzona');

	// Performance version 2
	Route::get('/performance/leadtime', 'Performance\PerformanceController@index')->name('performance.leadtime');
	Route::get('/performance/leadtime/download/{courier}/{courier_id}/{startDate}/{endDate}', 'Performance\PerformanceController@downloadUnfulfilled')->name('performance.leadtime.provider.download');
	Route::post('/performance/leadtime', 'Performance\PerformanceController@index')->name('performance.leadtime');
	Route::get('/performance/leadtime/provider/{pid}/{startDate?}/{endDate?}', 'Performance\PerformanceController@providerLeadTime')->name('performance.leadtime.provider');
	Route::get('/performance/leadtime/zone/{pid}/{zid}/{startDate?}/{endDate?}', 'Performance\PerformanceController@zoneLeadTime')->name('performance.leadtime.zone');

	Route::get('/performance/successrate', 'Performance\PerformanceController@successRate')->name('performance.successrate');
	Route::post('/performance/successrate', 'Performance\PerformanceController@successRate')->name('performance.successrate');
	Route::get('/performance/successrate/download/{courier}/{courier_id}/{startDate}/{endDate}', 'Performance\PerformanceController@downloadUndelivered')->name('performance.successrate.provider.download');
	Route::get('/performance/successrate/provider/{pid}', 'Performance\PerformanceController@providerSuccessRate')->name('performance.successrate.provider');
	Route::get('/performance/successrate/zone/{pid}/{zid}', 'Performance\PerformanceController@zoneSuccessRate')->name('performance.successrate.zone');

	Route::get('/checkDOField/{field?}/{value?}','DeliveryController@checkFieldExist')->name('checkDOField');


	
	Route::resource('/settings/postalcodes','PostalCodeController');
	Route::get('/settings/postalcodes/check-code/{code}','PostalCodeController@checkCode')->name('postalCode.checkCode');

	Route::resource('/settings/zones','ZoneController');
	Route::resource('/settings/stores','StoreController');
	Route::resource('/settings/provinces','ProvinceController');
	Route::resource('/settings/cities','CityController');
	Route::resource('/settings/holidays','HolidayController');
	Route::resource('/settings/nuskinstatus','NuskinStatusController');
	Route::resource('/settings/couriers','CourierController');
	Route::get('/settings/couriers/change-active/{id}', 'CourierController@changeActive')->name('courier.change'); //Post and Unpost

	Route::resource('/settings/courierstatus','CourierStatusController');
	Route::post('/settings/postalcodes/importExcel', 'PostalCodeController@import_excel')->name('importExcelPostal');
	Route::post('/settings/postalcodes/importExcelCourier', 'PostalCodeController@import_excel_courier')->name('importExcelCourier');
	Route::post('/settings/postalcodes/importExcelPostalCityProvince', 'PostalCodeController@import_excel_postalcityprovince')->name('importExcelPostalCityProvince');

	Route::get('/detailticket/{id}/{pid?}', 'TicketController@detailticket')->name('detailticket2');

    Route::resource('/settings/roles','RoleController');
    Route::resource('/settings/users','UserController');
	Route::get('/user-profile/{id}', 'UserController@userprofile')->name('profileuser');
	Route::post('/user-profile/{id}', 'UserController@updateprofile')->name('profileupdate');
    

	//User
	// Route::get('/user-menu', 'UserController@index')->name('usermenu');
	// Route::get('/user-menu/create', 'UserController@create')->name('usercreate');
	// Route::post('/user-menu/store', 'UserController@store')->name('userstore');
	// Route::get('/user-menu/edit/{id}', 'UserController@edit')->name('userdedit');
	// Route::get('/user-menu/destroy/{id}', 'UserController@destroy')->name('userdestroy');
	// Route::post('/user-menu/update/{id}', 'UserController@update')->name('userupdate');

	//Menu
	Route::get('/kelolamenu', 'KelolaMenu@index')->name('kelolamenu');
	Route::get('/kelolamenu/addMenu', 'KelolaMenu@addMenu')->name('add_menu_list');

	Route::get('/settings/topic', 'KelolaMenu@listtopics')->name('kelolatopics');
	Route::get('/settings/topic/add', 'KelolaMenu@addtopics')->name('createtopics');
	Route::post('/settings/storetopic', 'KelolaMenu@storetopics')->name('storetopics');
	Route::get('/settings/topic/edit/{id}', 'KelolaMenu@edittopics')->name('edittopics');
	Route::post('/settings/edittopic/{id}', 'KelolaMenu@storeedit')->name('edittopic');
	Route::get('/settings/deletetopic/{id}', 'KelolaMenu@destroy')->name('deletetopics');

	//kelolakodepos
	Route::get('/kelolakodepos', 'KelolaMenu@indexpos')->name('kelolakodepos');
	Route::post('/kelolakodepos/importExcel', 'KelolaMenu@importexcel')->name('importzip');
	Route::get('/kelolakodepos/createkode', 'KelolaMenu@createkode')->name('createkode');
	Route::post('/kelolakodepos/add-kode', 'KelolaMenu@addkode')->name('add-kode');
	Route::get('/kelolakodepos/editkode/{id}', 'KelolaMenu@nameeditkode')->name('kodeposedit');
	Route::post('/kelolakodepos/storeeditkode/{id}', 'KelolaMenu@storeeditkode')->name('storeeditkode');
	Route::get('/kelolakodepos/deletekode/{id}', 'KelolaMenu@destroykode')->name('destroykode');

	//Warehouse
	// Route::get('/settings/warehouse', 'KelolaRole@index')->name('kelolarole');
	// Route::get('/settings/warehouse/add', 'KelolaRole@gudang')->name('newgudang');
	// Route::post('/settings/addgudang', 'KelolaRole@addGudang')->name('add-gudang');
	// Route::get('/settings/warehouse/edit/{id_gudang}', 'KelolaRole@editgudang')->name('editgudangs');
	// Route::post('/settings/editgudang/{id_gudang}', 'KelolaRole@storegudang')->name('editgudang');
	// Route::get('/settings/delete/{id_gudang}', 'KelolaRole@destroy')->name('deletegudang');

	Route::get('/settings/warehouse', 'WarehouseController@index')->name('kelolarole');
	Route::get('/settings/warehouse/add', 'WarehouseController@gudang')->name('newgudang');
	Route::post('/settings/addgudang', 'WarehouseController@addGudang')->name('add-gudang');
	Route::get('/settings/warehouse/edit/{id_gudang}', 'WarehouseController@editgudang')->name('editgudangs');
	Route::post('/settings/editgudang/{id_gudang}', 'WarehouseController@storegudang')->name('editgudang');
	Route::delete('/settings/warehouse/delete/{id_gudang}', 'WarehouseController@destroy')->name('deletegudang');
	Route::post('settings/warehouse/getAllListJson', 'WarehouseController@getAllListJson')->name('warehouse.getAllListJson');

	Route::post('settings/warehouse/get-warehouse', 'WarehouseController@getWarehouse')->name('warehouse.getWarehouse');

	//Vendor
	Route::get('/listvendor', 'ListVendor@index')->name('list-vendor');

	//Zonasi
	Route::get('/list-zonasi', 'ListZonasi@index')->name('listzonasi');
	Route::get('/home/list-zonasi/create-zonasi', 'ListZonasi@addZonasi')->name('listzonasi.create');
	Route::post('/home/list-zonasi/store-zonasi', 'ListZonasi@storeZonasi')->name('listzonasi.add');
	Route::get('/home/list-zonasi/edit-zonasi/{id}', 'ListZonasi@editZonasi')->name('listzonasi.edit');
	Route::post('/home/list-zonasi/store-edit/{id}', 'ListZonasi@storeedit')->name('listzonasi.storeedit');
	Route::get('/home/list-zonasi/destroy-zonasi/{id}', 'ListZonasi@destroyZonasi')->name('listzonasi.delete');

	//Master Vendor
	Route::get('/master-vendor', 'MasterVendor@index')->name('list-mastervendor');
	Route::get('/master-vendor/create-mastervendor', 'MasterVendor@addVendor')->name('list-mastervendor.create');
	Route::post('/master-vendor/store-mastervendor', 'MasterVendor@storeVendor')->name('list-mastervendor.add');
	Route::get('/master-vendor/edit-mastervendor/{id}', 'MasterVendor@editVendor')->name('list-mastervendor.edit');
	Route::post('/master-vendor/store-edit/{id}', 'MasterVendor@storeedit')->name('list-mastervendor.storeedit');
	Route::get('/master-vendor/destroy-mastervendor/{id}', 'MasterVendor@destroyMasterVendor')->name('list-mastervendor.delete');

	//Kelola Vendor
	Route::get('/kelola-vendor', 'KelolaVendor@index')->name('list-kelolavendor');
	Route::get('/kelola-vendor/create-kelolavendor', 'KelolaVendor@addVendor')->name('list-kelolavendor.create');
	Route::post('/kelola-vendor/store-kelolavendor', 'KelolaVendor@storeVendor')->name('list-kelolavendor.add');
	Route::get('/kelola-vendor/edit-kelolavendor/{id}', 'KelolaVendor@editVendor')->name('list-kelolavendor.edit');
	Route::post('/kelola-vendor/store-edit/{id}', 'KelolaVendor@storeedit')->name('list-kelolavendor.storeedit');
	Route::get('/kelola-vendor/destroy-kelolavendor/{id}', 'KelolaVendor@destroyKelolaVendor')->name('list-kelolavendor.delete');

	//Post
	Route::group(['middleware' => ['can:view_setting']], function () {
		//Route::get('/sales', 'SalesController@index')->name('salesList');
		//Route::post('/sales/store', 'SalesController@store')->name('salesStore');
		Route::resource('/post','PostController');
		Route::get('/post/change-posted/{id}', 'PostController@changePosted')->name('post.change'); //Post and Unpost
		//Route::get('delivery-order/detail/{id}', 'DeliveryController@detailPickup')->name('detail_pickup');
		//Route::get('/delivery-order/doubleDO/{order_id}', 'DeliveryController@doubleDO')->name('doubleDO');
		//Route::get('/delivery-order/filterDO', 'DeliveryController@filterDO')->name('filterDO');	   
		//Route::post('/delivery-order/filterDO', 'DeliveryController@filterDO')->name('filterDO');	   
	});

	// List of Post
	Route::post('/post/allList', 'PostController@getAllListJson')->name('allPost');

	



});
