<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/deliveries/{status}', 'ResponStatusController@mrspeedy');
Route::get('/trackdemo/{appid}/{account}/{ref}/{hawb}', 'ResponStatusController@firstlogistic');


// Send SMS
Route::get('/sendsms', 'SendController@index');


// List of Deliveries
Route::get('/checkDOField/{field?}/{value?}','DeliveryController@checkFieldExist')->name('checkDOFieldApi');
Route::post('/deliveryOrder/allList', 'DeliveryController@getAllListJson')->name('allDeliveriesApi');
Route::post('/deliveryOrder/ajaxOrderDelivered', 'DeliveryController@ajaxOrderDelivered')->name('ajaxOrderDeliveredApi');
Route::post('/deliveryOrder/ajaxOrderOverDue', 'DeliveryController@ajaxOrderOverDue')->name('ajaxOrderOverDueApi');


// Delivery Order
Route::get('/delivery-order', 'DeliveryController@index')->name('list-pickupApi');
Route::get('delivery-order/detail/{id}', 'DeliveryController@detailPickup')->name('detail_pickupApi');
Route::get('/delivery-order/doubleDO/{order_id}', 'DeliveryController@doubleDO')->name('doubleDOApi');
Route::get('/delivery-order/filterDO', 'DeliveryController@filterDO')->name('filterDOApi');
Route::post('/delivery-order/filterDO', 'DeliveryController@filterDO')->name('filterDOApi');
Route::get('/delivery-order/template', 'DeliveryController@getTemplate')->name('delivery.getTemplateApi');

Route::post('/importExcel', 'DeliveryController@import_excel')->name('importExcelApi');
Route::get('delivery-order/add', 'DeliveryController@createlist')->name('createlistApi');
Route::post('/createlist/store', 'DeliveryController@store')->name('storedataApi');
Route::get('/delivery-order/preview', 'DeliveryController@uploadDataPreview')->name('previewApi');
Route::get('/delivery-order/savedo', 'DeliveryController@saveDO')->name('save-doApi');
Route::get('/delivery-order/cancelpreviewdo', 'DeliveryController@cancelPreviewDO')->name('cancelPreviewDoApi');

Route::get('/delivery-order/edit/{id}', 'DeliveryController@editDO')->name('edit_doApi');
Route::post('/delivery-order/saveeditdo/{id}', 'DeliveryController@saveEditDO')->name('saveEditDOApi');


//Ticket
Route::get('/complain-ticket/{id?}', 'TicketController@index')->name('listticketApi');
Route::get('/ticket/release/{id?}', 'TicketController@release')->name('releaseticketApi');
//Route::get('/ticket/list-ticket/filterTicket', 'TicketController@filterTicket')->name('filterTicketApi');
//Route::get('/detailticketdo/{order_id}', 'TicketController@detailcomplaindo')->name('detailcomplaindoApi');
Route::post('/storeeditticket/{id}', 'TicketController@storeeditticket')->name('storeeditticketApi');
Route::get('/complain-ticket/reopenticket/{id}', 'TicketController@reopenticket')->name('re-openApi');
Route::get('/complain-ticket/detail/{id}', 'TicketController@detailticket')->name('detailticketApi');

Route::get('/complain-ticket/add/{id?}', 'TicketController@ticket')->name('newticketApi');
Route::post('/ticket/addticket', 'TicketController@storeticket')->name('addticketApi');
Route::get('/getticketbyso/{id}','TicketController@getsoDetail')->name('getsodetailApi');


//Performance
Route::get('/performance-vendor/export', 'PerfomVendorController@exportVD')->name('export_vendorApi');
Route::get('/performance-vendor/detail/{id}', 'PerfomVendorController@detailVD')->name('vendordetailApi');
Route::get('/performance-vendor/filterZN', 'PerfomVendorController@filterVD')->name('filterzonaApi');