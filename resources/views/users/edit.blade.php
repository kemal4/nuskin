@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Edit User</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('users.index')}}">Users Management</a></li>
                <li class="breadcrumb-item active">Edit User</li>
              </ol>
            </div>
          </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        @if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                               @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                               @endforeach
                            </ul>
                          </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">{!! session('error') !!}</div>
                        @endif
                        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], 'files'=> true]) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Name</strong>
                                    {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Email</strong>
                                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Photo</strong>
                                    <br>
                                    @if($user->photo_path)
                                    <img alt="{{$user->name}}" class="img img-size-128 mr-2" src="{{ asset($user->photo_path) }}">
                                    @endif
                                    {!! Form::file('image', null, array('class' => 'form-control')) !!}
                                    {!! Form::hidden('photo_path', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Password</strong>
                                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Confirm Password:</strong>
                                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Role</strong>
                                    <div class="col-xs-12">
                                    @foreach($roles  as $role)
                                        <label>{{ Form::checkbox('roles[]', $role , in_array($role, $namerole) ? true : false, array('class' => 'name')) }}
                                        {{ $role  }}</label>
                                        @error('roles')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    @endforeach
                                    </div>
                                    {{-- <select name="roles" id="topic" class="form-control @error('roles') is-invalid @enderror select2" style="width: 100%;" value="{{ old('roles') }}">
                                            <option value="{{ $namerole }}" hidden class="form-control  @error('roles') is-invalid @enderror">{{ $namerole }}</option>
                                        @foreach($roles  as $role)
                                                <option value="{{ $role }}">{{ $role }}</option>
                                          @error('roles')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                        @endforeach
                                    </select> --}}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card-footer">
                                    <div class="row">
                                    <a href="{{ route('users.index')}}" class="btn btn-default mr-2 mb-2">Cancel</a>
                                    <button type="submit" class="btn btn-info mb-2"> Submit </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
@endsection