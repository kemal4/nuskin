@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">
    <section class="p-2 pb-0">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <a href="{{ route('users.index')}}" class="text-muted"><i class="fas fa-long-arrow-alt-left"></i> Back to list</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Content Header (Page header) -->
    <section class="content-header pt-0">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Create User</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('users.index')}}">Users Management</a></li>
                    <li class="breadcrumb-item active">Create User</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-9">
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <p></p><strong>Whoops!</strong> There were some problems with your input.</p>
                  <ul>
                     @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                     @endforeach
                 </ul>
              </div>
              @endif
              @if (session('error'))
                <div class="alert alert-danger">{!! session('error') !!}</div>
              @endif
              <div class="card">
                {!! Form::open(array('route' => 'users.store','method'=>'POST', 'files'=> true)) !!}
                    <div class="card-body">
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Name</strong>
                              {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Email</strong>
                              {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Photo</strong>
                              {!! Form::file('image', array('class' => 'form-control')) !!}
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Password</strong>
                              {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Confirm Password</strong>
                              {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Role</strong>
                              <div class="col-xs-12">
                              @foreach($roles  as $role)
                                  <label>{{ Form::checkbox('roles[]', $role , false, array('class' => 'name')) }}
                                  {{ $role  }}</label>
                                  @error('roles')
                                      <div class="invalid-feedback">{{ $message }}</div>
                                  @enderror
                              @endforeach
                              </div>
                              {{-- <select name="roles" id="topic" class="form-control @error('roles') is-invalid @enderror select2" style="width: 100%;" value="{{ old('roles') }}">
                                <option value="" selected disabled hidden class="form-control  @error('roles') is-invalid @enderror">Select Roles</option>
                                  @foreach($roles  as $role)
                                    <option value="{{ $role }}">{{ $role }}</option>
                                    @error('roles')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                  @endforeach
                              </select> --}}
                          </div>
                      </div>
                    
                    </div>
                    <div class="card-footer">
                            <a href="{{ route('users.index')}}" class="btn btn-default mr-2 mb-2"><i class="fas fa-times"></i> Cancel</a>
                            <div class="float-right"><button type="submit" class="btn btn-info mb-2"> Submit </button></div>
                    </div>
                {!! Form::close() !!}
              </div>
            </div>
        </div>
      </div>
    </section>
</div>

@endsection
