@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Users Management</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
            <li class="breadcrumb-item">Settings</li>
            <li class="breadcrumb-item active">Users Management</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
      <div class="container">
          <div class="row justify-content-md-center">
              <div class="col-12 col-md-10">
                  <div class="card">
                      <div class="card-header">
                        @if (session('error'))
                          <div class="alert alert-danger">{{ session('error') }}</div>
                        @endif
                          @can('create_setting')
                          <a href="{{ route('users.create') }}" class="btn btn-primary mr-3">
                            <i class="fas fa-plus mr-1"></i>Create User
                          </a>
                          @endcan
                      </div>
                      <div class="card-body">
                          @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                              <button type="button" class="close" data-dismiss="alert">×</button>
                              <p style="color: #fff;">{{ $message }}</p>
                          </div>
                          @endif
  
                          <table id="usermanage" class="table" style="border:1px solid #ddd">
                              <thead class="thead-light">
                                  <tr>
                                      <th>User Name</th>
                                      <th>Email</th>
                                      <th>Roles</th>
                                      <th width="100px" class="text-center">Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @php $i=0
                                  @endphp
                                  @foreach ($data as $key => $user)
                                  <tr>
                                      <td>
                                          @if($user->photo_path)
                                          <img alt="{{$user->name}}" class="img-circle img-size-32 mr-2" src="{{ asset($user->photo_path) }}">
                                          <a href="#"><strong>{{ $user->name }}</strong></a>
                                          @else
                                          <img alt="{{$user->name}}" class="img-circle img-size-32 mr-2" src="{{ asset('image/avatar/avatar6.png') }}">
                                          <a href="#"><strong>{{ $user->name }}</strong></a>
                                          @endif
                                      </td>
                                      <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                      <td>
                                          @if(!empty($user->getRoleNames())) @foreach($user->getRoleNames() as $v)
                                          <label class="badge badge-secondary">{{ $v }}</label> @endforeach @endif
                                      </td>
                                      <td class="text-right">
                                          <a class="btn btn-success btn-xs" href="{{ route('users.show',$user->id) }}" data-toggle='tooltip' title='Detail'><i class="fas fa-info-circle"></i></a> @can('edit_setting')
                                          <a class="btn btn-warning btn-xs" href="{{ route('users.edit',$user->id) }}" data-toggle='tooltip' title='Edit'><i class="fas fa-edit"></i></a> @endcan @can('delete_setting') {!! Form::open(['method' => 'DELETE','route'
                                          => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                          <button type="submit" class="btn btn-danger btn-xs" data-toggle='tooltip' title='Delete' onclick="myFunction()"><i class="fas fa-trash"></i></button> {!! Form::close() !!} @endcan
                                      </td>
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
  
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
</div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(function () {
      $('#usermanage').DataTable();
    });

    function myFunction() {
      confirm("Are you sure you want to delete this user?");
    }
  </script>
@endsection
