@extends('layoutAdmin.global')

@section('content')
<style type="text/css">
[type="file"] {
	height: 0;
	overflow: hidden;
	width: 0;
}

[type="file"] + label {
	background: #99c793;
	border: none;
	border-radius: 5px;
	color: #fff;
	cursor: pointer;
	display: inline-block;
	font-family: 'Poppins', sans-serif;
	font-size: inherit;
	font-weight: 600;
	margin-bottom: 1rem;
	outline: none;
	padding: 1rem 50px;
	position: relative;
	transition: all 0.3s;
	vertical-align: middle;

	&:hover {
	background-color: darken(#99c793, 10%);
	}
}

</style>
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Profile</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Profile</li>
              </ol>
            </div>
          </div>
        </div>
    </section>

	<div class="col-md-12">
		@if ($message = Session::get('success'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<p style="color: #fff;">{{ $message }}</p>
		</div>
		@endif
		@error('image')
		    <div class="alert alert-danger">
		  	<button type="button" class="close" data-dismiss="alert">×</button>
		    <p>{{ $message }}</p>
		    </div>
		@enderror
		<div class="alert alert-danger d-none" id="alert">
		  	<button type="button" class="close" data-dismiss="alert">×</button>
		    <p id="warning"> </p>
		</div>
	</div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary" style="height: 462px;">
	              <div class="card-body box-profile" style="padding-top: 150px;">
	                <div class="text-center">
	                   	<img alt="{{$user->name}}" class="profile-user-img img-fluid img-circle" src="{{ asset($user->photo_path) }}">
	                </div>

	                <h3 class="profile-username text-center">{{ $user->name }}</h3>

	                <p class="text-muted text-center">{{ $user->email }}</p>
	              </div>
	              <!-- /.card-body -->
	        </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-body" style="height: 462px;">
              	   {!! Form::model($user, ['method' => 'POST','route' => ['profileupdate', $user->id], 'files'=> true]) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Photo</strong>
                                    <br>
                                    @if($user->photo_path)
                                    <img alt="{{$user->name}}" id="img" class="img img-size-128 mr-2" src="{{ asset($user->photo_path) }}" width="200px" />
				            		<img src="" id="profile-img-tag" width="200px" />
                                    @endif
                                    {!! Form::hidden('photo_path', null, array('class' => 'form-control')) !!}
                                    <input type="file" id="file" name="image" class="mt-1" onchange="filename()"/>
									<label for="file" class="btn-2">upload</label>
				                <small class="d-block mt-1" style="color: red;" id="maxupload">Max. 2MB</small>
				                <small class="d-block mt-1" id="listname"></small>
				            </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Password</strong>
                                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Confirm Password:</strong>
                                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card-footer">
                                    <div class="row">
                                    <a href="{{ route('home')}}" class="btn btn-default mr-2 mb-2">Cancel</a>
                                    <button type="submit" class="btn btn-info mb-2"> Submit </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
              </div>

            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

</div>
@endsection

@section('scripts')
<script type="text/javascript">

	function filename() {
		var filesize = document.getElementById("file").files[0].size;

		if (filesize > 2097152) {
			var element = document.getElementById("alert");
			element.classList.remove("d-none");
			document.getElementById("warning").innerHTML = "File Size Too large (max 2mb)!";
			
			// return filesize;
		} else {
			// alert(filesize +" bites\nYou are good to go!");

			var fullname = document.getElementById('file').value;

				if (fullname) {
					var startIndex = (fullname.indexOf('\\') >= 0 ? fullname.lastIndexOf('\\') : fullname.lastIndexOf('/'));
				    var filename = fullname.substring(startIndex);
				    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				        filename = filename.substring(1);
				    }
			    	document.getElementById("maxupload").remove();
					document.getElementById("listname").innerHTML = filename;
				}
		}
	};

	function imagePreview(input) {	
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	            $('#profile-img-tag').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }

    }

    $("#file").change(function(){
	    document.getElementById("img").remove();
        imagePreview(this);
    });

</script>
@endsection