<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- 
  <link rel="shortcut icon" href="{{URL::asset('image/logo/tab_xs')}}" />
  -->
  <link rel="shortcut icon" href="{{URL::asset('image/logo_simple_2.svg')}}" />
  <title>NuSkin</title>
  <!-- CSRF Token -->
  <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
  @include('layoutAdmin.stylesheets')
</head>
  <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed sidebar-collapse">
    <div class="wrapper">
      <!-- Site wrapper -->
      @include('layoutAdmin.nav-top')

      
      @include('layoutAdmin.sidebar')   
      
      <!-- @include('layoutAdmin.alert-general')      -->

      @yield('content')

      @include('layoutAdmin.footer')

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
  <!-- /.control-sidebar -->

    <!-- ./wrapper -->
    @include('layoutAdmin.scripts')
  </body>
</html>
