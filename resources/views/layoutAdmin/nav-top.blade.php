  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
				<li class="nav-item dropdown">
				  <a class="nav-link" data-toggle="dropdown" href="#">
					<i class="far fa-bell"></i>
					@if($bellNotificationTotal = Session::get('bellNotificationTotal'))
					<span class="badge badge-danger navbar-badge">{{$bellNotificationTotal}}</span>
					@endif
				  </a>				  
				@if($bellNotification = Session::get('bellNotification'))					
					@if(!empty($bellNotificationTotal))
				  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
					<span class="dropdown-header">{{$bellNotificationTotal}} Notifications</span>
					
					@if($bellNotificationDOTotal = Session::get('bellNotificationDOTotal'))
					<div class="dropdown-divider"></div>
					<a href="{{ route('list-pickup')}}" class="dropdown-item">
					  <i class="fa fa-truck mr-2"></i> {{$bellNotificationDOTotal}} overdue Delivery Order<?=($bellNotificationDOTotal=='1')?'' : 's' ?>
					  <span class="float-right text-muted text-sm"></span>
					</a>
					@endif
					@if($bellNotificationTicketTotal = Session::get('bellNotificationTicketTotal'))					
					<div class="dropdown-divider"></div>
					<a href="{{ route('listticket')}}" class="dropdown-item">
					  <i class="fas fa-ticket-alt mr-2"></i> {{$bellNotificationTicketTotal}} overdue Complaint Ticket<?=($bellNotificationTicketTotal=='1')?'' : 's' ?>
					  <span class="float-right text-muted text-sm"></span>
					</a>
					@endif
					
					<!-- other notification
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item">
					  <i class="fas fa-file mr-2"></i> 3 new reports
					  <span class="float-right text-muted text-sm">2 days</span>
					</a>
					<div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
					-->
				  </div>
					@endif
				
				@endif				
				</li>

            <li class="nav-item">
              <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fas fa-power-off"></i>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
            @endguest
        </ul>
      </div>
    </ul>
  </nav>
  <!-- /.navbar -->