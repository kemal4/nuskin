
@isset($alertGeneral)

<div class='container'>

	@isset($alertGeneral['warning'])	
	
	<div class="alert alert-warning alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>{{ $alertGeneral['warning']['title'] }}</strong> {{ $alertGeneral['warning']['message'] }}
	</div>

	@endisset

	@isset($alertGeneral['danger'])
	
	<div class="alert alert-danger alert-dismissible">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>{{ $alertGeneral['danger']['title'] }}</strong> {{ $alertGeneral['danger']['message'] }}
	</div>
	
	@endisset
</div>

@endisset