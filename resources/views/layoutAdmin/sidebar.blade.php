<?php
  $p = substr(url()->current(), strrpos(url()->current(), '/') + 1);

  $url = Request::segment(1);
  
  //echo ' ================================================ p :'.$p;
  //echo ' ================================================ url :'.$url;
  
  /*
  $logosvg = 'image/logo_only_'.mt_rand(1, 5).'.svg';
  
  $logoColors = array(
        'E11F8F',
        'C1D32F',
        '462E8D',
        'AB4399',
        'FCD602',
        '00B3E2',
        'FED602'
      );
  $logoColor = $logoColors[mt_rand(0, 6)];
  */

  $user = Auth::user()->id;

  $logosvg = 'image/logo/logo_only.svg';
  $logoColor = '008AB0';

?>

<!-- Main Sidebar Container -->
<!-- 
  <aside class="main-sidebar sidebar-dark-primary elevation-4 sidebar-no-expand">
-->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Brand Logo -->
<!--
    <a href="{{ route('home') }}" class="brand-link" style="background: #333">
      <center>
    <img src="{{URL::asset('image/NewLogo1.png')}}" alt="NUSKIN"  style="width:100%;">
    </center>
    </a>  
    <img src="{{URL::asset('image/NewLogo1_logo.png')}}" alt="NUSKIN" class="brand-image-xl logo-xs" >
    <img src="{{URL::asset('image/NewLogo1_edit.png')}}" alt="NUSKIN" class="brand-image-xs logo-xl" >
    <img src="{{URL::asset('image/logo.svg')}}" alt="NUSKIN" class="brand-image-xs logo-xl" >
    
    <img src="{{URL::asset('image/logo_simple.svg')}}" alt="NUSKIN" class="brand-image-xl logo-xs" >
    <img src="{{URL::asset('image/NewLogo1_edit.png')}}" alt="NUSKIN" class="brand-image-xs logo-xl" >
    
    <img src="{{URL::asset('image/logo_simple.svg')}}" alt="NUSKIN" class="brand-image-xl logo-xs" >
    <div class="brand-image-xs logo-xl">
      <img src="{{URL::asset('image/logo_simple.svg')}}"  alt="NUSKIN">
-->
      <!--<span class="brand-text font-weight-light">NUSKIN</span>-->
    
<!--
    <img src="{{URL::asset($logosvg)}}" alt="NUSKIN" class="brand-image-xl logo-xs" >
    <div class="brand-image-xs logo-xl">
      <img src="{{URL::asset($logosvg)}}"  alt="NUSKIN">
      <img src="{{URL::asset('image/logo_lockup.svg')}}"  alt="NUSKIN">   
    </div>
-->   
    
    <a href="{{ route('home') }}" class="brand-link logo-switch" style="background: #333;">
      <center>
    <div class="brand-image-xl logo-xs" >     
                <svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g transform="translate(-801.000000, -12385.000000)" fill="#<?= $logoColor ?>">
                            <g transform="translate(801.000000, 12385.000000)">
                                <g>
                                    <g fill-rule="nonzero">
                                        <path class="dynamic-fill" d="M31.1082803,10.3076923 C31.1082803,10.3076923 34.7006369,10.1282051 37.4012739,13.1282051 C38.2929936,14.1282051 39.1592357,15.4102564 39.6433121,17.2051282 C38.3694268,7.51282051 30.089172,0 20.0509554,0 C10.0382166,0 1.73248408,7.51282051 0.433121019,17.2307692 C0.917197452,15.4358974 1.78343949,14.1538462 2.67515924,13.1538462 C5.40127389,10.1538462 8.96815287,10.3333333 8.96815287,10.3333333 C17.8853503,10.7948718 17.9363057,19.8205128 17.9363057,19.8205128 L17.9363057,39.7179487 C18.343949,39.7692308 18.7770701,39.7948718 19.2101911,39.8205128 L19.2101911,18.5384615 C19.2101911,15.6410256 18.3184713,12.8205128 16.3057325,10.8205128 C14.4713376,8.97435897 12.1019108,7.94871795 9.60509554,7.8974359 C10.6751592,6.02564103 12.1528662,4.46153846 13.910828,3.35897436 C15.7707006,2.20512821 17.8853503,1.58974359 20.0509554,1.58974359 C22.2165605,1.58974359 24.3566879,2.20512821 26.2165605,3.35897436 C27.9745223,4.46153846 29.4522293,6.02564103 30.522293,7.8974359 C28.0254777,7.94871795 25.6305732,8.97435897 23.7961783,10.8205128 C21.7834395,12.8205128 20.866242,15.6410256 20.866242,18.5384615 L20.866242,39.7692308 C21.2993631,39.7435897 21.7070064,39.7179487 22.1401274,39.6666667 L22.1401274,19.7948718 C22.1401274,19.7948718 22.1910828,10.7692308 31.1082803,10.3076923 M31.9235669,21.0512821 C31.9235669,21.0512821 30.5732484,21.0769231 29.4522293,21.7948718 C28.1019108,22.6410256 26.7006369,24.3589744 26.7006369,27.8974359 L26.7006369,38.6410256 C30.6751592,37.2051282 34.0636943,34.5384615 36.4076433,31.0769231 C37.9617834,28.5384615 38.0127389,26.5384615 37.6305732,25.0769231 C37.0700637,23.2051282 35.3630573,20.8717949 31.9235669,21.0512821 M36.9681529,16.3846154 C35.7707006,15.4358974 33.9872611,14.7179487 31.3121019,14.7179487 C31.3121019,14.7179487 23.7961783,14.7692308 23.7961783,21.3846154 L23.7961783,39.4358974 C24.2292994,39.3589744 24.6369427,39.2564103 25.044586,39.1538462 L25.044586,23.1794872 C25.044586,23.1794872 24.9936306,21.8974359 25.7070064,20.4615385 C26.4713376,18.8461538 28.1273885,17.0769231 31.6687898,16.974359 C31.6687898,16.974359 34.6496815,16.6923077 36.8917197,19.1282051 C37.9363057,20.3333333 38.8789809,21.7692308 38.9044586,25.7948718 C40.7898089,20.2051282 37.9363057,17.2307692 36.9681529,16.3846154 M8.78980892,14.7179487 C6.11464968,14.7179487 4.33121019,15.4358974 3.13375796,16.3846154 C2.1656051,17.2307692 -0.687898089,20.2051282 1.17197452,25.7948718 C1.19745223,21.7692308 2.14012739,20.3333333 3.18471338,19.1282051 C5.40127389,16.6923077 8.40764331,16.974359 8.40764331,16.974359 C11.9490446,17.0769231 13.6050955,18.8461538 14.3694268,20.4615385 C15.0828025,21.8717949 15.0318471,23.1794872 15.0318471,23.1794872 L15.0318471,39.1538462 C15.4394904,39.2564103 15.8726115,39.3589744 16.3057969,39.4358974 L16.3057325,21.4102564 C16.3312102,14.7692308 8.78980892,14.7179487 8.78980892,14.7179487 M10.6751592,21.7948718 C9.55414013,21.0769231 8.20382166,21.0512821 8.20382166,21.0512821 C4.7388535,20.8461538 3.03184713,23.2051282 2.49681529,25.0769231 C2.11464968,26.5384615 2.1910828,28.5384615 3.71974522,31.0769231 C6.06369427,34.5384615 9.4522293,37.2051282 13.4267516,38.6410256 L13.4267516,27.8974359 C13.4267516,24.3589744 12,22.6410256 10.6751592,21.7948718"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>
    </div>
    <div class="brand-image-xs logo-xl">
                <svg width="40px" height="40px" viewBox="0 0 40 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g transform="translate(-801.000000, -12385.000000)" fill="#<?= $logoColor ?>">
                            <g transform="translate(801.000000, 12385.000000)">
                                <g>
                                    <g fill-rule="nonzero">
                                        <path class="dynamic-fill" d="M31.1082803,10.3076923 C31.1082803,10.3076923 34.7006369,10.1282051 37.4012739,13.1282051 C38.2929936,14.1282051 39.1592357,15.4102564 39.6433121,17.2051282 C38.3694268,7.51282051 30.089172,0 20.0509554,0 C10.0382166,0 1.73248408,7.51282051 0.433121019,17.2307692 C0.917197452,15.4358974 1.78343949,14.1538462 2.67515924,13.1538462 C5.40127389,10.1538462 8.96815287,10.3333333 8.96815287,10.3333333 C17.8853503,10.7948718 17.9363057,19.8205128 17.9363057,19.8205128 L17.9363057,39.7179487 C18.343949,39.7692308 18.7770701,39.7948718 19.2101911,39.8205128 L19.2101911,18.5384615 C19.2101911,15.6410256 18.3184713,12.8205128 16.3057325,10.8205128 C14.4713376,8.97435897 12.1019108,7.94871795 9.60509554,7.8974359 C10.6751592,6.02564103 12.1528662,4.46153846 13.910828,3.35897436 C15.7707006,2.20512821 17.8853503,1.58974359 20.0509554,1.58974359 C22.2165605,1.58974359 24.3566879,2.20512821 26.2165605,3.35897436 C27.9745223,4.46153846 29.4522293,6.02564103 30.522293,7.8974359 C28.0254777,7.94871795 25.6305732,8.97435897 23.7961783,10.8205128 C21.7834395,12.8205128 20.866242,15.6410256 20.866242,18.5384615 L20.866242,39.7692308 C21.2993631,39.7435897 21.7070064,39.7179487 22.1401274,39.6666667 L22.1401274,19.7948718 C22.1401274,19.7948718 22.1910828,10.7692308 31.1082803,10.3076923 M31.9235669,21.0512821 C31.9235669,21.0512821 30.5732484,21.0769231 29.4522293,21.7948718 C28.1019108,22.6410256 26.7006369,24.3589744 26.7006369,27.8974359 L26.7006369,38.6410256 C30.6751592,37.2051282 34.0636943,34.5384615 36.4076433,31.0769231 C37.9617834,28.5384615 38.0127389,26.5384615 37.6305732,25.0769231 C37.0700637,23.2051282 35.3630573,20.8717949 31.9235669,21.0512821 M36.9681529,16.3846154 C35.7707006,15.4358974 33.9872611,14.7179487 31.3121019,14.7179487 C31.3121019,14.7179487 23.7961783,14.7692308 23.7961783,21.3846154 L23.7961783,39.4358974 C24.2292994,39.3589744 24.6369427,39.2564103 25.044586,39.1538462 L25.044586,23.1794872 C25.044586,23.1794872 24.9936306,21.8974359 25.7070064,20.4615385 C26.4713376,18.8461538 28.1273885,17.0769231 31.6687898,16.974359 C31.6687898,16.974359 34.6496815,16.6923077 36.8917197,19.1282051 C37.9363057,20.3333333 38.8789809,21.7692308 38.9044586,25.7948718 C40.7898089,20.2051282 37.9363057,17.2307692 36.9681529,16.3846154 M8.78980892,14.7179487 C6.11464968,14.7179487 4.33121019,15.4358974 3.13375796,16.3846154 C2.1656051,17.2307692 -0.687898089,20.2051282 1.17197452,25.7948718 C1.19745223,21.7692308 2.14012739,20.3333333 3.18471338,19.1282051 C5.40127389,16.6923077 8.40764331,16.974359 8.40764331,16.974359 C11.9490446,17.0769231 13.6050955,18.8461538 14.3694268,20.4615385 C15.0828025,21.8717949 15.0318471,23.1794872 15.0318471,23.1794872 L15.0318471,39.1538462 C15.4394904,39.2564103 15.8726115,39.3589744 16.3057969,39.4358974 L16.3057325,21.4102564 C16.3312102,14.7692308 8.78980892,14.7179487 8.78980892,14.7179487 M10.6751592,21.7948718 C9.55414013,21.0769231 8.20382166,21.0512821 8.20382166,21.0512821 C4.7388535,20.8461538 3.03184713,23.2051282 2.49681529,25.0769231 C2.11464968,26.5384615 2.1910828,28.5384615 3.71974522,31.0769231 C6.06369427,34.5384615 9.4522293,37.2051282 13.4267516,38.6410256 L13.4267516,27.8974359 C13.4267516,24.3589744 12,22.6410256 10.6751592,21.7948718"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </svg>

      <img src="{{URL::asset('image/logo/logo_lockup.svg')}}"  alt="NUSKIN">    
    </div>
    </center>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" id="topheader">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img alt="User Image" class="img-circle elevation-2" src="{{ asset(Auth::user()->photo_path) }}">
        </div>
        <div class="info">
          <a href="{{ route('profileuser', ['id' => $user])}}" class="d-block" style="text-transform:capitalize">{{ Auth::user()->name }} </a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         
         <li class="nav-item">
              <a href="{{ route('home') }}" class="nav-link  <?=($p=='dashboard')?'active' : '' ?>">
                <i class="fas fa-tachometer-alt nav-icon"></i>
                <p>Dashboard</p>
              </a>
          </li>
@can('view_delivery')
          <li class="nav-item">
                <a href="{{ route('list-pickup') }}" class="nav-link <?=($p=='delivery-order' || $p=='createlist' || $p=='detailPickup')?'active' : '' ?>">
                  <i class="fa fa-truck nav-icon"></i>
                  <p>Delivery Order</p>
                </a>
          </li>

@endcan
@can('view_ticket_delivery')
      <li class="nav-item has-treeview <?=($url=='complain-ticket')?'' : '' ?>">  <!-- menu-open -->
        <a href="#" class="nav-link <?=($url=='complain-ticket')?'active' : '' ?>">
          <i class="fas fa-ticket-alt nav-icon"></i>
          <p>
            Complain Ticket
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{ route('listticket')}}" class="nav-link <?php echo ($p =='delivery')? "blasteranActive" :'';?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Delivery</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('ticket.nonDVListticket')}}" class="nav-link <?php echo ($p =='non-delivery')? "blasteranActive" :'';?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Non-Delivery</p>
            </a>
          </li>
        </ul>
      </li>
@endcan
@can('view_performance')
          <li class="nav-item has-treeview <?=($url=='performance')?'' : '' ?>">  <!-- menu-open -->
            <a href="#" class="nav-link <?=($url=='performance')?'active' : '' ?>">
              <i class="fas fa-chart-line nav-icon"></i>
              <p>
                Performance
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('performance.leadtime')}}" class="nav-link <?php echo ($p =='leadtime')? "blasteranActive" :'';?>">
                  <!-- <i class="fa fa-building nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lead Time</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('performance.successrate')}}" class="nav-link <?php echo ($p =='successrate')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-chart-pie nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Success Rate</p>
                </a>
              </li>
            </ul>
          </li>
@endcan
@can('view_setting')
          <li class="nav-item">
            <a href="{{ route('post.index')}}" class="nav-link <?php echo ($p =='post')? "active" :'';?>">
              <i class="fas fa-exclamation-triangle nav-icon"></i>
              <p>Post</p>
            </a>
          </li>
@endcan
@can('view_setting')
          <li class="nav-item has-treeview <?=($url=='settings')?'' : '' ?>"> <!-- menu-open -->
            <a href="#" class="nav-link <?=($url=='settings' || $p =='users' || $p =='roles' || $p =='kelolarole' || $p =='kelolatopics')?'active' : '' ?>"> <!-- active -->
              <i class="fas fa-cog nav-icon"></i>
              <p>
                Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('users.index') }}" class="nav-link <?php echo ($p =='users')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-users nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    User Management
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('roles.index')}}" class="nav-link <?php echo ($p =='roles')? "blasteranActive" :'';?>">
                  <!-- <i class="far fa-user nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Role Management</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kelolarole')}}" class="nav-link <?php echo ($p =='kelolarole')? "blasteranActive" :'';?>">
                  <!-- <i class="fa fa-building nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Warehouse</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('kelolatopics')}}" class="nav-link <?php echo ($p =='kelolatopics')? "blasteranActive" :'';?>">
                  <!-- <i class="fa fa-list nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Topic</p>
                </a>
              </li>
               <li class="nav-item d-none">
                <a href="{{ route('listzonasi')}}" class="nav-link <?php echo ($p =='list-zonasi')? "active" :'';?>">
                  <!-- <i class="fa fa-map-marker nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Zone</p>
                </a>
              </li>
              <li class="nav-item d-none">
                <a href="{{ route('kelolakodepos')}}" class="nav-link <?php echo ($p =='kelolakodepos')? "active" :'';?>">
                  <!-- <i class="fa fa-envelope nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage Zip Code</p>
                </a>
              </li>
              <li class="nav-item d-none">
                <a href="{{ route('list-mastervendor')}}" class="nav-link <?php echo ($p =='master-vendor')? "active" :'';?>">
                  <!-- <i class="fa fa-truck nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Master Vendor</p>
                </a>
              </li>
              <li class="nav-item d-none">
                <a href="{{ route('list-kelolavendor')}}" class="nav-link <?php echo ($p =='kelola-vendor')? "active" :'';?>">
                  <!-- <i class="fa fa-truck nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Manage Vendor</p>
                </a>
              </li>
        
              <li class="nav-item">
                <a href="{{ route('couriers.index')}}" class="nav-link <?php echo ($p =='couriers')? "blasteranActive" :'';?>">
                  <!-- <i class="fa fa-truck nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Courier</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('postalcodes.index')}}" class="nav-link <?php echo ($p =='postalcodes')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-home nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Postal Code</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('zones.index')}}" class="nav-link <?php echo ($p =='zones')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-map-marker-alt nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Zone</p>
                </a>
              </li>
              <?php /* 
              <li class="nav-item">
                <a href="{{ route('stores.index')}}" class="nav-link <?php echo ($p =='stores')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-store nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Store</p>
                </a>
              </li> 
              */ ?> 
              <li class="nav-item">
                <a href="{{ route('provinces.index')}}" class="nav-link <?php echo ($p =='provinces')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-map-marker nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Province</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('cities.index')}}" class="nav-link <?php echo ($p =='cities')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-city nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>City</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('holidays.index')}}" class="nav-link <?php echo ($p =='holidays')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-calendar-alt nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Holidays</p>
                </a>
              </li>
              <?php /* 
              <li class="nav-item">
                <a href="{{ route('nuskinstatus.index')}}" class="nav-link <?php echo ($p =='nuskinstatus')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-thermometer-quarter nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nuskin Status</p>
                </a>
              </li>
              */ ?> 
              <?php /* 
              <li class="nav-item">
                <a href="{{ route('courierstatus.index')}}" class="nav-link <?php echo ($p =='courierstatus')? "blasteranActive" :'';?>">
                  <!-- <i class="fas fa-battery-quarter nav-icon ml-3xx"></i> -->
                  <i class="far fa-circle nav-icon"></i>
                  <p>Courier Status</p>
                </a>
              </li>
              */ ?> 
            </ul>
          </li>
@endcan     
      
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    
<?php
    //echo ' ================================================ p :'.$p;
    //echo ' ================================================ url :'.$url;
?>

    </div>
    <!-- /.sidebar -->

  </aside>

