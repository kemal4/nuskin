<!-- jQuery  -->
<script src="{{ asset('AdminLTE-master/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('AdminLTE-master/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('AdminLTE-master/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('AdminLTE-master/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('AdminLTE-master/plugins/chart.js/chartjs-plugin-datalabels.min.js') }}"></script>
<!-- Sparkline 
<script src="{{ asset('AdminLTE-master/plugins/sparklines/sparkline.js') }}"></script>
-->
<!-- JQVMap -->
<script src="{{ asset('AdminLTE-master/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('AdminLTE-master/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('AdminLTE-master/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('js/datepicker.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('AdminLTE-master/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('AdminLTE-master/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('AdminLTE-master/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('AdminLTE-master/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('AdminLTE-master/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>

<script src="{{ asset('AdminLTE-master/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>


<!-- AdminLTE App -->
<script src="{{ asset('AdminLTE-master/dist/js/adminlte.js') }}"></script>
<!-- <script src="{{ asset('AdminLTE-master/dist/js/adminlte.min.js') }}"></script> -->

<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="{{ asset('AdminLTE-master/dist/js/pages/dashboard.js') }}"></script>
-->
<!-- AdminLTE for demo purposes 
<script src="{{ asset('AdminLTE-master/dist/js/demo.js') }}"></script>
-->

<!-- DataTables -->
<script src="{{ asset('AdminLTE-master/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('AdminLTE-master/plugins/datatables-fixedcolumns/js/dataTables.fixedColumns.js') }}"></script>

<script src="{{ asset('AdminLTE-master/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>

<!--Sweet Alert
<script src="{{ asset('js/sweetalert/sweetalert2@9.js') }}" type="text/javascript"></script>
<script src="sweetalert2.all.min.js"></script>
<script src="{{ asset('AdminLTE-master/plugins/toastr/sweetalert2.js') }}" type="text/javascript"></script>
-->
<!--Toastr-->
<script src="{{ asset('AdminLTE-master/plugins/toastr/toastr.min.js') }}" type="text/javascript"></script>
<!--Sweet Alert-->
<script src="{{ asset('AdminLTE-master/plugins/sweetalert2/sweetalert2.all.js') }}" type="text/javascript"></script>
<script src="{{ asset('AdminLTE-master/plugins/sweetalert2/sweetalert2.all.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('AdminLTE-master/plugins/sweetalert2/sweetalert2.js') }}" type="text/javascript"></script>
<script src="{{ asset('AdminLTE-master/plugins/sweetalert2/sweetalert2.min.js') }}" type="text/javascript"></script>


<!--
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js" type="text/javascript"></script>
-->
<script src="{{ asset('AdminLTE-master/plugins/slick/slick.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('AdminLTE-master/plugins/ekko-lightbox/ekko-lightbox.min.js') }}" type="text/javascript"></script>




<!-- autocompleter 
<script src="{{ asset('js/jquery.autocompleter.js') }}"></script>
-->
@yield('scripts')

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});


  $(function() {
@isset($alertGeneral)
	
		@isset($alertGeneral['timeout'])
			toastr.options.timeOut = {{ $alertGeneral['timeout'] }};
		@endisset
		
	@isset($alertGeneral['warning'])
		toastr.warning('{{ $alertGeneral['warning']['title'] }}.')	
	@endisset
	
	@isset($alertGeneral['danger'])
		
		
			
		//toastr.warning('AAA You have Unprocessed Tickets.')
		toastr.error('{{ $alertGeneral['danger']['title'] }}{{ $alertGeneral['danger']['message'] }}.')
		//toastr.options.timeOut = 5000;

/*
		
		$(document).Toasts('create', {
			class: 'bg-warning', 
			title: 'BBB You have Unprocessed Tickets',
		  })
		
		$(document).Toasts('create', {
			class: 'bg-danger', 
			title: 'BBB {{ $alertGeneral['danger']['title'] }}',
		  })
*/		
/*
		$("#modal-danger .modal-title").html('CCC : {{ $alertGeneral['danger']['title'] }}')
		$("#modal-danger").modal()
		
		  
		swal.fire({
			type: 'error',
			title: 'swal {{ $alertGeneral['danger']['title'] }}.'
		  })
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-start',
      showConfirmButton: false,
      timer: 0
    });
		  
		Toast.fire({
			type: 'error',
			title: 'EEE {{ $alertGeneral['danger']['title'] }}.'
		  })
		Toast.fire({
			type: 'warning',
			title: 'EEE You have Unprocessed Tickets.'
		  })
		
		*/    
		
		  
		  
	toastr.options.timeOut = 5000;
	
	@endisset
		
@endisset

@if($alertGeneralType = Session::get('alertGeneralType'))
@switch($alertGeneralType)
    @case('success')
        toastr.success('{{ Session::get('alertGeneralTitle') }}.')	
        @break

    @case('info')
        toastr.info('{{ Session::get('alertGeneralTitle') }}.')	
        @break

    @default
        @break
@endswitch	

@endif

  });

</script>
<script type="text/javascript">

</script>
