@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Topics</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Settings</li>
              <li class="breadcrumb-item active">Edit Data</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <div class="card-header">
                    <!-- <h3 class="card-title">Quick Example <small>jQuery Validation</small></h3> -->
                  </div>

                  <form action="{{ route('storeeditkode',['id' => $koded->id])}}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Kode POS</label>
                            <input type="text" class="form-control" placeholder="{{ $koded->kodepos}}" name="kodepos" value="{{ $koded->kodepos }}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Zone</label>
                            <select name="zona" id="zona" class="form-control select2" style="width: 100%;">
                              <option value="" selected disabled hidden class="form-control">Select Zone</option>
                                @foreach($zonename  as $zname)
                                  <option value="{{ $zname }}">{{ $zname }}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Kelurahan</label>
                            <input type="text" class="form-control" placeholder="{{ $koded->kelurahan }}" name="kelurahan" value="{{ $koded->kelurahan }}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Lead Time Nuskin</label>
                           <input type="number" class="form-control" name="leadnuskin" min="1" placeholder="1">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Kecamatan</label>
                            <input type="text" class="form-control" placeholder="{{ $koded->kecamatan}}" name="kecamatan" value="{{ $koded->kecamatan }}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Lead Time FL</label>
                            <input type="number" class="form-control" name="leadfl" min="1" placeholder="1">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Kota</label>
                            <input type="text" class="form-control" placeholder="{{ $koded->city }}" name="city" value="{{ $koded->city }}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Lead Time JNE</label>
                            <input type="number" class="form-control" name="leadjne" min="1"placeholder="1">
                          </div>
                        </div>
                      </div>
                        
                        <div class="form-group">
                          <label>Provinsi</label>
                          <input type="text" class="form-control" placeholder="{{ $koded->provinsi}}" name="provinsi" value="{{ $koded->provinsi }}">
                        </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="{{ route('kelolakodepos')}}"   class="btn btn-success mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-success mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection