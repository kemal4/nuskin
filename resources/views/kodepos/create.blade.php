@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Kode Pos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Settings</li>
              <li class="breadcrumb-item active">Input Data</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <form action="{{ route('add-kode') }}" method="post" role="form">
                    @csrf
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kode POS</label>
                              <input type="text" class="form-control" name="kodepos">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Zone</label>
                              <select name="zona" id="zona" class="form-control select2" style="width: 100%;">
                                <option value="" selected disabled hidden class="form-control">Select Zone</option>
                                  @foreach($zonename  as $zname)
                                    <option value="{{ $zname }}">{{ $zname }}</option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kelurahan</label>
                              <input type="text" class="form-control" name="kelurahan">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Lead Time Nuskin</label>
                             <input type="number" class="form-control" name="leadnuskin" min="1" placeholder="1">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kecamatan</label>
                              <input type="text" class="form-control" name="kecamatan">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Lead Time FL</label>
                              <input type="number" class="form-control" name="leadfl" min="1" placeholder="1">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Kota</label>
                              <input type="text" class="form-control" name="city">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Lead Time JNE</label>
                              <input type="number" class="form-control" name="leadjne" min="1"placeholder="1">
                            </div>
                          </div>
                        </div>
                          
                          <div class="form-group">
                            <label>Provinsi</label>
                            <input type="text" class="form-control" name="provinsi">
                          </div>
                      </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="{{ route('kelolakodepos')}}"   class="btn btn-success mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-success mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection