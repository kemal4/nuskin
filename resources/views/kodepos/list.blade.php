@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Zip Code</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Settings</li>
              <li class="breadcrumb-item active">Zip Code</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              
              {{-- notifikasi form validasi --}}
                  @if ($errors->has('file'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('file') }}</strong>
                    </span>
                  @endif
              {{-- notifikasi sukses --}}
                  @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $sukses }}</strong>
                    </div>
                  @endif
            @can('create_setting')                  
            <div class="row">
                <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#importExcel" style="margin-bottom: 10px;">
                  Import Excel
                </button>
                <a href="{{ route('createkode')}}"   class="btn btn-primary mr-5 mb-2">
                  Add Kode POS
                </a>
            </div>
            @endcan
 <!-- Import Excel -->
 <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="{{ route('importzip')}}" enctype="multipart/form-data">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
              </div>
              <div class="modal-body">
                  {{ csrf_field() }}
                  <label>Choice file excel</label>
                  <div class="form-group">
                      <input type="file" name="file" required="required">
                  </div> 
              </div>
              <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Import</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
      </form>
  </div>
</div>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No </th>
                    <th>Zonasi</th>
                    <th>Kode Pos</th>
                    <th>Kelurahan</th>
                    <th>Kecamatan</th>
                    <th>Provinsi</th>
                    <th>Kota</th>
                    <th>Lead Time Nuskin</th>
                    <th>Lead Time FL</th>
                    <th>Lead Time JNE</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
    				        @php $i=1 @endphp
                    @foreach($kodepos as $kd)
                    <tr>
                      <td>{{ $i++}} </td>
                      <td>{{ $kd->label_zonasi }}</td>
                      <td>{{ $kd->kodepos }}</td>
                      <td>{{ $kd->kelurahan }}</td>
                      <td>{{ $kd->kecamatan }}</td>
                      <td>{{ $kd->provinsi }}</td>
                      <td>{{ $kd->city }}</td>
                      <td>{{ $kd->leadtime_nuskin }}</td>
                      <td>{{ $kd->leadtime_fl }}</td>
                      <td>{{ $kd->leadtime_jne }}</td>
                      <td>
                        @can('edit_setting')
                        <a href="{{route('kodeposedit', ['id' => $kd->id])}}">Edit</a> | 
                        @endcan
                        @can('delete_setting')
                        <a href="{{route('destroykode', ['id' =>  $kd->id ])}}" onclick="myFunction()">Delete</a>
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
  	function myFunction() {
  		alert('Are you sure want to delete ??');
  	}
  </script>
@endsection