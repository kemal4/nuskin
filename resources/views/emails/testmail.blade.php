
<p>{{$body}}</p>
<div>
	<table class="table table-bordered">
        <thead>
            <tr>
                <th>Hawb</th>
                <th>Invoice</th>
                <th>Dispatch Date</th>
                <th>Nuskin ETA</th>
            </tr>
        </thead>
        <tbody>		
			@foreach($allData as $use)
				<tr>
					<td>{{ $use->hawb }}</td>
					<td>{{ $use->order_id }}</td>
					<td>{{ date('M-d-y', strtotime($use->created_at)) }}</td>
					<td>{{ $use->eta_nuskin }}</td>		
				</tr>
			@endforeach		
        </tbody>
    </table>
</div>
