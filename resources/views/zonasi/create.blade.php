@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Zonasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Input Data</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <form action="{{ route('listzonasi.add') }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                          <label>Label Zonasi</label>
                          <input type="text" class="form-control" name="label">
                        </div>
                        <div class="form-group">
                          <label>Code Zonasi</label>
                          <input type="text" class="form-control" name="code">
                        </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="{{ route('listzonasi')}}"   class="btn btn-default mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-info mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection