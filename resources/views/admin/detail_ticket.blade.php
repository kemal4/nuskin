@extends('layoutAdmin.global')

@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<style type="text/css">
.{
  z-index: 100000;
}
</style>
@endsection

@section('content')
<div class="content-wrapper">
  <section class="p-2 pb-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <a href="{{ route('listticket')}}" class="text-muted"><i class="fas fa-long-arrow-alt-left"></i> Back to list</a>
        </div>
      </div>
    </div>
  </section>

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><span class="mr-1"> Ticket Detail : </span> <?= $ticketdetail->id_ticket  ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
            <li class="breadcrumb-item"><a href="{{ route('listticket')}}">Complain Ticket </a></li>
            <li class="breadcrumb-item active">Ticket Detail</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Modal Due Date -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Due Date</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <div class="row">
              <label for="idDueDate">Due Date : </label>
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="duedetail" id="idDueDate" readonly="readonly" class="form-control "> <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info" id="duedate" data-dismiss="modal">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal Due Dtae -->

  {{-- notifikasi sukses --}}
  @if ($berhasil = Session::get('berhasil'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $berhasil }}</strong>
  </div>
  @endif

  @error('image')
    <div class="alert alert-danger alert-block">{{ $message }}</div>
    <button type="button" class="close" data-dismiss="alert">×</button> 
  @enderror

  <form class="content" action="{{ route('storeeditticket', ['id' => $ticketdetail->id ]) }}" method="post" role="form" enctype="multipart/form-data">
    @csrf
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">
          <a href="{{ route('listticket',['id' => $ticketdetail->id]) }}" class="btn btn-default btn-block mb-3 d-none">Back to Tickets</a>
          @if($ticketdetail->status == "Solved")
          <button type="button" id="changeduedate" class="btn btn-block btn-primary btn-block mb-3" data-toggle="modal" data-target="#myModal" disabled><i class="nav-icon far fa-calendar-alt"></i> Change Due Date</button>
          @else
          <button type="button" id="changeduedate" class="btn btn-block btn-primary btn-block mb-3" data-toggle="modal" data-target="#myModal"><i class="nav-icon far fa-calendar-alt"></i> Change Due Date</button>
          <input type="hidden" name="duedetail" id="duedateval" value="">
          @endif
          
          <div class="card">
            <ul class="nav nav-pills flex-column">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <strong>Created Date:</strong>
                  <?php $date = date("m/d/Y", strtotime($ticketdetail->created_at));  ?>
                  <span class="float-right">{{$date}}</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <strong>Due Date:</strong>
                  <span class="float-right">{{ $ticketdetail->due }}</span>
                </a>
              </li>
            </ul>
          </div>
          
          <div class="card">
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    Topics:
                    <span class="float-right">{{$ticketdetail->topics}}</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    SO Number:
                    <span class="float-right">{{$ticketdetail->order_id}}</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          
          <div class="card">
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    Customer Name:
                    <span class="float-right">{{$ticketdetail->nama_customer}}</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    Complainer Name:
                    <span class="float-right">{{$ticketdetail->nama_pelapor}}</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    Complainer Email:
                    <span class="float-right">{{$ticketdetail->email}}</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    Complainer Phone:
                    <span class="float-right">{{$ticketdetail->telepon}}</span>
                  </a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <div class="card d-none">
            <div class="card-header">
              <h3 class="card-title">Labels</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body d-none p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="far fa-circle text-danger"></i> Important</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="far fa-circle text-warning"></i> Promotions</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"><i class="far fa-circle text-primary"></i> Social</a>
                </li>
              </ul>
            </div>
             <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><i class="far fa-circle text-danger"></i> {{$ticketdetail->status}}</h3>
              <div class="card-tools">
                @if($ticketdetail->status == "On Process")
                <div class="custom-control custom-switch mb-3">
                  <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" value="Solved">
                  <label class="custom-control-label" for="customSwitch1">Solved</label>
                </div>
                @elseif($ticketdetail->status == "Solved")
                <div class="custom-control custom-switch mb-3">
                  <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" value="Solved" disabled>
                  <label class="custom-control-label" for="customSwitch1">Solved</label>
                </div>
                @else
                <div class="custom-control custom-switch mb-3">
                  <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" value="On Process">
                  <label class="custom-control-label" for="customSwitch1">On Process</label>
                </div>
                @endif
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group">
                <textarea class="form-control" rows="4" name="description" value="{{$ticketdetail->description}}" required></textarea>
              </div>
              <div class="form-group mb-0">
                <div class="btn btn-default btn-file mb-2">
                  <i class="fas fa-paperclip"></i> Attachment
                  <input type="file" name="image[]" id="attach1" multiple="true" accept="image/*" onchange="filename()">
                </div>
                <small class="d-block mb-0" style="color: red;" id="maxupload">Max. 2MB</small>
                <small class="d-block mb-0" id="listname"></small>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <div class="float-right">
                <button type="submit" class="btn btn-info" id="ubahstatus"><i class="far fa-envelope"></i> Save</button>
              </div>
              <a href="{{ route('listticket',['id' => $ticketdetail->id]) }}" class="btn btn-default"><i class="fas fa-times"></i> Cancel</a>
            </div>
            <!-- /.card-footer -->
          </div>
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Attachments</h3>
            </div>
            <div class="card-body">
              <div class="row mb-3">
                @if($attachments)
                  @foreach($attachments as $file)
                    <div class="col-sm-2">  
                    <a href="{{URL::to('/')}}/upload/{{$file}}" data-toggle="lightbox">
                        <img class="img-fluid mr-2" src="{{URL::to('/')}}/upload/{{$file}}" alt="" title="image" width="200" height="100">
                    </a>
                    </div>
                  @endforeach
                @endif
                @if($image1 || $image2 )
                  <div class="col-sm-2">
                  <a href="{{URL::to('/')}}/upload/{{$ticketdetail->attachments}}" data-toggle="lightbox">
                      <img class="img-fluid mr-2" src="{{URL::to('/')}}/upload/{{$ticketdetail->attachments}}" alt="" title="image" width="200" height="100">
                  </a>
                  </div>
                  <div class="col-sm-2">
                  <a href="{{URL::to('/')}}/upload/{{$ticketdetail->attachments2}}" data-toggle="lightbox">
                      <img class="img-fluid mr-2" src="{{URL::to('/')}}/upload/{{$ticketdetail->attachments2}}" alt="" title="image" width="200" height="100">
                  </a>
                  </div>
                @endif
                @if($image1 == FALSE && $image2 == FALSE && $attachments == FALSE)
                  <div class="col-12">No Attachments</div>
                @endif
              </div>
              <hr>
              <div class="profile-img-tag"></div>
            </div>
          </div>
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Ticket Log</h3>
            </div>
            <!-- /.card-header -->
            <div class=" card-body" id="timeline">
              <!-- The timeline -->
              <div class="timeline timeline-inverse">

                @php $i=1 @endphp @foreach($ticketlogs as $logs)
                  <!-- timeline item -->
                  <div>
                      <i class="fa fa-comments bg-yellow"></i>
          
                      <div class="timeline-item mr-0">
                          <span class="time"><i class="fa fa-clock-o"></i> {{$logs->updated_at}}</span>
                          <h3 class="timeline-header" style="text-transform: lowercase"><a href="#" style="text-transform: capitalize">{{$logs->pic}}</a> {{ $logs->activity }}</h3>
                          <div class="timeline-body" style="background-color:#fff">{!! $logs->description !!}</div>
                      </div>
                    </div>
                  <!-- END timeline item -->
                @endforeach
                  
                  <!-- END timeline item -->
                  <div>
                      <i class="fas fa-clock bg-gray"></i>
                  </div>
                </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      

    </section>

    <!-- /.Main content -->
  </form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

  function filename() {
    var fullPath = document.getElementById('attach1').files;
      if (fullPath) {
          var multiname = "";
          var i;

          for (var i = 0; i < fullPath.length; i++)
          {
             multiname += fullPath[i].name + ", ";
          }

          document.getElementById("maxupload").remove();
          document.getElementById("listname").innerHTML = multiname;
      }
  }

  $(function() {
      var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img width="200" class="img-fluid ml-2 mr-2 mb-2">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
      };

      $('#attach1').on('change', function() {
          imagesPreview(this, 'div.profile-img-tag');
      });

  });

  $('#myModal').on('click','#duedate', function (e) {
    var str   = document.getElementById("changeduedate").innerHTML; 
    var date  = document.getElementById("idDueDate").value;
    var ada   = document.getElementById("changeduedate").innerHTML = date;
    document.getElementById("duedateval").value = date;
  });

  $('#idDueDate').datepicker({
   changeMonth: true,
   changeYear: true,
   minDate: 0,
   autoclose: true
 });

  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
  });

  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });

  function Upload() {
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      var fsize = $('#fileUpload')[0].files[0].size;
      if(fsize>2097152) //do something if file size more than 2 mb (2097152)
      {
        alert(fsize +" bites\nToo big!");
        var warn = "Image max 2mb";
        var element = document.getElementById("fileUpload");
        document.getElementById("tckt1").innerHTML = warn;
        element.classList.add("is-invalid");
      }else{
        alert(fsize +" bites\nYou are good to go!");
        
        var element = document.getElementById("fileUpload");
        element.classList.add("is-valid");
      }
      alert(fsize);
    }else{
      alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    } 
  }

  function Upload2() {
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      var fsize = $('#fileUpload2')[0].files[0].size;
      if(fsize>2097152) //do something if file size more than 2 mb (2097152)
      {
        alert(fsize +" bites\nToo big!");
        var warn = "Image max 2mb";
        var element = document.getElementById("fileUpload2");
        document.getElementById("tckt1").innerHTML = warn;
        element.classList.add("is-invalid");
      }else{
        alert(fsize +" bites\nYou are good to go!");
        
        var element = document.getElementById("fileUpload2");
        element.classList.add("is-valid");
      }
      alert(fsize);
    }else{
      alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    } 
  }

  $(function () {
   $('#example1').DataTable( {
    scrollY:        true,
    scrollX:        true,
    scrollCollapse: true,
    paging:         true,
  })
 });

  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });

  $("#datepick").datepicker({
    startDate: new Date() // set the minDate to the today's date
      // you can add other options here
    });
  </script>
  @endsection