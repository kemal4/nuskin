@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Delivery Order Preview</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Preview</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
	
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Preview : {{ session('warehouse_name') }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
              {{-- notifikasi form validasi --}}
                  @if ($errors->has('file'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('file') }}</strong>
                    </span>
                  @endif
              {{-- notifikasi sukses --}}
                  @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $sukses }}</strong>
                    </div>
                  @endif
               	
                  <!-- Import Excel -->
                  <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <form method="post" action="{{ route('importExcel')}}" enctype="multipart/form-data">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                          </div>
                          <div class="modal-body">
               
                            {{ csrf_field() }}
               
                            <label>Pilih file excel</label>
                            <div class="form-group">
                              <input type="file" name="file" required="required">
                            </div>
               
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Import</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
               <div class="card-body">
		<div class="row">
		</div>
		
		<div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fas fa-database"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Processed</span>
                <span class="info-box-number">{{ $data['total'] }}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="fas fa-file-upload"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Will be Saved</span>
                <span class="info-box-number">{{ $data['valid'] }}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">ZIP doesn't exist</span>
                <span class="info-box-number">{{ $data['postalCode'] }}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="fas fa-times"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Invalid Data</span>
                <span class="info-box-number">{{ $data['duplicate'] }}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
		
                <table id="example1" class="table table-bordered text-nowrap">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Invoice ID/SO</th>
                    <th>Invoice Date</th>
                    <th>Courier</th>
                    <th>HAWB</th>
                    <th>Delivery Number</th>
					<th>Customer ID</th>
					<th>Customer Name</th>
                    <th>Phone</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>Postal Code</th>
                    <th>Gross Weight</th>
                    <th>Net Weight</th>
                    <th>Volume</th>
                                    <th>Nu Skin lead time</th>
                                    <th>Nu Skin ETA</th>
                                    <th>Courier lead time</th>
                                    <th>Courier ETA</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php $i=1 @endphp
                    @foreach($data['data'] as $delven)
                    <tr>
                      <td>
						@if ($delven->duplicatedHawb || $delven->duplicatedDeliveryNumber) 
							<i class='fas fa-exclamation-triangle'  style='color:red'></i>
						@endif
						@if ($delven->zipNotExist )
							<i class='fas fa-exclamation-triangle'  style='color:#ffc107'></i>
						@endif
						{{ $i++ }} </td>
                      <td>
						@if($delven->is_partial_so) 
							<span class="badge badge-info" data-toggle="tooltip" title="Partial SO">
							{{$delven->order_id}}</span>
						@else
							  {{$delven->order_id}}
						@endif
					  </td>
                      <td>{{$delven->create_date}}</td>
                      <td>{{$delven->transporter}}</td>
                      <td>
						@if($delven->duplicatedHawb) 
							<span class="badge badge-danger" data-toggle="tooltip" title="Duplicated HAWB">
							{{$delven->hawb}}</span>
						@else
							{{$delven->hawb}}
						@endif
					  </td>					  
                      <td>
						@if($delven->duplicatedDeliveryNumber) 
							<span class="badge badge-danger" data-toggle="tooltip" title="Duplicated Delivery Number">
							{{$delven->delivery_number}}</span>
						@else					
							{{$delven->delivery_number}}						
						@endif
						
					  </td>
                      <td>{{$delven->customer_id}}</td>
                      <td>{{$delven->customer_name}}</td>
                      <td>{{$delven->phone}}</td>
                      <td>{{$delven->city}}</td>
                      <td class="text-nowrap">{{$delven->adress}}</td>
                      <td>
						@if($delven->zipNotExist) 
							<span class="badge badge-warning"  data-toggle="tooltip" title="Postal Code doesn't exist">
						@endif
						{{$delven->zip}}
						@if($delven->zipNotExist) 
							</span>
						@endif
					  </td>                      
                      <td>{{$delven->grosswiight}}</td>
                      <td>{{$delven->net_weight}}</td>
                      <td>{{$delven->volume}}</td>
                      <td>{{$delven->leadtime_nuskin}}</td>
                      <td>{{$delven->eta_nuskin}}</td>
                      <td>{{$delven->leadtime_courier}}</td>
                      <td>{{$delven->eta_provider}}</td>
                    </tr>
                    @endforeach
                 
                  </tbody>
                </table>
              </div>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row"> 
        <div class="col-12">
            <a href="{{ route('cancelPreviewDo') }}"  class="btn btn-default mr-3" style="margin-bottom: 10px;">
              Cancel
            </a>
            <a href="{{ route('save-do') }}"  class="btn btn-info mr-3" style="margin-bottom: 10px;">
              Import
            </a>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(function () {
       $('#example1').DataTable( {
        scrollY:        true,
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,
      })
    });
  </script>
@endsection