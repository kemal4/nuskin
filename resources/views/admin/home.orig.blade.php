@extends('layoutAdmin.global')

@section('content')
	  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <div class="card-body">
      @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
  </div>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h3>{{$data['summary']['totalSucceed']}} / {{$data['summary']['totalDelivery']}}</h3>
                <p>Delivery Succeed This Month &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
              </div>
              <div class="icon">
                <i class="fas fa-truck"></i>
              </div>
              <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h3>{{$data['summary']['totalFulfilled']}} / {{$data['summary']['totalSucceed']}}</h3>
                <p>Delivery Lead Time Fulfilled This Month</p>
              </div>
              <div class="icon">
                <i class="fas fa-shipping-fast"></i>
              </div>
              <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h3>{{$data['summary']['totalSolved']}} / {{$data['summary']['totalComplaint']}}</h3>
                <p>Delivery Complaint Solved This Month</p>
              </div>
              <div class="icon">
                <i class="fab fa-teamspeak"></i>
              </div>
              <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h3>{{$data['summary']['totalSolvedNon']}} / {{$data['summary']['totalComplaintNon']}}</h3>
                <p>Non-delivery Complaint This Month</p>
              </div>
              <div class="icon">
                <i class="fas fa-clipboard-list"></i>
              </div>
              <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
         
        </div>
        <!-- /.row -->
		
        
		<div class="row">
        <div class="col-12">
			<div class="card card-outline card-success">
				<div class="card-header">
					<h3 class="card-title">Courier Total Delivery</h3>
					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
					  </button>
					</div>
				</div>
				<div class="card-body">
					<div id="delsuccess" >
					</div>					
				</div>
			</div>
		</div>
		</div>
		
        
		
        
		<div class="row">
        <div class="col-12">
			<div class="card">
				<div class="card-header">
					Monthly Total Delivery
					<div class="card-tools">
					  <button type="button" class="btn btn-tool" data-card-widget="collapse">
						<i class="fas fa-minus"></i>
					  </button>
					</div>
				</div>
				<div class="card-body">
					<div id="deltotal" class="mb-3"></div>
				</div>
			</div>
		</div>
		</div>
		
        <!-- bar chart -->
        <!-- /.bar chart -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
/*
   let vendors = {!! json_encode($data['vendors']) !!};
   let monthLabel = {!! json_encode($data['monthLabel']) !!};
   let nuskinTotal = {!! json_encode($data['nuskinTotal']) !!};
*/   
   let vendors = @json($data['vendors']) ;
   let monthLabel = @json($data['monthLabel']) ;
   let nuskinTotal = @json($data['nuskinTotal']) ;
Highcharts.chart('delsuccess', {
    chart: {
        type: 'column'
    },
    title: {
        text: ""//'Monthly Graph'
    },
    subtitle: {
        text: ''//'Nuskin'
    },
    credits: {
      enabled: false
    },
    xAxis: {
        categories: monthLabel,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Delivery Number'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        footerFormat: '</table>',
        
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.3,
            borderWidth: 0
        }
    },
    series: vendors
});
</script>
<script>
  Highcharts.chart('deltotal', {
     chart: {
         type: 'line',
     },
     title: {
         text: 'Monthly Total Deliverey'
     },
     xAxis: {
         categories: monthLabel
     },
     yAxis: {
         title: {
             text: 'Total Delivery'
         }
     },
     plotOptions: {
         line: {
             dataLabels: {
                 enabled: true
             },
             enableMouseTracking: false
         }
     },
     credits: {
       enabled: false
     },
     series: [
       {
         name: 'Total Delivery Number Per Month',
         color: 'black',
         data: nuskinTotal
       }]
 });
 </script>
@endsection
