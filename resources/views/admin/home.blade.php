@extends('layoutAdmin.global')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Dashboard</h1>
				</div>
				<!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Dashboard</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row pb-3">
				<div class="col-md-3">
					<!-- small box -->
					<div class="small-box bg-success">
						<div class="inner">
							<h3>{{$data['summary']['totalSucceed']}} / {{$data['summary']['totalDelivery']}}</h3>
							<!-- <p>Delivery Succeed This Month &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p> -->
							<p>Delivery Succeed This Month &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
						</div>
						<div class="icon">
							<i class="fas fa-truck"></i>
						</div>
						<a href="{{ route('list-pickup')}}" class="small-box-footer">
							More info <i class="fas fa-arrow-circle-right"></i>
						</a>
						<!--- <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a> -->
					</div>
				</div>
				<!-- ./col -->
				<div class="col-md-3">
					<!-- small box -->
					<div class="small-box bg-success">
						<div class="inner">
							<h3>{{$data['summary']['totalFulfilled']}} / {{$data['summary']['totalSucceed']}}</h3>
							<p>Delivery Lead Time Fulfilled This Month</p>
						</div>
						<div class="icon">
							<i class="fas fa-shipping-fast"></i>
						</div>
						<a href="{{ route('list-pickup')}}" class="small-box-footer">
							More info <i class="fas fa-arrow-circle-right"></i>
						</a>
						<!--- <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-3">
					<!-- small box -->
					<div class="small-box bg-info">
						<div class="inner">
							<h3>{{$data['summary']['totalSolved']}} / {{$data['summary']['totalComplaint']}}</h3>
							<p>Delivery Complaint Solved This Month</p>
						</div>
						<div class="icon">
							<i class="fab fa-teamspeak"></i>
						</div>
						<a href="{{ route('listticket')}}" class="small-box-footer">
							More info <i class="fas fa-arrow-circle-right"></i>
						</a>
						<!--- <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-3">
					<!-- small box -->
					<div class="small-box bg-info">
						<div class="inner">
							<h3>{{$data['summary']['totalSolvedNon']}} / {{$data['summary']['totalComplaintNon']}}</h3>
							<p>Non-delivery Complaint This Month &nbsp;</p>
						</div>
						<div class="icon">
							<i class="fas fa-clipboard-list"></i>
						</div>
						<a href="{{ route('listticket')}}" class="small-box-footer">
							More info <i class="fas fa-arrow-circle-right"></i>
						</a>
						<!--- <a href="#" class="small-box-footer d-none">More info <i class="fas fa-arrow-circle-right"></i></a> -->
					</div>
				</div>
				<!-- ./col -->

			</div>
			<!-- /.row -->


			<div class="row">
				<div class="col-sm-12 col-md-8">

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Courier Total Delivery</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse">
											<i class="fas fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div id="delsuccess" >
									</div>					
								</div>
							</div>
						</div>
					</div>




					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Monthly Total Delivery</h3>					
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse">
											<i class="fas fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<div id="deltotal" class="mb-3"></div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="col-sm-12 col-md-4">
				
                    <div class="d-flex card">
                      <div class="card-header">
                          <h3 class="card-title">Announcement</h3>
                          <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                      </div>
	                      <!-- /.card-header -->
	                    @if($data['announcement'])
	                      <div class="card-body">
	                        <div class="media">
	                            <img src="{{ asset($data['announcement']['content']['photo_path'])}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
	                            <div class="media-body">
	                                <h3 class="dropdown-item-title mb-1"><strong>{{ $data['announcement']['content']['name'] }}</strong></h3>
	                                <p class="mb-0">{{ $data['announcement']['content']['message'] }}</p>
	                            </div>
	                        </div>
	                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <p class="text-sm text-muted mb-0 float-right"><i class="far fa-clock mr-1"></i> {{ $data['announcement']['time'] }}</p>
                      </div>
                      	@endif
                    </div>
                    
				
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title">System Log</h3>
                      </div>
                      <!-- /.card-header -->
                      <div class="card-body p-0">
                        <ul class="products-list product-list-in-card">
                          @foreach($recentActivities as $activity)
                          <li class="item pl-3 pr-3">
                            <div class="media">
                              <img class="img-size-50 mr-3 mr-2 img-circle" src="{{ asset($activity->causer->photo_path) }}" alt="User Avatar">
							 
                              <div class="media-body">
                                  <h3 class="dropdown-item-title mb-1"><strong style="text-transform:capitalize">{{ $activity->causer->name }}</strong></h3>
                                  <p class="mb-2">{{ $activity->description }}</p>
                                  <p class="mb-0 d-none">{{ $activity->changes }}</p>
                                  <p class="text-sm text-muted mb-0 float-right"><i class="far fa-clock mr-1"></i> {{$activity->created_at}}</p>
                              </div>
                            </div>
                          </li>
                          <!-- /.item -->
                          @endforeach
                        </ul>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer text-center">
                        <a href="{{ route('dashboard.systemlog') }}" class="uppercase">View All</a>
                      </div>
                      <!-- /.card-footer -->
                    </div>
				
				</div>
			</div>

			<!-- bar chart -->
			<!-- /.bar chart -->
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
/*
   let vendors = {!! json_encode($data['vendors']) !!};
   let monthLabel = {!! json_encode($data['monthLabel']) !!};
   let nuskinTotal = {!! json_encode($data['nuskinTotal']) !!};
*/   
   let vendors = @json($data['vendors']) ;
   let monthLabel = @json($data['monthLabel']) ;
   let nuskinTotal = @json($data['nuskinTotal']) ;
Highcharts.chart('delsuccess', {
    chart: {
        type: 'column'
    },
    title: {
        text: ""//'Monthly Graph'
    },
    subtitle: {
        text: ''//'Nuskin'
    },
    credits: {
      enabled: false
    },
    xAxis: {
        categories: monthLabel,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total Delivery Number'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        footerFormat: '</table>',

        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.3,
            borderWidth: 0
        }
    },
    series: vendors
});
</script>
<script>
  Highcharts.chart('deltotal', {
     chart: {
         type: 'line',
     },
     title: {
         text: 'Monthly Total Deliverey'
     },
     xAxis: {
         categories: monthLabel
     },
     yAxis: {
         title: {
             text: 'Total Delivery'
         }
     },
     plotOptions: {
         line: {
             dataLabels: {
                 enabled: true
             },
             enableMouseTracking: false
         }
     },
     credits: {
       enabled: false
     },
     series: [
       {
         name: 'Total Delivery Number Per Month',
         color: 'black',
         data: nuskinTotal
       }]
 });
</script>
@endsection
