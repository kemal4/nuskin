@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>System Log</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">System Log</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
              @if ($sukses = Session::get('sukses'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $sukses }}</strong>
                </div>
            @endif

            <div class="card-header">  
            </div>

            <div class="card-body"> 
                <div class="tab-content mt-2" id="custom-content-below-tabContent">
                    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                        <table id="postTable" class="table table-bordered text-nowrap" width="100%">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th>&nbsp;</th>-->
                                    <th>Profile Picture</th>
                                    <th>User</th>
                                    <th>Role</th>
                                    <th>Description</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                        </table>
                    </div> 
                </div>

            <!-- /.card-body -->
            </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  
   
    $('#postTable').DataTable({
        // scrollX:        true,
        processing: true,
        serverSide: true,
        "ajax": {
            "url": "{{ route('dashboard.systemlogJson') }}",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: "{{csrf_token()}}"},       
        },
        columns: [
            { "data": "image", "name": 'image' },
            { "data": "causer_name", "name": 'causer_name' },
            { "data": "role", "name": 'role' },
            { "data": "description", "name": 'description' },
            { "data": "created_at", "name": 'created_at' },
        ],
        "order": [[ 4, "desc" ]]
    });

    //cb(start, end);

/*
 $('#buttonImport').on('click', function () {
    var $btn = $(this).button('loading')
    // business logic...
    $btn.button('reset')
  })
  */   

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  
  let allSes = @json(Session::all());
  console.log(allSes);
  
  /*
  $('select').select2({
    theme: 'bootstrap4',
  });
  */
});   
  </script>
@endsection
