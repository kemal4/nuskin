@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Delivery Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('list-pickup')}}">Delivery Order</a></li>
              <li class="breadcrumb-item active">Edit Delivery Order</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="container">
		<div class="container-fluid">
        <form action="{{ route('saveEditDO', ['id' => $dataDO->id]) }}" method="post">
        @csrf
		  
        <div class="row">
          <div class="col-lg-6">
            <div class="card">

                <div class="card-header">
                    <h4>Customer Detail</h4>
                </div>

                <div class="card-body">
					<div class="row">
					  <div class="col">
						  						
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label control-label">Customer Name</label>
                            <div class="col">
								<input type="text" class="form-control " name="customname" value="{{ old('customname',$dataDO->customer_name) }}" required>
								  @error('customname')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label control-label">Customer ID</label>
                            <div class="col">
								<input type="text" class="form-control" name="customid" value="{{ old('customid',$dataDO->customer_id) }}" required="true">
								  @error('customid')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>       
                          </div>       
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label control-label">Customer Phone</label>
							<div class="col">
								<div class="input-group">								
									<div class="input-group-prepend">
									  <div class="input-group-text">+62</div>
									</div>
									<input type="text" class="form-control " name="phone" value="{{ old('phone',$dataDO->phone) }}" required>
								</div>
								  @error('phone')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>

					  
					  
					  {{-- </div>
					  					  								  <div class="col"> --}}			
					  
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label control-label">City</label>
                            <div class="col">
								<!--
								<input type="text" class="form-control " name="city" value="{{ old('city') }}" required>
								-->
								<select class="form-control custom-select" id="city" name="city" value="{{ old('city',$dataDO->city) }}" style="width: 100%;" required >
									<option value="{{ $dataDO->city }}" selected hidden class="form-control">{{ $dataDO->city }}</option>
								  	  @foreach($cities as $city)
									  <option value="{{ $city->name }}">{{ $city->name }}</option>
									  @endforeach
								</select>
								  @error('city')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label control-label">Zip</label>
                            <div class="col">
								<input type="text" class="form-control" name="zip" value="{{ old('zip',$dataDO->zip) }}" required minlength="5" maxlength="5" pattern="[0-9]{5}" placeholder="99999">
								  @error('zip')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
					  </div>					
					</div>
					<div class="row">	
					  <div class="col">							
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label control-label">Address</label>
                            <div class="col">
								<textarea rows="4" class="form-control" name="adress" value="{{ old('adress',$dataDO->adress) }}" required>{{ old('adress',$dataDO->adress) }}</textarea>
								@error('adress')
								  <div class="invalid-feedback">{{ $message }}</div>
								@enderror
							</div>
                          </div>	
					  </div>					  
					</div>
					
			
				
				</div>
            </div>	
        </div>
        <div class="col-lg-6">
            <div class="card">

                <div class="card-header">
					<div class="row">
					  <div class="col-sm-8">
						<h4>Delivery Detail</h4>
                      </div>					  
					  {{-- <div class="col-sm-4">
					  					  						<h5>Product Dimension</h5>
					  					                        </div> --}}
                    </div>
					  
                </div>

                <div class="card-body">
					
					<div class="row">
					  <div>
					  
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Invoice ID/SO</label>
                            <div class="col">
								<input type="text" class="form-control " name="invoiceid" value="{{ old('invoiceid',$dataDO->order_id) }}" required>
								  @error('invoiceid')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Invoice Date</label>
                            <div class="col">
								<input type="date" class="form-control " name="createdate" value="{{ old('createdate',$dataDO->create_date) }}" required>
								  @error('createdate')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">HAWB</label>
                            <div class="col">
								<input type="text" class="form-control " id="hawb"  name="hawb" value="{{ old('hawb',$dataDO->hawb) }}" required>
								  @error('hawb')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>     
                          </div>     
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Delivery Number</label>
                            <div class="col">
								<input type="text" class="form-control " id="delivery_number" name="delivery_number" value="{{ old('delivery_number',$dataDO->delivery_number) }}" required>
								  @error('delivery_number')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>				
					  
							
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Warehouse</label>
                            <div class="col">
								<select name="gudangdo" id="gudangid" value="{{ old('gudangdo',$dataDO->warehouse_id) }}" class="form-control custom-select" style="width: 100%;" required>
								  <option value="{{ $dataDO->warehouse_id }}" selected hidden class="form-control">{{ $dataDO->warehouse_name }}</option>
								  @foreach($listgudang as  $id_gudang => $lokasi_gudang)
								  <option value="{{ $id_gudang }}">{{ $lokasi_gudang }}</option>
								  @endforeach
								</select>
								@error('gudangdo')
								  <div class="invalid-feedback">{{ $message }}</div>
								@enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Courier</label>
                            <div class="col">
								<select name="transporter" id="transporter" value="{{ old('transporter',$dataDO->courier_id) }}" class="form-control custom-select" style="width: 100%;" required>
								  <option value="{{ $dataDO->courier_id }}" selected hidden class="form-control">{{ $dataDO->transporter }}</option>
								  @foreach($listcourier as  $id => $display_name)
								  <option value="{{ $id }}">{{ $display_name }}</option>
								  @endforeach
							  </select>
							  @error('transporter')
								  <div class="invalid-feedback">{{ $message }}</div>
							  @enderror
							</div>                                
                          </div>                                
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Order Type</label>
                            <div class="col">
								<!--
								<input type="text" class="form-control " name="ordertype" value="LTC1" readonly>
								-->
								<select name="ordertype" id="ordertype" value="{{ old('ordertype',$dataDO->ordertype) }}" class="form-control custom-select" style="width: 100%;" required>
								  <option selected class="form-control" value="LTC1">-- LTC1 --</option>								  
								</select>
								  @error('ordertype')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>        
                          </div>        
						
						  
						  
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Gross Weight (kg)</label>
							<div class="col">
								<div class="input-group">
									<input type="text" class="form-control " name="grosswight" value="{{ old('grosswight',$dataDO->grosswiight) }}" required>
									<div class="input-group-prepend">
										<span class="input-group-text">kg</span>
									</div>
								</div>
								  @error('grosswight')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Net Weight (kg)</label>
							<div class="col">
								<div class="input-group">
									<input type="text" class="form-control " name="netweight"value="{{ old('netweight',$dataDO->net_weight) }}" required>							
									<div class="input-group-prepend">
										<span class="input-group-text">kg</span>
									</div>
								</div>
								  @error('netweight')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-5 col-form-label control-label">Volume (cm3)</label>
                            <div class="col">
								<div class="input-group">
									<input type="text" class="form-control " name="volume" value="{{ old('volume',$dataDO->volume) }}" required>
									<div class="input-group-prepend">
										<span class="input-group-text">cm3</span>
									</div>
								</div>
								  @error('volume')
								  <div class="invalid-feedback">{{ $message }}</div>
								  @enderror
							</div>
                          </div>
						  
                      </div>
					  
                    </div>
                  </div>

                  <div class="card-footer">
                    <div class="row">
                      <!-- <div class="col-sm-6"></div> -->
                      <div class="col-sm">
                        <a href="{{ route('list-pickup')}}"   class="btn btn-default ml-auto">Cancel
                        </a>
                       <button type="submit" class="btn btn-info ml-2"> Submit </button>
                      </div>

                    </div>
                  </div>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
	  </form>	  

    </div>
    </section>
</div>

@endsection


@section('scripts')
  <script>
  	
$(document).ready(function() {
	
	$('#city').select2({});
	
}); 
  
  </script>
@endsection