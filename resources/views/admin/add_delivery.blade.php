@extends('layoutAdmin.global')
@section('content')

<div class="content-wrapper">
  <section class="p-2 pb-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <a href="{{ route('list-pickup')}}" class="text-muted"><i class="fas fa-long-arrow-alt-left"></i> Back to list</a>
        </div>
      </div>
    </div>
  </section>
  <!-- Content Header (Page header) -->
  <section class="content-header pt-0">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Delivery Order</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('list-pickup')}}">Delivery Order</a></li>
                        <li class="breadcrumb-item active">Add Delivery Order</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <form action="{{ route('storedata') }}" method="post">
                @csrf
                <div class="row justify-content-md-center">
                    <div class="col-12 col-md-3">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Customer Detail</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Customer Name</label>
                                    <input type="text" class="form-control " name="customname" value="{{ old('customname') }}" required>
                                    @error('customname')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Customer ID</label>
                                    
                                        <input type="text" class="form-control" name="customid" value="{{ old('customid') }}" maxlength="11" required="true"> @error('customid')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    
                                </div>
                                <div class="form-group">
                                    <label>Customer Phone</label>
                                    
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">+62</div>
                                            </div>
                                            <input type="text" class="form-control " name="phone" value="{{ old('phone') }}" required>
                                        </div>
                                        @error('phone')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <!--<input type="text" class="form-control " name="city" value="{{ old('city') }}" required>-->
                                    <select class="form-control custom-select" id="city" name="city" value="{{ old('city') }}" style="width: 100%;" required>
                                      <option value="" selected disabled hidden >-- Select City --</option>
		                                  @foreach($cities as $city)
		                                  <option value="{{ $city->name }}">{{ $city->name }}</option>
		                                  @endforeach
		                                </select>
                                    @error('city')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Zip</label>
                                    
                                        <input type="text" class="form-control" name="zip" value="{{ old('zip') }}" required minlength="5" maxlength="5" pattern="[0-9]{5}" placeholder="99999"> @error('zip')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    
                                        <textarea rows="4" class="form-control" name="adress" value="{{ old('adress') }}" required></textarea> @error('adress')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-7">
                      <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Delivery Detail</h3>
                        </div>
                    
                        <div class="card-body">
                            <div class="form-group">
                                <label>Invoice ID/SO</label>
                                
                                    <input type="text" class="form-control " name="invoiceid" value="{{ old('invoiceid') }}" maxlength="10" required> @error('invoiceid')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>Invoice Date</label>
                                
                                    <input type="date" class="form-control " name="createdate" value="{{ old('createdate') }}" required> @error('createdate')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>HAWB</label>
                                
                                    <input type="text" class="form-control " id="hawb" name="hawb" value="{{ old('hawb') }}" required> @error('hawb')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>Delivery Number/Customer Reference</label>
                                
                                    <input type="text" class="form-control " id="delivery_number" name="delivery_number" value="{{ old('delivery_number') }}" maxlength="10" required> @error('delivery_number')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
            
                            <div class="form-group">
                                <label>Warehouse</label>
                                
                                    <select name="gudangdo" id="gudangid" class="form-control custom-select" style="width: 100%;" required>
            								  <option value="" selected disabled hidden class="form-control">-- Select Warehouse --</option>
            								  @foreach($listgudang as  $id_gudang => $lokasi_gudang)
            								  <option value="{{ $id_gudang }}">{{ $lokasi_gudang }}</option>
            								  @endforeach
            								</select> @error('gudangdo')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>Courier</label>
                                
                                    <select name="transporter" id="transporter" class="form-control  custom-select" style="width: 100%;" required>
            								  <option value="" selected disabled hidden class="form-control">-- Select --</option>
            								  @foreach($listcourier as  $id => $display_name)
            								  <option value="{{ $id }}">{{ $display_name }}</option>
            								  @endforeach
            							  </select> @error('transporter')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>Order Type</label>
                                
                                    <!--
            								<input type="text" class="form-control " name="ordertype" value="LTC1" readonly>
            								-->
                                    <select name="ordertype" id="ordertype" class="form-control custom-select" style="width: 100%;" required>
            								  <option selected class="form-control" value="LTC1">-- LTC1 --</option>
            								</select> @error('ordertype')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                        </div>
                      </div>
                      <!-- /.card -->
                      <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Product Dimension</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Gross Weight (kg)</label>
                                
                                    <div class="input-group">
                                        <input type="text" class="form-control " name="grosswight" value="{{ old('grosswight') }}" required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">kg</span>
                                        </div>
                                    </div>
                                    @error('grosswight')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>Net Weight (kg)</label>
                                
                                    <div class="input-group">
                                        <input type="text" class="form-control " name="netweight" value="{{ old('netweight') }}" required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">kg</span>
                                        </div>
                                    </div>
                                    @error('netweight')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                            <div class="form-group">
                                <label>Volume (cm3)</label>
                                
                                    <div class="input-group">
                                        <input type="text" class="form-control " name="volume" value="{{ old('volume') }}" required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">cm3</span>
                                        </div>
                                    </div>
                                    @error('volume')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('list-pickup')}}" class="btn btn-default"><i class="fas fa-times"></i> Cancel</a>
                            <div class="float-right">
                              <button type="submit" class="btn btn-info"> Submit </button>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection @section('scripts')
<script>
    $(document).ready(function() {
        /*
        $('#gudangid').select2({
        	theme: 'bootstrap4',
        });
        */
        $('#city').select2({});

        $('#delivery_number').on("blur", function() {
            if ($(this).val().length < 5) {
                return
            }

            let tempVal = $(this).val()
            let ajaxUrl = "{{ route('checkDOField', ['delivery_number', 'value_to_replace' ]) }}"

            $.ajax({
                method: "GET",
                url: ajaxUrl.replace('value_to_replace', tempVal),
            }).done(function(data) {
                console.log("data")
                console.log(data)
                if (data.field_exist) {
                    toastr.error('Order with Delivery Number : '.concat(tempVal, ' already exist.'))
                    $('#delivery_number').val('')
                    $('#delivery_number').focus()
                }
            }).fail(function(data) {
                console.log("data")
                console.log(data)
                alert("something went wrong")
            });
        });


        $('#hawb').on("blur", function() {
            if ($(this).val().length < 5) {
                return
            }

            let tempVal = $(this).val()
            let ajaxUrl = "{{ route('checkDOField', ['hawb', 'value_to_replace' ]) }}"

            $.ajax({
                method: "GET",
                url: ajaxUrl.replace('value_to_replace', tempVal),
            }).done(function(data) {
                console.log("data")
                console.log(data)
                if (data.field_exist) {
                    toastr.error('Order with hawb : '.concat(tempVal, ' already exist.'))
                    $('#hawb').val('')
                    $('#hawb').focus()
                }
            }).fail(function(data) {
                console.log("data")
                console.log(data)
                alert("something went wrong")
            });
        });



    });
</script>
@endsection
