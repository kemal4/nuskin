@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">

      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Performance</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
       <div class="card card-primary card-outline">

        <div class="card-body">
          <div class="row">
              <div class="col-md-12">
                <center><label style="color: red"><h3> Lead Time By : </h3></label></center>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group" id="input-group">
                    <button type="button" class="btn btn-block btn-info btn-sm" id="listnuskin">Nu Skin</button>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group" id="input-group">
                    <button type="button" class="btn btn-block btn-info btn-sm" id="listvendor">Vendor</button>
                  </div>
                </div>

              </div>
              <div class="col-md-12" id="choicevendor">
                  <center><h3>Vendor</h3> </center>
              </div>
              <div class="col-md-12" id="choicenuskin">
                  <center><h3>NuSkin</h3> </center>
              </div>
            <!-- row -->
            </div>
          </div>
          <hr>
          <div class="card-body" id="">
                  <!-- start table -->
                  <div class="tab-content mt-2" id="custom-content-below-tabContent">
                    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                      <table id="vendors" class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Vendor</th>
                            <th>Delivery Success</th>
                            <th>Exceeding Lead Time</th>
                            <th>Total Complain</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($perfom as $pf) 
                          <tr>
                              <td>{{ $pf->transporter}}</td>
                              <td>{{ $pf->delivsuccess}}</td>
                              <td>{{ $pf->exceeding}}</td>
                              @if($pf->transporter == 'JNE')
                                <td>{{ $tcktJNE}}</td>
                              @elseif($pf->transporter == 'FL')
                                <td>{{ $tcktFL}}</td>
                              @else
                                <td>{{ $tcktSpeedy}}</td>
                              @endif
                              <td><a href="{{ route('vendordetail', ['id' => $pf->transporter]) }}">Detail</a></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- end table -->
          </div>
        </div>
      </section>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript"> 
    $(function(){
      $('#choicevendor').hide()

      $('#vendors').DataTable();

      $("#listvendor").click(function(){
          $('#choicenuskin').hide()
          $("#choicevendor").show();
      })

      $("#listnuskin").click(function(){
          $("#choicevendor").hide();
          $('#choicenuskin').show()
      })

      $('.btn-sm').on('click', function(){
        $('.btn-sm').removeClass('active');
        $(this).addClass('active');
      })

    });
  </script>
@endsection