@extends('layoutAdmin.global')


@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Complain Ticket</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
            <li class="breadcrumb-item active">Complain Ticket</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>


  <div class="modal fade" id="modal-danger" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Danger Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          @can('create_ticket_delivery')		  
          <!--Button Add new Ticket -->
          <div class="card-header">
            <h3 class="card-title">
              <a href="{{ route('newticket',['id' => 0] )}}"   class="btn btn-primary mr-5 mb-2">
                Add Ticket
              </a>
            </h3>
          </div>
          @endcan          
          <!-- Filter  -->
          <div class="card-header d-none">
            <form action="{{ route('filterTicket') }}" method="GET">
              @csrf
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Filter SO Number :</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-search"></i></span>
                      </div>
                      <input type="text" class="form-control" name="filtersonumber" value="">
                    </div>
                  </div>
                  <input type="submit" value="Cari" class="btn btn-primary">
                  <a href="{{ route('listticket')}}" class="btn btn-secondary">Reset</a>
                </div>
              </div>
            </form>
          </div>

          <!-- /.card-header -->
          <div class="card-body">
            {{-- notifikasi form validasi --}}
            @if ($errors->has('file'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('file') }}</strong>
            </span>

            @endif
            {{-- notifikasi sukses --}}
            @if ($sukses = Session::get('sukses'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $sukses }}</strong>
            </div>
            @endif

            {{-- notifikasi error --}}
            @if ($error = Session::get('error'))
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $error }}</strong>
            </div>
            @endif
            <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">All</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-content-below-belum-tab" data-toggle="pill" href="#custom-content-below-belum" role="tab" aria-controls="custom-content-below-belum" aria-selected="false">Unprocessed</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-sedang" role="tab" aria-controls="custom-content-below-sedang" aria-selected="false">On Process</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-content-below-solved-tab" data-toggle="pill" href="#custom-content-below-solved" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Solved</a>
              </li>
            </ul>
            <div class="tab-content mt-2" id="custom-content-below-tabContent">
              <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                <table id="tickets_all" class="table text-nowrap table-bordered">
                  <thead class="thead-light">
                    <tr>
                      <th>Due Date</th>
                      <th>Action</th>
                      <th>Status</th>
                      <th>Ticket Id</th>
                      <th>SO Number</th>
                      <th>Customer Id</th>
                      <th>Time</th>
                      <th>Topic</th>
                      <th>Complainer Name</th>
                      <th>Complainer Email</th>
                      <th>Complainer Phone</th>
                      <th>Customer Name</th>
                    </tr>
                  </thead>
                  <tbody>

                    @php $i=1 @endphp
                    @foreach($ticket as $tckt)
                    <?php  
                    $date = date("m/d/Y",strtotime($tckt->created_at)); 
                    ?>
                    <tr>
                      @if ($tckt->isOverDue)
                       <td><span class="badge badge-danger ">
                        {{$tckt->due}}
                      </span></td>              
                      @else
                      <td>{{$tckt->due}}</td>         
                      @endif
                      <td>
                        @can('edit_ticket_delivery')
                        <a href="{{route('detailticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'>
                          <i class='fas fa-info-circle'></i>
                        </a> 
                        @endcan
                        @if($tckt->status == 'Solved')
                        @can('edit_ticket_delivery')
                        <a class='btn btn-xs btn-warning btn-modal-open' data-toggle='tooltip' title='Re Open' tid="{{$tckt->id}}">
                          <i class="far fa-folder-open"></i>
                        </a> 
                        @endcan
                        @else
                        @if($tckt->is_locked)
                        <a href="{{route('releaseticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-info' data-toggle='tooltip' title='Release Lock'>
                         <i class="fas fa-lock-open"></i>
                         <!--<i class='fas fa-unlock-alt'></i>-->
                       </a> 							
                       @endif							
                       @endif
                     </td>


                     <td><span class="badge badge-<?=($tckt->status=='Solved')?'success' : (($tckt->status=='On Proccess')?'info' : 'warning') ?> ">
                       {{$tckt->status}}
                     </span></td>

                     <td>{{$tckt->id_ticket}}</td>
                     <td>{{$tckt->order_id}}</td>
                     <td>{{$tckt->customer_id}}</td>
                     <td>{{$date}}</td>
                     <td>{{$tckt->topics}}</td>
                     <td>{{$tckt->nama_pelapor}}</td>
                     <td>{{$tckt->email}}</td>
                     <td>{{$tckt->telepon}}</td>
                     <td>{{$tckt->nama_customer}}</td>
                     

                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane fade" id="custom-content-below-belum" role="tabpanel" aria-labelledby="custom-content-below-belum-tab">
              <table id="example2" class="table text-nowrap table-bordered">
                <thead class="thead-light">
                  <tr>
                    <th>Action</th>
                    <th>Status</th>
                    <th>Ticket Id</th>
                    <th>SO Number</th>
                    <th>Customer Id</th>
                    <th>Time</th>
                    <th>Topic</th>
                    <th>Complainer Name</th>
                    <th>Complainer Email</th>
                    <th>Complainer Phone</th>
                    <th>Customer Name</th>
                  </tr>
                </thead>
                <tbody>

                  @php $i=1 @endphp
                  @foreach($ticketbelum as $tckt)
                  <?php  
                  $date = date("m/d/Y",strtotime($tckt->created_at)); 
                  ?>
                  <tr>

                    <td>
                      @can('edit_ticket_delivery')
                      <a href="{{route('detailticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'>
                       <i class='fas fa-search'></i>
                     </a> 
                     @endcan
                     @if($tckt->status == 'Solved')
                     @can('edit_ticket_delivery')
                     <a class='btn btn-xs btn-warning btn-modal-open' data-toggle='tooltip' title='Re Open' tid="{{$tckt->id}}">
                       <i class="far fa-folder-open"></i>
                     </a> 
                     @endcan
                     @else
                     @if($tckt->is_locked)
                     <a href="{{route('releaseticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-info' data-toggle='tooltip' title='Release Lock'>
                       <i class="fas fa-lock-open"></i>
                       <!--<i class='fas fa-unlock-alt'></i>-->
                     </a> 							
                     @endif							
                     @endif
                   </td>
                   <td><span class="badge badge-<?=($tckt->status=='Solved')?'success' : (($tckt->status=='On Proccess')?'info' : 'warning') ?> ">
                     {{$tckt->status}}
                   </span></td>
                   <td>{{$tckt->id_ticket}}</td>
                   <td>{{$tckt->order_id}}</td>
                   <td>{{$tckt->customer_id}}</td>
                   <td>{{$date}}</td>
                   <td>{{$tckt->topics}}</td>
                   <td>{{$tckt->nama_pelapor}}</td>
                   <td>{{$tckt->email}}</td>
                   <td>{{$tckt->nama_customer}}</td>
                   <td>{{$tckt->telepon}}</td>
                 </tr>
                 @endforeach
               </tbody>
             </table>
           </div>
           <!-- /.tab-pane --> 
           <div class="tab-pane fade" id="custom-content-below-sedang" role="tabpanel" aria-labelledby="custom-content-below-sedang-tab">
            <table id="example3" class="table text-nowrap table-bordered">
              <thead class="thead-light">
                <tr>
                  <th>Action</th>
                  <th>Status</th>
                  <th>Ticket Id</th>
                  <th>SO Number</th>
                  <th>Customer Id</th>
                  <th>Time</th>
                  <th>Topic</th>
                  <th>Complainer Name</th>
                  <th>Complainer Email</th>
                  <th>Complainer Phone</th>
                  <th>Customer Name</th>
                </tr>
              </thead>
              <tbody>

                @php $i=1 @endphp
                @foreach($ticketdiproses as $tckt)
                <?php  
                $date = date("m/d/Y",strtotime($tckt->created_at)); 
                ?>
                <tr>

                  <td>
                    @can('edit_ticket_delivery')
                    <a href="{{route('detailticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'>
                     <i class='fas fa-search'></i>
                   </a> 
                   @endcan
                   @if($tckt->status == 'Solved')
                   @can('edit_ticket_delivery')
                   <a class='btn btn-xs btn-warning btn-modal-open' data-toggle='tooltip' title='Re Open' tid="{{$tckt->id}}">
                     <i class="far fa-folder-open"></i>
                   </a> 
                   @endcan
                   @else
                   @if($tckt->is_locked)
                   <a href="{{route('releaseticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-info' data-toggle='tooltip' title='Release Lock'>
                     <i class="fas fa-lock-open"></i>
                     <!--<i class='fas fa-unlock-alt'></i>-->
                   </a> 							
                   @endif							
                   @endif
                 </td>
                 <td><span class="badge badge-<?=($tckt->status=='Solved')?'success' : (($tckt->status=='On Proccess')?'info' : 'warning') ?> ">
                   {{$tckt->status}}
                 </span></td>
                 <td>{{$tckt->id_ticket}}</td>
                 <td>{{$tckt->order_id}}</td>
                 <td>{{$tckt->customer_id}}</td>
                 <td>{{$date}}</td>
                 <td>{{$tckt->topics}}</td>
                 <td>{{$tckt->nama_pelapor}}</td>
                 <td>{{$tckt->email}}</td>
                 <td>{{$tckt->nama_customer}}</td>
                 <td>{{$tckt->telepon}}</td>
               </tr>
               @endforeach
             </tbody>
           </table>
         </div>
         <!-- /.tab-pane -->
         <div class="tab-pane fade" id="custom-content-below-solved" role="tabpanel" aria-labelledby="custom-content-below-solved-tab">
          <table id="example4" class="table text-nowrap table-bordered">
            <thead class="thead-light">
              <tr>
                <th>Action</th>
                <th>Status</th>
                <th>Ticket Id</th>
                <th>SO Number</th>
                <th>Customer Id</th>
                <th>Time</th>
                <th>Topic</th>
                <th>Complainer Name</th>
                <th>Complainer Email</th>
                <th>Complainer Phone</th>
                <th>Customer Name</th>
              </tr>
            </thead>
            
            <tbody>

              @php $i=1 @endphp
              @foreach($ticketsolved as $tckt)
              <?php  
              $date = date("m/d/Y",strtotime($tckt->created_at)); 
              ?>
              <tr>

                <td>
                  @can('edit_ticket_delivery')
                  <a href="{{route('detailticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-success' data-toggle='tooltip' title='View Detail'>
                   <i class='fas fa-search'></i>
                 </a> 
                 @endcan
                 @if($tckt->status == 'Solved')
                 @can('edit_ticket_delivery')
                 <a class='btn btn-xs btn-warning btn-modal-open' data-toggle='tooltip' title='Re Open' tid="{{$tckt->id}}">
                   <i class="far fa-folder-open"></i>
                 </a> 
                 @endcan
                 @else
                 @if($tckt->is_locked)
                 <a href="{{route('releaseticket', ['id' => $tckt->id])}}" class='btn btn-xs btn-info' data-toggle='tooltip' title='Release Lock'>
                   <i class="fas fa-lock-open"></i>
                   <!--<i class='fas fa-unlock-alt'></i>-->
                 </a> 							
                 @endif							
                 @endif
               </td>
               <td><span class="badge badge-<?=($tckt->status=='Solved')?'success' : (($tckt->status=='On Proccess')?'info' : 'warning') ?> ">
                 {{$tckt->status}}
               </span></td>
               <td>{{$tckt->id_ticket}}</td>
               <td>{{$tckt->order_id}}</td>
               <td>{{$tckt->customer_id}}</td>
               <td>{{$date}}</td>
               <td>{{$tckt->topics}}</td>
               <td>{{$tckt->nama_pelapor}}</td>
               <td>{{$tckt->email}}</td>
               <td>{{$tckt->nama_customer}}</td>
               <td>{{$tckt->telepon}}</td>
             </tr>
             @endforeach
           </tbody>

         </table>
       </div>
       <!-- /.tab-pane -->
     </div>
     <!-- /.tab-content mt-2 -->  
   </div>
   <!-- /.card-body -->
 </div>
 <!-- /.card -->

</div>
<!-- /.col -->
</div>
<!-- /.row -->
<!-- Modal-->
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Status</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <p class="text-center">Apakah Anda Yakin Ingin Mengubah Status?</p>
        </div>
        <div class="modal-footer">
          <a class="btn btn-primary btn-action-update" href="#">Yes</a>
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</section>
<!-- /.content -->
</div>
@endsection

@section('scripts')

<!--Sweet Alert-->

<script type="text/javascript">
  $("a.btn-modal-open").on("click", function() {
    let urlOpenTicket = "{{ route('re-open','to_replace')}}"
    Swal.fire({
      title: 'Are you sure you want to change status?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
        if (result.value) { //yes
          urlOpenTicket = urlOpenTicket.replace('to_replace', $(this).attr("tid"))
          $.ajax({
            method: "GET",
            url: urlOpenTicket,
          }).done(function() {
            alert("success. page will reload")
            window.location.reload()
          }).fail(function(){
            alert("something went wrong")
          });
        }
      })
  });


  $(function () {

    $("#tickets_all").DataTable({
     "fnDrawCallback": function( oSettings ) {					  
       $('[data-toggle="tooltip"]').tooltip();   
     },
        scrollX:        true,
        "fixedColumns": {
          "leftColumns": 2
        },
        // scrollCollapse: true,
        'paging':         true,
      });


    // $('#example2').DataTable({

    //     processing: true,
    //     serverSide: true,
    //     "ajax": {
    //         "url": "{{ route('allNotSolvedTicket') }}",
    //         "dataType": "json",
    //         "type": "POST",
    //         "data":{ _token: "{{csrf_token()}}"},       
    //     },
    //     columns: [
    //         { "data": "action", "name": 'action' },
    //         { "data": "status", "name": 'status' },
    //         { "data": "id_ticket", "name": 'id_ticket' },
    //         { "data": "order_id", "name": 'order_id' },
    //         { "data": "created_at", "name": 'created_at' },
    //         { "data": "topics", "name": 'topics' },
    //         { "data": "nama_pelapor", "name": 'nama_pelapor' },
    //         { "data": "email", "name": 'email' },
    //         { "data": "telepon", "name": 'telepon' },
    //         { "data": "nama_customer", "name": 'nama_customer' },
            
    //     ],

    // });

    $("#example2").DataTable({
        // "fixedColumns": {
        //   "leftColumns": 2
        // },
        // scrollCollapse: true,
        paging:         true,
      });
    $("#example3").DataTable({
        // "fixedColumns": {
        //   "leftColumns": 2
        // },
        // scrollCollapse: true,
        paging:         true,
      });
    $("#example4").DataTable({
        // scrollCollapse: true,
        paging:         true,
      });

    $("#example2").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
    $("#example3").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
    $("#example4").wrap("<div style='overflow:auto; width:100%;position:relative;'></div>");
  });
</script>


@endsection