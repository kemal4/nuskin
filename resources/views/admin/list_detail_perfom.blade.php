@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
         @if($id == 'JNE')
         <h1>Performance JNE</h1>
         @elseif($id == 'FL')
         <h1>Performance FL</h1>
         @else
         <h1>Performance Mr Speedy</h1>
         @endif
       </div>
       <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item active">Performance</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
 <div class="card card-primary card-outline">

  @if($id == 'JNE')
  <div class="card-body">
    <center>
      <div class="chart-container">
        <div class="pie-chart-container" style="width:40%">
          <canvas id="pie-chart"></canvas>
        </div>
      </div>
    </center>
  </div>
  @else
  <div class="card-body">
    <center>
      <div class="chart-container">
        <div class="pie-chart-container" style="width:40%">
          <canvas id="pie-chart"></canvas>
        </div>
      </div>
    </center>
  </div>
  @endif

  <div class="card-body">
    
    <form action="{{ route('filterzona') }}" method="GET">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Filter By Month :</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
              </div>
              <input type="Month" class="form-control" name="filtermonth">
            </div>
          </div>
           <input type="hidden" name="transporter" value="<?= $id ?>">
       </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Filter By Years :</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
              </div>
              <input type="text" id="datepicker" class="form-control" name="filteryears">
            </div>
          </div>
           <input type="hidden" name="transporter" value="<?= $id ?>">
       </div>
       <div class="col-md-6">
        <div class="form-group">
          <label></label>
          <div class="input-group">
            <div class="input-group-prepend mt-2">
              <center>
                <input type="submit" value="Filter" class="btn btn-secondary btn-md">
                <a href="{{ route('vendordetail', ['id' => $id])}}"   class="btn btn-secondary btn-md">Reset
                </a>
              </center>
            </div>
          </div>
        </div>
      </div>
      <!-- row -->
    </div>
    <!-- card-body -->
  </form>
</div>

<div class="card-body">
  <!-- start table -->
  <div class="tab-content mt-2" id="custom-content-below-tabContent">
    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">

      <table id="vendors" class="table table-bordered">
       <thead>
        <tr>
          <th rowspan="2" style="text-align: center;">Zone</th>
          <th rowspan="2" style="text-align: center;">City</th>
          <th rowspan="2" style="text-align: center;">Zip</th>
          <th rowspan="2" style="text-align: center;">Total Delivery</th>
          <th colspan="2" style="text-align: center;">Fulfilled</th>
          <th colspan="2" style="text-align: center;">Unfulfilled</th>
        </tr>
        <tr>
         <th>Qty</th>
         <th>%</th>
         <th>Qty</th>
         <th>%</th>
       </tr>
     </thead>
     <tbody>

       @if($id == 'JNE')
       @foreach($zonacity as $zona) 
       <tr>
        <td> {{ $zona->label_zonasi}} </td>
        <td> {{ $zona->city}} </td>
        <td> {{ $zona->zip}} </td>
        <td> {{ $zona->total}} </td>
        <td> {{ $zona->fullfill }} </td>
        <td> {{ $zona->pctFulfilled      = ($zona->fullfill / ($zona->fullfill + $zona->unfull)) *100 }}% </td>
        <td> {{ $zona->unfull}} </td>
        <td> {{ $zona->pctUnfulfilled = ($zona->unfull / ($zona->fullfill + $zona->unfull)) *100 }}% </td>
       </tr>
       @endforeach
       @elseif($id == 'FL')
       @foreach($zonafl as $znFL) 
       <tr>
        <td> {{ $znFL->label_zonasi}} </td>
        <td> {{ $znFL->city}} </td>
        <td> {{ $znFL->zip}} </td>
        <td> {{ $znFL->total}} </td>
        <td> {{ $znFL->fullfill}} </td>
        <td> {{ $znFL->pctFulfilled   = ($znFL->fullfill / ($znFL->fullfill + $znFL->unfull)) *100 }}% </td>
        <td> {{ $znFL->unfull}} </td>
        <td> {{ $znFL->pctUnfulfilled = ($znFL->unfull / ($znFL->fullfill + $znFL->unfull)) *100 }}% </td>
       </tr>
       @endforeach
       @else
        @foreach($zonaspeedy as $znFL) 
       <tr>
          <td> {{ $znFL->label_zonasi}} </td>
          <td> {{ $znFL->city}} </td>
          <td> {{ $znFL->zip}} </td>
          <td> {{ $znFL->total}} </td>
          <td> {{ $znFL->fullfill}} </td>
          <td> {{ $znFL->pctFulfilled   = ($znFL->fullfill / ($znFL->fullfill + $znFL->unfull)) *100 }}% </td>
          <td> {{ $znFL->unfull}} </td>
          <td> {{ $znFL->pctUnfulfilled = ($znFL->unfull / ($znFL->fullfill + $znFL->unfull)) *100 }}% </td>
       </tr>
       @endforeach
       @endif

     </tbody>
   </table>
   
  @if($id == 'JNE')
  <div class="form-group">
    <a href="{{ route('export_vendor')}}" class="btn btn-primary mr-5 mb-2">
      Download
    </a>
  </div>
  @elseif($id == 'FL')
  <div class="form-group">
    <a href="{{ route('export_vendor')}}" class="btn btn-primary mr-5 mb-2">
      Download
    </a>
  </div>
  @else 

  @endif

</div>
</div>
<!-- end table -->

   <div class="form-group">
    <a href="{{ route('performance_vendor')}}" class="btn btn-primary mr-5 mb-2">
      Back
    </a>
  </div>
</div>
</div>



</section>
</div>
@endsection

@section('scripts')
<script>
  $(function(){
      //get the pie chart canvas

      var cData = JSON.parse(`<?php echo $chart_data; ?>`);
      var ctx = $("#pie-chart");

      //pie chart data
      var data = {
        labels: cData.label,
        datasets: [
          {
            label: "Users Count",
            data: cData.data,
            backgroundColor: [
              "#163adb",
              "#db2316",
            ],
            borderColor: [
              "#CDA776",
              "#989898",
            ],
            borderWidth: [1, 1]
          }
        ]
      };

      var vendor = "<?php echo $id; ?>";
      //options
      if (vendor == 'JNE') {
        var options = {
          responsive: true,
          title: {
            display: true,
            position: "top",
            text: "Total Deliveries JNE",
            fontSize: 18,
            fontColor: "#111"
          },
          legend: {
            display: true,
            position: "bottom",
            labels: {
              fontColor: "#333",
              fontSize: 16
            }
          }
        };
      }else if(vendor == 'FL') {
        var options = {
          responsive: true,
          title: {
            display: true,
            position: "top",
            text: "Total Deliveries FL",
            fontSize: 18,
            fontColor: "#111"
          },
          legend: {
            display: true,
            position: "bottom",
            labels: {
              fontColor: "#333",
              fontSize: 16
            }
          }
        };

      }else{
        var options = {
          responsive: true,
          title: {
            display: true,
            position: "top",
            text: "Total Deliveries Mr Speedy",
            fontSize: 18,
            fontColor: "#111"
          },
          legend: {
            display: true,
            position: "bottom",
            labels: {
              fontColor: "#333",
              fontSize: 16
            }
          }
        };
      }


      //create Pie Chart class object
      var chart1 = new Chart(ctx, {
        type: "pie",
        data: data,
        options: options
      });

  });
</script>
<script type="text/javascript"> 
  $(function(){
    $('#vendors').DataTable();

    $('#datepicker').datepicker({
      autoclose: true,
      language: 'id',
      format   : 'yyyy'
    });
    $('#datemonth').datepicker({
      autoclose: true,
      language: 'id',
      format   : 'mm'
    });
    datemonth

  });
</script>
@endsection