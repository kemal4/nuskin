@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Ticket Detail : <?= $ticketdetail->id_ticket  ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
            <li class="breadcrumb-item"><a href="{{ route('listticket')}}">Complain Ticket </a></li>
            <li class="breadcrumb-item active">Ticket Detail</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Modal Due Date -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Due Date</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <div class="row">
              <label for="idDueDate">Due Date : </label>
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="duedetail" id="idDueDate" readonly="readonly" class="form-control "> <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="duedate" data-dismiss="modal">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal Due Dtae -->

  {{-- notifikasi sukses --}}
  @if ($berhasil = Session::get('berhasil'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $berhasil }}</strong>
  </div>
  @endif
  <form class="content" action="{{ route('storeeditticket', ['id' => $ticketdetail->id ]) }}" method="post" role="form" enctype="multipart/form-data">
    @csrf
    <!-- Main content -->
    <section class="content">
	  <div class="row">
		<div class="col-sm">
	
		  <div class="card">
		  
			<div class="card-header border-transparent d-none">
				<h3 class="card-title">Ticket Detail</h3>
							
				<div class="card-tools">
				  <a href="" class="btn btn-outline-secondary btn-xs">Back</a>
				  <button type="button" class="btn btn-outline-info btn-xs"><i class="fas fa-minus"></i> Save</button>
				</div>
			</div>
			
			<div class="card-body">
				<div class="float-right">
					<div class="row">
						<div class="col">
						</div>
						<div class="col">
						  <a href="" class="btn btn-block btn-outline-secondary btn-xs">Back</a>						  
						</div>
						<div class="col">
						  <button type="button" class="btn btn-block btn-outline-info btn-xs">Save</button>				
						</div>
					</div>
					<div class="row">
						<div class="col">
						  <a href="" class="btn btn-info btn-xs">Change Due Date</a>
						  <button type="button" class="btn btn-info btn-xs ml-1">Solve Complaint</button>				
						</div>
					</div>
				</div>
			  <div class="row">
				<div class="col-sm-4">
				
			<div class="info-box">
              <div class="info-box-content">
					<strong>Status </strong>
					<h3>{{$ticketdetail->status}}</h3>
              </div>
              <!-- /.info-box-content -->
            </div>	
			<div class="info-box">
              <div class="info-box-content">
                <?php $date = date("m/d/Y", strtotime($ticketdetail->created_at));  ?>
                  <strong> Created Date </strong>
                  <h3 class="text-muted">{{ $date }}</h3>
                  <br>
                  <strong> Due Date </strong>
                  <h3 class="text-muted">{{ $ticketdetail->due }}</h3>
              </div>
              <!-- /.info-box-content -->
            </div>	
<div class="info-box d-none">
  <div class="inner">
					info-box inner
					<h3 >{{$ticketdetail->status}}</h3>
  </div>
</div>
<div class="small-box d-none">
  <div class="inner">
					small-box	inner
					<h3 >{{$ticketdetail->status}}</h3>
  </div>
</div>
<div class="card d-none">
  <div class="card-body">
					<strong> card </strong>
					<h3>{{$ticketdetail->status}}</h3>
  </div>
</div>


			
					
				</div>
				<div class="col-sm">
				
				  <table class="table table-borderless text-nowrap">
                    <tbody>
                      <tr>
                        <th style="width: 1%;">Topics :</th>
                        <td>{{$ticketdetail->topics}}</td>
                      </tr>
                      <tr>
                        <th>Invoice Id :</th>
                        <td>{{$ticketdetail->order_id}}</td>
                      </tr>
                      <tr>
                        <th>Customer Name :</th>
                        <td>{{$ticketdetail->nama_customer}}</td>
                      </tr>
                      <tr>
                        <th>Complainer Name :</th>
                        <td>{{$ticketdetail->nama_pelapor}}</td>
                      </tr>
                      <tr>
                        <th>Complainer Email :</th>
                        <td>{{$ticketdetail->email}}</td>
                      </tr>
                      <tr>
                        <th>Complainer Phone :</th>
                        <td>{{$ticketdetail->telepon}}</td>
                      </tr>
                    </tbody>
				  </table>
				
				
				</div>
			  </div>
			  <div class="row">
				<div class="col-sm-12 col-md-6">									
                          <div class="form-group">
                            <label class="col col-form-label control-label">Description</label>
                            <div class="col">
								<textarea class="form-control" rows="3" id="description" name="description" value="{{$ticketdetail->description}}" required></textarea>
								@error('adress')
								  <div class="invalid-feedback">{{ $message }}</div>
								@enderror
							</div>
                          </div>
				</div>
			<!--	
			  </div>
			  <div class="row">
			-->  
				<div class="col-xs-12 col-md-6">
					<label class="col-sm col-form-label control-label">Attachment</label>
							
					<div class="row">
					<div class="col-sm">							
					<div class="testSlick">
					  <div>							
						<a href="{{URL::to('/')}}/image/200400101.jpeg" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/200400101.jpeg" alt="" title="image">
						</a>
					  </div>
					  <div>							
						<a href="{{URL::to('/')}}/image/200400102.jpeg" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/200400102.jpeg" alt="" title="image">
						</a>
					  </div>
					  <div>							
						<a href="{{URL::to('/')}}/image/NewLogo2.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo2.png" alt="" title="image">
						</a>
					  </div>
					  <div>						
						<a href="{{URL::to('/')}}/image/NewLogo3.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo3.png" alt="" title="image">
						</a>
					  </div>
					  <div>						
						<a href="{{URL::to('/')}}/image/NewLogo4.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo4.png" alt="" title="image">
						</a>
					  </div>
					  <div>						
						<a href="{{URL::to('/')}}/image/NewLogo5.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo5.png" alt="" title="image">
						</a>
					  </div>
					  <div>						
						<a href="{{URL::to('/')}}/image/NewLogo1.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo1.png" alt="" title="image">
						</a>
					  </div>
					  <div class="col-sm-4 text-center align-self-center">
						<i class="fas fa-plus-square"></i>
					  </div>
					</div>
					</div>
					</div>
					<!--
					<div class="row">
					  <div class="col-sm-4 border">							
						<a href="{{URL::to('/')}}/image/NewLogo2.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo2.png" alt="" title="image">
						</a>
					  </div>
					  <div class="col-sm-4 border">						
						<a href="{{URL::to('/')}}/image/NewLogo3.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo3.png" alt="" title="image">
						</a>
					  </div>
					  <div class="col-sm-4 border">						
						<a href="{{URL::to('/')}}/image/NewLogo4.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo4.png" alt="" title="image">
						</a>
					  </div>
					  <div class="col-sm-4 border">						
						<a href="{{URL::to('/')}}/image/NewLogo5.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo5.png" alt="" title="image">
						</a>
					  </div>
					  <div class="col-sm-4 border">						
						<a href="{{URL::to('/')}}/image/NewLogo1.png" data-toggle="lightbox" data-gallery="attach-gallery">
							<img class="img-fluid" src="{{URL::to('/')}}/image/NewLogo1.png" alt="" title="image">
						</a>
					  </div>
					  <div class="col-sm-4 text-center align-self-center">
						<i class="fas fa-plus-square"></i>
					  </div>
					</div>
					-->
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="row">
		<div class="col-sm">
	
		  <div class="card">
			<div class="card-header border-transparent d-none">
				<h3 class="card-title">Activity</h3>			
			</div>
			<div class="card-body">
			  <div class="row">
				<div class="col-sm">
					<h3 class="">Activity</h3>
					
				
					<table class="table table-bordered table-hover" id="activitylogs">
					  <thead class="thead-light">
						<tr>
						  <th>Status</th>
						  <th>Date</th>
						  <th>Time</th>
						  <th>Description</th>
						  <th>Updated by</th>
						</tr>
					  </thead>
					  <tbody>
						<?php 
						$dateactivity = $ticketdetail->updated_at;
						$timeactivity = $ticketdetail->updated_at;
						?>

						@php $i=1 @endphp @foreach($ticketlogs as $logs)
						<tr>
						  <td>{{$logs->activity}}</td>
						  <td><?= date("m-d-Y", strtotime($dateactivity));  ?></td>
						  <td><?= date("H:i", strtotime($timeactivity));  ?></td>
						  <td>{{$logs->description}}</td>
						  <td>{{$logs->pic}}</td>
						</tr>
						@endforeach
					  </tbody>
					</table>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	
    </section>
    <!-- /.Main content -->
  </form>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  $('#myModal').on('click','#duedate', function (e) {
    var str   = document.getElementById("changeduedate").innerHTML; 
    var date  = document.getElementById("idDueDate").value;
    var ada   = document.getElementById("changeduedate").innerHTML = date;
    document.getElementById("duedateval").value = date;
  });

  $('#idDueDate').datepicker({
   changeMonth: true,
   changeYear: true,
   minDate: 0,
   autoclose: true
 });

  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
  });

  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });

  function Upload() {
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      var fsize = $('#fileUpload')[0].files[0].size;
      if(fsize>2097152) //do something if file size more than 2 mb (2097152)
      {
        alert(fsize +" bites\nToo big!");
        var warn = "Image max 2mb";
        var element = document.getElementById("fileUpload");
        document.getElementById("tckt1").innerHTML = warn;
        element.classList.add("is-invalid");
      }else{
        alert(fsize +" bites\nYou are good to go!");
        
        var element = document.getElementById("fileUpload");
        element.classList.add("is-valid");
      }
      alert(fsize);
    }else{
      alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    } 
  }

  function Upload2() {
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
      var fsize = $('#fileUpload2')[0].files[0].size;
      if(fsize>2097152) //do something if file size more than 2 mb (2097152)
      {
        alert(fsize +" bites\nToo big!");
        var warn = "Image max 2mb";
        var element = document.getElementById("fileUpload2");
        document.getElementById("tckt1").innerHTML = warn;
        element.classList.add("is-invalid");
      }else{
        alert(fsize +" bites\nYou are good to go!");
        
        var element = document.getElementById("fileUpload2");
        element.classList.add("is-valid");
      }
      alert(fsize);
    }else{
      alert("Please upgrade your browser, because your current browser lacks some new features we need!");
    } 
  }

  $(function () {
   $('#example1').DataTable( {
    scrollY:        true,
    scrollX:        true,
    scrollCollapse: true,
    paging:         true,
  })
 });

  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
  });
  
  

  $("#datepick").datepicker({
    startDate: new Date() // set the minDate to the today's date
      // you can add other options here
    });
	
	$(document).ready(function(){
		
	$('.testSlick').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3
	});
		
});
  </script>
  @endsection