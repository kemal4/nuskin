
@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">
    <section class="p-2 pb-0">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <a href="{{ route('list-pickup')}}" class="text-muted"><i class="fas fa-long-arrow-alt-left"></i> Back to list</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Content Header (Page header) -->
    <section class="content-header pt-0">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Delivery Order Detail</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
                        <li class="breadcrumb-item"><a href="{{ route('list-pickup')}}">Delivery Order </a></li>
                        <li class="breadcrumb-item active">Detail</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              
                <div class="col-12 col-sm-3">
                    <div class="info-box mb-3 bg-warning">
                      <span class="info-box-icon"><i class="fas fa-tag"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Status</span>
                        <span class="info-box-number" style="text-transform: capitalize;">{{$delven->status}}</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Invoice ID</h3>
                        </div>
                        <div class="card-body">
                            <h4><strong>{{$delven->order_id}}</strong></h4>
                            <p class="mb-0">{{$delven->create_date}}</p>
                        </div>
                    </div>
                    <div class="card d-md-none">
                        <div class="card-header">
                          <h3 class="card-title">Customer Name</h3>
                        </div>
                        <div class="card-body">
                            <h4><strong>{{$delven->customer_name}}</strong></h4>
                            <p class="mb-0">{{$delven->customer_id}}</p>
                        </div>
                    </div>
                    <div class="card d-md-none">
                        <div class="card-header">
                          <h3 class="card-title">Customer Detail</h3>
                        </div>
                        <div class="card-body">
                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Address</strong>
                            <p class="text-muted">
                                {{$delven->adress}}, {{$delven->city}}, {{$delven->zip}}
                            </p>
                            <strong><i class="fas fa-phone mr-1"></i> Phone</strong>
                            <p class="text-muted">{{$delven->phone}}</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 col-sm-9 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Delivery Detail</h3>
                        </div>
                        <div class="card-body">
                            <dl class="row">
                              <dt class="col-sm-4">Delivery Number:</dt>
                              <dd class="col-sm-8">{{$delven->delivery_number}}</dd>
                              <dt class="col-sm-4">HAWB:</dt>
                              <dd class="col-sm-8">{{$delven->hawb}}</dd>
                              <dt class="col-sm-4">Courier:</dt>
                              <dd class="col-sm-8">{{$delven->transporter}}</dd>
                              <dt class="col-sm-4">Warehouse:</dt>
                              <dd class="col-sm-8">{{$delven->warehouse_name}}</dd>
                              <dt class="col-sm-4">Gross Weight (kg):</dt>
                              <dd class="col-sm-8">{{$delven->grosswiight}}</dd>
                              <dt class="col-sm-4">Nett Weight (kg)</dt>
                              <dd class="col-sm-8">{{$delven->net_weight}}</dd>
                              <dt class="col-sm-4">Volume:</dt>
                              <dd class="col-sm-8">{{$delven->volume}}</dd>
                              <dt class="col-sm-4">Created By:</dt>
                              <dd class="col-sm-8">{{$delven->pic}}</dd>
                            </dl>
                        </div>
                    </div>
                    <!-- /card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Delivery Log</h3>
                            <span class="float-right">
                                <a href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#liveTracking"><i data-toggle="tooltip" title="" data-original-title="Live Tracking" class="fas fa-map-pin"></i>
                                </a>
                            </span>
                        </div>
                        <div class=" card-body" id="timeline">
                            <!-- The timeline -->
                            <div class="timeline timeline-inverse">
  
                                @php $i=1
                                @endphp

                            <!-- tracking logs from db by diwa -->
                                @foreach($deliverylogs as $logs)
                                <!-- timeline item -->
                                <div>
                                    <i class="fa fa-comments bg-yellow"></i>
  
                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{$logs->updated_at}}</span>
                                        <h3 class="timeline-header no-border"><a href="#">Changed By : </a><label style="color: blue"> {{ $logs->pic }} </label></h3>
                                        <h3 class="timeline-header no-border"><a href="#">Description</a> {{$logs->message}}</h3>
                                        <h3 class="timeline-header no-border"><a href="#">Status</a> {{$logs->status}}</h3>
                                    </div>
                                </div>
                                <!-- END timeline item -->
                                @endforeach



                        @if ( 2 == 3 )
                        <!-- real time tracking by ryan -->

                            @if (!(array_key_exists("Error",$body)))
                                @foreach($body['track_history'] as $bo)
                                <!-- timeline item -->
                                <div>
                                    <i class="fa fa-comments bg-yellow"></i>

                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{$bo['date_time']}}</span>
                                        {{-- 
                                        <h3 class="timeline-header no-border"><a href="#">Changed By : </a><label style="color: blue"> {{ $logs->pic }} </label></h3>
                                        --}}
                                    @if ($body['transporter'] == 'JNE')
                                        <h3 class="timeline-header no-border"><a href="#">Description</a> {{$bo['desc']}}</h3>
                                    @endif
                                        <h3 class="timeline-header no-border"><a href="#">Status</a> {{$bo['status']}}</h3>
                                        <h3 class="timeline-header no-border"><a href="#">City</a> {{$bo['city']}}</h3>
                                        <h3 class="timeline-header no-border"><a href="#">Receiver</a> {{$bo['receiver']}}</h3>
                                    </div>
                                </div>
                                <!-- END timeline item -->
                                @endforeach
                            @endif

                        @endif

                                <!-- END timeline item -->
                                <div>
                                    <i class="fa fa-clock-o bg-gray"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /card -->
                </div>
                
                <div class="col-12 col-md-3 d-none d-md-block">
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Customer Name</h3>
                        </div>
                        <div class="card-body">
                            <h4><strong>{{$delven->customer_name}}</strong></h4>
                            <p class="mb-0">{{$delven->customer_id}}</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">Customer Detail</h3>
                        </div>
                        <div class="card-body">
                            <strong><i class="fas fa-map-marker-alt mr-1"></i> Address</strong>
                            <p class="text-muted">
                                {{$delven->adress}}, {{$delven->city}}, {{$delven->zip}}
                            </p>
                            <strong><i class="fas fa-phone mr-1"></i> Phone</strong>
                            <p class="text-muted">{{$delven->phone}}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<div class="modal fade" id="liveTracking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Live Tracking</h3>
                <span class="float-right">
                </span>
            </div>
            <div class=" card-body" id="timeline">
                <!-- The timeline -->
                <div class="timeline timeline-inverse">
          
                    @php $i=1
                    @endphp
                @if (!(array_key_exists("Error",$body)))
                    @foreach($body['track_history'] as $bo)
                    <!-- timeline item -->
                    <div>
                        <i class="fa fa-comments bg-yellow"></i>
          
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{$bo['date_time']}}</span>
                            {{-- 
                            <h3 class="timeline-header no-border"><a href="#">Changed By : </a><label style="color: blue"> {{ $logs->pic }} </label></h3>
                            --}}
                        @if ($body['transporter'] == 'JNE')
                            <h3 class="timeline-header no-border"><a href="#">Description</a> {{$bo['desc']}}</h3>
                        @endif
                            <h3 class="timeline-header no-border"><a href="#">Status</a> {{$bo['status']}}</h3>
                            <h3 class="timeline-header no-border"><a href="#">City</a> {{$bo['city']}}</h3>
                            <h3 class="timeline-header no-border"><a href="#">Receiver</a> {{$bo['receiver']}}</h3>
                        </div>
                    </div>
                    <!-- END timeline item -->
                    @endforeach
                @else
                <div>
                    <div class="timeline-item">
                        <h3 class="timeline-header no-border"><a href="#">Error</a> {{$body['Error']}}</h3>
                    </div>
                </div>
                @endif
                    <!-- END timeline item -->
                    <div>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
