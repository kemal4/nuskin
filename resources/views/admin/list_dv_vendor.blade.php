@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Delivery Order</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Delivery Order</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            @if ($errors->has('file'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('file') }}</strong>
                </span>
              @endif
              @if ($sukses = Session::get('sukses'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $sukses }}</strong>
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            <div class="card-header">
                @can('create_delivery')
                  <div class="row"> 
                    <div class="col-md-6">
                        <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#importExcel" style="margin-bottom: 10px;">
                          Import File
                        </button>
                        <a href="{{ route('createlist') }}"  class="btn btn-primary mr-2" style="margin-bottom: 10px;">
                          <!--<i class="fas fa-plus-circle"></i> -->
                          Add Delivery Order
                        </a>
                        <a class="btn btn-primary mr-2" style="margin-bottom: 10px;" href="javascript:void(0)" id="filterButton">
                            Filter
                        </a>
                        <button type="button" class="btn btn-primary mr-2" data-toggle="modal" data-target="#Edit" id="editButton" style="margin-bottom: 10px;">Edit</button>
                    </div>
                  </div>
                @endcan     
            </div>

            <div class="card-body"> 
        
                <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Over Due date</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-solved" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Delivered</a>
                    </li>
                </ul>
                <div class="tab-content mt-2" id="custom-content-below-tabContent">
                    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                        <table id="allDO" class="table table-bordered text-nowrap">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th>&nbsp;</th>-->
                                    <th><input type="checkbox" id="allCheckBox"></th>
                                    <th>Action</th>
                                    <th>Status</th>
                                    <th>Invoice ID/SO</th>
                                    <th>Invoice Date</th>
                                    <th>Courier</th>
                                    <th>HAWB</th>
                                    <th>Delivery Number</th>
                                    <th>Handover Date</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Phone</th>
                                    <th>City</th>
                                    <th>Postal Code</th>
                                    <th>Warehouse</th>
                                    <th>Nu Skin lead time</th>
                                    <th>Nu Skin ETA</th>
                                </tr>
                            </thead>
                        </table>
                    </div> 
                    <div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <table id="duedateDO" class="table table-bordered text-nowrap ">
                            <thead class="thead-light">
                                <tr>
                                    <th>Action</th>
                                    <th>Status</th>
                                    <th >Invoice ID/SO</th>
                                    <th>Invoice Date</th>
                                    <th>Courier</th>
                                    <th>HAWB</th>
                                    <th>Delivery Number</th>
                                    <th>Handover Date</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Phone</th>
                                    <th>City</th>
                                    <th>Postal Code</th>
                                    <th>Warehouse</th>
                                    <th>Nu Skin lead time</th>
                                    <th>Nu Skin ETA</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="custom-content-solved" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                        <table id="solvedDO" class="table table-bordered text-nowrap ">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th>No</th>-->
                                    <th>Action</th>
                                    <th>Status</th>
                                    <th >Invoice ID/SO</th>
                                    <th>Invoice Date</th>
                                    <th>Courier</th>
                                    <th>HAWB</th>
                                    <th>Delivery Number</th>
                                    <th>Handover Date</th>
                                    <th>Customer ID</th>
                                    <th>Customer Name</th>
                                    <th>Phone</th>
                                    <th>City</th>
                                    <th>Postal Code</th>
                                    <th>Warehouse</th>
                                    <th>Nu Skin lead time</th>
                                    <th>Nu Skin ETA</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            <!-- /.card-body -->
            </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<div class="modal fade" id="filterModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content text-sm">
                  <form action="{{ route('filterDO') }}" method="POST" id="itemForm" name="itemForm" class="form-horizontal">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                    {{ csrf_field() }}
                    
                    <input type="hidden" name="filter_start_date" id="filter_start_date">
                    <input type="hidden" name="filter_end_date" id="filter_end_date">
                    <div class="form-group row">
                        <label for="filterDeliveryDate" class="col-sm-4 col-form-label control-label">Invoice Date</label>
                        <div class="col-sm-8">
                            <div id="filterDeliveryDate" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span name="filterDeliveryDate"></span> 
                                <i class="fa fa-caret-down float-right"></i>
                            </div>
                            <!--
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control float-right" id="filterDeliveryDate" name="filterDeliveryDate">
                            </div>
                            -->
                        </div>
                    </div>
 
                    <div class="form-group row">
                        <label for="filterInvoice" class="col-sm-4 col-form-label control-label">Invoice</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="filter_invoice" name="filter_invoice" placeholder="Invoice">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="filterHawb" class="col-sm-4 col-form-label control-label">HAWB</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="filter_hawb" name="filter_hawb" placeholder="HAWB">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="filterCustomer" class="col-sm-4 col-form-label control-label">Customer Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="filter_customer" name="filter_customer" placeholder="Customer">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="courier_id" class="col-sm-4 col-form-label control-label">Courier</label>
                        <div class="col-sm-8">
                            <select class="form-control custom-select" id="filter_courier_id" name="filter_courier_id" >
                                <option value="" selected disabled hidden >-- Select Courier --</option>
                                  @foreach($couriers as  $courier)
                                  <option value="{{ $courier->id }}">{{ $courier->display_name }}</option>
                                  @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nuskin_status_id" class="col-sm-4 col-form-label control-label">Nuskin Status</label>
                        <div class="col-sm-8">
                            <select class="form-control custom-select" id="filter_status_id" name="filter_status_id" >
                                <option value="" selected disabled hidden >-- Select Status --</option>
                                  @foreach($statuses as $status)
                                  <option value="{{ $status->id }}">{{ $status->display_name }}</option>
                                  @endforeach
                            </select>
                        </div>
                    </div>
                    
          
            </div>
            <div class="modal-footer">          
                <button type="button" id="closeFilter" class="btn btn-secondary"  data-dismiss="modal" >Cancel</button>
                <button type="button" id="resetFilter" class="btn btn-secondary" >Reset</button>
                <button type="submit" id="applyFilter" class="btn btn-info" data-loading-text="Loading...">Apply</button>
            </div>
                </form>
        </div>
    </div>
</div>
        
                  <!-- Import Excel -->
            <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form id="importForm" method="post" action="{{ route('importExcel')}}" enctype="multipart/form-data">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Import File</h5>
                            </div>
                            <div class="modal-body">
                                {{ csrf_field() }}
                                <label>Select a file (xls, xlsx, csv)</label>
                                <div class="form-group">
                                    <input type="file" name="file" required="required" accept=".xls,.xlsx,.csv">
                                </div>
                                <div class="form-group">
                                    <a href="{{ route('delivery.getTemplate') }}">
                                      <!--<i class="fas fa-plus-circle"></i> -->
                                      Download Template
                                    </a>
                                </div>
                                <label>Warehouse</label>
                                <select name="wh_id" id="wh_id" class="form-control custom-select" style="width: 100%;" required>
                                  <option value="" selected disabled hidden >-- Select Warehouse --</option>
                                  @foreach($warehouses as  $id_gudang => $lokasi_gudang)
                                  <option value="{{ $id_gudang }}">{{ $lokasi_gudang }}</option>
                                  @endforeach
                                </select>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" id="buttonImport" class="btn btn-info" data-loading-text="Loading...">Preview</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
<div class="card collapsed-card d-none">
              <div class="card-header border-transparent">
                Filter
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-plus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <form action="{{ route('filterDO') }}" method="GET">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">                 
                        <div class="card card-success">

                          <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Date :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i>
                                        </span>
                                      </div>
                                      <input type="date" class="form-control" name="filterdate" value="">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label>Filter By Status :</label>
                                    <div class="input-group">
                                      <select name="flterstatus" class="form-control select2" style="width: 100%;">
                                          <option value="" selected disabled hidden class="form-control">-- Select Status--</option>
                                          <option value="solved">Delivered</option>
                                          <option value="fail">Undelivered</option>
                                          <option value="onprocess">On Process</option>
                                          <option value="return">Return</option>
                                          <option value="incident">Incident</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Month :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                      </div>
                                      <input type="month" class="form-control" name="filtermonth">
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label></label>
                                    <div class="input-group">
                                      <center>
                                      <input type="submit" value="Filter" class="btn btn-secondary btn-md">
                                      <a href="{{ route('list-pickup')}}"   class="btn btn-secondary btn-md">Reset
                                      </a>
                                      </center>
                                    </div>
                                  </div>
                                </div>
                              <!-- row -->
                            </div>
                            <!-- card-body -->
                          </div>
                                <!-- </div> -->
                        </div>
                      </div>     
                    </div>
                  </form>
              </div>
            </div>    
  
@endsection

@section('scripts')
  <script type="text/javascript">
    
    $(function () {
        
      $('a[data-toggle="pill"]').on( 'shown.bs.tab', function (e) {
            //$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            //$.fn.dataTable.tables( true ).columns.adjust().draw();
            //$($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();

            //$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                //.responsive.recalc()
                //.scroller.measure()
                .fixedColumns().relayout();         
        } );
     
    //  $(document).on('shown.bs.tab', 'a[data-toggle="pill"]', function (e) {
    //           $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    //        });
          
      $('#allDO').DataTable( {  
            //"dom": "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            //  "<'row'<'col-xs-12't>>"+
            //  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
            "fnDrawCallback": function( oSettings ) {                     
                      $('[data-toggle="tooltip"]').tooltip({
                            trigger : 'hover'
                        });   
                    },
            select:{
                style:     'os',
                className: 'row-selected'
            },
            responsive: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
            },
            "scrollX":true,
            "fixedColumns": {
                "leftColumns": 3
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                     "url": "{{ route('allDeliveries') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"},
                     
                   },
            "columns": [
                //{ "data": "temp" },
                //{ "data": 'DT_RowIndex', "name": 'DT_RowIndex'},
                { "data": "check_box", "name": '' , "orderable": false, },
                { "data": "options", "name": '' , "orderable": false },
                { "data": "status", "name": 'status' },
                { "data": "order_id", "name": 'order_id' },
                { "data": "create_date", "name": 'create_date' },
                { "data": "transporter", "name": 'transporter' },
                { "data": "hawb", "name": 'hawb' },
                { "data": "customer_refer", "name": 'customer_refer' },
                { "data": "created_at", "name": 'created_at' },
                { "data": "customer_id", "name": 'customer_id' },
                { "data": "customer_name", "name": 'customer_name' },
                { "data": "phone", "name": 'phone' },
                { "data": "city", "name": 'city' },
                { "data": "zip", "name": 'zip' },
                { "data": "warehouse_name", "name": 'warehouse_name' },
                { "data": "leadtime_nuskin", "name": 'leadtime_nuskin' },
                { "data": "eta_nuskin", "name": 'eta_nuskin' },
                //{ "data": "leadtime_courier", "name": 'leadtime_courier' },
                //{ "data": "eta_provider", "name": 'eta_provider' },
                //{ "data": "options", "name": 'options' , "orderable": false}
            ],

      })
      {{-- $.fn.DataTable.ext.pager.numbers_length = 5; --}}
      
      $('#solvedDO').DataTable( {   
            //"dom": "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            //  "<'row'<'col-xs-12't>>"+
            //  "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
            "fnDrawCallback": function( oSettings ) {                     
                      $('[data-toggle="tooltip"]').tooltip({
                            trigger : 'hover'
                        }); 
                    },
            "scrollX":true,
            //scrollY: 200,
            "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '},
            "fixedColumns": {
                "leftColumns": 3
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                     "url": "{{ route('ajaxOrderDelivered') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"},
                     
                   },
            "columns": [
                //{ "data": "temp" },
                //{ "data": 'DT_RowIndex', "name": 'DT_RowIndex'},
                { "data": "options", "name": '' , "orderable": false },
                { "data": "status", "name": 'status' },
                { "data": "order_id", "name": 'order_id' },
                { "data": "create_date", "name": 'create_date' },
                { "data": "transporter", "name": 'transporter' },
                { "data": "hawb", "name": 'hawb' },
                { "data": "customer_refer", "name": 'customer_refer' },
                { "data": "created_at", "name": 'created_at' },
                { "data": "customer_id", "name": 'customer_id' },
                { "data": "customer_name", "name": 'customer_name' },
                { "data": "phone", "name": 'phone' },
                { "data": "city", "name": 'city' },
                { "data": "zip", "name": 'zip' },
                { "data": "warehouse_name", "name": 'warehouse_name' },
                { "data": "leadtime_nuskin", "name": 'leadtime_nuskin' },
                { "data": "eta_nuskin", "name": 'eta_nuskin' },
                //{ "data": "leadtime_courier", "name": 'leadtime_courier' },
                //{ "data": "eta_provider", "name": 'eta_provider' },
                //{ "data": "options", "name": 'options' , "orderable": false}
            ]
      })

      $('#duedateDO').DataTable( {
            "fnDrawCallback": function( oSettings ) {                     
                      $('[data-toggle="tooltip"]').tooltip({
                            trigger : 'hover'
                        });   
                    },
            "scrollX":true,
            "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '},
            "fixedColumns": {
                "leftColumns": 3
            },
            "processing": true,
            "serverSide": true,
            "ajax": {
                     "url": "{{ route('ajaxOrderOverDue') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"},
                     
                   },
            "columns": [
                //{ "data": "temp" },
                //{ "data": 'DT_RowIndex', "name": 'DT_RowIndex'},
                { "data": "options", "name": '' , "orderable": false },
                { "data": "status", "name": 'status' },
                { "data": "order_id", "name": 'order_id' },
                { "data": "create_date", "name": 'create_date' },
                { "data": "transporter", "name": 'transporter' },
                { "data": "hawb", "name": 'hawb' },
                { "data": "customer_refer", "name": 'customer_refer' },
                { "data": "created_at", "name": 'created_at' },
                { "data": "customer_id", "name": 'customer_id' },
                { "data": "customer_name", "name": 'customer_name' },
                { "data": "phone", "name": 'phone' },
                { "data": "city", "name": 'city' },
                { "data": "zip", "name": 'zip' },
                { "data": "warehouse_name", "name": 'warehouse_name' },
                { "data": "leadtime_nuskin", "name": 'leadtime_nuskin' },
                { "data": "eta_nuskin", "name": 'eta_nuskin' },
                //{ "data": "leadtime_courier", "name": 'leadtime_courier' },
                //{ "data": "eta_provider", "name": 'eta_provider' },
                //{ "data": "options", "name": 'options' , "orderable": false}
            ]
      })


    $('#filterButton').click(function () {
        $('#saveBtn').val("create...");
        $('#modelHeading').html("Filter");
        $('#filterModel').modal('show');
        
        
    });
    $('#resetFilter').click(function () {
        $('#itemForm').trigger("reset");
        start = '';
        end = '';
        $('#filterDeliveryDate span').html('');
        $('#filter_start_date').val('');
        $('#filter_end_date').val('');
        //cb(start, end);
    });
    
    
    
    //$('#buttonImport').click(function (e) 
    $('#buttonImport').on('click', function (e) 
    //$('#importForm').on('submit', function (e) 
    {
        let isValidForm = $('#importForm')[0].checkValidity();
        //alert(isValidForm);
        
        if(!isValidForm){
            //return false;
            $('#importForm').find(':submit').click();
            //$('#importForm').submit();
        }
        else {
            e.preventDefault();
            
            Swal.fire({
                title: ''+$( "#wh_id option:selected" ).text(),
                text: "Are you sure you want to import from here ?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
              }).then((result) => {
                if (result.value) { //yes
                  //$('#importForm').submit();
                  //$this.submit();
                  //return true;
                  //$('#importForm').find(':submit').click();
                  $('#importForm').submit();
                }
              });
        }
        
    });

    
    //Date range picker
    //$('#filterDeliveryDate').daterangepicker()

    
    //let start = moment().subtract(29, 'days');
    //let end = moment();
    let start = '';
    let end = '';
    function cb(start, end) {
        /*
        if(start && start.length === 0){ 
        //if( isNaN(start)){ 
            start = moment().subtract(29, 'days');
            //start = moment();
            end = moment();
        }
        */
        if(!isNaN(start)){
            $('#filterDeliveryDate span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#filter_start_date').val(start.format('MM/DD/YYYY'));
            $('#filter_end_date').val(end.format('MM/DD/YYYY'));            
        }
        console.log('start : '+start);
        console.log('isNaN : '+isNaN(start));
    }
        console.log('start : '+start);
        console.log('isNaN : '+isNaN(start));
        console.log('lenthg : '+start.length);

    $('#filterDeliveryDate').daterangepicker({
        //autoUpdateInput: false,
        //startDate: start,//(start?start:moment().subtract(29, 'days')),
        startDate: ((start.length === 0)?moment().subtract(29, 'days'):start),
        //endDate: end,//(end?end:moment()),
        endDate: ((end.length === 0)?moment().subtract(28, 'days'):end),
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    //cb(start, end);

/*
 $('#buttonImport').on('click', function () {
    var $btn = $(this).button('loading')
    // business logic...
    $btn.button('reset')
  })
  */

    });
   

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
    
    let allSes = @json(Session::all());
    console.log(allSes);
    
    /*
    $('select').select2({
        theme: 'bootstrap4',
    });
    */

    $('#editButton').click(function()
    {
        if($(this).html() == "Edit")
        {
            $(this).html("Cancel").removeClass().addClass("btn btn-danger mr-2")
            $(this).after($("<button id='confirm'>Confirm</button>").addClass("btn btn-primary mr-2").css("margin-bottom", "10px"))
            $(".warehouse_text").hide()
            $(".warehouse_select").show()
            // $.ajax({
            //     type: "POST",
            //     url: "{{ route('warehouse.getWarehouse') }}",
            //     dataType: "json",
            //     type: "POST",
            //     data:{ _token: "{{csrf_token()}}"},
            //     success: function(data)
            //     {
            //         console.log('success', data)
            //     }

            // })
        }
        else if($(this).html() == "Cancel")
        {
            $(this).html("Edit").removeClass().addClass("btn btn-primary mr-2")
            $("#confirm").remove()
            $(".warehouse_select").hide()
            $(".warehouse_text").show()
        }
        
    })

    // $('#allCheckBox').ready(function()
    // {
    //     $(this).change(function()
    //     {
    //         alert($(this).prop("id"))
    //     })
    // })

    //End
});   
  </script>
@endsection