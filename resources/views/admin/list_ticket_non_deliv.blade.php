@extends('layoutAdmin.global')


@section('content')
<style type="text/css">
  td { font-size: 11px;
       color:  #000; }
  th { font-size: 14px;
       color: #696969; }
</style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Non Delivery</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Ticket</li>
              <li class="breadcrumb-item active">Non-delivery</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
      @csrf
      <div class="row">
        <div class="col-12">
          <div class="card">
          
           <!--Button Add new Ticket -->
            <div class="card-header">
              <h3 class="card-title">
                <a href="#"   class="btn btn-primary mr-5 mb-2">
                  Add Ticket
                </a>
              </h3>
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">
              @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif  
              
              {{-- notifikasi form validasi --}}
                  @if ($errors->has('file'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('file') }}</strong>
                    </span>

                  @endif
              {{-- notifikasi sukses --}}
                  @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $sukses }}</strong>
                    </div>
                  @endif
              
                {{-- notifikasi error --}}
                @if ($error = Session::get('error'))
                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $error }}</strong>
                </div>
              @endif
              <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">All</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-content-below-belum-tab" data-toggle="pill" href="#custom-content-below-belum" role="tab" aria-controls="custom-content-below-belum" aria-selected="false">Unprocessed</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-sedang" role="tab" aria-controls="custom-content-below-sedang" aria-selected="false">On Process</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="custom-content-below-solved-tab" data-toggle="pill" href="#custom-content-below-solved" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Solved</a>
                </li>
                <li class="nav-item d-none">
                  <a class="nav-link" id="custom-content-below-solved-tab" data-toggle="pill" href="#content-non-deliv" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Non Delivery</a>
                </li>
              </ul>
              <div class="tab-content mt-2" id="custom-content-below-tabContent">
                <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                  <table id="example1" class="table table-bordered  table-responsive">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Ticket Id</th>
                      <th>SO Number</th>
                      <th>Time</th>
                      <th>Topics</th>
                      <th>Complainer Names</th>
                      <th>Complainer Emails</th>
                      <th>Customer Names</th>
                      <th>Phones</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
  
                      @php $i=1 @endphp
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="#" type="button" class="btn btn-block bg-gradient-primary btn-sm">Detail
                            </a> 
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane fade" id="custom-content-below-belum" role="tabpanel" aria-labelledby="custom-content-below-belum-tab">
                  <table id="example2" class="table table-bordered  table-responsive">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Ticket Id</th>
                      <th>SO Number</th>
                      <th>Time</th>
                      <th>Topics</th>
                      <th>Complainer Names</th>
                      <th>Complainer Emails</th>
                      <th>Customer Names</th>
                      <th>Phones</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
  
                      @php $i=1 @endphp
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="#" type="button" class="btn btn-block bg-gradient-primary btn-sm">Detail
                            </a> 
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                <!-- /.tab-pane --> 
                <div class="tab-pane fade" id="custom-content-below-sedang" role="tabpanel" aria-labelledby="custom-content-below-sedang-tab">
                  <table id="example3" class="table table-bordered  table-responsive">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Ticket Id</th>
                      <th>SO Number</th>
                      <th>Time</th>
                      <th>Topics</th>
                      <th>Complainer Names</th>
                      <th>Complainer Emails</th>
                      <th>Customer Names</th>
                      <th>Phones</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
  
                      @php $i=1 @endphp
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="#" type="button" class="btn btn-block bg-gradient-primary btn-sm">Detail
                            </a> 
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane fade" id="custom-content-below-solved" role="tabpanel" aria-labelledby="custom-content-below-solved-tab">
                  <table id="example4" class="table table-bordered  table-responsive">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Ticket Id</th>
                      <th>SO Number</th>
                      <th>Time</th>
                      <th>Topics</th>
                      <th>Complainer Names</th>
                      <th>Complainer Emails</th>
                      <th>Customer Names</th>
                      <th>Phones</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
  
                      @php $i=1 @endphp
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="#" type="button" class="btn btn-block bg-gradient-primary btn-sm">Detail
                            </a> 
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane fade" id="content-non-deliv" role="tabpanel" aria-labelledby="custom-content-below-belum-tab">
                  <table id="example2" class="table table-bordered  table-responsive">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Ticket Id</th>
                      <th>ID Member</th>
                      <th>Time</th>
                      <th>Topics</th>
                      <th>Complainer Names</th>
                      <th>Complainer Emails</th>
                      <th>Customer Names</th>
                      <th>Phones</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
  
                      @php $i=1 @endphp
                      <tr>
                        <td><?php echo $i++; ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="#" type="button" class="btn btn-block bg-gradient-primary btn-sm">Detail
                            </a> 
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                 <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content mt-2 -->  
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
     
    </section>
    <!-- /.content -->
  </div>
@endsection


@section('scripts')

  <script type="text/javascript">
   
    $(function () {
      $("#example1").DataTable();
      $("#example2").DataTable();
      $("#example3").DataTable();
      $("#example4").DataTable();
      
    });
  </script>

<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="js/promise-polyfill.js" type="text/javascript"></script>
@endsection