@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add New Ticket</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item"><a href="{{ route('listticket')}}">Complain Ticket </a></li>
              <li class="breadcrumb-item active">Add New Ticket</li>
            </ol>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
    	@if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                               @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                               @endforeach
                            </ul>
                          </div>
                        @endif	
    </div>
	
	<div class="container">

    <!-- Main content -->
    <form action="{{ route('addticket') }}" method="post" enctype="multipart/form-data">
    @csrf
		<div class="row">
			<div class="col-lg-6">
				<div class="card card-secondary">
					<div class="card-body">
						<div class="form-group row" >
							<label class="col-sm-3 " for="hawb">Complaint Type</label>
							<select name="complaintType" id="complaintType" class="form-control select2 col-sm-9" style="width: 100%;" value="" required>
								<option value="Delivery">Delivery</option>
								<option value="Non-Delivery">Non-Delivery</option>
							</select>
						</div>
						<div class="form-group row" >
							<label class="col-sm-3 " for="so-number">SO Number</label>
							<input type="text" class="form-control col-sm-9" name="order_id" maxlength="10"  id="so-number" value="{{ $order_id }}" placeholder="SO Number" {{-- onchange="checkSO()" --}} required="">
						</div>
						<div class="form-group row" >
							<label class="col-sm-3 " for="hawb">HAWB</label>
							<!--
							<input type="text" class="form-control col-sm-9 " name="hawb" id="hawb" value="{{ $hawb }}" placeholder="HAWB"  required>
							-->
							<select name="hawb" id="hawb" class="form-control select2 col-sm-9" style="width: 100%;" value="" required>
								@if (!empty($hawb) )
									<option value="{{ $hawb }}" selected  class="form-control ">{{ $hawb }} - {{ $status }}</option>
								@else
									<option value="" selected disabled hidden class="form-control ">Select HAWB</option>
								@endif
							</select>
							
						</div>
						<div class="form-group row" id="div_customer_id">
							<label class="col-sm-3 " for="customer">Customer Id</label>
							<input type="text" class="form-control col-sm-9" name="customerId" placeholder="Customer Id" value="{{-- $customerId --}}" maxlength="11" disabled>
						</div>
						<div class="form-group row" >
							<label class="col-sm-3 " for="customer">Customer Name</label>
							<input type="text" class="form-control col-sm-9" name="customer" placeholder="Customer Name" value="{{ $customerName }}"  required>
						</div>
						<div class="form-group row" >
							<label class="col-sm-3 " for="nama_pelapor" >Complainer Name</label>
							<input type="text" class="form-control col-sm-9" name="nama_pelapor" placeholder="Complainer Name" value="{{ $nama_pelapor }}" required>
						</div>
						<div class="form-group row" >
							<label class="col-sm-3 " for="email" >Complainer Email</label>
							<input type="text" class="form-control col-sm-9 " name="email" placeholder="Complainer Email" value="{{ old('email') }}" required>
						</div>
						<div class="form-group row" >
							<label class="col-sm-3 " for="telepon" >Complainer Phone</label>											
							<div class="col-sm-9 my-1" style="padding-left: 0px;padding-right: 0px;">
								<div class="input-group">
									<div class="input-group-prepend">
									  <div class="input-group-text">+62</div>
									</div>
									<input type="text" class="form-control" name="telepon" placeholder="Complainer Phone" value="{{ $telepon }}" required>
								</div>
							</div>
						</div>
								
					</div>
				</div>
			</div>
			
			
			<div class="col-lg-6">
				<div class="card card-secondary">
					<div class="card-body">
								<div class="form-group row" >
									<label class="col-sm-3 " for="topic">Topic</label>
									<select name="topic" id="topic" class="form-control select2 col-sm-9" style="width: 100%;" value="" required>
										<option value="" class="form-control ">Select Topic</option>
										@foreach($topicname  as $picname)
											<option value="{{ $picname }}">{{ $picname }}</option>
										@endforeach
									</select>
									<input type="hidden" id="hiddenTopic" name="topic" value="{{ $topicname[4] }}" disabled>
								</div>
								
								<div class="form-group row">
									<label  class="col-sm-3 " for="topic">Attachment</label>
									<div  class="col-sm-9" style="padding-left: 0px;padding-right: 0px;" 	>
										<input type="file" class="form-control mb-2" name="attachments" id="fileUpload" onchange="Upload()" accept="image/*">
										<input type="file" class="form-control" name="attachments2" id="fileUpload2" onchange="Upload2()" accept="image/*">
										<label id="tckt1" class= "mb-2" style="color: #ff0000"><small>* max 2mb</small></label>
										<label id="tckt2"  style="color: #ff0000"><small></small></label>
										
									</div>
								</div>
								<div class="form-group">
									<label>Information</label>
									<textarea class="form-control pb-3" rows="3" name="message"  required></textarea>
								</div>
								<br>
								<br>
								
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-md-12">	
			<div class="form-group row">									
					<a href="{{ route('listticket')}}" class="btn btn-default mr-2 mb-2">Cancel</a>
					<button type="submit" class="btn btn-info mb-2"> Submit </button>
				  <div class="col-sm"></div>
<!-- 				  <div class="col-md-2 mb-2"></div>
				  <div class="col-md-8"></div> -->
			</div>
		</div>

    </form>
  </div>
  </div>
@endsection

@section('scripts')

  <script>
	$("#shownondelivere").hide();

	let selectHawb = document.getElementById('hawb');
	let selength = selectHawb.options.length;
	
    let urlGetBySO = "{{ route('getsodetail', 'so_to_replace') }}"
    let urlGetBySO2 = "{{ route('getsodetail', 'so_to_replace') }}"
    $('input[name="order_id"]').on("blur", function() {
    	$('#topic option').show()
      if ($(this).val().length<5) {
        return
      }
	  
	  selength = selectHawb.options.length;
	  for (i = selength-1; i >= 0; i--) {
		  selectHawb.options[i] = null;
		}
	  
	  let tempId = $('input[name="order_id"]').val()
      urlGetBySO = urlGetBySO2.replace('so_to_replace', tempId)
      $.ajax({
        method: "GET",
        url: urlGetBySO,
      }).done(function(data) {
        console.log("data")
        console.log(data)
		if(data.ticket_exist){
			if(data.topics)
			{
				for (i=0; i < data.topics.length; i++)
				{
					$('#topic option[value="' + data.topics[i] + '"]').hide()
					$('input[name="hawb"]').val(data.last_hawb);
					$('input[name="customer"]').val(data.customer_name);
					$('input[name="nama_pelapor"]').val(data.customer_name);
					$('input[name="telepon"]').val(data.phone);		
					1
					$.each(data.allHawb , function(k,v){
						$(selectHawb).append('<option value=' + v.hawb + '>' + v.hawb + ' - ' + v.status + '</option>');
					});				
				}
			}
			else
			{
				toastr.error('Ticket with SO : '.concat(tempId,". Every topic has been used."))
				$('input[name="order_id"]').val('')
				
					$('input[name="customer"]').val('')
					$('input[name="nama_pelapor"]').val('')
					$('input[name="telepon"]').val('')
					
				$('input[name="order_id"]').focus()
			}
			
		}
		else {
			if(data.doesnt_exist){
			toastr.error('Delivery with SO : '.concat(tempId," doesn't exist."))
				//$('input[name="order_id"]').val('')
				
				$('input[name="customer"]').val('')
				$('input[name="nama_pelapor"]').val('')
				$('input[name="telepon"]').val('')
				
				$('input[name="order_id"]').focus()
			}
			else {
				$('input[name="customer"]').val(data.customer_name);
				$('input[name="nama_pelapor"]').val(data.customer_name);
				$('input[name="telepon"]').val(data.phone);				
				$('input[name="hawb"]').val(data.last_hawb);

				$.each(data.allHawb , function(k,v){
					$(selectHawb).append('<option value=' + v.hawb + '>' + v.hawb + ' - ' + v.status + '</option>');
				});
			}
			
		}
      }).fail(function(data){
		  
        //console.log("data")
        console.log(data)
        alert("something went wrong")
      });
    });


/*

    let urlGetBySO = "{{ route('getsodetail', 'so_to_replace') }}"
    let urlGetBySO2 = "{{ route('getsodetail', 'so_to_replace') }}"
    $('input[name="order_id"]').on("blur", function() {
      if ($(this).val().length<5) {
        return
      }
	  let tempId = $('input[name="order_id"]').val()
      urlGetBySO = urlGetBySO2.replace('so_to_replace', tempId)
      $.ajax({
        method: "GET",
        url: urlGetBySO,
      }).done(function(data) {
        console.log("data")
        console.log(data)
		if(data.ticket_exist){
			toastr.error('Ticket with SO : '.concat(tempId,' already exist.'))
			$('input[name="order_id"]').val('')
			$('input[name="order_id"]').focus()
		}
		else {
			if(data.ticket_exist){
				$('input[name="order_id"]').val('')
				$('input[name="order_id"]').focus()
			}
			else {
				$('input[name="customer"]').val(data.customer_name);
				$('input[name="nama_pelapor"]').val(data.customer_name);
				$('input[name="telepon"]').val(data.phone);				
				$('input[name="hawb"]').val(data.last_hawb);				
			}
			
		}
      }).fail(function(data){
		  
        console.log("data")
        console.log(data)
        alert("something went wrong")
      });
    });





	function checkSO() {
		let tempId = $('#so-number').val();
		if (tempId.length<5) {
			return;
		  };
		
		urlGetBySO = urlGetBySO.replace('so_to_replace', tempId);
		  $.ajax({
			method: "GET",
			url: urlGetBySO,
		  }).done(function(data) {
			console.log("data");
			console.log(data);
			if(data.ticket_exist){
				toastr.error('Ticket with SO : '.concat(tempId,' already exist.'));
				$('input[name="order_id"]').val('');
				$('input[name="order_id"]').focus();
			}
			else {
				if(data.ticket_exist){
					$('input[name="order_id"]').val('');
					$('input[name="order_id"]').focus();
				}
				else {
					$('input[name="customer"]').val(data.customer_name);
					$('input[name="nama_pelapor"]').val(data.customer_name);
					$('input[name="telepon"]').val(data.phone);				
					$('input[name="hawb"]').val(data.last_hawb);				
				}
				
			}
		  }).fail(function(data){
			  
			console.log("data");
			console.log(data);
			alert("something went wrong");
		  });
	}
*/
  function Upload() {
	if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		var fsize = $('#fileUpload')[0].files[0].size;
		if(fsize>2097152) //do something if file size more than 2 mb (2097152)
		{
			alert(fsize +" bites\nToo big!");
			var warn = "Image max 2mb";
              var element = document.getElementById("fileUpload");
              document.getElementById("tckt1").innerHTML = warn;
              element.classList.add("is-invalid");
		}else{
			//alert(fsize +" bites\nYou are good to go!");
			
              var element = document.getElementById("fileUpload");
              element.classList.add("is-valid");
		}
		//alert(fsize);
	}else{
		alert("Please upgrade your browser, because your current browser lacks some new features we need!");
	} 
  }

  function Upload2() {
	if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		var fsize = $('#fileUpload2')[0].files[0].size;
		if(fsize>2097152) //do something if file size more than 2 mb (2097152)
		{
			alert(fsize +" bites\nToo big!");
			var warn = "Image max 2mb";
              var element = document.getElementById("fileUpload2");
              document.getElementById("tckt1").innerHTML = warn;
              element.classList.add("is-invalid");
		}else{
			//alert(fsize +" bites\nYou are good to go!");
			
              var element = document.getElementById("fileUpload2");
              element.classList.add("is-valid");
		}
		//alert(fsize);
	}else{
		alert("Please upgrade your browser, because your current browser lacks some new features we need!");
	} 
  }

  		// $("#div_customer_id").hide()

		$("#complaintType").change(function(){
			$("#so-number").attr("required",$(this).val() == "Delivery").val("")
			//$("#so-number option[value='']").attr('selected', 'selected'); 
			$("#hawb").prop('disabled', $(this).val() == "Non-Delivery").attr("required",$(this).val() == "Delivery").val("").html('"<option value="" selected disabled hidden class="form-control ">Select HAWB</option>"')
			$("#topic").prop('disabled', $(this).val() == "Non-Delivery").attr("required",$(this).val() == "Delivery").val("General")
			$("#hiddenTopic").prop('disabled', $(this).val() == "Delivery")
			$("input[name='customerId']").prop('disabled', $(this).val() == "Delivery").attr("required",$(this).val() == "Non-Delivery")

			// if($(this).val() == "Delivery")
			// {
			// 	$("#div_customer_id").hide()
			// }
			// else
			// {
			// 	$("#div_customer_id").show()
			// }
		});
	


  </script>
  
  <script>
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'id',
      format   : 'yyyy-mm-dd'
    });

    
  </script>
  @endsection

