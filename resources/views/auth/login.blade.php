@extends('layouts.appadminLTE')

@section('content')
<?php
  /*
  $logoAll = array(
				'Logo1.png',
				'Logo2.png',
				'Logo3.png',
				'Logo4.png',
				'Logo5.png',
				'Logo6.png',
				'Logo7.png'
			);
  $logoFile = $logoAll[mt_rand(0, 6)];
  */
  
  $logoFile = 'Logo1.png';

	$baseImgUrl = URL::asset('image/logo/').'/';

?>

<div class="login-box">
  <div class="login-logo">
    <!-- <img src="{{URL::asset('image/image001.jpg')}}"> -->
    <!-- <b>NU SKIN</b> -->
  </div>
  <div class="card">
    <div class="card-body login-card-body">

		<p class="login-box-msg">
        <img id="loginLogo" src="{{ $baseImgUrl.$logoFile }}" class="img-fluid" style="width:60%;">
		<!-- 
		<img src="{{URL::asset('image/logo/image001.jpg')}}"> 
		-->
		</p>
        <form method="POST" action="{{ route('login') }}" class="login100-form">
            @csrf
        <div class="input-group mb-3">
            <input id="first-name" class="form-control @error('email') is-invalid @enderror input100" type="text" name="email" placeholder="Email">
            <span class="focus-input100"></span>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
          <!-- <input type="password" class="form-control" placeholder="Password"> -->
            <input class="form-control" type="password" name="password" placeholder="Password">
            <span class="focus-input100"></span>
               @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

        <p class="mb-1">
            <span class="txt1">
                Forgot
            </span>
            @if (Route::has('password.request'))
                <a class="txt2" href="{{ route('password.request') }}">
                    {{ __('Username/password?') }}
                </a>
            @endif
        </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
/*
    $(document).ready(function(){
		let datalogo = @ json($logoAll);
			
        let interval = setInterval(function(){		
            //$('#loginLogo').attr('src','{{ $baseImgUrl}}'+datalogo[Math.floor(Math.random() * (6) )]+"?"+new Date().getTime());
			
			$("#loginLogo").fadeOut(400, function() {
				$("#loginLogo").attr('src','{{ $baseImgUrl}}'+datalogo[Math.floor(Math.random() * (6) )]+"?"+new Date().getTime());
			}).fadeIn(400);
			
        },3000);
    });
*/	
</script>

@endsection
