@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Master Vendor</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Edit Master Vendor</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

              <form action="{{ route('list-mastervendor.storeedit', ['id' => $editven->id ]) }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                          <label>Label Vendor</label>
                        <input type="text" class="form-control" placeholder="{{$editven->label_vendor}}" name="label" value="{{$editven->label_vendor}}">
                        </div>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label>Code Vendor</label>
                      <input type="text" class="form-control" placeholder="{{$editven->code_vendor}}" name="code" value="{{$editven->code_vendor}}">
                      </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="{{ route('list-kelolavendor')}}"   class="btn btn-default mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-info mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
              </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection