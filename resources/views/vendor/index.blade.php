@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Master Vendor</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Master Vendor</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              
                  @if ($errors->has('file'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('file') }}</strong>
                    </span>
                  @endif

                  @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $sukses }}</strong>
                    </div>
                  @endif

                @can('create_setting')
                <a href="{{ route('list-mastervendor.create')}}" class="btn btn-primary mr-5 mb-2">
                  Add Vendor
                </a>
                @endcan

                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Label Vendor</th>
                    <th>Code Vendor</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
    				        @php $i=1 @endphp
                    @foreach($vendor as $vdr)
                    <tr>
                      <td>{{ $vdr->label_vendor }}</td>
                      <td>{{ $vdr->code_vendor }}</td>
                        <td>
                          @can('edit_setting')
                          <a href="{{ route('list-mastervendor.edit', ['id' => $vdr->id])}}">Edit</a> | 
                          @endcan
                          @can('delete_setting')
                          <a href="{{ route('list-mastervendor.delete', ['id' => $vdr->id ] )}}" onclick="myFunction()">Delete</a>
                          @endcan
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
  	function myFunction() {
  		alert('Are you sure want to delete ??');
  	}
  </script>
@endsection