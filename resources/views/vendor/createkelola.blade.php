@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Manage Vendor</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Input Data</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <form action="{{ route('list-kelolavendor.add') }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                          <label>Vendor</label>
                          <select name="vendor" id="vendor" class="form-control select2" style="width: 100%;">
                            <option value="" selected disabled hidden class="form-control">Pilih Vendor</option>
                            @foreach($labelvendor  as $vdr)
                            <option value="{{ $vdr }}">{{ $vdr }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Zonasi</label>
                          <select name="zonasi" id="zonasi" class="form-control select2" style="width: 100%;">
                            <option value="" selected disabled hidden class="form-control">Pilih Zonasi</option>
                            @foreach($labelzonasi as $zonasi)
                            <option value="{{ $zonasi }}">{{ $zonasi }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Value</label>
                          <input type="text" class="form-control" name="value">
                        </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="#"   class="btn btn-default mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-info mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection