@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Manage Vendor</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
              <li class="breadcrumb-item active">Edit Manage Vendor</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <form action="{{ route('list-kelolavendor.storeedit', ['id' => $editkelvendor->id ]) }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                          <label>Vendor</label>
                          <select name="vendor" id="vendor" class="form-control select2" style="width: 100%;">
                            <option value="{{ $label_vendor }}"hidden class="form-control">{{ $label_vendor }}</option>
                            @foreach($labelvendor  as $vdr)
                            <option value="{{ $vdr }}">{{ $vdr }}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label>Zonasi</label>
                        <select name="zonasi" id="zonasi" class="form-control select2" style="width: 100%;">
                          <option value="{{ $label_zonasi }}"hidden class="form-control">{{ $label_zonasi }}</option>
                          @foreach($labelzonasi as $zonasi)
                          <option value="{{ $zonasi }}">{{ $zonasi }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label>Value</label>
                        <input type="text" class="form-control" placeholder="{{ $editkelvendor->value }}" name="value" value="{{ $editkelvendor->value }}">
                      </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="{{ route('list-kelolavendor')}}"   class="btn btn-default mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-info mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection