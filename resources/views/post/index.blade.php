@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Post</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Post</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
              @if ($sukses = Session::get('sukses'))
                <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                  <strong>{{ $sukses }}</strong>
                </div>
            @endif

            <div class="card-header">
                @can('view_setting')
                  <div class="row"> 
                    <div class="col-md-6">
                      {{-- Post Button --}}                      
                      <a href="{{ route('post.create') }}"  class="btn btn-primary mr-2" style="margin-bottom: 10px;">
                        <!--<i class="fas fa-plus-circle"></i> -->
                        Add Post
                      </a>
                    </div>
                  </div>
                @endcan     
            </div>

            <div class="card-body"> 
                <div class="tab-content mt-2" id="custom-content-below-tabContent">
                    <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                        <table id="postTable" class="table table-bordered text-nowrap" width="100%">
                            <thead class="thead-light">
                                <tr>
                                    <!--<th>&nbsp;</th>-->
                                    <th>Action</th>
                                    <th>Title</th>
                                    <th>Type</th>
                                    <th>Message</th>
                                    <th>Created at</th>
                                    <th>Posted</th>
                                </tr>
                            </thead>
                        </table>
                    </div> 
                </div>

            <!-- /.card-body -->
            </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  
   
    // $('#postTable').DataTable({
    //     scrollX:        true,
    //     processing: true,
    //     serverSide: true,
    //     "ajax": {
    //         "url": "{{ route('allPost') }}",
    //         "dataType": "json",
    //         "type": "POST",
    //         "data":{ _token: "{{csrf_token()}}"},       
    //     },
    //     columns: [
    //         { "data": "action", "name": 'action' },
    //         { "data": "title", "name": 'title' },
    //         { "data": "type", "name": 'type' },
    //         { "data": "message", "name": 'message' },
    //         { "data": "created_at", "name": 'created_at' },
    //         { "data": "posted", "name": 'posted' },
    //     ]
    // });

    $('#postTable').DataTable({
        // scrollX:        true,
        processing: true,
        serverSide: true,
        "ajax": {
            "url": "{{ route('allPost') }}",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: "{{csrf_token()}}"},       
        },
        columns: [
            { "data": "action", "name": 'action' },
            { "data": "title", "name": 'title' },
            { "data": "type", "name": 'type' },
            { "data": "message", "name": 'message' },
            { "data": "created_at", "name": 'created_at' },
            { "data": "posted", "name": 'posted' },
        ],
        "order": [[ 4, "desc" ]]
    });
    
    $('#filterButton').click(function () {
        $('#saveBtn').val("create...");
        $('#modelHeading').html("Filter");
        $('#filterModel').modal('show');
    
    
    });
    $('#resetFilter').click(function () {
        $('#itemForm').trigger("reset");
    start = '';
    end = '';
    $('#filterDate span').html('');
    $('#filter_start_date').val('');
    $('#filter_end_date').val('');
    //cb(start, end);
    });
  
  
  
    //$('#buttonImport').click(function (e) 
    $('#buttonImport').on('click', function (e) 
  //$('#importForm').on('submit', function (e) 
  {
    let isValidForm = $('#importForm')[0].checkValidity();
    //alert(isValidForm);
    
    if(!isValidForm){
      //return false;
      $('#importForm').find(':submit').click();
      //$('#importForm').submit();
    }
    else {
      e.preventDefault();
      
      Swal.fire({
        title: ''+$( "#wh_id option:selected" ).text(),
        text: "Are you sure you want to import from here ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
        }).then((result) => {
        if (result.value) { //yes
          //$('#importForm').submit();
          //$this.submit();
          //return true;
          //$('#importForm').find(':submit').click();
          $('#importForm').submit();
        }
        });
    }
    
    });

  
    //Date range picker
    //$('#filterDeliveryDate').daterangepicker()

  
    //let start = moment().subtract(29, 'days');
    //let end = moment();
  let start = '';
  let end = '';
    function cb(start, end) {
    /*
    if(start && start.length === 0){ 
    //if( isNaN(start)){ 
      start = moment().subtract(29, 'days');
      //start = moment();
      end = moment();
    }
    */
    if(!isNaN(start)){
      $('#filterDate span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#filter_start_date').val(start.format('MM/DD/YYYY'));
      $('#filter_end_date').val(end.format('MM/DD/YYYY'));      
    }
    console.log('start : '+start);
    console.log('isNaN : '+isNaN(start));
    }
    console.log('start : '+start);
    console.log('isNaN : '+isNaN(start));
    console.log('lenthg : '+start.length);

    $('#filterDate').daterangepicker({
    //autoUpdateInput: false,
        //startDate: start,//(start?start:moment().subtract(29, 'days')),
        startDate: ((start.length === 0)?moment().subtract(29, 'days'):start),
        //endDate: end,//(end?end:moment()),
        endDate: ((end.length === 0)?moment().subtract(28, 'days'):end),
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    //cb(start, end);

/*
 $('#buttonImport').on('click', function () {
    var $btn = $(this).button('loading')
    // business logic...
    $btn.button('reset')
  })
  */   

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  
  let allSes = @json(Session::all());
  console.log(allSes);
  
  /*
  $('select').select2({
    theme: 'bootstrap4',
  });
  */
});   
  </script>
@endsection
