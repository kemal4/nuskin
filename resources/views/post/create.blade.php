@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">
    <section class="p-2 pb-0">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <a href="{{ route('post.index')}}" class="text-muted"><i class="fas fa-long-arrow-alt-left"></i> Back to list</a>
          </div>
        </div>
      </div>
    </section>
    <!-- Content Header (Page header) -->
    <section class="content-header pt-0">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Create Post</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('post.index')}}">Post</a></li>
                    <li class="breadcrumb-item active">Create Post</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-9">
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <p></p><strong>Whoops!</strong> There were some problems with your input.</p>
                  <ul>
                     @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                     @endforeach
                 </ul>
              </div>
              @endif
              <div class="card">
                {!! Form::open(array('route' => 'post.store','method'=>'POST')) !!}
                    <div class="card-body">
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Title</strong>
                              {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control')) !!}
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Type</strong>
                              <select name="type" id="topic" class="form-control @error('type') is-invalid @enderror select2" style="width: 100%;" value="{{ old('roles') }}">
                                <option value="" selected disabled hidden class="form-control  @error('type') is-invalid @enderror">Select Type</option>
                                    <option value="Announcement">Announcement</option>
                                    @error('type')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <option value="Type 1">Type1 </option>
                                    @error('type')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <option value="Type 2">Type 2</option>
                                    @error('type')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                              </select>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12">
                          <div class="form-group">
                              <strong>Message</strong>
                              {!! Form::textarea('message', null, array('placeholder' => 'Message','class' => 'form-control')) !!}
                          </div>
                      </div>
                    </div>
                    <div class="card-footer">
                            <a href="{{ route('users.index')}}" class="btn btn-default mr-2 mb-2"><i class="fas fa-times"></i> Cancel</a>
                            <div class="float-right"><button type="submit" class="btn btn-info mb-2"> Submit </button></div>
                    </div>
                {!! Form::close() !!}
              </div>
            </div>
        </div>
      </div>
    </section>
</div>

@endsection
