@extends('layoutAdmin.global')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1>Master Warehouse</h1>
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
	              <li class="breadcrumb-item">Settings</li>
	              <li class="breadcrumb-item active">Master Warehouse</li>
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>

	<section class="content">
      	<div class="row">
        	<div class="col-12">
          		<div class="card">
            		<div class="card-header">
              			<h3 class="card-title">List Warehouse</h3>
            		</div> <!-- /.card-header -->
		            <div class="card-body">			
		            	@can('create_setting')
						 <a href="{{ route('newgudang')}}"   class="btn btn-primary mr-5 mb-2">
									Add Warehouse
						 </a>
						 @endcan
							<table id="example1" class="table table-bordered">
								<thead class="thead-light">
									<tr>
									  <th>No</th>
									  <th>Code Warehouse</th>
									  <th>Warehouse</th>
									  <th>Action</th>
									</tr>
								</thead>
									<tbody>
									  @php $i=1 @endphp
									  @foreach($gudang as $gdg)
									  <tr>
										<td>{{ $i++ }}</td>
										<td>{{$gdg->code_gudang}}</td>
										<td>{{$gdg->lokasi_gudang}}</td>
										<td>
											@can('create_setting')
											<a href="{{ route('editgudangs', ['id_gudang' => $gdg->id_gudang ])}}" class="btn btn-warning btn-sm" data-toggle='tooltip' title='Edit'><i class="fas fa-edit"></i></a> 
											@endcan @can('delete_setting')<a href="{{ route('deletegudang', ['id_gudang' => $gdg->id_gudang ])}}" class="btn btn-danger btn-sm" data-toggle='tooltip' title='Delete' onclick="myFunction()"><i class="fas fa-trash"></i></a>@endcan</td>
									  </tr>
									  @endforeach
									</tbody>
						    </table>
		            </div>
		        </div>
		    </div>
		</div>
	</section>

	
	</div>
	<script type="text/javascript">
		function myFunction() {
			alert('Are you sure Want To Delete ??');
		}
	</script>
@endsection