@extends('layoutAdmin.global')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1>Kelola Users</h1>
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
<!-- 	              <li class="breadcrumb-item">Home</li>
	              <li class="breadcrumb-item active">DataTables</li> -->
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>

		@if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
        </div>
		@endif


	<section class="content">
      	<div class="row">
        	<div class="col-12">
          		<div class="card">
            		<div class="card-header">
              			<h3 class="card-title">List User</h3>
            		</div> <!-- /.card-header -->
		            <div class="card-body">
		            	<a  class="btn btn-primary float-right" href="{{ route('usercreate')}}" role="">Add User</a>

						<table class="table">
							<thead>
								<tr>
									<th scope="col">Name</th>
									<th scope="col">Email</th>
									<th scope="col">Roles</th>
									<th scope="col">Aksi</th>
								</tr>
							</thead>
							<tbody>

								@foreach($users as $use)
								<tr>
									<td>{{ $use->name }}</td>
									<td>{{ $use->email }}</td>
									<td>{{ $use->role }}</td>
									<td> 
										@if($use->role == 'SuperAdmin')
										<a href="{{ route('userdedit', ['id'=>$use->id ])}}" >Edit</a>
										@else
										<a href="{{ route('userdedit', ['id'=>$use->id ])}}" >Edit</a> | 
										<a href="{{ route('userdestroy', ['id'=>$use->id ])}}"  onclick="return confirm('Are you sure you want to delete this user?');" >Delete</a>
										@endif
									</td>
								</tr>
								@endforeach

							</tbody>
						</table>
		            </div>
		        </div>
		    </div>
		</div>
	</section>

	</div>

@endsection