@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Home</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <div class="card-header">
                    <!-- <h3 class="card-title">Quick Example <small>jQuery Validation</small></h3> -->
                  </div>

                  <form action="{{ route('userstore') }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                          <label>Email</label>
                          <input type="text" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                          <label>Roles</label>
                            <select name="listroles" id="zona" class="form-control select2" style="width: 100%;">
                               <option value="" selected disabled hidden class="form-control">Select Role</option>
                               <option value="Admin">Admin</option>
                               <option value="User">User</option>
                               <option value="warehouse">Warehouse</option>
                               <option value="crm">CRM</option>
                             </select>
                        </div>
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-md-4">
                          <a href="{{ route('usermenu')}}"   class="btn btn-success mr-5 col-sm-12">Cancel
                          </a>
                        </div>
                        <div class="col-md-4">
                          <button type="submit" class="btn btn-success mr-5 col-sm-12"> Submit </button>
                        </div>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection