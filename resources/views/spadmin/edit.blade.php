@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Warehouse</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item"><a href="{{ route('kelolarole')}}">Master Warehouse </a></li>
              <li class="breadcrumb-item active">Edit Warehouse</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">

                  <form action="{{ route('editgudang', ['id_gudang' => $editgudang->id_gudang ]) }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label>Code Warehouse</label>
                        <input type="text" class="form-control" placeholder="{{ $editgudang->code_gudang }}" name="codegudang" value="{{ $editgudang->code_gudang }}">
                      </div>
                        <div class="form-group">
                          <label>Warehouse</label>
                          <input type="text" class="form-control" placeholder="{{ $editgudang->lokasi_gudang }}" name="gudang" value="{{ $editgudang->lokasi_gudang }}">
                        </div>
                    </div>

                    <div class="card-footer">
                      <div class="row">
                          <a href="{{ route('kelolarole')}}"class="btn btn-default mr-2 mb-2">Cancel</a>
                          <button type="submit" class="btn btn-info mb-2"> Submit </button>
                      </div>
                    </div>
                    
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

  
@endsection