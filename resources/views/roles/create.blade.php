@extends('layoutAdmin.global')

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>Create Role</h1>
              </div>
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('roles.index')}}">Role Management</a></li>
                    <li class="breadcrumb-item active">Create Role</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Permission : </strong>
                            <br>
							<div class="row">
                            @foreach($permission as $subMenu)
								<div class="card card-dark col-sm-6">
								  <div class="card-header">
									<h3 class="card-title">{{ $subMenu['title'] }}</h3>
									<div class="card-tools">
									  <button type="button" class="btn btn-tool" data-card-widget="collapse">
										<i class="fas fa-minus"></i>
									  </button>
									</div>
									<!-- /.card-tools -->
								  </div>
								  <!-- /.card-header -->
								  <div class="card-body">
							
                            @foreach($subMenu['data'] as $value)
                                <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                                {{ $value->name }}</label>
                            <br/>
                            @endforeach
							
							
								  </div>
								  <!-- /.card-body -->
								  <div class="card-footer">
								  </div>
								  <!-- /.card-footer -->
								</div>
								<!-- /.card -->

                            @endforeach
                        </div>
							
							
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card-footer">
                            <div class="row">
                                  <a href="{{ route('roles.index')}}" class="btn btn-default mr-2 mb-2">Cancel
                                  </a>
                                 <button type="submit" class="btn btn-info mb-2"> Submit </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

@endsection