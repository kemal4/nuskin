@extends('layoutAdmin.global')

@section('content')

<div class="content-wrapper">

  <section class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
              <div class="col-sm-6">
                  <h1>Role Management</h1>
              </div>
              <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                      <li class="breadcrumb-item">Settings</li>
                      <li class="breadcrumb-item active">Role Management</li>
                  </ol>
              </div>
          </div>
      </div>
  </section>
  
  <section class="content">
      <div class="container">
          <div class="row justify-content-md-center">
              <div class="col-12 col-md-9">
                  <div class="card">
                      <div class="card-header">
                          @can('create_setting')
                          <a href="{{ route('roles.create') }}" class="btn btn-primary mr-3" style="margin-bottom: 10px;">
                            <i class="fas fa-plus mr-1"></i>Create Role
                        </a> @endcan
                      </div>
                      <div class="card-body">
  
                          @if ($message = Session::get('success'))
                          <div class="alert alert-success alert-block">
                              <button type="button" class="close" data-dismiss="alert">×</button>
                              <p style="color: #fff;">{{ $message }}</p>
                          </div>
                          @endif
  
                          <table id="rolemanage" class="table" style="border:1px solid #ddd">
                              <thead class="thead-light">
                                  <tr>
                                      <th>Name</th>
                                      <th width="100px" class="text-right">Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  @foreach ($roles as $key => $role)
                                  <tr>
                                      <td>{{ $role->name }}</td>
                                      <td class="text-right">
                                          <a class="btn btn-success btn-xs" href="{{ route('roles.show',$role->id) }}" data-toggle='tooltip' title='Detail'>
                                              <i class="fas fa-info-circle"></i>
                                          </a> @can('edit_setting')
                                          <a class="btn btn-warning btn-xs" href="{{ route('roles.edit',$role->id) }}" data-toggle='tooltip' title='Edit'>
                                            <i class="fas fa-edit"></i> </a>
                                          @endcan
                                          @can('delete_setting')
                                          {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                          <button type="submit" class="btn btn-danger btn-xs" onclick="myFunction()" data-toggle='tooltip' title='Delete'>
                                              <i class="fas fa-trash"></i></button>
                                          {!! Form::close() !!} @endcan
  
                                      </td>
                                  </tr>
                                  @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

</div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        $('#rolemanage').DataTable();
    });

    function myFunction() {
      confirm("Are you sure you want to delete this user?");
    }
</script>
@endsection
