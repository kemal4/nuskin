@extends('layoutAdmin.global')

@section('content')
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Detail Role</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('roles.index')}}">Role Management</a></li>
                <li class="breadcrumb-item active">Detail Role</li>
              </ol>
            </div>
          </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name:</strong>
                                {{ $role->name }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Permissions:</strong>
								
								<div class="row">
									@foreach($permission as $subMenu)
										<div class="card card-dark col-sm-6">
										  <div class="card-header">
											<h3 class="card-title">{{ $subMenu['title'] }}</h3>
											<div class="card-tools">
											  <button type="button" class="btn btn-tool" data-card-widget="collapse">
												<i class="fas fa-minus"></i>
											  </button>
											</div>
											<!-- /.card-tools -->
										  </div>
										  <!-- /.card-header -->
										  <div class="card-body">
									@if(!empty($subMenu['data']))
									@foreach($subMenu['data'] as $value)
										<label>{{ $value->name }}</label>
									<br/>
									@endforeach
									@endif
									
										  </div>
										  <!-- /.card-body -->
                                          <!-- /.card-footer -->
                                        </div>
                                        <!-- /.card -->

                                    @endforeach
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card-footer">
                                <a href="{{ route('roles.index')}}" class="btn btn-default">Back</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
@endsection