@extends('layoutAdmin.global')


@section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance - Lead Time - {{ $zones->providerName }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('performance.leadtime')}}">Performance</a></li>
              <!-- <li class="breadcrumb-item active">Delivery Order</li> -->
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="card">

      <div class="row">
        <div class="col-12">
            <div class="card-body">
			
                   <form action="{{ route('filterDO') }}" method="GET">
                    {{ csrf_field() }}
                    <div class="row d-none">
                      <div class="col-md-12">
                        <div class="card card-success">

                          <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Date :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i>
                                        </span>
                                      </div>
                                      <input type="date" class="form-control" name="filterdate" value="">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Month :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                      </div>
                                      <input type="month" class="form-control" name="filtermonth">
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label></label>
                                    <div class="input-group">
                                      <center>
                                      <input type="submit" value="Filter" class="btn btn-secondary btn-md">
                                      <a href="{{ route('list-pickup')}}"   class="btn btn-secondary btn-md">Reset
                                      </a>
                                      </center>
                                    </div>
                                  </div>
                                </div>
                              <!-- row -->
                            </div>
                            <!-- card-body -->
                          </div>
                                <!-- </div> -->
                        </div>
                      </div>
                    </div>
                  </form>
                  <hr>



	<div class="card">
	  <div class="row">

	    <div class="col-md">
			<!-- table information -->
			<table class='table'>
				<thead>
					<tr>
						<td>Zone</td>
						<td>Delivered</td>
						<td>On Time</td>
						<td>Late</td>
						<td>action</td>
					</tr>
				</thead>
				<tbody>
						@foreach($zones as $provider)
					<tr>
						<td>{{ $provider->code_zonasi }}</td>
						<td>{{ $provider->totalDelivered }}</td>
						<td>{{ $provider->totalOnTime }}</td>
						<td>{{ $provider->totalLate }}</td>
						<td><a href="{{route('performance.leadtime.zone', ['pid'=>$zones->providerId , 'zid'=>$provider->id ])}}">detail</a></td>
					</tr>
						@endforeach
				</tbody>
			</table>
		</div>	
	    <div class="col-md">
	      <div id="piechart"></div>
	    </div>	
	  </div>
	  <hr>

		<div  class="row">
		
		@php $idiv = 12/$totalChart; for($i=0;$i<$totalChart; $i++){ @endphp
		
			<div class="col-sm-<?= $idiv ?>">
				<div id="piechart_<?= $i ?>"></div>
			</div>
			
		@php } @endphp	
		<!--
                        <div class="col-sm">
                                <div id="piechart2"></div>
                        </div>
                        <div class="col-sm">
                                <div id="piechart3"></div>
                        </div>
		-->
		</div>


	</div>



            </div>
          </div>
        </div>
      </div >
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection




@section('scripts')
	 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
	<script type="text/javascript">
	// Load google charts
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	// Draw the chart and set the chart values
	function drawChart() {
	  var data = google.visualization.arrayToDataTable( @json($totals) );

	  // Optional; add a title and set the width and height of the chart
	  var options = {
		'title':'Total Delivered',
		'is3D':true,
		'pieHole': 0.4,
		animation:{
			duration: 1500,
			startup: true
			}
		//'legend':{'position':'bottom'}
		};
	  //var options = {'title':'My Average Day', 'width':550, 'height':400};

	  // Display the chart inside the <div> element with id="piechart"
	  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	  chart.draw(data, options);
	
	@php $i = 0; @endphp
	@foreach($zones as $provider)
		@if ($provider->totalDelivered && $provider->totalDelivered > 0)
		
       data = google.visualization.arrayToDataTable( @json($provider->chartData) );

          // Optional; add a title and set the width and height of the chart
          options = {
                'title': '{{ $provider->code_zonasi }}' ,
                'is3D':true,
                'legend':'none'
                };
          //var options = {'title':'My Average Day', 'width':550, 'height':400};

          // Display the chart inside the <div> element with id="piechart"
          //chart = new google.visualization.PieChart(document.getElementById('piechart_{{ $loop->index }}'));
          chart = new google.visualization.PieChart(document.getElementById('piechart_{{ $i }}'));
          chart.draw(data, options);

		@endif	
		@php $i++; @endphp
	@endforeach




	}
	</script>  



@endsection
