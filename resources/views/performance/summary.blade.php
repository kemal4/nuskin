@extends('layoutAdmin.global')


@section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('performance.summary')}}">Performance</a></li>
              <!-- <li class="breadcrumb-item active">Delivery Order</li> -->
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="card">

      <div class="row">
        <div class="col-12">
            <div class="card-body">
			
                   <form action="{{ route('filterDO') }}" method="GET">
                    {{ csrf_field() }}
                    <div class="row d-none">
                      <div class="col-md-12">
                        <div class="card card-success">

                          <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Date :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i>
                                        </span>
                                      </div>
                                      <input type="date" class="form-control" name="filterdate" value="">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Month :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                      </div>
                                      <input type="month" class="form-control" name="filtermonth">
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label></label>
                                    <div class="input-group">
                                      <center>
                                      <input type="submit" value="Filter" class="btn btn-secondary btn-md">
                                      <a href="{{ route('list-pickup')}}"   class="btn btn-secondary btn-md">Reset
                                      </a>
                                      </center>
                                    </div>
                                  </div>
                                </div>
                              <!-- row -->
                            </div>
                            <!-- card-body -->
                          </div>
                                <!-- </div> -->
                        </div>
                      </div>
                    </div>
                  </form>
                  <hr>



	<div class="card">
	  <div class="row">

	    <div class="col-sm-6">
	      <div id="piechart"></div>
	    </div>
	    <div class="col-md-6">
	    </div>
	
	  </div>

		<div  class="row">
			<div class="col-sm">
				<div id="piechart1"></div>
			</div>
                        <div class="col-sm">
                                <div id="piechart2"></div>
                        </div>
                        <div class="col-sm">
                                <div id="piechart3"></div>
                        </div>

		</div>


	</div>



            </div>
          </div>
        </div>
      </div >
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection




@section('scripts')
	 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
	<script type="text/javascript">
	// Load google charts
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	// Draw the chart and set the chart values
	function drawChart() {
	  var data = google.visualization.arrayToDataTable([
	  ['Vendor', 'Total'],
	  ['JNE', 8],
	  ['FL', 20],
	  ['Mr Speedy', 5],
	]);

	  // Optional; add a title and set the width and height of the chart
	  var options = {
		'title':'Total Delivery',
		'is3D':true,
		'pieHole': 0.4,
		//'legend':{'position':'bottom'}
		};
	  //var options = {'title':'My Average Day', 'width':550, 'height':400};

	  // Display the chart inside the <div> element with id="piechart"
	  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	  chart.draw(data, options);



          data = google.visualization.arrayToDataTable([
          ['Type', 'Total'],
          ['Fulfilled', 28],
          ['Unfulfilled', 16],
        ]);

          // Optional; add a title and set the width and height of the chart
          options = {
                'title':'JNE',
                'is3D':true,
                'legend':'none'
                };
          //var options = {'title':'My Average Day', 'width':550, 'height':400};

          // Display the chart inside the <div> element with id="piechart"
          chart = new google.visualization.PieChart(document.getElementById('piechart1'));
          chart.draw(data, options);



          data = google.visualization.arrayToDataTable([
          ['Type', 'Total'],
          ['Fulfilled', 280],
          ['Unfulfilled', 66],
        ]);

          // Optional; add a title and set the width and height of the chart
          options = {
                'title':'FL',
                'is3D':true,
                'legend':'none'
                //'legend':{'position':'bottom'}
                };
          //var options = {'title':'My Average Day', 'width':550, 'height':400};

          // Display the chart inside the <div> element with id="piechart"
          chart = new google.visualization.PieChart(document.getElementById('piechart2'));
          chart.draw(data, options);




          data = google.visualization.arrayToDataTable([
          ['Type', 'Total'],
          ['Fulfilled', 228],
          ['Unfulfilled', 16],
        ]);

          // Optional; add a title and set the width and height of the chart
          options = {
                'title':'Mr Speedy',
                'is3D':true,
                'legend':'none'
                //'legend':{'position':'bottom'}
                };
          //var options = {'title':'My Average Day', 'width':550, 'height':400};

          // Display the chart inside the <div> element with id="piechart"
          chart = new google.visualization.PieChart(document.getElementById('piechart3'));
          chart.draw(data, options);





	}
	</script>  



@endsection

