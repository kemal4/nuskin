@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance: Lead Time - {{ $zones->providerName }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item active">Performance</li>
              <li class="breadcrumb-item"><a href="{{ route('performance.leadtime')}}">Lead Time</a></li>
              <li class="breadcrumb-item active">{{ $zones->providerName }}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
	
	
		@if($hasChartData)
		<div class="row pb-2">
		  <div class="col-sm-5">
			<div class="card h-100 mb-0">
			  <div class="card-header">
				<h3 class="card-title">{{ $zones->providerName }} Successful Deliveries</h3>			
			  </div>
			  <div class="card-body">
				<div id="piechart"></div>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-7">
			<div class="card h-100 mb-0">
			  <div class="card-header">
				<h3 class="card-title">Zone Fulfillment Rate</h3>			
			  </div>
			  <div class="card-body">
				<div class="row justify-content-sm-center">			
				@foreach($zones as $idx => $provider)
				  @if ($provider->totalDelivered && $provider->totalDelivered > 0)
					<div class="col-sm-6">
						<div id="piechart_<?= $idx ?>"></div>
					</div>
				  @endif
				@endforeach					
				</div>
				<div class="d-flex justify-content-center">
				
				  <span class="mt-auto">
					<i class="fa fa-square" style="color:#3366CC"></i> Fulfilled
					&nbsp;&nbsp;&nbsp;
					<i class="fas fa-circle" style="color:#DC3912"></i> Unfulfilled
				  </span>
				</div>
			  </div>
			</div>			
		  </div>		
		</div>
		@endif
		<div class="row">
		  <div class="col-sm-12">
			
			<div class="card">
				  
			  <div class="card-header">
                  <h3 class="card-title">Zone Fulfillment</h3>
			  
                  <div class="d-flex justify-content-end align-items-center">
				  
					  <form action="{{ route('performance.leadtime') }}" method="GET">
					  {{ csrf_field() }}
						
								<label class="col-form-label ml-auto">Period: &nbsp;</label>
								<input type="date" class="form-control-row" name="filterStartDate" value="<?= $filterStartDate ?>" data-toggle="tooltip" title="Start Date">
								&nbsp;-&nbsp;
								<input type="date" class="form-control-row" name="filterEndDate" value="<?= $filterEndDate ?>" data-toggle="tooltip" title="End Date">
								<button type="submit" class="btn btn-primary btn-sm ml-2">Set</button>
											
					  </form>
				  
				  </div>
			  </div>
			  <div class="card-body">
					  
				<div class="row">
				  <div class="col-sm-12">				   
				  
				  
					<!-- table information -->
					<table class='table'>
						<thead class="thead-light">
							<tr>
								<th>Zone</th>
								<th>Delivered</th>
								<th>Fulfilled</th>
								<th>Unfulfilled</th>
								<th>action</th>
							</tr>
						</thead>
						<tbody>								
						  @foreach($zones as $provider)
							<tr>
								<td>{{ $provider->code }}</td>
								<td>{{ $provider->totalDelivered }}</td>
								<td>{{ $provider->totalOnTime }}</td>
								<td>{{ $provider->totalLate }}</td>
								<td><a href="{{route('performance.leadtime.zone', ['pid'=>$zones->providerId , 'zid'=>$provider->id ,'startDate'=>$filterStartDate,'endDate'=>$filterEndDate ])}}">
									<!--detail-->
									<i class="fas fa-search-plus" data-toggle="tooltip" title="View Detail"></i>
									</a>
								</td>
							</tr>
						  @endforeach
						</tbody>
					</table>
				  </div>		
				</div>

			
				
			  </div>
			  
			  <div class="card-footer">
				<a href="{{ url()->previous() }}" class="btn btn-default">Back
                </a>
			  </div>
			</div>
		  </div>		
		</div>
		
	

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection




@section('scripts')
	 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
	<script type="text/javascript">
	// Load google charts
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	// Draw the chart and set the chart values
	function drawChart() {
	  var data = google.visualization.arrayToDataTable( @json($totals) );

	  // Optional; add a title and set the width and height of the chart
	  var options = {
		'title':'Total Delivered',
		'is3D':true,
		'pieHole': 0.4,
		animation:{
			duration: 1500,
			startup: true
			}
		//'legend':{'position':'bottom'}
		};
	  //var options = {'title':'My Average Day', 'width':550, 'height':400};

	  // Display the chart inside the <div> element with id="piechart"
	  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	  chart.draw(data, options);
	
	
	@foreach($zones as $idx =>  $provider)
		@if ($provider->totalDelivered && $provider->totalDelivered > 0)
		
       data = google.visualization.arrayToDataTable( @json($provider->chartData) );

          // Optional; add a title and set the width and height of the chart
          options = {
                'title': '{{ $provider->code }}' ,
                'is3D':true,
                'legend':'none'
                };
          //var options = {'title':'My Average Day', 'width':550, 'height':400};

          // Display the chart inside the <div> element with id="piechart"
          //chart = new google.visualization.PieChart(document.getElementById('piechart_{{ $loop->index }}'));
          chart = new google.visualization.PieChart(document.getElementById('piechart_{{ $idx }}'));
          chart.draw(data, options);

		@endif	
	@endforeach




	}
	</script>  



@endsection
