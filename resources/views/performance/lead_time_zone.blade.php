@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance: Lead Time - {{ $info->providerName }} - {{ $info->name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item active">Performance</li>
              <li class="breadcrumb-item"><a href="{{ route('performance.leadtime')}}">Lead Time</a></li>
              <li class="breadcrumb-item"><a href="{{ url()->previous() }}">{{ $info->providerName }} </a></li>
              <li class="breadcrumb-item active">{{ $info->name }}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12"> 
          <div class="card">
			<div class="card-header">
              <h3 class="card-title">Postal Fulfillment</h3>
			</div>
            <div class="card-body">
			                   
			  <table class='table'>
				<thead class="thead-light">
					<tr>
						<th>No.</th>
						<th>Zip Code</th>
						<th>City</th>
						<!--<td>City</td>-->
						<th>Delivered</th>
						<th>Fulfilled - {{ $info->fulPct}}%</th>
						<th>Unfulfilled - {{ $info->unfulPct}}%</th>
						<th>action</th>
					</tr>
				</thead>
				<tbody>
						@foreach($zipCodes as $zipCode)
					<tr>
						<td>{{ $zipCode->idx }}</td>
						<td>{{ $zipCode->zip }}</td>
						<td>{{ $zipCode->city_name }}</td>
						<td>{{ $zipCode->totalDelivered }}</td>
						<td>{{ $zipCode->totalOnTime }}</td>
						<td>{{ $zipCode->totalLate }}</td>
						<td></td>
					</tr>
						@endforeach
				</tbody>
			  </table>



            </div>
			  <div class="card-footer">
				<a href="{{ url()->previous() }}" class="btn btn-default">Back
                </a>
			  </div>
          </div>
        </div>
      </div >
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection



