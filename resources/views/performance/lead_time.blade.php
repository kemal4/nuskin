@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance: Lead Time</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item active">Performance</li>
              <li class="breadcrumb-item active">Lead Time</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
			
		@if($hasChartData)
		<div class="row pb-2">
		  <div class="col-sm-5">
			<div class="card h-100 mb-0">
			  <div class="card-header">
				<h3 class="card-title">NuSkin Successful Deliveries</h3>			
			  </div>
			  <div class="card-body">
				<div id="piechart"></div>
			  </div>
			</div>
		  </div>
		  <div class="col-sm-7">
			<div class="card h-100 mb-0">
			  <div class="card-header">
				<h3 class="card-title">Courier Fulfillment Rate</h3>			
			  </div>
			  <div class="card-body">
				<div class="row justify-content-sm-center">			
				@foreach($providers as $idx => $provider)
				  @if ($provider->totalDelivered && $provider->totalDelivered > 0)
					<div class="col-sm-4">
						<div id="piechart_<?= $idx ?>"></div>
					</div>
				  @endif
				@endforeach			
				</div>
				<div class="d-flex justify-content-center">
				
				  <span class="mt-auto">
					<i class="fa fa-square" style="color:#3366CC"></i> Fulfilled
					&nbsp;&nbsp;&nbsp;
					<i class="fas fa-circle" style="color:#DC3912"></i> Unfulfilled
				  </span>
				</div>
			  </div>
			</div>			
		  </div>		
		</div>
		@endif
		<div class="row">
		  <div class="col-sm-12">
			
			<div class="card">
				  
			  <div class="card-header">
				<h3 class="card-title">Courier Fulfillment</h3>

                  <div class="d-flex justify-content-end align-items-center">
				  
					  <form action="{{ route('performance.leadtime') }}" method="GET">
					  {{ csrf_field() }}
						  
								<label class="col-form-label ml-auto">Period: &nbsp;</label>
								<input type="date" class="form-control-row" name="filterStartDate" value="<?= $filterStartDate ?>" data-toggle="tooltip" title="Start Date">
								&nbsp;-&nbsp;
								<input type="date" class="form-control-row" name="filterEndDate" value="<?= $filterEndDate ?>" data-toggle="tooltip" title="End Date">
								<button type="submit" class="btn btn-primary btn-sm ml-2">Set</button>
								
					  </form>
				</div>
				<!-- /.card-tools -->
			  </div>
			  <div class="card-body">
					

				<!--
				<div class="row mb-1">
				  <div class="col-sm-12">
					<h3 class="card-title">Courier Fulfillment</h3>	
					 
					  <form action="{{ route('performance.leadtime') }}" method="GET">
					  {{ csrf_field() }}
						<div class="row">
						  <div class="col-sm-12">
						  
							<div class="input-group">
								<label class="col-form-label ml-auto">Period: </label>
								<input type="date" class="form-control-row" name="filterStartDate" value="<?= $filterStartDate ?>" data-toggle="tooltip" title="Start Date">
								&nbsp;-&nbsp;
								<input type="date" class="form-control-row" name="filterEndDate" value="<?= $filterEndDate ?>" data-toggle="tooltip" title="End Date">
								<button type="submit" class="btn btn-primary btn-sm ml-2">Set</button>
							</div>					
						  </div>					
						</div>					
					  </form>
				  </div>		
				</div>
				-->
				
				
				<div class="row">
				  <div class="col-sm-12">				   
					<!-- table information -->
					<table class='table'>
						<thead class="thead-light">
							<tr>
								<th>Courier</th>
								<th>Delivered</th>
								<th>Fulfilled</th>
								<th>Unfulfilled</th>
								<th>action</th>
							</tr>
						</thead>
						<tbody>
								@foreach($providers as $provider)
							<tr>
								<td>{{ $provider->display_name }}</td>
								<td>{{ $provider->totalDelivered }}</td>
								<td>{{ $provider->totalOnTime }}</td>
								<td>{{ $provider->totalLate }}</td>
								<td><a href="{{route('performance.leadtime.provider', ['pid'=>$provider->id ,'startDate'=>$filterStartDate,'endDate'=>$filterEndDate])}}">
										<i class="fas fa-search-plus" data-toggle="tooltip" title="View Detail"></i>
									</a>
									
									<a href="{{route('performance.leadtime.provider.download', ['courier'=>$provider->display_name ,'courier_id'=>$provider->id ,'startDate'=>$filterStartDate,'endDate'=>$filterEndDate])}}">
										<i class="fas fa-cloud-download-alt"  data-toggle="tooltip" title="Download Unfulfilled Data"></i>
									</a>
									
								</td>
							</tr>
								@endforeach
						</tbody>
					</table>
				  </div>		
				</div>				
			  </div>			  
			</div>
		  </div>		
		</div>

		<div class="row pb-2 d-none">
		  <div class="col-sm-5">
			<div class="card h-100 mb-0">
			  <div class="card-header">
				<h3 class="card-title">NuSkin Successful Deliveries</h3>			
			  </div>
			  <div class="card-body">
				<div class="chart-container" style="aspectRatio: 1; ">
				    <canvas id="chDonut1"></canvas>				
				</div>
			  </div>
			</div>
		  </div>	
		  <div class="col-sm-7">
			<div class="card h-100 mb-0">
			  <div class="card-header">
				<h3 class="card-title">Courier Fulfillment Rate</h3>			
			  </div>
			  <div class="card-body">
                <canvas id="stackedBar"></canvas>
			  </div>
			</div>			
		  </div>		
		</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection




@section('scripts')
	 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
	<script type="text/javascript">
	
	
	// Load google charts
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawChart);

	// Draw the chart and set the chart values
	function drawChart() {
	  var data = google.visualization.arrayToDataTable( @json($totals) );
//console.log(@json($totals));
	  // Optional; add a title and set the width and height of the chart
	  var options = {
		//'title':'Total Delivered',
		'is3D':true,
		'pieHole': 0.4,
		//'legend.position':'labeled' ,
		//'legend.alignment':"center",
		//'legend':{'position':'labeled','alignment':'center'}
		};
	  //var options = {'title':'My Average Day', 'width':550, 'height':400};

	  // Display the chart inside the <div> element with id="piechart"
	  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
	  chart.draw(data, options);

	@foreach($providers as $idx => $provider)		
		@if($provider->totalDelivered && $provider->totalDelivered > 0)
		
       data = google.visualization.arrayToDataTable( @json($provider->chartData) );

          // Optional; add a title and set the width and height of the chart
          options = {
                'title': '{{ $provider->display_name }}' ,
                'is3D':true,
                'legend':'none',
				//'legend':{'position':'bottom'}
					titleTextStyle: {
						color: 'black',    // any HTML string color ('red', '#cc00cc')
						fontName: 'Arial', // i.e. 'Times New Roman'
						fontSize: 12, // 12, 18 whatever you want (don't specify px)
						bold: true,    // true or false
						italic: false   // true of false
					}
                };
          chart = new google.visualization.PieChart(document.getElementById('piechart_{{ $idx }}'));
          chart.draw(data, options);

			
		@endif
	@endforeach




	}
	
	
	
let colors = ['#3366cc','#dc3912','#ff9900','#109618','#990099','#0099c6'];

/* large pie/donut chart */
let chPie = document.getElementById("piechartjs");
if (chPie) {
  new Chart(chPie, {
    type: 'pie',
    data: {
      labels: ['Desktop', 'Phone', 'Tablet', 'Unknown'],
      datasets: [
        {
          backgroundColor: [colors[1],colors[0],colors[2],colors[5]],
          borderWidth: 0,
          data: [50, 40, 15, 5]
        }
      ]
    },
    plugins: [{
      beforeDraw: function(chart) {
        var width = chart.chart.width,
            height = chart.chart.height,
            ctx = chart.chart.ctx;
        ctx.restore();
        var fontSize = (height / 70).toFixed(2);
        ctx.font = fontSize + "em sans-serif";
        ctx.textBaseline = "middle";
        var text = chart.config.data.datasets[0].data[0] + "%",
            textX = Math.round((width - ctx.measureText(text).width) / 2),
            textY = height / 2;
        ctx.fillText(text, textX, textY);
        ctx.save();
      }
    }],
    options: {layout:{padding:0}, legend:{display:false}, cutoutPercentage: 50}
  });
}

/* 3 donut charts */
var donutOptions = {
   tooltips: {
     enabled: true
   },
  cutoutPercentage: 35, 
  legend: {display:true,position:'right',align:'start', padding:5, labels: {pointStyle:'circle', usePointStyle:true}},
  plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                
                  let sum = 0;
                  let dataArr = ctx.chart.data.datasets[0].data;
                  dataArr.map(data => {
                      sum += data;
                  });
                  let percentage = (value*100 / sum).toFixed(2)+"%";
                  return percentage;

              
                },
                color: 'white',
                     }
        }
};

// donut 1
let summ = @json($totals);
<?php
	$arrLab = [];
	$arrNum = [];
	foreach($totals as $key=> $aaa){
		if($key == 0)
			continue;
		$arrLab[] = $aaa[0];
		$arrNum[] = $aaa[1];
	}
?>

var chDonutData1 = {
    labels: @json($arrLab),
    datasets: [
      {
        backgroundColor: colors.slice(0,3),
        borderWidth: 0,
        data: @json($arrNum)
      }
    ]
};


var chDonut1 = document.getElementById("chDonut1").getContext('2d');
if (chDonut1) {
  new Chart(chDonut1, {
      type: 'pie',
      data: chDonutData1,
      options: donutOptions
  });
}


        



// Label formatter function
const formatter = (value, ctx) => {
  const otherDatasetIndex = ctx.datasetIndex === 0 ? 1 : 0;
  const total =
    ctx.chart.data.datasets[otherDatasetIndex].data[ctx.dataIndex] + value;

  return `${(value / total * 100).toFixed(0)}%`;
};

const data = [
  {
    // stack: 'test',
    label: "New",
    backgroundColor: "#1d3f74",
    data: [6310, 5742, 4044, 5564],
    // Change options only for labels of THIS DATASET
    datalabels: {
      color: "white",
      formatter: formatter
    }
  },
  {
    // stack: 'test',
    label: "Repeat",
    backgroundColor: "#6c92c8",
    data: [11542, 12400, 12510, 11450],
    // Change options only for labels of THIS DATASET
    datalabels: {
      color: "yellow",
      formatter: formatter
    }
  }
];

const options = {
  maintainAspectRatio: false,
  spanGaps: false,
  responsive: true,
  legend: {
    display: true,
    position: "bottom",
    labels: {
      fontColor: "#fff",
      boxWidth: 14,
      fontFamily: "proximanova"
    }
  },
  plugins: {
    // Change options for ALL labels of THIS CHART
    datalabels: {
      color: "#white",
      align: "center"
    }
  },
  scales: {
    
    xAxes: [
      {
        stacked: true,
        gridLines: {
          display: false
        },
        ticks: {
          fontColor: "#fff"
        }
      },
      {
        type: 'category',
        offset: true,
        position: 'top',
        ticks: {
          fontColor: "#fff",
          callback: function(value, index, values) {
            return data[0].data[index] + data[1].data[index]
          }
        }
      }
    ],
    yAxes: [
      {
        stacked: true,
        display: false,
        ticks: {
          fontColor: "#fff"
        }
      }
    ]
  }
};

const ctx = document.getElementById("stackedBar").getContext("2d");

new Chart(ctx, {
  type: "horizontalBar",
  data: {
    labels: ["Jun", "July", "Aug", "Sept"],
    datasets: data
  },
  options: options
});



   

	
	</script>  



@endsection