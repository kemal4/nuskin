@extends('layoutAdmin.global')


@section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Performance - Lead Time - {{ $info->providerName }} - {{ $info->label_zonasi }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('performance.leadtime')}}">Performance</a></li>
              <!-- <li class="breadcrumb-item active">Delivery Order</li> -->
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="card">

      <div class="row">
        <div class="col-12">
            <div class="card-body">
			
                   <form action="{{ route('filterDO') }}" method="GET">
                    {{ csrf_field() }}
                    <div class="row d-none">
                      <div class="col-md-12">
                        <div class="card card-success">

                          <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Date :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i>
                                        </span>
                                      </div>
                                      <input type="date" class="form-control" name="filterdate" value="">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Filter By Month :</label>
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                      </div>
                                      <input type="month" class="form-control" name="filtermonth">
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label></label>
                                    <div class="input-group">
                                      <center>
                                      <input type="submit" value="Filter" class="btn btn-secondary btn-md">
                                      <a href="{{ route('list-pickup')}}"   class="btn btn-secondary btn-md">Reset
                                      </a>
                                      </center>
                                    </div>
                                  </div>
                                </div>
                              <!-- row -->
                            </div>
                            <!-- card-body -->
                          </div>
                                <!-- </div> -->
                        </div>
                      </div>
                    </div>
                  </form>
                  <hr>



	<div class="card">
	  <div class="row">
		<!--
	    <div class="col-md">
	      <div id="piechart"></div>
	    </div>
		-->
	    <div class="col-md">
			<!-- table information -->
			<table class='table'>
				<thead>
					<tr>
						<td>Zip Code</td>
						<td>City</td>
						<td>Delivered</td>
						<td>On Time</td>
						<td>Late</td>
						<td>action</td>
					</tr>
				</thead>
				<tbody>
						@foreach($zipCodes as $zipCode)
					<tr>
						<td>{{ $zipCode->zip }}</td>
						<td>{{ $zipCode->city }}</td>
						<td>{{ $zipCode->totalDelivered }}</td>
						<td>{{ $zipCode->totalOnTime }}</td>
						<td>{{ $zipCode->totalLate }}</td>
						<td></td>
					</tr>
						@endforeach
				</tbody>
			</table>
		</div>		
	  </div>
	  <hr>


	</div>



            </div>
          </div>
        </div>
      </div >
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



@endsection



