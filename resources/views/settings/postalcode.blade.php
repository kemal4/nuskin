@extends('layoutAdmin.global')


@section('content')

<?php $model_name = "Postal Code"; ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $model_name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">{{ $model_name }}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-12 col-md-10">
            @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <div class="card">
              <div class="card-header">
                @can('create_setting')
                    <a class="btn btn-primary" href="javascript:void(0)" id="createNewItem">
                      Create New {{ $model_name }}
                    </a>
                    <button type="button" class="btn btn-primary mr-2 d-none" data-toggle="modal" data-target="#importExcel" style="margin-bottom: 10px;">
                      Import Excel
                    </button>
                    <button type="button" class="btn btn-primary mr-2 d-none" data-toggle="modal" data-target="#importExcel2" style="margin-bottom: 10px;">
                      Import Excel 2
                    </button>
                    <button type="button" class="btn btn-primary mr-2 d-none" data-toggle="modal" data-target="#importExcel3" style="margin-bottom: 10px;">
                      Import Excel postal city courier
                    </button>
                @endcan
              </div>
              <div class="card-body">
                      
                <table class="table text-nowrap data-table" style="border:1px solid #ddd">
                  <thead class="thead-light">
                    <tr>
                      <th width="10px">No.</th>
                      <th>Code</th>
                      <th>Zone</th>
                      <th>City</th>
                      <th>LeadTime</th>
                      <th width="75px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
          
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </section>
    <!-- /.content -->
</div>


<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="itemForm" name="itemForm" class="form-horizontal">
                  <input type="hidden" name="model_id" id="model_id">
                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-body">
                       
                                <div class="form-group row">
                                    <label for="code" class="col-sm-3 col-form-label control-label">Code</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Enter {{ $model_name }}" maxlength="5" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nuskinLeadTime" class="col-sm-3 control-label">Nuskin LeadTime</label>
                                    <div class="col-sm-9">
                          <div class="input-group">
                            <input type="number" class="form-control" id="nuskinLeadTime" name="nuskinLeadTime" placeholder="Enter Nuskin LeadTime" max="240" required >
                            <div class="input-group-append">
                              <span class="input-group-text">days</span>
                            </div>
                          </div>
                                    </div>
                                    
                                </div>
                      
                                <div class="form-group row">
                                    <label for="zone_id" class="col-sm-3 col-form-label control-label">Zone</label>
                                    <div class="col-sm-9">
                                        <select class="form-control custom-select" id="zone_id" name="zone_id" required >
                            <option value="" selected disabled hidden >-- Select Zone --</option>
                              @foreach($zones as  $zone)
                              <option value="{{ $zone->id }}">{{ $zone->code }}</option>
                              @endforeach
                          </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="city_id" class="col-sm-3 col-form-label control-label">City</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="city_id" name="city_id" style="width: 100%;" required >
                            <option value="" selected disabled hidden>-- Select City --</option>
                              @foreach($cities as  $city)
                              <option value="{{ $city->id }}">{{ $city->name }}</option>
                              @endforeach
                          </select>
                                    </div>
                                </div>
                  
                  </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                    <div class="col-12">
                      <div class="card">
              <div class="card-header">
                <h3 class="card-title">Courier LeadTime</h3>
                <div class="card-tools">
                  <!-- Buttons, labels, and many other things can be placed here! -->
                  <!-- Here is a label for example
                  <span class="badge badge-primary">Label</span>
                -->
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
                        <div class="card-body">
                  @foreach ($couriers as $courier)
                                <div class="form-group row">
                                    <label for="cour_{{ $courier->id }}" class="col-sm-3 col-form-label control-label">{{ $courier->display_name }}</label>
                                    <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="number" class="form-control" id="cour_{{ $courier->id }}" name="courierLeadTime[{{ $courier->id }}]" placeholder="Enter {{ $courier->display_name }} LeadTime" max="240">
                            <div class="input-group-append">
                              <span class="input-group-text">days</span>
                            </div>
                                    </div>
                                    </div>
                                </div>
                  @endforeach
                       
                  </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                  </div>
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-info" id="saveBtn" value="create">Save
                    </button>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Import Excel -->
<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('importExcelPostal')}}" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <label>Choice file excel</label>
                    <div class="form-group">
                        <input type="file" name="file" required="required" accept=".xls,.xlsx,.csv">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Import Excel courier-->
<div class="modal fade" id="importExcel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('importExcelCourier')}}" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Excel 2</h5>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <label>Choice file excel</label>
                    <div class="form-group">
                        <input type="file" name="file" required="required" accept=".xls,.xlsx,.csv">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Import Excel Postal City Province-->
<div class="modal fade" id="importExcel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('importExcelPostalCityProvince')}}" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Excel Postal Province City</h5>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <label>Choice file excel</label>
                    <div class="form-group">
                        <input type="file" name="file" required="required" accept=".xls,.xlsx,.csv">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </div>
        </form>
    </div>
</div>
    

@endsection




@section('scripts')
    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.data-table').DataTable({
      "fnDrawCallback": function( oSettings ) {
            $('[data-toggle="tooltip"]').tooltip({
              trigger : 'hover'
            });
          },
    //responsive: true,
    "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '},
    
        processing: true,
        serverSide: true,
        ajax: "{{ route('postalcodes.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'code', name: 'code'},
            {data: 'zone.code', name: 'zone.code', orderable: false},
            {data: 'city.name', name: 'city.name', orderable: false},
            {data: 'nuskinLeadTime', name: 'nuskinLeadTime'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewItem').click(function () {
        $('#saveBtn').val("create...");
        $('#model_id').val('');
        $('#itemForm').trigger("reset");
    $('#city_id').select2().trigger('change');
        $('#modelHeading').html("Create New {{ $model_name }}");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editProduct', function () {
      let model_id = $(this).data('id');
      $.get("{{ route('postalcodes.index') }}" +'/' + model_id +'/edit', function (data) {
      $('#modelHeading').html("Edit {{ $model_name }}");
          $('#saveBtn').val("edit...");
          $('#ajaxModel').modal('show');
                  
          $('#model_id').val(data.id);
          $('#code').val(data.code);
          $('#zone_id').val(data.zone_id);
          $('#city_id').val(data.city_id);
      $('#city_id').select2().trigger('change');
      //$('#city_id').select2('data', {id: data.city_id, a_key: data.city.name});
      //$('#city_id').select2('data', {id: data.city_id, text: data.city.name});
          $('#nuskinLeadTime').val(data.nuskinLeadTime);
            
      $.each(data.courier_leadtimes, function(k, v) {
        $('#cour_'+v.courier_id).val(v.courierLeadTime);
      });
            
      })
   });
    
    $('#saveBtn').click(function (e) {
    
    let isValidForm = $('#itemForm')[0].checkValidity();
    if(isValidForm){
      e.preventDefault();
      $(this).html('Sending..');
              
      $.ajax({
        data: $('#itemForm').serialize(),
        url: "{{ route('postalcodes.store') }}",
        type: "POST",
        dataType: 'json',
        success: function (data) {
          $('#saveBtn').html('Save');
          $('#itemForm').trigger("reset");
          $('#ajaxModel').modal('hide');
          table.draw();
          toastr.success(data.success);
       
        },
        error: function (data) {
          console.log('Error:', data);
          $('#saveBtn').html('Save');
        }
      });
      
    }
    });
    
    $('body').on('click', '.deleteProduct', function () {
     
        let model_id = $(this).data("id");
    
    
    Swal.fire({
      title: 'Are you sure you want to Delete?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
      }).then((result) => {
      if (result.value) { //yes
        $.ajax({
          type: "DELETE",
          url: "{{ route('postalcodes.store') }}"+'/'+model_id,
          success: function (data) {
            table.draw();
            toastr.success(data.success);
          },
          error: function (data) {
            console.log('Error:', data);
          }
        });;
      }
      })
    });
     
  });
  
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
    trigger : 'hover'
  });
  
  $('#city_id').select2();
  /*
  $('select').select2({
    theme: 'bootstrap4',
        dropdownParent: $('#myModal')
  });
  */
});

$('input[name="code"]').on("blur", function() {
  
  if ($(this).val().length<1) {
    return
  }
  let code = $('input[name="code"]').val()
  let urlGetCode = "{{ route('postalCode.checkCode', 'code_to_replace') }}".replace('code_to_replace', code)
  $.ajax({
    method: "GET",
    url: urlGetCode,
  }).done(function(data) {
    console.log("data")
    console.log(data)
    if(data){
      toastr.error(code + ' already exist.')
      $('input[name="code"]').val('')
      {{--$('input[name="nuskinLeadTime"]').val('')
                  $('#zone_id').val('')
                  $('#city_id').val('')
                  $('#cour_1').val('')
                  $('#cour_2').val('')
                  $('#cour_3').val('')
                  $('#cour_6').val('')--}}

        
      $('input[name="order_id"]').focus()
    }
  }).fail(function(data){
  
    //console.log("data")
    console.log(data)
    alert("something went wrong")
  });
});
</script>


@endsection
