@extends('layoutAdmin.global')


@section('content')

<?php $model_name = "Courier Status"; ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $model_name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">{{ $model_name }}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
	
    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-12 col-md-9">
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
			
				<div class="card">
					<div class="card-header">
						@can('create_setting')
						<a class="btn btn-primary" href="javascript:void(0)" id="createNewItem">
							Create New {{ $model_name }}
						</a>
						@endcan		
					</div>
												
			        <div class="card-body">
						<table class="table table-bordered text-nowrap data-table">
							<thead class="thead-light"> 
								<tr>
									<th>No</th>
									<th>Name</th>
									<th>Keyword</th>
									<th>Match Word</th>
									<th>Courier</th>
									<th>Nuskin Status</th>
									<th width="280px">Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
	            <!-- /.card-body -->
	          </div>
          <!-- /.card -->
          </div>	
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
   
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="itemForm" name="itemForm" class="form-horizontal">
                   <input type="hidden" name="model_id" id="model_id">
				   
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="contain" class="col-sm-3 control-label">Keyword</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="contain" name="contain" placeholder="Enter Keyword" required >
                        </div>
                    </div>
                    <div class="form-group row custom-control custom-switch">		
						<input type="checkbox" class="custom-control-input" id="match_word" name="match_word" value="1">
						<label class="custom-control-label" for="match_word"> Match Word</label>
                    </div>
					
                    <div class="form-group row">
                        <label for="courier_id" class="col-sm-3 control-label">Courier</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="courier_id" name="courier_id" required >
								<option value="" selected disabled hidden >-- Select Courier --</option>
								  @foreach($couriers as  $courier)
								  <option value="{{ $courier->id }}">{{ $courier->display_name }}</option>
								  @endforeach
							</select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nuskin_status_id" class="col-sm-3 control-label">Nuskin Status</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="nuskin_status_id" name="nuskin_status_id" required >
								<option value="" selected disabled hidden >-- Select Status --</option>
								  @foreach($statuses as $status)
								  <option value="{{ $status->id }}">{{ $status->display_name }}</option>
								  @endforeach
							</select>
                        </div>
                    </div>
      
                    <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-info" id="saveBtn" value="create">Save
                     </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    

@endsection




@section('scripts')
    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.data-table').DataTable({
			"fnDrawCallback": function( oSettings ) {					  
					  $('[data-toggle="tooltip"]').tooltip({
							trigger : 'hover'
						});   
					},
		//responsive: true,			
		"language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '},
		
        processing: true,
        serverSide: true,
        ajax: "{{ route('courierstatus.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'contain', name: 'contain'},
            {data: 'match_word', name: 'match_word'},
            {data: 'courier.display_name', name: 'courier.display_name', orderable: false},
            {data: 'nuskin_status.display_name', name: 'nuskin_status.display_name', orderable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewItem').click(function () {
        $('#saveBtn').val("create...");
        $('#model_id').val('');
        $('#itemForm').trigger("reset");
        $('#modelHeading').html("Create New {{ $model_name }}");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editProduct', function () {
      var model_id = $(this).data('id');
      $.get("{{ route('courierstatus.index') }}" +'/' + model_id +'/edit', function (data) {
		  $('#modelHeading').html("Edit {{ $model_name }}");
          $('#saveBtn').val("edit...");
          $('#ajaxModel').modal('show');
		  
          $('#model_id').val(data.id);
          $('#name').val(data.name);
          $('#contain').val(data.contain);
		  $('#match_word').prop("checked", data.match_word);
          $('#courier_id').val(data.courier_id);
          $('#nuskin_status_id').val(data.nuskin_status_id);
      })
   });
    
    $('#saveBtn').click(function (e) {       
    
		let isValidForm = $('#itemForm')[0].checkValidity();
		if(isValidForm){
			e.preventDefault();
			$(this).html('Sending..');
							
			$.ajax({
			  data: $('#itemForm').serialize(),
			  url: "{{ route('courierstatus.store') }}",
			  type: "POST",
			  dataType: 'json',
			  success: function (data) {
				  $('#saveBtn').html('Save');
				  $('#itemForm').trigger("reset");
				  $('#ajaxModel').modal('hide');
				  table.draw();
				  toastr.success(data.success);
			 
			  },
			  error: function (data) {
				  console.log('Error:', data);
				  $('#saveBtn').html('Save');
			  }
		  });
			
		}
    });
    
    $('body').on('click', '.deleteProduct', function () {
     
        var model_id = $(this).data("id");
		
		
		Swal.fire({
			title: 'Are you sure you want to Delete?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		  }).then((result) => {
			if (result.value) { //yes
			  $.ajax({
					type: "DELETE",
					url: "{{ route('courierstatus.store') }}"+'/'+model_id,
					success: function (data) {
						table.draw();					
					  toastr.success(data.success);
					},
					error: function (data) {
						console.log('Error:', data);
					}
				});;
			}
		  })	  
    });
     
  });
  
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
		trigger : 'hover'
	});   
	
	/*
	$('select').select2({
		theme: 'bootstrap4',
	});
	*/
});   
</script>


@endsection