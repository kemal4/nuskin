@extends('layoutAdmin.global')


@section('content')

<?php $model_name = "Store"; ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $model_name }}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">{{ $model_name }}</li>
            </ol>
          </div>
        </div>
      </div>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-12">
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
          <div class="card">
            <div class="card-header">
              @can('create_setting')
              <a class="btn btn-primary" href="javascript:void(0)" id="createNewItem">
                Create New {{ $model_name }}
              </a>
              @endcan
            </div>
            <div class="card-body">
                    
              <table class="table data-table">
                <thead class="thead-light">
                  <tr>
                    <th class="20px">No</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>lat</th>
                    <th>long</th>
                    <th width="50px" class="text-right">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
        
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
   
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="itemForm" name="itemForm" class="form-horizontal">
                   <input type="hidden" name="model_id" id="model_id">
           
                    <div class="form-group row">
                        <label for="code" class="col-sm-3 control-label">Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="code" name="code" placeholder="Enter Code" required >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Store Name" required>
                        </div>
                    </div>
          
                    <div class="form-group row">
                        <label for="city_id" class="col-sm-3 control-label">City</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="city_id" name="city_id" style="width: 100%;" required >
                  <option value="" selected disabled hidden >-- Select City --</option>
                  @foreach($cities as  $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                  @endforeach
              </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address_address" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-9">
                            <textarea rows="4" class="form-control" id="address_address" name="address_address" placeholder="Enter {{ $model_name }} Description" ></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address_latitude" class="col-sm-3 control-label">Lat</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="address_latitude" name="address_latitude" placeholder="Enter Code"  >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address_longitude" class="col-sm-3 control-label">Long</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="address_longitude" name="address_longitude" placeholder="Enter Code"  >
                        </div>
                    </div>
      
                    <div class="form-group">
                     <button type="submit" class="btn btn-info float-right" id="saveBtn" value="create">Save
                     </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    

@endsection




@section('scripts')
    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    
    var table = $('.data-table').DataTable({
      "fnDrawCallback": function( oSettings ) {
            $('[data-toggle="tooltip"]').tooltip({
              trigger : 'hover'
            });
          },
    //responsive: true,
    "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '},
    
        processing: true,
        serverSide: true,
        ajax: "{{ route('stores.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'code', name: 'code'},
            {data: 'name', name: 'name'},
            {data: 'city.name', name: 'city.name', orderable: false},
            {data: 'address_address', name: 'address_address'},
            {data: 'address_latitude', name: 'address_latitude'},
            {data: 'address_longitude', name: 'address_longitude'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
     
    $('#createNewItem').click(function () {
        $('#saveBtn').val("create...");
        $('#model_id').val('');
        $('#itemForm').trigger("reset");
    $('#city_id').select2().trigger('change');
        $('#modelHeading').html("Create New {{ $model_name }}");
        $('#ajaxModel').modal('show');
    });
    
    $('body').on('click', '.editProduct', function () {
      var model_id = $(this).data('id');
      $.get("{{ route('stores.index') }}" +'/' + model_id +'/edit', function (data) {
      $('#modelHeading').html("Edit {{ $model_name }}");
          $('#saveBtn').val("edit...");
          $('#ajaxModel').modal('show');
      
          $('#model_id').val(data.id);
          $('#code').val(data.code);
          $('#name').val(data.name);
          $('#city_id').val(data.city_id);
      $('#city_id').select2().trigger('change');
          $('#address_address').val(data.address_address);
          $('#address_latitude').val(data.address_latitude);
          $('#address_longitude').val(data.address_longitude);
      })
   });
    
    $('#saveBtn').click(function (e) {
    
    let isValidForm = $('#itemForm')[0].checkValidity();
    if(isValidForm){
      e.preventDefault();
      $(this).html('Sending..');
              
      $.ajax({
        data: $('#itemForm').serialize(),
        url: "{{ route('stores.store') }}",
        type: "POST",
        dataType: 'json',
        success: function (data) {
          $('#saveBtn').html('Save');
          $('#itemForm').trigger("reset");
          $('#ajaxModel').modal('hide');
          table.draw();
          toastr.success(data.success);
       
        },
        error: function (data) {
          console.log('Error:', data);
          $('#saveBtn').html('Save');
        }
      });
      
    }
    });
    
    $('body').on('click', '.deleteProduct', function () {
     
        var model_id = $(this).data("id");
    
    
    Swal.fire({
      title: 'Are you sure you want to Delete?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
      }).then((result) => {
      if (result.value) { //yes
        $.ajax({
          type: "DELETE",
          url: "{{ route('stores.store') }}"+'/'+model_id,
          success: function (data) {
            table.draw();
            toastr.success(data.success);
          },
          error: function (data) {
            console.log('Error:', data);
          }
        });;
      }
      })
    });
     
  });
  
  
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
    trigger : 'hover'
  });
  
  /*
  $('select').select2({
    theme: 'bootstrap4',
  });
  */
});
</script>


@endsection
