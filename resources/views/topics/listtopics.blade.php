@extends('layoutAdmin.global')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Topic</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}">Home</a></li>
              <li class="breadcrumb-item">Settings</li>
              <li class="breadcrumb-item active">Master Topic</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
  <section class="content">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-10">
          {{-- notifikasi form validasi --}}
          @if ($errors->has('file'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('file') }}</strong>
            </span>
          @endif
          {{-- notifikasi sukses --}}
          @if ($sukses = Session::get('sukses'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $sukses }}</strong>
            </div>
          @endif
          <div class="card">
            <div class="card-header">
                @can('create_setting')
                <a href="{{ route('createtopics')}}"   class="btn btn-primary">
                  <i class="fas fa-plus mr-1"></i>Add Topic
                </a>
                @endcan
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="tbltopics" class="table" style="border:1px solid #ddd">
                  <thead class="thead-light">
                    <tr>
                      <th width=5%>No</th>
                      <th>Topics</th>
                      <th width="15%" class="text-right">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $i=1
                    @endphp
                    @foreach($topics as $tp)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{ $tp->name }}</td>
                      <td class="text-right">
                          @can('edit_setting')
                          <a href="{{ route('edittopics', ['id' => $tp->id ])}}" class="btn btn-warning btn-xs" data-toggle='tooltip' title='Edit'><i class="fas fa-edit"></i></a>
                          @endcan
                          @can('delete_setting')<a href="{{ route('deletetopics', ['id' => $tp->id ])}}" data-toggle='tooltip' title='Delete' class="btn btn-danger btn-xs" onclick="myFunction()"><i class="fas fa-trash"></i></a>
                          @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </section>
    <!-- /.content -->
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    $(function () {
    $('#tbltopics').DataTable();
    });

    function myFunction() {
      alert('Ara you sure Delete this topic ??');
    }
  </script>
@endsection
