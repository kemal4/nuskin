@extends('layoutAdmin.global')

@section('content')

  <div class="content-wrapper">

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Topic</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item"><a href="{{ route('kelolatopics')}}">Master Topics </a></li>
              <li class="breadcrumb-item active">Add Topic</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
 
                  <!--<div class="card-header">
                    <h3 class="card-title">Quick Example <small>jQuery Validation</small></h3>
                  </div> -->

                  <form action="{{ route('storetopics') }}" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                          <label>Name</label>
                          <input type="text" class="form-control" name="name" required>
                        </div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                          <a href="{{ route('kelolatopics')}}"   class="btn btn-default mr-2 mb-2">Cancel
                          </a>
                          <button type="submit" class="btn btn-info mb-2"> Submit </button>
                      </div>

                    </div>
                  </form>
              
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
    </div>
    </section>
  </div>

@endsection