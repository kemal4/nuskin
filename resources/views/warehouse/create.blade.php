@extends('layoutAdmin.global')


@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Warehouse</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
              <li class="breadcrumb-item"><a href="{{ route('kelolarole')}}">Master Warehouse</a></li>
              <li class="breadcrumb-item active">Add Warehouse</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-secondary">
            <div class="card-body">

                <form action="{{ route('add-gudang') }}" method="post">
                  @csrf
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Code Warehouse</label>
                        <input type="text" class="form-control" name="codegudang" placeholder="Code Warehouse" required>
                      </div>
                      <div class="form-group">
                        <label>Warehouse</label>
                        <input type="text" class="form-control" name="gudang" placeholder="Warehouse" required>
                      </div>
                    </div>             

                    <div class="col-md-12">
                      <div class="card-footer">
                        <div class="row">
                            <a href="{{ route('kelolarole')}}"   class="btn btn-default mr-2 mb-2">Cancel
                            </a>
                            <button type="submit" class="btn btn-info mb-2"> Submit </button>
                      </div>
                    </div>

                    </div>
                </form>

              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </section>
  </div>
  
  
  <script>
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'id',
      format   : 'yyyy-mm-dd'
    });
  </script>

@endsection