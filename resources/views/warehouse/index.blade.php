@extends('layoutAdmin.global')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
	      <div class="container-fluid">
	        <div class="row mb-2">
	          <div class="col-sm-6">
	            <h1>Master Warehouse</h1>
	          </div>
	          <div class="col-sm-6">
	            <ol class="breadcrumb float-sm-right">
	              <li class="breadcrumb-item"><a href="{{ route('home')}}"> Home </a></li>
	              <li class="breadcrumb-item">Settings</li>
	              <li class="breadcrumb-item active">Master Warehouse</li>
	            </ol>
	          </div>
	        </div>
	      </div><!-- /.container-fluid -->
	    </section>

	<section class="content">
      	<div class="row">
        	<div class="col-12">
          		<div class="card">
            		<div class="card-header">
              			<h3 class="card-title">List Warehouse</h3>
            		</div> <!-- /.card-header -->
		            <div class="card-body">			
		            	@can('create_setting')
						<a href="{{ route('newgudang')}}"   class="btn btn-primary mr-5 mb-2">
							Add Warehouse
						</a>
						@endcan
						<div class="card-body">	
            			    <div class="tab-content mt-2" id="custom-content-below-tabContent">
            			        <div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
            			            <table id="salesOrder" class="table table-bordered text-nowrap">
            			                <thead class="thead-light">
            			                    <tr>
            			                        <!--<th>&nbsp;</th>-->
            			                        <th>No</th>
                                    			<th>Code Warehouse</th>
                                    			<th>Warehouse</th>
                                    			<th>Action</th>
            			                    </tr>
            			                </thead>
            			            </table>
            			        </div> 
            			    </div>
			
            			<!-- /.card-body -->
            			</div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>

	
	</div>
	
@endsection

@section('scripts')
	<script type="text/javascript">
		function myFunction() {
			alert('Are you sure Want To Delete ??');
		}

		var table = $('#salesOrder').DataTable({
        processing: true,
        serverSide: true,
        "ajax": {
            "url": "{{ route('warehouse.getAllListJson') }}",
            "dataType": "json",
            "type": "POST",
            "data":{ _token: "{{csrf_token()}}"},       
        },
        columns: [
        	{ "data": "DT_RowIndex", "name": 'DT_RowIndex' },
            { "data": "code_gudang", "name": 'code_gudang' },
            { "data": "lokasi_gudang", "name": 'lokasi_gudang' },
            { "data": "action", "name": 'action' },
        ]
    });
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
	$('body').on('click', '.deleteProduct', function () {
     
        var model_id = $(this).data("id");
        var url = "{{route('deletegudang', 'replace_id')}}".replace('replace_id', model_id)

        
    Swal.fire({
        title: 'Are you sure you want to Delete?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) { //yes
          $.ajax({
        type: "DELETE",
        url: url,
        success: function (data) {
          table.draw();
          if(data.success)
          {
          	toastr.success(data.message);
          }
          else
          {
          	toastr.error(data.message);
          }
          
        },
        error: function (data) {
          console.log('Error:', data);
        }
      });;
        }
      })
  	});
	</script>
@endsection