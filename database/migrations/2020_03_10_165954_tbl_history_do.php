<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblHistoryDo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tbl_historydo')) {
    //
        Schema::create('tbl_historydo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_delivery');
            $table->string('date_vendor');
            $table->string('status');
            $table->string('pic');
            $table->string('message');
            $table->timestamps();
        });
			
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
