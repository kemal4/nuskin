<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostalCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postal_codes', function (Blueprint $table) {
            $table->mediumIncrements('id');
			$table->string('code', 5)->unique(); 	//VARCHAR
			$table->string('zone_code')->nullable(); 	//VARCHAR
			$table->unsignedTinyInteger('nuskinLeadTime')->nullable()->default(0);	// 255
			$table->unsignedTinyInteger('zone_id')->nullable()->default(1);	// 255
            $table->timestamps();
			$table->unsignedSmallInteger('city_id')->nullable()->default(1);	// 255
			// $table->string('city')->nullable(); 	//VARCHAR
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_codes');
    }
}
