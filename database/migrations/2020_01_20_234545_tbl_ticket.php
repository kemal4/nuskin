<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_ticket', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id');
            $table->string('email');
            $table->string('nama_pelapor');
            $table->string('customer_id')->nullable();
            $table->string('nama_customer');
            $table->string('telepon');
            $table->string('attachments');
            $table->string('message', 500);
            $table->string('prioritas');
            $table->string('topics');
            $table->string('status');
            $table->string('transporter');
            $table->string('zip');
            $table->string('non_deliv');
            $table->string('date');
            $table->timestamps();
            
            $table->boolean('is_locked')->nullable();
            $table->string('locked_by'); 
            $table->timestamp('locked_time')->useCurrent();
            $table->string('attachments2');
            $table->string('multiattachment');
            $table->string('attachments_file_names');           
            $table->bigInteger('delivery_order_id')->unsigned(); // for foreign key to do
            $table->string('id_ticket');
            $table->string('due');
            $table->string('hawb');
            $table->string('pic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
