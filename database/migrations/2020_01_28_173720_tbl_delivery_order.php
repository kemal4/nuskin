<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblDeliveryOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('tbl_delivery_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('create_date')->nullable();
            $table->string('order_id')->nullable();
            $table->string('transporter')->nullable();
            $table->string('order_type')->nullable();
            $table->string('customer_refer')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('zip')->nullable();
            $table->string('grosswiight')->nullable();
            $table->string('net_weight')->nullable();
            $table->string('volume')->nullable();
            $table->string('hawb')->nullable();
            $table->string('adress')->nullable();
            $table->string('status')->nullable();
            $table->string('delivery_number')->nullable();
            $table->string('pic');
            $table->string('is_preview');
            $table->string('is_deleted');
            $table->string('deliver_date');
            $table->string('fulfillday');        
            $table->boolean('isFulfilled')->nullable();
            $table->boolean('isDelivered')->nullable();
            $table->timestamps();
			$table->softDeletes();
			
            $table->boolean('is_partial_so')->nullable();
            $table->boolean('is_delivered')->nullable();
			$table->timestamp('delivered_date')->nullable();
			$table->timestamp('eta_nuskin')->nullable();
			$table->timestamp('eta_provider')->nullable();
			$table->boolean('on_time_nuskin')->nullable();
			$table->boolean('on_time_provider')->nullable();
			$table->unsignedTinyInteger('leadtime_nuskin')->default(0);
			$table->unsignedTinyInteger('leadtime_courier')->default(0);
			$table->unsignedTinyInteger('warehouse_id')->default(0);
			$table->unsignedTinyInteger('status_id')->default(1); // for foreign key to table status
			$table->string('warehouse_name')->default(''); 
			$table->boolean('is_complain')->default(0);
			$table->unsignedSmallInteger('courier_id')->default(1); // for foreign key to table courier
			
			/*
			$table->mediumInteger('')->unsigned();
			$table->unsignedTinyInteger('')->default(0);
			$table->tinyInteger('')->unsigned();
            $table->boolean('')->nullable();
            $table->boolean('')->nullable();
            $table->boolean('')->nullable();
            $table->boolean('')->nullable();
			$table->boolean('')->default(0);
			$table->boolean('')->nullable();
			*/
			
			

			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
