<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblKelolaVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tbl_kelola_vendor')) {
    //
        Schema::create('tbl_kelola_vendor', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('label_vendor');
            $table->string('label_zonasi');
            $table->string('value');
            $table->timestamps();
        });
			
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
