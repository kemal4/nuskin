<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierLeadtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {	
		
        Schema::create('courier_leadtimes', function (Blueprint $table) {
            $table->mediumIncrements('id');
			$table->unsignedTinyInteger('courierLeadTime')->nullable()->default(0);	// 255
			//$table->unsignedMediumInteger('postal_code_id')->unsigned();
			//$table->unsignedTinyInteger('courier_id')->unsigned();
			$table->mediumInteger('postal_code_id')->unsigned();
			$table->smallInteger('courier_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('courier_leadtimes', function (Blueprint $table) {
            $table->unique(['postal_code_id','courier_id']);
			
        });
        Schema::table('courier_leadtimes', function (Blueprint $table) {
            $table->foreign('courier_id')->references('id')->on('couriers')->onDelete('cascade');
			
        });
        Schema::table('courier_leadtimes', function (Blueprint $table) {
            $table->foreign('postal_code_id')->references('id')->on('postal_codes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_leadtimes');
    }
}
