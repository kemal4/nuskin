<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier_statuses', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name');
            $table->string('contain');
			$table->unsignedSmallInteger('courier_id')->default(0);
			$table->unsignedTinyInteger('nuskin_status_id')->default(0);
            $table->boolean('match_word')->nullable()->default(0);
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_statuses');
    }
}
