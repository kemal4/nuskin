<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblCounting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tbl_counting')) {
    //
        Schema::create('tbl_counting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('years');
            $table->string('month');
            $table->string('vendor');
            $table->string('deliveries');
            $table->string('delivered');
            $table->string('fail');
            $table->string('ontime');
            $table->string('late');
            $table->string('failpersen');
            $table->string('latepersen');
            $table->timestamps();
        });
			
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
