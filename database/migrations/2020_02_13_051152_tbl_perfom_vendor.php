<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblPerfomVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tbl_perfom_vendor')) {
    //
        Schema::create('tbl_perfom_vendor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hawb');
            $table->string('city');
            $table->string('zip');
            $table->string('transporter');
            $table->string('status');
            $table->string('start_date');
            $table->string('deliver_date');
            $table->string('fullfilday');
            $table->timestamps();
        });
			
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
