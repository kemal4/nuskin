<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblResStatusLocal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_respon_local', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('delivery_id');
            $table->unsignedSmallInteger('order_id');
            $table->string('address');
            $table->string('status');
            $table->string('order_name');
            $table->string('courier');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_respon_local');
    }
}
