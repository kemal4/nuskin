<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblKodepos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kodepos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kodepos');
            $table->string('kelurahan');
            $table->string('kecamatan');
            $table->string('city');
            $table->string('provinsi');
            $table->string('id_zonasi');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
