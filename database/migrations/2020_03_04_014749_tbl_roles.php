<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tbl_roles')) {
    //
        Schema::create('tbl_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_users');
            $table->string('role');
            $table->timestamps();
        });
			
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
