<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if(!Schema::hasTable('tbl_vendor')) {
    //
			Schema::create('tbl_vendor', function (Blueprint $table) {
				$table->tinyIncrements('id');
				$table->string('code_vendor');
				$table->string('label_vendor');
				$table->string('common_name');
				$table->string('pic_email');
				$table->timestamps();
			});
			
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
