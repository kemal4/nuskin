<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblTicketLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('tbl_ticket_log')) {
    //
        Schema::create('tbl_ticket_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_ticket');
            $table->string('activity');
            $table->string('description',500);
            $table->string('pic');
            $table->string('due');
            $table->timestamps();
        });
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
