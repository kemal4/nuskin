<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couriers', function (Blueprint $table) {
				$table->smallIncrements('id');
				$table->string('code');
				$table->string('name');
				$table->string('display_name');
				$table->string('pic')->nullable();
				$table->string('pic_email')->nullable();
				$table->boolean('is_sms_notification')->nullable()->default(0);
				$table->boolean('is_delivery_check')->nullable()->default(0);
                $table->boolean('active')->nullable()->default(0);
				$table->timestamps();
				$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couriers');
    }
}
