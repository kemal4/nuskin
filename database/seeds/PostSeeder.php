<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) { 
    		$type = ['Announcement'];
    		DB::table('post')->insert([
        	    'title' => 'title '.$i,
                'type' => $type[rand(0, count($type) - 1)],
                'message' => 'Message '.$i,
                'posted' => true,
                'pic' => 1,
        	    'created_at' => Carbon::now(),
        	    'updated_at' => Carbon::now(),
        	]);
    	}

    }
}
