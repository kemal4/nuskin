<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		
        $permissions = [
			['name' => 'view_delivery' , 'show' => true, 'menu_id' => 2],
			['name' => 'create_delivery' , 'show' => true, 'menu_id' => 2],
			['name' => 'edit_delivery' , 'show' => true, 'menu_id' => 2],
			['name' => 'delete_delivery' , 'show' => true, 'menu_id' => 2],
			
			['name' => 'view_ticket_delivery' , 'show' => true, 'menu_id' => 3],
			['name' => 'create_ticket_delivery' , 'show' => true, 'menu_id' => 3],
			['name' => 'edit_ticket_delivery' , 'show' => true, 'menu_id' => 3],
			['name' => 'delete_ticket_delivery' , 'show' => true, 'menu_id' => 3],
			['name' => 'reopen_ticket_delivery' , 'show' => true, 'menu_id' => 3],
			
			['name' => 'view_other_ticket' , 'show' => true, 'menu_id' => 4],
			['name' => 'create_other_ticket' , 'show' => true, 'menu_id' => 4],
			['name' => 'edit_other_ticket' , 'show' => true, 'menu_id' => 4],
			['name' => 'delete_other_ticket' , 'show' => true, 'menu_id' => 4],
			
			['name' => 'view_performance' , 'show' => true, 'menu_id' => 5],
			['name' => 'create_performance' , 'show' => true, 'menu_id' => 5],
			['name' => 'edit_performance' , 'show' => true, 'menu_id' => 5],
			['name' => 'delete_performance' , 'show' => true, 'menu_id' => 5],
			
			['name' => 'view_setting' , 'show' => true, 'menu_id' => 6],
			['name' => 'create_setting' , 'show' => true, 'menu_id' => 6],
			['name' => 'edit_setting' , 'show' => true, 'menu_id' => 6],
			['name' => 'delete_setting' , 'show' => true, 'menu_id' => 6],
			
			
        ];


        foreach ($permissions as $permission) {
             Permission::create( $permission);
        }
		
    }
}
