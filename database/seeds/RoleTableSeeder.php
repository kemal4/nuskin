<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$role = Role::create(['name' => 'warehouse']);
        $role->givePermissionTo('view_delivery');
        $role->givePermissionTo('create_delivery');
        $role->givePermissionTo('edit_delivery');
        $role->givePermissionTo('delete_delivery');
		
		$role = Role::create(['name' => 'crm_team'])
            ->givePermissionTo(['view_ticket_delivery', 'create_ticket_delivery', 'edit_ticket_delivery', 'delete_ticket_delivery', '']);

        $role = Role::create(['name' => 'crm_supervisor'])
            ->givePermissionTo(['view_ticket_delivery', 'create_ticket_delivery', 'edit_ticket_delivery', 'delete_ticket_delivery', 'reopen_ticket_delivery']);
        
		$role = Role::create(['name' => 'super_admin']);
        $role->givePermissionTo(Permission::all());
		
		/*
        $role = Role::find(4);
        $role->givePermissionTo(Permission::all());
		*/
		
		//$role = Role::find(4);
		
		
		
    }
}
